#!/usr/bin/env python
import pygtk
pygtk.require('2.0')
 
import gtk
gtk.threads_init()
import gobject
import threading
import urllib


#import gnome.vfs

# We need to do this to load the VFS module on
# the main thread, this is a hack, yes, but it works
#try:
#    gnome.vfs.get_file_info("storage:///nothinghere")
#except:
# pass

from storage.applet import sql_query
from storage.applet import results_popup
from storage.applet import render_thread

from libstoragenl import storagenl

class QueryEntry(gtk.HBox):

    def __init__(self):
        gtk.HBox.__init__(self)

        label = gtk.Label("Open:")
        self.add(label)
        
        self.searchEntry = gtk.Entry()
        self.searchEntry.connect("focus-in-event", self.onEntryFocusIn)
        self.searchEntry.connect("focus-out-event", self.onEntryFocusOut)
        self.searchEntry.connect("changed", self.onEntryChanged)
        self.searchEntry.connect("activate", self.onActivate)
        self.add(self.searchEntry)


        self.dnd_source = gtk.Image()
        self.dnd_source.set_from_stock(gtk.STOCK_OPEN, gtk.ICON_SIZE_BUTTON)
        eventbox = gtk.EventBox()
        eventbox.add(self.dnd_source)
        
        eventbox.connect("drag_data_get", self.sendCallback)
        target = [('text/uri-list', 0, 0)]
        eventbox.drag_source_set(gtk.gdk.BUTTON1_MASK, target, gtk.gdk.ACTION_COPY)

        
        self.add(eventbox)
        
        # Initialize the idle timer
        self.idleTimer = 0
        
        # Create the results popup
        self.resultsPopup = results_popup.ResultsPopup(self.searchEntry)
        self.searchResultsLock = threading.Lock()
        
        # Create the NL Parser
        self.parser = storagenl.NlParser()
        
        self.sqlStr = None
        
    def sendCallback(self, widget, context, selection, targetType, eventTime):
        print ("sendcallback")
        if (self.sqlStr != None):
            escapedQuery = urllib.quote(self.sqlStr,'')
            uri = "storage:///%s/%s" % (escapedQuery, self.searchStr)
            print ("Sending back %s" % uri)
            selection.set(selection.target, 8, uri)

    def onEntryFocusIn(self, entry, event):
        # Show the results window (if hidden)
        if len(self.searchEntry.get_text()) > 0:
            self.resultsPopup.show_all()
            #self.idleTimer = gtk.timeout_add(400, self.doSearch)


    def onEntryFocusOut(self, entry, event):
        # Hide the results window
        if self.idleTimer != 0:
            gtk.timeout_remove(self.idleTimer)
            self.idleTimer = 0
        self.resultsPopup.hide()
        return gtk.FALSE


    def onEntryChanged(self, entry):
        # Lots of things we need to check for here:
        if self.idleTimer != 0:
            # If there was an old idle timer, clear it
            gtk.timeout_remove(self.idleTimer)

        if len(self.searchEntry.get_text()) == 0:
            # If the user did something to clean the entry, hide the results window
            self.resultsPopup.hide()
        #elif event.keyval == gtk.keysyms.space:
            # If the user entered a space, search immediately
            # NOTE: Disabled untile applet is fast enough for this to not be annoying
            #self.doSearch()
        #else:
            # Otherwise, start a new idle timer
        #    self.idleTimer = gtk.timeout_add(400, self.doSearch)
        return gtk.FALSE


    def onActivate(self, entry):
        self.doSearch()
        return gtk.FALSE


    def onDelete(self, window, event):
        return gtk.FALSE


    def onDestroy(self, window):
        gtk.main_quit()


    def doSearch(self):
        # Clear any existing idle timer
        if self.idleTimer != 0:
            gtk.timeout_remove(self.idleTimer)
            self.idleTimer = 0
        # Get the search string
        searchStr = self.searchEntry.get_text()
        print "Search string: " + searchStr

        self.searchStr = "%s" % searchStr

        # Parse the search string
        sqlStr = self.parser.get_sql_query("Find " + searchStr)
        self.sqlStr = "%s" % sqlStr
        print "Resulting SQL: " + sqlStr
        
        # Query the DB
        if self.sqlStr != "NO VALID QUERY FOUND":
            query = sql_query.SQLQuery(sqlStr, self.onItemFound)
            query.start()
        return gtk.FALSE


    def onItemFound(self, searchResults):
	if self.idleTimer != 0:
            return
        # NOTE: This method is called outside the GTK main thread by sql_query!!
        # Lock the search results list
        self.searchResultsLock.acquire()
        
        self.searchResults = [ ]

        print "starting rendering"
        if searchResults != None:
            for item in searchResults:
                print "Working on item " + item.get_item_path()
                renderer = item.get_renderer()
                name = renderer.render_text()
                pixbuf = renderer.render_thumbnail()
                uri = item.get_item_path()
                self.searchResults.append((name, pixbuf, uri))
        else:
            self.resultsPopup.hide()
        print "done rendering"
        
        # Grab the GTK global lock
        gtk.threads_enter()
        
        # We need to trap all errors inside here, they cause fatal problems
        try:
            # This will cause the items to be added back in the main GTK thread
            gtk.idle_add(self.addItems);
        except:
            print ("Problem in query thread in GTK code")
        
        # Release the GTK global lock
        gtk.threads_leave()
        # Unlock the search results list
        self.searchResultsLock.release()


    def addItems(self):
        if self.idleTimer != 0:
            return

        # Lock the search results list
        self.searchResultsLock.acquire()        
        if self.searchResults == None:
            self.resultsPopup.clearSearchResults();
        elif len(self.searchResults) == 0:
            self.resultsPopup.clearSearchResults();
        else:
            self.resultsPopup.addSearchResults(self.searchResults)
        # Unlock the search results list
        self.searchResultsLock.release()


    def main(self):
        gtk.main()



if __name__ == "__main__":
    gargamel = QueryEntry()
    gargamel.main()
