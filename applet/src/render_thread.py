import pygtk
pygtk.require('2.0')
import gtk

import threading

import storage.renderer_factory

class RenderThread(threading.Thread):
    def __init__(self, item):
        threading.Thread.__init__(self)
        self.item = item
        self.setName(item.get_item_path())

    def run(self):
        renderer = storage.renderer_factory.get_renderer_for_item(self.item)
        self.item.pixbuf = renderer.render_thumbnail()
        self.item.name = renderer.render_text()
        print "Rendering thread run"
