import pygtk
pygtk.require('2.0')

import gtk

from storage.applet import search_results_list

class ResultsPopup(gtk.Window):

    def __init__(self, widgetToAlignWith):
        gtk.Window.__init__(self, gtk.WINDOW_POPUP)
        
        # Create the results window
        self.contentsFrame = gtk.Frame()
        self.contentsFrame.set_shadow_type(gtk.SHADOW_OUT)
        self.add(self.contentsFrame)

        self.widgetToAlignWith = widgetToAlignWith
        self.resultsList = None


    def clearSearchResults(self):
        # Delete the results list, if it exists
        if (self.resultsList != None):
            self.contentsFrame.remove(self.resultsList)
            del (self.resultsList)
            self.resultsList = None


    def addSearchResults(self, searchResults):
        # Make a new results list
        self.clearSearchResults()
        
        self.resultsList = search_results_list.SearchResultsList()
        self.resultsList.addSearchResults(searchResults)
        
        self.contentsFrame.add(self.resultsList)
        
        self.positionWindow()
        self.show_all()


    def positionWindow(self):
        # Get our own dimensions & position
        self.show()
        self.realize()
        ourWidth  = (self.window.get_geometry())[2]
        ourHeight = (self.window.get_geometry())[3]

        # Get the dimensions/position of the widgetToAlignWith
        self.widgetToAlignWith.realize()
        (entryX, entryY) = self.widgetToAlignWith.window.get_origin()
        entryWidth  = (self.widgetToAlignWith.window.get_geometry())[2]
        entryHeight = (self.widgetToAlignWith.window.get_geometry())[3]

        # Get the screen dimensions
        screenHeight = gtk.gdk.screen_height()
        screenWidth = gtk.gdk.screen_width()

        if entryX + ourWidth < screenWidth:
            # Align to the left of the entry
            newX = entryX
        else:
            # Align to the right of the entry
            newX = (entryX + entryWidth) - ourWidth

        if entryY + entryHeight + ourHeight < screenHeight:
            # Align to the bottom of the entry
            newY = entryY + entryHeight
        else:
            newY = entryY - ourHeight

        # -"Coordinates locked in captain."
        # -"Engage."
        self.move(newX, newY)


