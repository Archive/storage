import pygtk
pygtk.require('2.0')

import urllib

import gtk
import gobject
import gnome

magic_column_image = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB, False, 8, 1, 64)
magic_column_image.fill(0xffffffff)

class SearchResultsList(gtk.ScrolledWindow):
    def __init__ (self):
        gtk.ScrolledWindow.__init__(self)
        
        self.store = self.createModel()
        self.treeview = gtk.TreeView(self.store)
        self.createColumns(self.treeview)
        self.treeview.connect("row-activated", self.rowActivated)
        
        self.add(self.treeview)
        
        # Set the preferred width for the result list
        self.set_size_request(600, 400)
        
        # Only show scrollbars when they make sense
        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.treeview.set_headers_visible(gtk.FALSE)

    def createModel(self):
        # Our model has 4 columns, the magic pixbuf, the renderer pixbuf, the title string, and the uri string
        store = gtk.ListStore(gtk.gdk.Pixbuf.__gtype__, gtk.gdk.Pixbuf.__gtype__, gobject.TYPE_STRING, gobject.TYPE_STRING)
        return store


    def createColumns(self, treeview):
        # Our treeview has 2 columns, the renderer pixbuf and the titlestring, corresponding to columns 0 and 1 of the model
        treeview.append_column(gtk.TreeViewColumn('', gtk.CellRendererPixbuf(), pixbuf=0))
        treeview.append_column(gtk.TreeViewColumn('', gtk.CellRendererPixbuf(), pixbuf=1))
        treeview.append_column(gtk.TreeViewColumn('', gtk.CellRendererText(), text=2))


    def rowActivated(self, tree, path, column):
        # Get the uri from the model.
        model = tree.get_model()
        iter = model.get_iter(path)
        uri = "%s/%s" % (model.get_value(iter, 3), urllib.quote(model.get_value(iter, 2)))

        print ("Going to %s" % uri)

        # Display it.
        gnome.url_show(uri)


    def addSearchResults(self, searchResults):
        for result in searchResults:
            row = []
            row.append(magic_column_image)
            row.append(result[1]) # Pixbuf
            row.append(result[0]) # Name
            row.append(result[2]) # URI
            
            # Add the row to the model
            self.store.append(row)


