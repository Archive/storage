import gtk
import threading
import storage

class SQLQuery(threading.Thread):
    def __init__(self, queryString, queryDoneCallback):
        threading.Thread.__init__(self)
        self.queryString = queryString
        self.queryDoneCallback = queryDoneCallback
        
    def run(self):
        print ("Running query " + self.queryString)

        store = storage.Store("query applet")
        items = store.getItemsMatchingQuery(self.queryString)

        print ("Found " + str(len(items)) + " results")

        valid_items = [ ]

        for item in items:
            toplevel = item.getAttribute("storage:top_level")

            if toplevel != None and toplevel.lower() == 't':
                valid_items.append(item)

        self.queryDoneCallback(valid_items)
