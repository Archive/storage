#!/usr/bin/env python
import pygtk
pygtk.require('2.0')

from storage.applet.query_entry import QueryEntry
import gtk

window = gtk.Window(gtk.WINDOW_TOPLEVEL)
window.connect('destroy', lambda w: gtk.main_quit())
query_entry = QueryEntry()
window.add(query_entry)
window.show_all()

gtk.main()
