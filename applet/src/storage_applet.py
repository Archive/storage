#!/usr/bin/env python
import pygtk
pygtk.require('2.0')

# Must import gargamel first to get thread initialization
from storage.applet.query_entry import QueryEntry

import gtk
import gobject
import gnome.applet


class StorageApplet(gnome.applet.Applet):
    def __init__(self):
        self.__gobject_init__()

    def init(self):
        self.query_entry = QueryEntry()
        self.add(self.query_entry)
        self.show_all()
        return gtk.TRUE

gobject.type_register(StorageApplet)


def foo(applet, iid):
    return applet.init()


gnome.applet.bonobo_factory("OAFIID:GNOME_StorageApplet_Factory", 
                               StorageApplet.__gtype__, 
                               "Storage", "0", foo)

