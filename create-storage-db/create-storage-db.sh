#!/bin/sh

DB_NAME=gargamel

dropdb $DB_NAME
createdb $DB_NAME
psql -f create-storage-db $DB_NAME
