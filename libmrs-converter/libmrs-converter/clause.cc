#include "clause.hh"

#include <vector>

using namespace std;
using namespace MRS;

Clause::Clause(xmlNodePtr xmlNode) : xmlNode(xmlNode) {
}

Clause::~Clause() {

}

xmlNodePtr Clause::getNewNode(OldToNew &oldToNew) {
  OldToNew::const_iterator iter = oldToNew.find(this->xmlNode);
  if (iter == oldToNew.end()) {
    // Uhoh! We didn't find the node, this is bad...
    return NULL;
  } else {
    xmlNodePtr toReturn = (*iter).second;
    return toReturn;
  }
}

xmlNodePtr Clause::getNewNodeWithChildText(OldToNew &oldToNew, string childText) {
  xmlNodePtr newNode = this->getNewNode(oldToNew);
  xmlNodeSetContent(newNode, (const xmlChar *)childText.c_str());
  return newNode;
}

// xmlNodePtr Clause::copyNode(xmlNodePtr prototypeNode) {
//   return xmlCopyNode(prototypeNode, 2);
// }

// xmlNodePtr Clause::copyNodeWithNewAttribute(xmlNodePtr prototypeNode, string new_attribute, string value) {
//   xmlNodePtr copyNode = xmlCopyNode(prototypeNode, 2);
//   xmlSetProp(copyNode, (const xmlChar *)new_attribute.c_str(), (const xmlChar *)value.c_str());
//   return copyNode;
// }

ConstantClause::ConstantClause(string constant, xmlNodePtr xmlNode) : Clause(xmlNode), constant(constant) {
}

xmlNodePtr ConstantClause::getValue(Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew) {
  return this->getNewNodeWithChildText(oldToNew, this->constant);
}

UnmodifiedClause::UnmodifiedClause(xmlNodePtr xmlNode) : Clause(xmlNode) {
}

xmlNodePtr UnmodifiedClause::getValue(Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew) {
  return getNewNode(oldToNew);
}


ArgumentClause::ArgumentClause(int argumentNumber, xmlNodePtr xmlNode) : 
  Clause(xmlNode), argumentNumber(argumentNumber) {

}

xmlNodePtr ArgumentClause::getValue(Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew) {
  vector<Feature *> arguments = relation.getArguments();

  Feature *argument = arguments[this->argumentNumber];

  string argument_id = argument->getName();
  return this->getNewNodeWithChildText(oldToNew, argument_id);
}

FeatureClause::FeatureClause(string featureName, xmlNodePtr xmlNode) : Clause(xmlNode), featureName(featureName) {
}

xmlNodePtr FeatureClause::getValue(Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew) {
  cout << "Relation address is " << (int)(&relation) << endl;
  cout << "Getting value for " << this->featureName << endl;
  cout << "Name of feature is " << relation.getFeatureStructure().name() << endl;
  string value = relation.getFeatureStructureValue(this->featureName);
  return this->getNewNodeWithChildText(oldToNew, value);
}

ChildrenClause::ChildrenClause(xmlNodePtr xmlNode) : Clause(xmlNode) {
}

xmlNodePtr ChildrenClause::getValue(Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew) {
  xmlNodePtr returnNode = this->getNewNode(oldToNew);
  cout << "Getting value of childrenclause" << endl;
  vector<xmlNodePtr>::iterator iter;
  for (iter = children.begin(); iter != children.end(); ++iter) {
    cout << "Adding child..." << endl;
    xmlNodePtr child = *iter;
    xmlAddChild(returnNode, child);
  }

  return returnNode;
}

BoundVariableClause::BoundVariableClause(xmlNodePtr xmlNode) : Clause(xmlNode) {
}

xmlNodePtr BoundVariableClause::getValue(Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew) {
  Quantifier *quantifier = dynamic_cast<Quantifier *>(&relation);
  if (quantifier) {
    string quantifier_id = quantifier->getBoundVariable()->getName();
    return this->getNewNodeWithChildText(oldToNew, quantifier_id);
  } else {
    return NULL;
  }
}

