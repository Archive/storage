#ifndef CLAUSE_HH_
#define CLAUSE_HH_

#include <string>

#include <libmrs/mrs.hh>
#include <libmrs/relation.hh>
#include <libmrs/feature.hh>
#include <libmrs/quantifier.hh>

#include <libxml/tree.h>

typedef hash_map<xmlNodePtr,xmlNodePtr> OldToNew;

class Clause {
public:
  Clause(xmlNodePtr xmlNode);
  virtual ~Clause();
  virtual xmlNodePtr getValue(MRS::Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew) = 0;
protected:
  xmlNodePtr xmlNode;
  //xmlNodePtr copyNodeWithNewAttribute(xmlNodePtr prototypeNode, string new_attribute, string value);
  //xmlNodePtr copyNode(xmlNodePtr);
  xmlNodePtr getNewNode(OldToNew &oldToNew);
  xmlNodePtr getNewNodeWithChildText(OldToNew &oldToNew, std::string childText);
};

class ConstantClause : public Clause {
public:
  ConstantClause(std::string constant, xmlNodePtr xmlNode);
  xmlNodePtr getValue(MRS::Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew);
protected:
  std::string constant;
};

class UnmodifiedClause : public Clause {
public:
  UnmodifiedClause(xmlNodePtr xmlNode);
  xmlNodePtr getValue(MRS::Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew);
};

class ArgumentClause : public Clause {
public:
  ArgumentClause(int argumentNumber, xmlNodePtr xmlNode);
  xmlNodePtr getValue(MRS::Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew);
protected:
  int argumentNumber;
};

class FeatureClause : public Clause {
public:
  FeatureClause(std::string featureName, xmlNodePtr xmlNode);
  xmlNodePtr getValue(MRS::Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew);
protected:
  std::string featureName;
};

class ChildrenClause : public Clause {
public:
  ChildrenClause(xmlNodePtr xmlNode);
  xmlNodePtr getValue(MRS::Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew);
};

class BoundVariableClause : public Clause {
public:
  BoundVariableClause(xmlNodePtr xmlNode);
  xmlNodePtr getValue(MRS::Relation &relation, vector<xmlNodePtr> &children, OldToNew &oldToNew);
};

#endif
