#include "constants.hh"

using namespace std;

void Constants::addConstant(string name, string value) {
  constantNameToValue[name] = value;
}

string Constants::getConstant(string name) throw (ConstantNotDefinedException) {
  map<string, string>::iterator iter = this->constantNameToValue.find(name);
  if (iter == this->constantNameToValue.end()) {
    throw ConstantNotDefinedException();
  }
   
  return (*iter).second;
}
