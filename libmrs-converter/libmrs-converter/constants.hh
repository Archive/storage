#ifndef CONSTANTS_HH
#define CONSTANTS_HH

#include <string>
#include <map>

class ConstantNotDefinedException {

};

class Constants {
public:
  void addConstant(std::string name, std::string value);
  std::string getConstant(std::string name) throw (ConstantNotDefinedException);

private:
  std::map<std::string, std::string> constantNameToValue;
}; 

#endif
