#include "converter.hh"

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>

#include "expression.hh"

#include "xmlutils.hh"

using namespace std;

namespace MRS {
  Converter::Converter(string semanticTranslationFile) {
    xmlDocPtr doc;
    xmlNodePtr relNode;
    xmlNodePtr rootNode;

    xmlSubstituteEntitiesDefault(1);
    doc = xmlParseFile (semanticTranslationFile.c_str());   
    rootNode = xmlDocGetRootElement(doc);

    for (relNode = rootNode->children; relNode != NULL; relNode = relNode->next) {
      if (nodeIs(relNode, "predicate") || nodeIs(relNode, "quantifier")) {
	string name = getNodeProperty(relNode, string("name"));
	Expression *expression = new Expression(relNode, this->constants);
	this->predicateNameToExpression[name] = expression;

      } else if (nodeIs(relNode, "constant")) {

	string name = getNodeProperty(relNode, string("name"));      
	string constantValue = getNodeContents(relNode);
	this->constants.addConstant(name, constantValue);

      }
    }
  }


  Expression *Converter::findExpression(string name) {
    map<string, Expression *>::iterator iter = this->predicateNameToExpression.find(name);

    if (iter == this->predicateNameToExpression.end()) {
      cerr << "Couldn't find an expression for " << name << endl;
      return NULL;
    } else {
      return (*iter).second;
    }
  }

  vector<xmlNodePtr> Converter::convertRelation(RelationTree *relationTree) {
    Relation *relation = relationTree->getRelation();
    vector<RelationTree *> children = relationTree->getChildren();
    vector<xmlNodePtr> childNodes;

    /* Recurse over child relations and convert them */
    vector<RelationTree *>::iterator iter;
    for (iter = children.begin(); iter != children.end(); ++iter) {
      RelationTree *child = *iter;
      vector<xmlNodePtr> childNodesToAdd = this->convertRelation(child);

      childNodes.insert(childNodes.end(), childNodesToAdd.begin(), childNodesToAdd.end());
    }
    
    /* Convert the relation itself */
    Expression *expression = findExpression(relation->getName());
    if (expression) {
      return expression->resolve(*relation, childNodes);
    } else {
      return childNodes;
    }
  }

  string Converter::convertToString (RelationTree *tree) {
    xmlDocPtr doc = this->convert(tree);

    char *document_string;
    int size;
    xmlKeepBlanksDefault(0);
    xmlDocDumpFormatMemory(doc, (xmlChar**)&document_string, &size, TRUE);
    
    xmlFreeDoc(doc);

    return string(document_string);
  }

  xmlDocPtr Converter::convert(RelationTree *tree) {
    vector<xmlNodePtr> childNodes = this->convertRelation(tree);

    xmlDocPtr doc = xmlNewDoc((const xmlChar *)"1.0");
    xmlNodePtr topNode = xmlNewDocNode(doc, NULL, (const xmlChar *)"SemanticConversion", NULL);
    doc->children = topNode;

    vector<xmlNodePtr>::iterator iter;
    for (iter = childNodes.begin(); iter != childNodes.end(); ++iter) {
      xmlNodePtr child = *iter;
      xmlAddChild(topNode, child);
    }

    return doc;
  }



};

