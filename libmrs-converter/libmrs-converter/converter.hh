#ifndef CONVERTER_HH_
#define CONVERTER_HH_

namespace MRS {
  class Converter;
}

#include <string>
#include <map>
#include <list>
#include <vector>

#include <libmrs/mrs.hh>
#include <libmrs/relation-tree.hh>
#include <libmrs/quantifier.hh>
#include <libmrs/relation.hh>

#include <libmrs-converter/expression.hh>

#include <libmrs-converter/constants.hh>

namespace MRS {

  class Converter {
  public:
    Converter(std::string semantics_filename);
    xmlDocPtr convert (RelationTree *tree);
    std::string convertToString (RelationTree *tree);

  protected:
    Expression *findExpression(std::string name);

    std::vector<xmlNodePtr> convertRelation(RelationTree *relation);

    Constants constants;
    std::map<std::string, Expression *> predicateNameToExpression;
  };

};

#endif
