#include "expression.hh"

#include <iostream.h>

#include "xmlutils.hh"

using namespace std;

static string nodesToString(vector<xmlNodePtr> childNodes) {
    xmlNodePtr topNode = xmlNewNode(NULL, (const xmlChar *)"SemanticConversion");

    vector<xmlNodePtr>::iterator iter;
    for (iter = childNodes.begin(); iter != childNodes.end(); ++iter) {
      xmlNodePtr child = *iter;
      xmlAddChild(topNode, child);
    }

    return nodeToString(topNode);
}

Clause *Expression::parseMRSClause(xmlNodePtr clauseNode) {
  Clause *clause = NULL;

  if (nodeIs(clauseNode, "arg")) {
    string argumentString = getNodeProperty(clauseNode, "num");
    int argumentNumber = atoi(argumentString.c_str()) - 1;
    clause = new ArgumentClause(argumentNumber, clauseNode);
  } else if (nodeIs(clauseNode, "constant")) {
    string constantName = getNodeProperty(clauseNode, "name");
    try {
      string value = this->constants.getConstant(constantName);
      clause = new ConstantClause(value, clauseNode);
    } catch (ConstantNotDefinedException e) {
      clause = new ConstantClause(constantName, clauseNode);
    }
  } else if (nodeIs(clauseNode, "feature")) {
    string featureName = getNodeProperty(clauseNode, "name");
    clause = new FeatureClause(featureName, clauseNode);
  } else if (nodeIs(clauseNode, "BoundVariable")) {
    clause = new BoundVariableClause(clauseNode);
  } else if (nodeIs(clauseNode, "Children")) {
    clause = new ChildrenClause(clauseNode);
  } else if (nodeIs(clauseNode, "ConstantArg")) {
    clause = new FeatureClause("CARG", clauseNode);
  } else {
    cerr << "Creating expression, unknown node type " << getNodeName(clauseNode) << endl;
    clause = NULL;
  }
  
  return clause;
}

void Expression::parseParentNode(xmlNodePtr parentNode) {
  xmlNodePtr clauseNode = parentNode->children;

  for (; clauseNode != NULL; clauseNode = clauseNode->next) {
    if (clauseNode->type == XML_ELEMENT_NODE) {
      if (mrsNode(clauseNode)) {
	/* MRS Node */
	Clause *clause = parseMRSClause(clauseNode);
	if (clause != NULL) {
	  this->clauses.push_back(clause);
	}
      } else {
	/* Not an MRS Node */
	Clause *clause = new UnmodifiedClause(clauseNode);
	this->clauses.push_back(clause);
	this->parseParentNode(clauseNode);
      }
    } else if (clauseNode->type == XML_TEXT_NODE) {
      /* Plain Text */
      Clause *clause = new UnmodifiedClause(clauseNode);
      this->clauses.push_back(clause);
    } else {
      /* WTF */
      cerr << "Creating Expression, unknown node type " << clauseNode->type << endl;
      continue;
    }
    
  } 
}

Expression::Expression(xmlNodePtr expressionNode, Constants &constants) : constants(constants), expressionNode(expressionNode) {
  parseParentNode(expressionNode);
}


static xmlNodePtr copyXMLNode(xmlNodePtr oldNode, OldToNew &oldToNew);
static xmlNodePtr copyXMLNode(xmlNodePtr oldNode, OldToNew &oldToNew) {
  // FIXME: make namespaces less $#*(&(* verbose


  /* Copy with attributes and namespaces */
  xmlNodePtr newNode = xmlCopyNode(oldNode, 2);

  oldToNew[oldNode] = newNode;

  xmlNodePtr oldChildNode = oldNode->children;

  for (; oldChildNode != NULL; oldChildNode = oldChildNode->next) {
    xmlNodePtr newChildNode = copyXMLNode(oldChildNode, oldToNew);
    xmlAddChild(newNode, newChildNode);
  }

  return newNode;
}

vector<xmlNodePtr> Expression::resolve(MRS::Relation &relation, vector<xmlNodePtr> &children) {
  OldToNew oldToNew;

  xmlNodePtr nodeTree = copyXMLNode(this->expressionNode, oldToNew);

  for (vector<Clause *>::iterator clauseIter = this->clauses.begin(); clauseIter != this->clauses.end(); ++clauseIter) {
    Clause *clause = *clauseIter;
    clause->getValue(relation, children, oldToNew);
  }

  vector<xmlNodePtr> xmlNodes;
  xmlNodePtr childNode = nodeTree->children;
  for (; childNode != NULL; childNode = childNode->next) {
    xmlNodes.push_back(childNode);
  }

  return xmlNodes;
}

