#ifndef EXPRESSION_HH_
#define EXPRESSION_HH_

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>

#include <string>
#include <vector>
#include <map>

#include <libmrs/mrs.hh>
#include <libmrs/relation.hh>

#include <libmrs-converter/clause.hh>
#include <libmrs-converter/constants.hh>

class Expression {
 public:
  Expression(xmlNodePtr expressionNode, Constants &constants);
  std::vector<xmlNodePtr> resolve(MRS::Relation &relation, std::vector<xmlNodePtr> &children);
 private:
  void parseParentNode(xmlNodePtr parentNode);
  Clause *parseMRSClause(xmlNodePtr clauseNode);
  std::vector<Clause *> clauses;
  Constants &constants;
  xmlNodePtr expressionNode;
};

#endif
