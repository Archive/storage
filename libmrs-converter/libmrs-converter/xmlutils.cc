#include "xmlutils.hh"
#include <iostream.h>

using namespace std;

string getNodeName(xmlNodePtr node) {
  return string((const char *)node->name);
}

string getNodeContents(xmlNodePtr node) {
  xmlChar *contents = xmlNodeGetContent(node);
  string result((char *)contents);
  xmlFree(contents);
  return result;
}

bool nodeIs(xmlNodePtr node, const char *type) {
  return (strcmp ((const char *)node->name, type) == 0);
}

string getNodeProperty(xmlNodePtr node, string property_name) {
  xmlChar *property = xmlGetProp(node, (const xmlChar *)property_name.c_str());
  string result;
  if (property != NULL) {
    result = (char *)property;
    xmlFree(property);
  }
  
  return result;
}

bool mrsNode(xmlNodePtr node) {
  if (node->ns) {
    return (strcmp((const char *)node->ns->href, "http://www.designfu.org/mrs") == 0);
  } else {
    cerr << "FIXME: tried seeing if node '" << getNodeName(node) << "' w/o namespace is an MRS node, be we don't search for default NSs." << endl;
    return false;
  }
}

std::string nodeToString(xmlNodePtr node) {
  xmlDocPtr doc = node->doc;
  xmlBufferPtr buffer = xmlBufferCreate();

  xmlNodeDump(buffer, doc, node, 0, 0);

  string contents = (const char *)xmlBufferContent(buffer);

  xmlBufferFree(buffer);

  return contents;
}

