#ifndef XMLUTILS_HH
#define XMLUTILS_HH

#include <libxml/tree.h>
#include <string>

std::string getNodeName(xmlNodePtr node);
std::string getNodeContents(xmlNodePtr node);
std::string getNodeProperty(xmlNodePtr node, std::string property_name);
std::string nodeToString(xmlNodePtr node);

bool nodeIs(xmlNodePtr node, const char *type);
bool mrsNode(xmlNodePtr node);
#endif
