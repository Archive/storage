#include <libmrs/mrs.hh>
#include <libmrs/parser.hh>
#include <libmrs/resolver.hh>

#include <libmrs-converter/converter.hh>

#include <iostream.h>

#include <string>
#include <vector>

using namespace std;

int main (int argv, char **argc) {
  char *grammar = NULL;
  char *semantics = NULL;

  grammar = strdup(GRAMMAR_FILE);
  semantics = strdup(argc[1]);

  MRS::Parser *parser = new MRS::Parser (grammar);
  MRS::Resolver resolver;
  MRS::Converter converter(semantics);

  while (true) {
    //cout << endl << endl << endl << "========================================================================" << endl << endl << endl << endl;
    cout << endl << "Parse: ";

    string toParse;

    getline(cin, toParse);

    vector<MRS::MRS *> parsings = parser->parseToMRS(toParse);

    cout << "Found " << parsings.size() << " interpretations:" << endl;

    vector<MRS::MRS *>::iterator iter;
    for (iter = parsings.begin(); iter != parsings.end(); ++iter) {
      MRS::MRS *parsing = *iter;

      vector<MRS::RelationTree *> trees = resolver.resolve(*parsing);
      vector<MRS::RelationTree *>::iterator treesIter;

      for (treesIter = trees.begin(); treesIter != trees.end(); ++treesIter) {
	MRS::RelationTree *tree = *treesIter;
	cout << "Tree {" << endl << tree->toString() << "}" << endl;
	cout << "Results in " << converter.convertToString(tree) << endl;
      }
      cout << endl << endl;
    }
  }

  return 0;

}

