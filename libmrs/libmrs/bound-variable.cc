#include "bound-variable.hh"

using namespace PET;
using namespace std;

namespace MRS {
  BoundVariable::BoundVariable (MRS &mrs, PET::fs &variable) : Feature(variable) {
    this->variable_number = mrs.next_id_number++;
    this->binding_quantifier = NULL;
  }

  BoundVariable::~BoundVariable () {

  }

  Quantifier* BoundVariable::getBindingQuantifier() {
    return this->binding_quantifier;
  }

  vector<Relation *> BoundVariable::getReferencingRelations() {
    return this->referencing_relations;
  }

  string BoundVariable::getName() {
    std::stringstream outstream;

    outstream << "x" << this->variable_number;

    return outstream.str();
  }

  string BoundVariable::toString(bool recursive) {
    std::stringstream outstream;

    outstream << this->getName();

    outstream << " [";
    if (this->binding_quantifier != NULL) {
      outstream << this->binding_quantifier->toString();
    } else {
      outstream << "unbound";
    }
    outstream << "](";
    
    std::vector<Relation *>::iterator i;
    
    for (i = this->referencing_relations.begin(); i != this->referencing_relations.end(); ++i) {
      Relation *relation = *i;
      outstream << relation->toString() << " ";
    }
    
    outstream << ")";
    
    return outstream.str();
  }

  void BoundVariable::boundBy(Quantifier *quantifier) {
    assert(this->binding_quantifier == NULL);
    this->binding_quantifier = quantifier;
  }

  void BoundVariable::referencedBy(Relation *relation) {
    this->referencing_relations.push_back(relation);
  }

};
