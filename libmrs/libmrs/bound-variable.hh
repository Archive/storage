#ifndef BOUND_VARIABLE_HH_
#define BOUND_VARIABLE_HH_

namespace MRS {
  class BoundVariable;
};

#include <string>
#include <vector>

#include <libmrs/mrs.hh>
#include <libmrs/relation.hh>
#include <libmrs/feature.hh>
#include <libmrs/quantifier.hh>

namespace MRS {

  class BoundVariable : public Feature {
  public:
    BoundVariable (MRS &mrs, PET::fs &event);
    virtual ~BoundVariable ();
    virtual std::string toString(bool recursive=false);
    virtual std::string getName();

    virtual void boundBy(Quantifier *quantifier);
    virtual void referencedBy(Relation *relation);

    virtual std::vector<Relation *> getReferencingRelations();
    virtual Quantifier *getBindingQuantifier();

  protected:
    int variable_number;
    std::vector<Relation *>referencing_relations;
    Quantifier *binding_quantifier;
  };

};

#endif
