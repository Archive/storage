#include "event.hh"

using namespace PET;
using namespace std;

namespace MRS {

  Event::Event (MRS &mrs, PET::fs &event) : Feature(event) {
    this->variable_number = mrs.next_id_number++;
  }

  Event::~Event () {

  }

  string Event::getName() {
    std::stringstream outstream;

    outstream << "event" << this->variable_number;
    return outstream.str();
  }

  string Event::toString(bool recursive) {
    return this->getName();
  }
};
