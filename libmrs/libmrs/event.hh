#ifndef EVENT_HH_
#define EVENT_HH_

namespace MRS {
  class Event;
};

#include <string>

#include <libmrs/mrs.hh>
#include <libmrs/feature.hh>

namespace MRS {

  class Event : public Feature {
  public:
    Event (MRS &mrs, PET::fs &event);
    virtual ~Event ();
    virtual std::string toString(bool recursive=false);
    virtual std::string getName();

  protected:
    int variable_number;
  };

};

#endif
