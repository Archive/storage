#include "mrs.hh"
#include "feature.hh"
#include "bound-variable.hh"

using namespace PET;
using namespace std;

namespace MRS {


  Feature::Feature(PET::fs &feature) : feature(feature) {
  }

  PET::fs &Feature::getFeatureStructure() {
    return this->feature;
  }

  string Feature::getFeatureStructureValue(string valueName) {
    PET::fs &feature = this->getFeatureStructure();

    fs value = feature.get_path_value(valueName.c_str());

    string name = string(value.name()).substr(1);
    
    return name;
  }

  Feature *Feature::Factory(MRS &mrs, fs &feature) {
    int featureType = feature.type();
    
    Feature *newFeature = NULL;

    if (PET::subtype(featureType, handleType)) {
      newFeature = new Handle(mrs, feature);
    } else if (PET::subtype(featureType, ref_indType) || PET::subtype(featureType, full_ref_indType)) {
      newFeature = new BoundVariable(mrs, feature);
    } else if (PET::subtype(featureType, eventType)) {
      newFeature = new Event(mrs, feature);
    } else if (PET::subtype(featureType, non_explType)) {
      newFeature = new UnknownFeature(mrs, feature, featureType);
    } else {
      newFeature = new UnknownFeature(mrs, feature, featureType);
    }
    
    return newFeature;
  }

  UnknownFeature::UnknownFeature(MRS &mrs, PET::fs &feature, int type) : Feature(feature) {
    this->type = type;
    this->feature = feature;
  }

  UnknownFeature::~UnknownFeature() {

  }

  string UnknownFeature::toString(bool recursive) {
    return "unknown type " + this->getName();
  }

  string UnknownFeature::getName() {
    return printnames[this->type];
  }


}
