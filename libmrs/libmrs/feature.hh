#ifndef FEATURE_HH_
#define FEATURE_HH_

namespace MRS {
  class Feature;
};

#include <string>

#include <libmrs/mrs.hh>

namespace MRS {


  class Feature {
  public:
    virtual ~Feature() { }

    virtual std::string toString(bool recursive=false) = 0;
    virtual std::string getName() = 0;

    static Feature *Factory(MRS &mrs, PET::fs &feature);

    virtual std::string getFeatureStructureValue(string valueName);
    virtual PET::fs &   getFeatureStructure();

  protected:
    Feature(PET::fs &feature);

  private:
    PET::fs feature;
  };

  class UnknownFeature : public Feature {
  public:
    UnknownFeature(MRS &mrs, PET::fs &feature, int type);

    virtual ~UnknownFeature();
    virtual std::string toString(bool recursive=false);
    virtual std::string getName();

    int type;
    PET::fs feature;
  };

};

#endif
