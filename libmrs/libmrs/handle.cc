#include "handle.hh"

using namespace PET;
using namespace std;

namespace MRS {

  Handle::Handle(MRS &mrs, PET::fs &handle) : Feature(handle) {
    this->handle_number = mrs.next_id_number++;
  }

  Handle::~Handle() {

  }

  string Handle::getName() {
    std::stringstream tmpstream;
    tmpstream << "h" << this->handle_number;
    return tmpstream.str();
  }

  string Handle::toString(bool recursive) {
    string output = getName();
    
    if (recursive) {
      output += ":\n";

      std::vector<Relation*>::iterator i;
    
      for (i = this->relations.begin(); i != this->relations.end(); ++i) {    
	Relation *relation = *i;
	output += relation->toString();
      }

    } else {
      std::vector<Relation*>::iterator i;
    
      output += "(";

      for (i = this->relations.begin(); i != this->relations.end(); ++i) {    
	Relation *relation = *i;
	output += relation->toString();
	output += " ";
      }
      output += ")";
    }

    return output;
  }

  void Handle::addRelation(Relation *relation) {
    this->relations.push_back(relation);
  }

  vector<Relation *> Handle::getRelations() {
    return this->relations;
  }

};
