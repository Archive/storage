#ifndef HANDLE_HH_
#define HANDLE_HH_

namespace MRS {
  class Handle;
};

#include <libmrs/mrs.hh>
#include <libmrs/relation.hh>
#include <libmrs/feature.hh>

namespace MRS {

  class Handle : public Feature {
  public:
    Handle(MRS &mrs, PET::fs &feature);

    virtual ~Handle();
    virtual std::string toString(bool recursive=false);
    virtual std::string getName();

    void addRelation(Relation *relation);
    std::vector<Relation *> getRelations();

  protected:
    std::vector<Relation *> relations;
    int handle_number;
  };

};

#endif
