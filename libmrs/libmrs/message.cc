#include "mrs.hh"

using namespace PET;
using namespace std;

namespace MRS {
  Message::Message(MRS &mrs, PET::fs &relation) : Relation(mrs, relation) {
    this->messageArgument = dynamic_cast<Handle *>(this->getFeature("MARG"));
    assert (this->messageArgument != NULL);
  }
  
  Message::~Message() {
    
  }
  
  string Message::toString(bool recursive) {
    std::stringstream output;

    output << this->name;

    if (recursive) {
      output << endl;
      output << "  " << "Message Argument: " << this->messageArgument->toString() << ")" << endl;
    }

    return output.str();
  }

  Handle * Message::getMessageArgument() {
    cout << "Getting the MARG, which is " << this->messageArgument->toString() << endl;
    return this->messageArgument;
  }

}
