#ifndef MESSAGE_HH_
#define MESSAGE_HH_

namespace MRS {
  class Message;
};

#include <vector>
#include <string>

#include <libmrs/relation.hh>
#include <libmrs/mrs.hh>
#include <libmrs/bound-variable.hh>

namespace MRS {

  class Message : public Relation {
  public:
    Message(MRS &mrs, PET::fs &relation);
    virtual ~Message();

    virtual std::string toString(bool recursive=false);

    virtual Handle *getMessageArgument();
  protected:
    Handle *messageArgument;
  };

}

#endif
