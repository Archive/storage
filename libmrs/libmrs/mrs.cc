#include "mrs.hh"
#include "cheap.h"
#include "types.h"

using namespace PET;
using namespace std;

namespace MRS {

  int handleType;       
  int ref_indType;     
  int full_ref_indType;
  int eventType;
  int non_explType;

  int quant_relType;
  int message_relType;

  // Maps abbreviations like "hcons" into type names defined in
  // mrs.set in the grammar like "mrs_hcons" that specify the exact
  // name of features like HCONS
  char *mrs_role_name(string abstr_name) {
    const char *mrs_name = ("mrs_" + abstr_name).c_str();
    char *name = cheap_settings->assoc("mrs-role-names", mrs_name);
    if(name == 0)
      throw error("undefined role name `" + string(mrs_name) + "'.");
    return name;
  }
  
  // Same as mrs_role_name() but for types (like mrs_full_ref-ind)
  char *mrs_type_name(string abstr_name) {
    const char *mrs_name = ("mrs_" + abstr_name).c_str();
    char *name = cheap_settings->assoc("mrs-type-names", mrs_name);
    if(name == 0)
      throw error("undefined type name `" + string(mrs_name) + "'.");
    return name;
  }



  MRS::MRS(item &base_item) {
    this->next_id_number = 0;

    handleType       = PET::lookup_type("handle");  
    ref_indType      = PET::lookup_type("ref-ind");
    full_ref_indType = PET::lookup_type("full_ref-ind");
    eventType        = PET::lookup_type("event");
    non_explType     = PET::lookup_type("non_expl");

    quant_relType    = PET::lookup_type("quant_rel");
    message_relType  = PET::lookup_type("message");


    fs root = base_item.get_fs();
    root.expand();

    fs mrs = root.get_path_value(cheap_settings->value("mrs-path"));

    //DEBUG
    //mrs.print(stderr);
    //cerr << endl << endl;

    fs relations       = mrs.get_path_value(mrs_role_name("rels"));
    fs currentRelation = relations.get_attr_value(BIA_LIST);
    fs lastRelation    = relations.get_attr_value(BIA_LAST);

    // Create a new "Relation" for each relation in the 
    // MRS feature structure and add it to this.relations
    while(currentRelation.valid() && currentRelation != lastRelation )
    {
      fs relation = currentRelation.get_attr_value(BIA_FIRST);
      if(!relation.valid())
	throw error("invalid MRS LISZT");

      this->relations.push_back(getRelation(relation));

      currentRelation = currentRelation.get_attr_value(BIA_REST);
    }

    fs hook = mrs.get_attr_value(mrs_role_name("hook"));

    if (!hook.valid()) throw error("invalid HOOK");

    fs top = hook.get_attr_value(mrs_role_name("ltop"));
    if (!top.valid()) throw error("invalid LTOP");
    this->topLabel = dynamic_cast<Handle *>(this->getFeature(top));

    fs index = hook.get_attr_value(mrs_role_name("index"));
    if (!index.valid()) throw error("invalid INDEX");
    this->index    = dynamic_cast<Event *>(this->getFeature(index));

    fs hcons = mrs.get_attr_value(mrs_role_name("hcons"));
    this->hcons = new HCons(*this, hcons);
  }

  HCons* MRS::getHCons() {
    return this->hcons;
  }

  string MRS::toString() {
    std::stringstream output;

    std::vector<Relation *>::iterator i;

    for (i = this->relations.begin(); i != this->relations.end(); ++i) {
      Relation *relation = *i;
      output << relation->toString(true) << endl;
    }

    output << endl;

    output << this->hcons->toString() << endl;

    output << "Top:   " << this->topLabel->toString() << endl;
    output << "Index: " << this->index->toString() << endl;

    return output.str();
  }

  // Get the MRS::Relation corresponding to the relation in 
  // feature structure fs. Create a new MRS::Relation using feature
  // if necessary, otherwise pull up the existing one matching
  // that feature
  Relation *MRS::getRelation(fs &feature) {
    if(!feature.valid())
      return 0;

    dag_node *node = feature.dag();

    // If the dag_node doesn't exist in the map already, assign this
    // dag node to a new feature instance
    if(this->dag_node_to_feature.find(node) == this->dag_node_to_feature.end()) {
      this->dag_node_to_feature[node] =  Relation::Factory(*this, feature);
    }

    return dynamic_cast<Relation*>(this->dag_node_to_feature[node]);
  }

  // Get the MRS::Feature corresponding to the relation in 
  // feature structure fs. Create a new MRS::Feature using feature
  // if necessary, otherwise pull up the existing one matching
  // that feature. Use MRS::getRelation() if this is a relation
  // (should only really be needed one place in mrs.cc)
  Feature *MRS::getFeature(fs &feature) {
    if(!feature.valid())
      return 0;

    dag_node *node = feature.dag();

    // If the dag_node doesn't exist in the map already, assign this
    // dag node to a new feature instance
    if(this->dag_node_to_feature.find(node) == this->dag_node_to_feature.end()) {
      Feature *newFeature = Feature::Factory(*this, feature);

      if (dynamic_cast<Handle*>(newFeature) != NULL) {
	this->handles.push_back((Handle*) newFeature);	
      }

      this->dag_node_to_feature[node] = newFeature;
    }

    return this->dag_node_to_feature[node];
  }

  vector<Quantifier *> MRS::getQuantifiers() {
    vector<Quantifier *> quantifiers;

    vector<Relation *>::iterator i;

    for (i = this->relations.begin(); i != this->relations.end(); ++i) {
      // dynamic_cast relation a quantifier, if its not NULL, its a quantifier
      Quantifier *quantifier = dynamic_cast<Quantifier *>(*i);
      if (quantifier != NULL) {
	quantifiers.push_back(quantifier);
      }
    }

    return quantifiers;
  }

  Handle *MRS::getTopHandle() {
    return this->topLabel;

  }

  Event *MRS::getIndex() {
    return this->index;
  }

  vector<Relation *> MRS::getRelations() {
    return this->relations;
  }
  

  HCons::HCons(MRS &mrs, fs &hcons) {
    if(hcons.valid()) {
      fs liszt = hcons.get_attr_value(BIA_LIST);
      fs last = hcons.get_attr_value(BIA_LAST);

      // Iterate through the outscopes constraints in the HCons feature
      // structure and add each constraint to this->hcons
      while(liszt.valid() && liszt != last) {
	fs pair = liszt.get_attr_value(BIA_FIRST);
	if(!pair.valid())
          throw error("invalid H-CONS value in MRS");

        fs sc_arg = pair.get_attr_value(mrs_role_name("harg"));
        fs outscpd = pair.get_attr_value(mrs_role_name("larg"));
	
	Handle *outscopes = dynamic_cast<Handle *>(mrs.getFeature(sc_arg));
	Handle *outscoped = dynamic_cast<Handle *>(mrs.getFeature(outscpd));

	HandleConstraint *constraint = new HandleConstraint(outscopes, outscoped);
	this->hcons.push_back(constraint);

	this->outscopes_to_outscoped[outscopes] = outscoped;

//         if(rhs == m->top()) {
//           char foo[128];
//           sprintf(foo, "MRS top handle (h%d) outscoped by h%d", rhs, lhs);
//           throw error(foo);
//         }

        liszt = liszt.get_attr_value(BIA_REST);
      }
    }
  }

  vector<Handle *> HCons::getQEQHandles(Handle *handle) {
    vector<Handle *> result;

    map<Handle *, Handle *>::iterator handleIter = this->outscopes_to_outscoped.find(handle);
    if (handleIter != this->outscopes_to_outscoped.end()) {
      Handle *childHandle = (*handleIter).second;
      result.push_back(childHandle);
    }
    return result;
  }

  string HCons::toString() {
    string output;
    std::vector<HandleConstraint *>::iterator i;

    output += "HCons:\n";

    for (i = this->hcons.begin(); i != this->hcons.end(); ++i) {
      HandleConstraint *constraint = *i;
      output += "   ";
      output += constraint->outscopes->toString(false);
      output += " qeq ";
      output += constraint->outscoped->toString(false);
      output += "\n";
    }

    return output;
  }

  HandleConstraint::HandleConstraint (Handle *outscopes, Handle *outscoped) {
    this->outscopes = outscopes;
    this->outscoped = outscoped;
  }


};

