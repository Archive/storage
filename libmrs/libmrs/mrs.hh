#ifndef MRS_HH_
#define MRS_HH_

#include <map>
#include <string>
#include <vector>

namespace MRS {
  class HCons;
  class HandleConstraint;
  class MRS;
};

#include <libmrs/pet-parser.hh>

#include <libmrs/feature.hh>
#include <libmrs/bound-variable.hh>
#include <libmrs/event.hh>
#include <libmrs/handle.hh>
#include <libmrs/relation.hh>
#include <libmrs/quantifier.hh>
#include <libmrs/message.hh>

namespace MRS {

  class MRS {
  public:
    MRS (PET::item &root);

    std::string toString();

    std::vector<Quantifier *> getQuantifiers();
    std::vector<Relation *>   getRelations();

    Handle *getTopHandle();
    Event *getIndex();

    HCons *getHCons();

    /* Primarily for use within MRS */
    Feature*  getFeature(PET::fs &feature);
    Relation* getRelation(PET::fs &feature);

    int next_id_number;

  protected:


    std::vector<Relation *> relations;
    std::vector<Handle *> handles;
    HCons *hcons;

    Handle *topLabel;
    Event  *index;

    std::map<PET::dag_node *, Feature *> dag_node_to_feature;
  };

  class HCons {
  public:
    HCons(MRS &mrs, PET::fs &hcons);

    std::vector<Handle *> getQEQHandles(Handle *handle);

    std::string toString();
    std::vector<HandleConstraint *> hcons;
    std::map<Handle *, Handle *> outscopes_to_outscoped;
  };

  class HandleConstraint {
  public:
    HandleConstraint (Handle *outscopes, Handle *outscoped);
    Handle *outscopes;
    Handle *outscoped;
  };


  /* Internals */

  char *mrs_role_name(string abstr_name);
  char *mrs_type_name(string abstr_name);

  extern int handleType;       
  extern int ref_indType;     
  extern int full_ref_indType;
  extern int eventType;
  extern int non_explType;

  extern int quant_relType;
  extern int message_relType;
};


#endif
