#include "parser.hh"
#include "mrs.hh"
#include "resolver.hh"

#include <pet-system.h>
#include <chart.h>
#include <inputchart.h>
#include <tsdb++.h>
#include <parse.h>
#include <mrs.h>
#include <k2y.h>
#include <semantics.h>
#include <options.h>

#include <string>
#include <values.h>

using namespace std;

namespace PET {
  extern int opt_k2y, opt_nth_meaning;

  FILE *ferr, *fstatus, *flog;

  grammar *Grammar;
  settings *cheap_settings;
}

namespace MRS {

  Parser::Parser (string grammar_file) {
    char *grammar_file_cstr = strdup (grammar_file.c_str());

    this->grammar_id = 1;

    /* PET likes to write to files for some perverse reason */
    PET::ferr = stderr;
    PET::fstatus = stderr;
    PET::flog = (FILE *)NULL;

    /* Load the settings file corresponding to the grammar */
    cout << "Reading settings from " << PET::settings::basename(grammar_file_cstr);
    this->cheapSettings = New PET::settings(PET::settings::basename(grammar_file_cstr), grammar_file_cstr, "reading");

    PET::cheap_settings = this->cheapSettings;

    /* FIXME: why does it use the YY tokenizer when we uncomment this? */
    PET::options_from_settings(this->cheapSettings);

    //PET::init_options();

    PET::opt_default_les = true;
    PET::pedgelimit = 10000;
    PET::memlimit = 100 * 1024 * 1024;

    PET::verbosity = 0;

    /* Attempt to load the grammar file */
    try { 
      this->grammar = New PET::grammar(grammar_file_cstr);
      PET::Grammar = this->grammar;
      cout << endl << endl;
      cout << PET::ntypes << " types loaded" << endl;
    } catch(PET::error &e) {
      fprintf(stderr, "\naborted\n");
      e.print(stderr);
      delete this->grammar;
      delete this->cheapSettings;
    }

    // FIXME: needed? 
    PET::initialize_version();
  }

  vector<MRS *>
  Parser::parseToMRS (string phrase, int max_results) {
    using namespace PET;

    vector<MRS *> parsings;

    PET::opt_k2y = 0;
    PET::opt_yy = 0;

    if (max_results > 0) {
      PET::opt_nsolutions = max_results;
    }

    chart *Chart = 0;

    fs_alloc_state FSAS;
    input_chart i_chart(New end_proximity_position_map);
  
    list<error> errors;
    analyze(i_chart, phrase, Chart, FSAS, errors, this->grammar_id);
  
    if(!errors.empty()) cerr << "Errors found" << endl;
  

    vector<item *>::iterator iter;
    // Iterate through the possible parses
    for(iter = Chart->readings().begin(); iter != Chart->readings().end(); ++iter) {
      item *it = *iter;
    
      MRS *parsing = new MRS(*it);
      parsings.push_back(parsing);
    }

    return parsings;
  }

}
