#ifndef PARSER_HH_
#define PARSER_HH_

namespace MRS {
  class Parser;
};

#include <string>
#include <vector>

#include <libmrs/pet-parser.hh>
#include <libmrs/mrs.hh>

namespace MRS {

  class Parser {
  public:  
    Parser (std::string grammar_file);
    
    std::vector<MRS *> parseToMRS (std::string phrase, int max_results=-1);

  protected:
    PET::grammar *grammar;
    PET::settings *cheapSettings;
    
  private:
    /* A unique number associated with this->Grammar */
    int grammar_id;
  };

};


#endif /* PARSER_HH_ */
