#include "mrs.hh"

using namespace PET;
using namespace std;

namespace MRS {
  Quantifier::Quantifier(MRS &mrs, PET::fs &relation) : Relation(mrs, relation) {
    this->boundVariable = dynamic_cast<BoundVariable *>(this->getFeature("ARG0"));
    assert (this->boundVariable != NULL);
    this->boundVariable->boundBy(this);
  }
  
  Quantifier::~Quantifier() {
    
  }
  
  string Quantifier::toString(bool recursive) {
    std::stringstream output;

    output << this->name;

    if (recursive) {
      output << endl;
      output << "  " << "Binds: " << this->boundVariable->toString() << ")" << endl;

      if (this->features.find("RSTR") != this->features.end()) {
	output << "  " << "RSTR: " <<  this->getRstr()->toString() << endl;
      }

      if (this->features.find("BODY") != this->features.end()) {
	output << "  " << "BODY: " <<  this->getFeature("BODY")->toString() << endl;
      }
    }

    return output.str();
  }

  vector<Relation *> Quantifier::getBoundRelations() {
    return this->boundVariable->getReferencingRelations();
  }

  BoundVariable *Quantifier::getBoundVariable() {
    return this->boundVariable;
  }

  Handle *Quantifier::getRstr() {
    Handle *handle = dynamic_cast<Handle *>(this->getFeature("RSTR"));
    assert(handle != NULL);
    return handle;
  }

}
