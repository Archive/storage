#ifndef QUANTIFIER_HH_
#define QUANTIFIER_HH_

namespace MRS {
  class Quantifier;
};

#include <vector>
#include <string>

#include <libmrs/relation.hh>
#include <libmrs/mrs.hh>
#include <libmrs/bound-variable.hh>

namespace MRS {

  class Quantifier : public Relation {
  public:
    Quantifier(MRS &mrs, PET::fs &relation);
    virtual ~Quantifier();

    virtual std::string toString(bool recursive=false);

    virtual std::vector<Relation *> getBoundRelations();
    virtual BoundVariable *getBoundVariable();

    virtual Handle *getRstr();

  protected:
    BoundVariable *boundVariable;
  };

}

#endif
