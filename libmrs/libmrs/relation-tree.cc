#include "mrs.hh"
#include "relation-tree.hh"

#include <string>

using namespace std;

namespace MRS {

  RelationTree::RelationTree(Relation *relation) {
    this->relation = relation;
  }

  RelationTree::RelationTree(Relation *relation, RelationTree *parent) {
    this->relation = relation;
    parent->addChild(this);
  }


  void RelationTree::addChild(RelationTree *child) {
    this->children.push_back(child);
  }

  Relation *RelationTree::getRelation() {
    return this->relation;
  }

  vector<RelationTree *> &RelationTree::getChildren() {
    return this->children;
  }

  string RelationTree::toString() {
    return this->toString("");
  }

  string RelationTree::toString(string indent) {
    string result;
    if (this->relation != NULL) {
      result = indent + this->relation->toString(false) + "\n";
    }

    for(vector<RelationTree *>::iterator iter = this->children.begin(); iter != this->children.end(); ++iter) {
      RelationTree *childRelation = *iter;
      result += childRelation->toString(indent + "\t");
    }

    return result;
  }

}
