#ifndef RELATION_TREE_HH
#define RELATION_TREE_HH
  
namespace MRS {
  class RelationTree;
}

#include <vector>
#include <string>

#include <libmrs/mrs.hh>
#include <libmrs/relation.hh>

namespace MRS {
  class RelationTree {
  public:
    RelationTree(Relation *relation);
    RelationTree(Relation *relation, RelationTree *parent);
    std::string toString();

    Relation *getRelation();
    std::vector<RelationTree *> &getChildren();
    void addChild(RelationTree *child);

  private:
    std::string toString(std::string indent);
    Relation *relation;
    std::vector<RelationTree *> children;
  };
}

#endif /*RELATION_TREE_HH*/
