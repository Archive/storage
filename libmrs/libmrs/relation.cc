#include "mrs.hh"

using namespace PET;
using namespace std;

namespace MRS {

  Relation::~Relation() {
  }

  string Relation::toString(bool recursive) {
    std::stringstream output;
    std::map<std::string, Feature *>::iterator i;


    output << this->name;

    if (recursive) {
      output << endl;
      for (i = this->features.begin(); i != this->features.end(); ++i) {
	string featureName = i->first;
	Feature *argument = i->second;
	output << "  " << featureName << ": " << argument->toString() << endl;
      }
    }

    return output.str();
  }

  string Relation::getName() {
    return this->name;
  }

  Feature *Relation::getFeature(string featureName) {
    Feature *feature;
    std::map<std::string, Feature *>::iterator i = this->features.find(featureName);
    if (i != this->features.end()) {
      feature = (*i).second;
    } else {
      feature = NULL;
    }
    return feature;
  }

  std::map<std::string, Feature *> Relation::getFeatures() {
    return this->features;
  }

  std::vector<Feature *> Relation::getArguments() {
    return this->arguments;
  }

  Handle *Relation::getHandle() {
    assert(handle != NULL);
    return this->handle;
  }

  //  Relation::Relation(MRS &mrs, PET::fs &relation) : mrs(mrs) {
  Relation::Relation(MRS &mrs, PET::fs &relation) : Feature(relation), mrs(mrs) {
    this->name = relation.name();

    this->mrs = mrs;
    this->handle = NULL;

    char *possibleFeatures[9] = {"LBL", "ARG0", "ARG1", "ARG2", "ARG3", "RSTR", "MARG", "BODY", NULL};

    // Look through all the features and add them to our features list if they exist
    for (int i=0; possibleFeatures[i] != NULL; i++) {
      string featureName(possibleFeatures[i]);
      fs feature = relation.get_attr_value((char *)featureName.c_str());
      if (feature.valid()) {
	// The feature is present in this relation
	Feature *childFeature;

	childFeature = mrs.getFeature(feature);
	if (childFeature != NULL) {
	  this->features[featureName] = childFeature;
	}
	
	if (featureName.compare(0, 3, string("ARG")) == 0) {
	  this->arguments.push_back(childFeature);
	}

	if (featureName == "LBL") {
	  Handle *handle = dynamic_cast<Handle*>(childFeature);
	  handle->addRelation(this);
	  this->handle = handle;
	}

	BoundVariable *bound_variable = dynamic_cast<BoundVariable *>(childFeature);
	if (bound_variable != NULL) {
	  bound_variable->referencedBy(this);
	}

      }
    }
  }

  Relation *Relation::Factory(MRS &mrs, PET::fs &relation) {
    int relationType = relation.type();

    Relation *newRelation = NULL;

    if (PET::subtype(relationType, quant_relType)) {
      newRelation = new Quantifier(mrs, relation);
    } else if (PET::subtype(relationType, message_relType)) {
      newRelation = new Message(mrs, relation);
    } else {
      newRelation = new Relation(mrs, relation);
    }
    
    return newRelation;
  }

};
