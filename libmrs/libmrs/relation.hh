#ifndef RELATION_HH_
#define RELATION_HH_

namespace MRS {
  class Relation;
};

#include <map>
#include <string>

#include <libmrs/feature.hh>
#include <libmrs/handle.hh>
#include <libmrs/mrs.hh>

namespace MRS {
  
  class Relation : public Feature {
  public:
    Relation(MRS &mrs, PET::fs &relation);
    virtual ~Relation();

    virtual std::string getName();
    virtual std::string toString(bool recursive=false);

    static Relation *Factory(MRS &mrs, PET::fs &relation);

    virtual Handle *getHandle();

    Feature* getFeature(std::string featureName);

    std::map<std::string, Feature *> getFeatures();
    std::vector<Feature *> getArguments();

  protected:
    MRS &mrs;
    Handle *handle;
    std::vector<Feature *> arguments;
    std::map<std::string, Feature *> features;
    std::string name;
  };
}

#endif
