#include "resolver.hh"

#include <vector>
#include <ext/hash_map>

#define DEBUG_RESOLVER 0

using namespace std;

namespace MRS {

  static void addHandle (Handle *handle, RelationTree *parent, HCons *hcons, set<BoundVariable *> &bvs);


  /* Gets all the arguments in the list of relations */
  static set<BoundVariable *> getUnboundVariables(vector<Relation *> relations, set<BoundVariable *>bvs) {
    set<BoundVariable *> result;

    vector<Relation *>::iterator iter;
    for (iter = relations.begin(); iter != relations.end(); ++iter) {
      Relation *relation = *iter;
      vector<Feature *> arguments = relation->getArguments();
      for (vector<Feature*>::iterator argIter = arguments.begin(); argIter != arguments.end(); ++argIter) {
	BoundVariable *bv = dynamic_cast<BoundVariable*>(*argIter);
	if (bv != NULL) {
	  if (bvs.find(bv) == bvs.end()) {
	    result.insert(bv);
	  }
	}
      }
    }

    return result;
  }

  static void addHandleArguments(Relation *relation, RelationTree *parent, HCons *hcons, set<BoundVariable *> &bvs) {
    vector<Feature *> arguments = relation->getArguments();
    for (vector<Feature*>::iterator iter = arguments.begin(); iter != arguments.end(); ++iter) {
      Handle *handle = dynamic_cast<Handle *>(*iter);
      if (handle != NULL) {
	addHandle(handle, parent, hcons, bvs);
      }
    }
    
    Handle *marg = dynamic_cast<Handle *>(relation->getFeature("MARG"));
    if (marg != NULL) {
      addHandle(marg, parent, hcons, bvs);
    }
  }

  static RelationTree *addQuantifier(Quantifier *quantifier, RelationTree *parent, HCons *hcons, set<BoundVariable *>bvs) {
#if DEBUG_RESOLVER
    cout << "Adding quantifier " << quantifier->toString(false) << endl;
    cout << "  to {" << endl << parent->toString() << "}" << endl;
#endif
    RelationTree *treeChild = new RelationTree(quantifier, parent);
#if DEBUG_RESOLVER
    cout << "  now {" << endl << parent->toString() << "}" << endl << endl;
#endif

    BoundVariable *bv = quantifier->getBoundVariable();
    bvs.insert(bv);

    Handle *handle = quantifier->getRstr();
    addHandle(handle, treeChild, hcons, bvs);

#if DEBUG_RESOLVER
    cout << "After adding quantifier " << quantifier->toString(false) << " was {" << endl << parent->toString() << "}" << endl << endl;
#endif

    return treeChild;
  }

  static void addRelationWithTree(Relation *relation, RelationTree *relationTree, HCons *hcons, set<BoundVariable *> &bvs) {
    addHandleArguments(relation, relationTree, hcons, bvs);
  }

  static void addRelation(Relation *relation, RelationTree *parent, HCons *hcons, set<BoundVariable *> &bvs) {
#if DEBUG_RESOLVER
    cout << "Adding relation " << relation->toString(false) << endl;
    cout << "  to {" << endl << parent->toString() << "}" << endl;
#endif
    RelationTree *treeChild = new RelationTree(relation, parent);
#if DEBUG_RESOLVER
    cout << "  now {" << endl << parent->toString() << "}" << endl << endl;
#endif
    addRelationWithTree(relation, treeChild, hcons, bvs);
#if DEBUG_RESOLVER
    cout << "After adding relation " << relation->toString(false) << " was {" << endl << parent->toString() << "}" << endl << endl;
#endif
  }

  static void addHandle (Handle *handle, RelationTree *parent, HCons *hcons, set<BoundVariable *> &bvs) {
#if DEBUG_RESOLVER
    cout << "Adding handle " << handle->toString(false) << endl;
#endif

    vector<Relation *> relations = handle->getRelations();
    set<BoundVariable *> unboundVariables = getUnboundVariables(relations, bvs);

    for(set<BoundVariable*>::iterator iter = unboundVariables.begin(); iter != unboundVariables.end(); ++iter) {
      Quantifier *quantifier = (*iter)->getBindingQuantifier();
      parent = addQuantifier(quantifier, parent, hcons, bvs);
    }

    for(vector<Relation*>::iterator iter = relations.begin(); iter != relations.end(); ++iter) {
      addRelation(*iter, parent, hcons, bvs);
    }

    vector<Handle *> qeqHandles = hcons->getQEQHandles(handle);

    for(vector<Handle*>::iterator iter = qeqHandles.begin(); iter != qeqHandles.end(); ++iter) {
      addHandle(*iter, parent, hcons, bvs);
    }
  }

  vector<RelationTree *> Resolver::resolve(MRS &mrs) {
    Handle *topHandle = mrs.getTopHandle();
    vector<Relation *> topRelations = topHandle->getRelations();
    int topRelationsLength = topRelations.size();
    assert(topRelationsLength == 1);

    Relation *topRelation = topRelations[0];

#if DEBUG_RESORVER
    cout << "Adding relation " << topRelation->toString(false) << endl;
#endif

    RelationTree *featureTree = new RelationTree(topRelation);
    set<BoundVariable *> empty;
    addRelationWithTree(topRelation, featureTree, mrs.getHCons(), empty);

    vector<RelationTree *> trees;
    trees.push_back(featureTree);

    return trees;
  }

//   typedef hash_map<Feature *, vector<Feature *> > RelationToChildrenMap;

//   static void insertChildrenIntoTree (Relation *relation, RelationTree &featureTree, 
// 				      FeatureTree::iterator treeIter, RelationToChildrenMap &relationToChildren);

//   vector<FeatureTree > Resolver::resolve(MRS &mrs) {


//     cout << "Finding scope" << endl;

//     // Print out the quantifiers
//     vector<Quantifier *> quantifiers = mrs.getQuantifiers();
//     vector<Quantifier *>::iterator quants;
//     for (quants = quantifiers.begin(); quants != quantifiers.end(); ++quants) {
//       Quantifier *quantifier = *quants;
//       vector<Relation *> boundRelations = quantifier->getBoundRelations();

//       vector<Relation *>::iterator j;

//       cout << quantifier->toString(false) << " binds:" << endl;

//       for (j = boundRelations.begin(); j != boundRelations.end(); ++j) {
// 	Relation *relation = *j;
// 	cout << "  " << relation->toString(false) << endl;
//       }
	
//       cout << endl << endl;

//     }

//     // Do the actual resolving
//     RelationToChildrenMap relationToChildren;
//     vector<Relation *> relations = mrs.getRelations();

//     for (vector<Relation *>::iterator i = relations.begin(); i != relations.end(); ++i) {
//       Relation *relation = *i;

//       vector<Feature *> children;

//       HCons *hcons = mrs.getHCons();

//       cout << "Looking at relation " << relation->toString() << endl;

//       Quantifier *quantifier = dynamic_cast<Quantifier *>(relation);
//       if (quantifier) {
// 	//Handle *handle = quantifier->getHandle();
//       } else {
// 	Message *message = dynamic_cast<Message *>(relation);
// 	if (message) {
// 	  Handle *messageArgument = message->getMessageArgument();
// 	  Handle *outscoped       = hcons->outscopes_to_outscoped[messageArgument];

// 	  if (outscoped != NULL) {
// 	    cout << "Outscoped is " << outscoped->toString() << endl;
	    
// 	    vector<Relation *> relationChildren = outscoped->getRelations();
	    
// 	    // Need to convert the vector<Relation *> into a vector<Feature *>
// 	    vector<Relation *>::iterator childIter;
// 	    for (childIter = relationChildren.begin(); childIter != relationChildren.end(); ++childIter) {
// 	      Relation *relationChild = *childIter;
// 	      children.push_back(relationChild);
// 	    }
// 	  }
// 	} else {
// 	  children = relation->getArguments();
// 	}
//       }

//       relationToChildren[relation] = children;
//     }

//     cout << "Outside the loop" << endl;	

//     Handle *topHandle = mrs.getTopHandle();
//     std::vector<Relation *> topRelations = topHandle->getRelations();
//     assert (topRelations.size() == 1);
//     Feature *topFeature = topRelations[0];
//     Relation *topRelation = dynamic_cast<Relation *>(topFeature);
    
//     assert(topRelation != NULL);

//     FeatureTree featureTree(topRelation);
//     insertChildrenIntoTree(topRelation, featureTree, featureTree.begin(), relationToChildren);

//     vector<FeatureTree > trees;
//     trees.push_back(featureTree);
//     return trees;
//   }




//   static void insertChildrenIntoTree (Relation *relation, 
// 				      FeatureTree &featureTree,
// 				      FeatureTree::iterator position,
// 				      RelationToChildrenMap &relationToChildren) {
//     vector<Feature *> children = relationToChildren[relation];

//     for (vector<Feature *>::iterator i = children.begin(); i != children.end(); ++i) {
//       Feature *childFeature = *i;
//       FeatureTree::iterator childIter = featureTree.append_child(position, childFeature);
      
//       Relation *childRelation = dynamic_cast<Relation *>(childFeature);
//       if (childRelation != NULL) {
// 	insertChildrenIntoTree(childRelation, featureTree, childIter, relationToChildren);
//       }
//     }
//   }








  // START OF MRSRESOLVE IMPLEMENTATION


//   typedef hash_map<Handle *, set<Handle *> > Bindings;

//   class BindingsAndSisters {
//   public:
//     BindingsAndSisters(set<Relation *>pendingless, set<Relation *>relations,
// 		       Handle *top_handle, Bindings bindings, Handle *pending_qeq, void *unknown) {
//       assert (false);
//     }
//     Bindings *bindings;
//     set<Relation *> sisters;
//     Handle *pending_qeq;
//   };


//   static set<Relation *> relationSetDifference(set<Relation *> &relations, set<Relation *> &toRemove);
//   static set<Relation *> relationSetUnion(set<Relation *> &relations, set<Relation *> &toAdd);
//   static Handle *determineNewPending(Handle top_handle, Bindings bindings, Handle *pending_qeq);
//   static set<Relation *> calculateImpossibleTopRels(Handle top_handle, set<BoundVariable *> quantifier_bound_variables, Bindings bindings, set<Relation *> unused_relations, set<Relation *> scoping_relations, Handle *pending_qeq);
//   static set<BindingsAndSisters *> makeCombinationsOfTopRels (Handle top_handle,
// 							      set<Relation *> top_rels,
// 							      set<Relation *> something,
// 							      set<Relation *> pending_top_rels,
// 							      set<Quantifier *> possible_quant_top_rels,
// 							      Handle *pending_qeq,
// 							      Bindings bindings);
//   static void sisterStructureScope (set<Relation *> sisters,
// 				    set<BoundVariable *> quantifier_bound_variables,
// 				    Bindings bindings,
// 				    set<Relation *>something,
// 				    set<Handle *>scoping_handles,
// 				    Handle *pending_qeq,
// 				    set<Relation *>scoping_rels);



//   static Handle *determineNewPending(Handle top_handle,
// 				  Bindings bindings,
// 				  Handle *pending_qeq) {
//     assert (false);
//     return NULL;
//   }

//   static set<Relation *> calculateImpossibleTopRels(Handle top_handle,
// 				    set<BoundVariable *> quantifier_bound_variables,
// 				    Bindings bindings,
// 				    set<Relation *> unused_relations,
// 				    set<Relation *> scoping_relations,
// 				    Handle *pending_qeq) {
//     assert (false);
//     set<Relation *> foobar;
//     return foobar;

//   }

//   static set<BindingsAndSisters *> makeCombinationsOfTopRels (Handle top_handle,
// 							      set<Relation *> top_rels,
// 							      set<Relation *> something,
// 							      set<Relation *> pending_top_rels,
// 							      set<Quantifier *> possible_quant_top_rels,
// 							      Handle *pending_qeq,
// 							      Bindings bindings) {
//     assert (false);
//     set<BindingsAndSisters *> foobar;
//     return foobar;
//   }

//   static bool isHandleArgRel(Relation *relation) {
//     bool is_handle_arg = false;
//     std::vector<Feature *> arguments = relation->getArguments();
//     for (std::vector<Feature *>::iter i = arguments.begin(); i != arguments.end(); ++i) {
//       Feature *argument = *i;
//       if (dynamic_cast<Handle *>(argument) != NULL) {
// 	is_handle_arg = true;
//       }
//     }
//     return is_handle_arg;
//   }

//   static void sisterStructureScope (Relation *first_rel,
// 				    set<Relation *>top_rels,
// 				    set<BoundVariable *> quantifier_bound_variables,
// 				    Bindings bindings,
// 				    set<Relation *> other_rels,
// 				    set<Handle *>scoping_handles,
// 				    Handle *pending_qeq,
// 				    set<Relation *>scoping_rels) {
//     assert (false);

//     if (isHandleArgRel(first_rel)) {
//   }


//   static void createScopedStructures(Handle top_handle,
// 				     set<BoundVariable *> quantifier_bound_variables,
// 				     Bindings bindings,
// 				     set<Relation *> unused_relations,
// 				     set<Handle *> scoping_handles,
// 				     /* scoped-p */
// 				     set<Relation *> scoping_relations,
// 				     Handle *pending_qeq) {

//     assert (unused_relations.size() > 0);

//     pending_qeq = determineNewPending(top_handle, bindings, pending_qeq /*scoped-p*/);
//     set<Relation *> impossible_top_rels = calculateImpossibleTopRels (top_handle, quantifier_bound_variables,
// 								      bindings, unused_relations, scoping_relations,
// 								      pending_qeq);
//     set<Relation *>::iterator i;

//     set<Handle *> impossible_handles;
//     // Build impossible_handles as handles of relations that are impossible
//     for (i = impossible_top_rels.begin(); i != impossible_top_rels.end(); ++i) {
//       Relation *relation = *i;
//       impossible_handles.insert(relation->getHandle());
//     }

//     set<Relation *> possible_top_rels;
//     // Build possible_top_rels as rels that aren't impossible AND have handles
//     // that are not impossible
//     for (i = unused_relations.begin(); i != unused_relations.end(); ++i) {
//       Relation *relation = *i;
//       if (impossible_top_rels.find(relation) == impossible_top_rels.end()) {
// 	Handle *handle = relation->getHandle();
// 	if (impossible_handles.find(handle) == impossible_handles.end()) {
// 	  possible_top_rels.insert(relation);
// 	}
//       }
//     }

//     set<Relation *>pending_top_rels;
//     set<Quantifier *>possible_quant_top_rels;
//     set<Relation *>top_rels;

//     for (i = possible_top_rels.begin(); i != possible_top_rels.end(); ++i) {
//       Relation *relation = *i;
//       if (pending_qeq != NULL) {
// 	if (relation->getHandle() == pending_qeq) {
// 	  pending_top_rels.insert(relation);
// 	}
// 	Quantifier *quantifier = dynamic_cast<Quantifier *>(relation);
// 	if (quantifier != NULL) {
// 	  possible_quant_top_rels.insert(quantifier);
// 	}
//       }
//       if (relation->getHandle() == &top_handle) {
// 	top_rels.insert(relation);
//       }
//     }

//     bool are_top_rels = (top_rels.size() > 0);
//     bool are_possible_quant_top_rels = (possible_quant_top_rels.size() > 0);
//     bool are_pending_top_rels = (pending_top_rels.size() > 0);


// // 	  (if 
// // 	      (if pending-qeq 
// // 		  (and  (or (not top-rels) *alex-mode*)
// // 			(or possible-quant-top-rels pending-top-rels))
// // 		;; if we've got a pending-qeq, there can be no
// // 		;; known top-rels (maybe iffy?) and there must
// // 		;; be either possible quantifiers or relations
// // 		(and possible-top-rels 
// // 		     (subsetp top-rels possible-top-rels)))
// // 	      ;; unless there are some possible rels and all the top rels
// // 	      ;; are possible, we fail
//     if (pending_qeq && are_top_rels 
// 	&& (are_possible_quant_top_rels || are_pending_top_rels)) {
// //             ;; if we've got a pending-qeq, there can be no
// //             ;; known top-rels (maybe iffy?) and there must
// //             ;; be either possible quantifiers or relations
// //             (and possible-top-rels 
// //                  (subsetp top-rels possible-top-rels)))

//       set<BindingsAndSisters *> combinations_of_top_rels;

//       combinations_of_top_rels = makeCombinationsOfTopRels(top_handle, 
// 							   top_rels,
// 							   relationSetDifference(possible_top_rels, top_rels),
// 							   pending_top_rels, 
// 							   possible_quant_top_rels,
// 							   pending_qeq, 
// 							   bindings);

//       set<BindingsAndSisters *>::iterator j;

//       for (j = combinations_of_top_rels.begin(); j != combinations_of_top_rels.end(); ++j) {
// 	/* For each possible set of rels which can have their handles 
// 	   bound to the top_handle, generate a set of results */
// 	BindingsAndSisters *rel_comb_and_bindings = *j;
// 	set<Relation *> sisters = rel_comb_and_bindings->sisters;

// 	scoping_handles.insert(&top_handle);

// 	sisterStructureScope (sisters, 
// 			      quantifier_bound_variables,
// 			      *(rel_comb_and_bindings->bindings),
// 			      relationSetDifference(unused_relations, sisters),
// 			      scoping_handles,
// 			      rel_comb_and_bindings->pending_qeq,
// 			      scoping_relations);
				
//       }
//     }
//   }

//   static set<BindingsAndSisters *> makeCombinationOfTopRels (Handle *top_handle, set<Relation *>known_top_rels,
// 							     set<Relation *> other_possibles, set<Relation *>pending,
// 							     set<Quantifier *> possible_quants, Handle *pending_qeq,
// 							     Bindings bindings) {
//     if (pending_qeq != NULL) {
//       set<BindingsAndSisters *> bass;

//       set<Relation *> pendingless = relationSetDifference(other_possibles, pending);
//       if (pending.size() > 0) {
// 	bass.insert(new BindingsAndSisters(pendingless, relationSetUnion(pending, known_top_rels),
// 					   top_handle, bindings, pending_qeq, NULL));
//       }
//       if (possible_quants.size() > 0) {
// 	/////// FIXME: START HERE
//       }
//       //FIXME
//       return bass;
//     } else {
//       BindingsAndSisters *bas = new BindingsAndSisters(other_possibles, known_top_rels, top_handle, bindings, NULL, NULL);
//       set<BindingsAndSisters *> bass;
//       bass.insert(bas);
//       return bass;
//     }
//   }
  
//   static set<Relation *> relationSetDifference(set<Relation *> &relations, set<Relation *> &toRemove) {
//     set<Relation *> new_set;
//     set_difference(relations.begin(), relations.end(),
// 		   toRemove.begin(), toRemove.end(),
// 		   inserter(new_set, new_set.begin()));
//     return new_set;
//   }

//   static set<Relation *> relationSetUnion(set<Relation *> &relations, set<Relation *> &toAdd) {
//     set<Relation *> new_set;
//     set_union(relations.begin(), relations.end(),
// 	      toAdd.begin(), toAdd.end(),
// 	      inserter(new_set, new_set.begin()));
//     return new_set;
//   }

}
