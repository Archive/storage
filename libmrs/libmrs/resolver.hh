#ifndef RESOLVER_HH_
#define RESOLVER_HH_

namespace MRS {
  class Resolver;
}

#include <libmrs/mrs.hh>
#include <libmrs/handle.hh>
#include <libmrs/relation-tree.hh>

#include <ext/hash_map>
#include <set>

namespace MRS {

  class Resolver {
  public:
    vector<RelationTree *> resolve(MRS &mrs);
  private:
  };

}

#endif /* RESOLVER_HH_ */
