#include "mrs.hh"
#include "parser.hh"
#include "resolver.hh"

#include <iostream.h>

#include <string>
#include <vector>

using namespace std;

int main (int argv, char **argc) {
  char *grammar = NULL;

  if (argv == 1) {
    grammar = strdup(GRAMMAR_FILE);
  } else if (argv != 2) {
    cout << "Usage: nl-parser GRAMMAR_FILE.grm" << endl;
    grammar = strdup(argc[1]);
    return -1;
  }

  MRS::Parser *parser = new MRS::Parser (grammar);
  MRS::Resolver resolver;

  while (true) {
    //cout << endl << endl << endl << "========================================================================" << endl << endl << endl << endl;
    cout << endl << "Parse: ";

    string toParse;

    getline(cin, toParse);

    vector<MRS::MRS *> parsings = parser->parseToMRS(toParse);

    cout << "Found " << parsings.size() << " interpretations:" << endl;

    int counter = 0;
    vector<MRS::MRS *>::iterator iter;
    for (iter = parsings.begin(); iter != parsings.end(); ++iter) {
      MRS::MRS *parsing = *iter;

      cout << "Interpretation " << ++counter << ":" << endl;
      cout << parsing->toString() << endl;;

      vector<MRS::RelationTree *> trees = resolver.resolve(*parsing);
      vector<MRS::RelationTree *>::iterator treesIter;

      for (treesIter = trees.begin(); treesIter != trees.end(); ++treesIter) {
	MRS::RelationTree *tree = *treesIter;
	cout << tree->toString();
      }
      cout << endl << endl;
    }
  }

  return 0;

}

