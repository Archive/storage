#include "semantic-to-sql.hh"


#include <string>
#include <sstream>
#include <iostream>

using namespace std;

static void parseToplevel(xmlNodePtr parentNode, std::stringstream &outstream);

static bool nodeIs(xmlNodePtr node, const char *type) {
  return (strcmp ((const char *)node->name, type) == 0);
}

static string getContents(xmlNodePtr node) {
  xmlChar *property = xmlNodeGetContent(node);
  string result;
  if (property != NULL) {
    result = (char *)property;
    xmlFree(property);
  }
  
  return result;
}




static void  parseText(xmlNodePtr node, std::stringstream &outstream) {
  outstream << getContents(node);
}

static char *getNamedChildContents(xmlNodePtr parentNode, const char *childName);
static char *getNamedChildContents(xmlNodePtr parentNode, const char *childName) {
  xmlNodePtr node = parentNode->children;

  for (; node != NULL; node = node->next) {
    if (node->type == XML_ELEMENT_NODE) {
      // FIXME: check namespace == mrs
      if (nodeIs(node, childName)) {
	return (char *)xmlNodeGetContent (node);
      } else {
	char *possible = getNamedChildContents(node->children, childName);
	if (possible != NULL) return possible;
      }
    }
  }

  return NULL;
}




static void parseAttributeInList(xmlNodePtr parent, std::stringstream &outstream) {
  char *attribute = getNamedChildContents(parent, "attribute");
  char *searchList = getNamedChildContents(parent, "list");
  char *arg = getNamedChildContents(parent, "arg");

  outstream << arg << ".recordID in (SELECT recordID From AttrSoup WHERE AttrSoup.attrName = \'" << attribute << "\' AND AttrSoup.attrValue in (" << searchList << ")) ";
}

static void parseSelect(xmlNodePtr selectNode, std::stringstream &outstream) {
  xmlNodePtr node = selectNode->children;

  char *boundVariable = NULL;
  xmlNodePtr whereNode = NULL;

  for (; node != NULL; node = node->next) {
    if (node->type == XML_ELEMENT_NODE) {
      if (nodeIs(node, "bound")) {
	boundVariable = getNamedChildContents(node, "BoundVariable");
      } else if (nodeIs(node, "where")) {
	whereNode = node;
      }
    } else if (node->type == XML_TEXT_NODE) {
      parseText(node, outstream);
    }
  } 

  if ((boundVariable == NULL) || (whereNode == NULL)) return;

  outstream << "SELECT DISTINCT " << boundVariable << ".recordID FROM AttrSoup AS " << boundVariable << " WHERE ("; 
  parseToplevel(whereNode, outstream);
  outstream << ")";

  xmlFree(boundVariable);
}

static void parseNot(xmlNodePtr node, std::stringstream &outstream) {
  outstream << "NOT (";
  parseToplevel(node, outstream);
  outstream << ")";
}

static void parseExists(xmlNodePtr node, std::stringstream &outstream) {
  outstream << "EXISTS (";
  parseToplevel(node, outstream);
  outstream << ")";
}

static void parseArg(xmlNodePtr node, std::stringstream &outstream) {
  outstream << getContents(node);
}

static void parseConstant(xmlNodePtr node, std::stringstream &outstream) {
  outstream << getContents(node);
}

static void parseToplevel(xmlNodePtr parentNode, std::stringstream &outstream) {
  xmlNodePtr node = parentNode->children;

  for (; node != NULL; node = node->next) {
    if (node->type == XML_ELEMENT_NODE) {
      if (nodeIs(node, "select")) {
	parseSelect(node, outstream);
      } else if (nodeIs(node, "not")) {
	parseNot(node, outstream);
      } else if (nodeIs(node, "exists")) {
	parseExists(node, outstream);
      } else if (nodeIs(node, "arg")) {
	parseArg(node, outstream);
      } else if (nodeIs(node, "constant")) {
	parseConstant(node, outstream);
      } else if (nodeIs(node, "AttributeInList")) {
	parseAttributeInList(node, outstream);
      } else {
	parseToplevel(node, outstream);
      }
    } else if (node->type == XML_TEXT_NODE) {
      parseText(node, outstream);
    }
  } 
}

string semantic_to_sql(xmlDocPtr doc) {

  std::stringstream outstream;

  if (doc == NULL
      || doc->xmlRootNode == NULL
      || doc->xmlRootNode->name == NULL) {
    xmlFreeDoc(doc);
    cerr << "Failed to open doc!" << endl;
    return "";
  }

  parseToplevel(doc->xmlRootNode, outstream);

  return outstream.str();
}
