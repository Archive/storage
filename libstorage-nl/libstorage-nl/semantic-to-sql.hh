#ifndef SEMANTIC_TO_SQL_H__
#define SEMANTIC_TO_SQL_H__

#include <string>
#include <libxml/tree.h>

std::string semantic_to_sql(xmlDocPtr doc);

#endif
