extern "C" {
#include "storage-nl-parser.h"
}

#include <semantic-to-sql.hh>

#include <libstorage/storage-store.h>
#include <libstorage/storage-item.h>

#include <libmrs/mrs.hh>
#include <libmrs/parser.hh>
#include <libmrs/resolver.hh>
#include <libmrs-converter/converter.hh>

#include <string.h>
#include <stdlib.h>

#include <string>
#include <vector>

using namespace std;

static GObjectClass *parent_class = NULL;

struct _StorageNlParserPrivate {
  MRS::Parser *parser;
  MRS::Resolver *resolver;
  MRS::Converter *converter;

  char *syntactic_grammar;
  char *semantic_grammar;
};

enum
{
  PROP_0,
  PROP_SYNTACTIC_GRAMMAR,
  PROP_SEMANTIC_GRAMMAR
};

GSList *
storage_nl_parser_get_matching_items (StorageNlParser *parser,
				      const char *nlphrase) {
  char *sql_query;

  sql_query = storage_nl_parser_get_sql_query (parser, nlphrase);

  return NULL;
}

char *
storage_nl_parser_get_sql_query (StorageNlParser *parser,
				 const char *nlphrase_char) {
  string sql_query;

  std::string nlphrase(nlphrase_char);

  if (parser->priv->parser == NULL) {
    parser->priv->parser = new MRS::Parser (parser->priv->syntactic_grammar);
    parser->priv->resolver = new MRS::Resolver ();
    parser->priv->converter = new MRS::Converter (parser->priv->semantic_grammar);
  }

  vector<MRS::MRS *> parsings = parser->priv->parser->parseToMRS(nlphrase);
  cout << "Found " << parsings.size() << " interpretations:" << endl;

  vector<MRS::MRS *>::iterator iter;
  for (iter = parsings.begin(); iter != parsings.end(); ++iter) {
    MRS::MRS *parsing = *iter;
    
    vector<MRS::RelationTree *> trees = parser->priv->resolver->resolve(*parsing);
    vector<MRS::RelationTree *>::iterator treesIter;
    
    for (treesIter = trees.begin(); treesIter != trees.end(); ++treesIter) {
      MRS::RelationTree *tree = *treesIter;
      cout << "Tree {" << endl << tree->toString() << "}" << endl;
      sql_query = semantic_to_sql(parser->priv->converter->convert(tree));
      cout << "Results in " << sql_query << endl;
    }
    cout << endl << endl;
  }
  

  return g_strdup(sql_query.c_str());
}

static void
storage_nl_parser_set_property (GObject *object,
				guint prop_id,
				const GValue *value,
				GParamSpec *pspec)
{
        StorageNlParser *parser = STORAGE_NL_PARSER (object);
                                                                                                                             
        switch (prop_id) {
	        case PROP_SYNTACTIC_GRAMMAR:
		        g_free (parser->priv->syntactic_grammar);
			parser->priv->syntactic_grammar = g_strdup (g_value_get_string (value));
			break;
                case PROP_SEMANTIC_GRAMMAR:
		        g_free (parser->priv->semantic_grammar);
			parser->priv->semantic_grammar = g_strdup (g_value_get_string (value));
			break;
        }
}

static void
storage_nl_parser_get_property (GObject *object,
				guint prop_id,
				GValue *value,
				GParamSpec *pspec)
{
        StorageNlParser *parser = STORAGE_NL_PARSER (object);
                                                                                                                             
        switch (prop_id) {
                case PROP_SYNTACTIC_GRAMMAR:
                        g_value_set_string (value, parser->priv->syntactic_grammar);
                        break;
                case PROP_SEMANTIC_GRAMMAR:
                        g_value_set_string (value, parser->priv->semantic_grammar);
                        break;
        }
}



static void
storage_nl_parser_init (StorageNlParser *parser)
{
	parser->priv = g_new0 (StorageNlParserPrivate, 1);
}

static void
storage_nl_parser_finalize (GObject *object)
{
	StorageNlParser *parser;
                                                                                                                        
	parser = STORAGE_NL_PARSER(object);
	g_return_if_fail(parser != NULL);

	delete (parser->priv->parser);
	delete (parser->priv->resolver);
	delete (parser->priv->converter);

	g_free (parser->priv->syntactic_grammar);
	g_free (parser->priv->semantic_grammar);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}


static void
storage_nl_parser_class_init (StorageNlParserClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = storage_nl_parser_finalize;
	object_class->set_property = storage_nl_parser_set_property;
	object_class->get_property = storage_nl_parser_get_property;

	parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

	g_object_class_install_property (object_class,
					 PROP_SYNTACTIC_GRAMMAR,
					 g_param_spec_string ("syntactic-grammar",
							      "Syntactic Grammar",
							      "Filename of the LiNGO grammar to use",
							      NULL,
							      (GParamFlags) (G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT_ONLY)));

	g_object_class_install_property (object_class,
					 PROP_SEMANTIC_GRAMMAR,
					 g_param_spec_string ("semantic-grammar",
							      "Semantic Grammar",
							      "Filename of the semantic grammar to use",
							      NULL,
							      (GParamFlags) (G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT_ONLY)));

}

GType
storage_nl_parser_get_type (void)
{
        static GType type = 0;

        if (type == 0) {
                GTypeInfo info = {
                        sizeof (StorageNlParserClass),
                        NULL, /* GBaseInitFunc */
			NULL, /* GBaseFinalizeFunc */
			(GClassInitFunc) storage_nl_parser_class_init, 
			NULL, /* GClassFinalizeFunc */
			NULL, /* user supplied data */
                        sizeof (StorageNlParser), 
			0, /* n_preallocs */
			(GInstanceInitFunc) storage_nl_parser_init,
			NULL
                };

                type = g_type_register_static (G_TYPE_OBJECT, "StorageNlParser", &info, (GTypeFlags)0);
        }

        return type;
}

StorageNlParser *
storage_nl_parser_new ()
{
  return storage_nl_parser_new_with_grammars (SYNTACTIC_GRAMMAR_FILE,
					      SEMANTIC_GRAMMAR_FILE);
}

StorageNlParser   *
storage_nl_parser_new_with_grammars (const char *syntactic_grammar_file,
				     const char *semantic_grammar_file)
{
  StorageNlParser *parser;

  parser = STORAGE_NL_PARSER(g_object_new (STORAGE_TYPE_NL_PARSER,
					   "syntactic-grammar", syntactic_grammar_file,
					   "semantic-grammar", semantic_grammar_file,
					   NULL));

  return parser;
}
