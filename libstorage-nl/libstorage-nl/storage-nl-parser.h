#ifndef STORAGE_NL_PARSER_H
#define STORAGE_NL_PARSER_H

#include <libstorage/storage-item.h>

G_BEGIN_DECLS

#define STORAGE_TYPE_NL_PARSER		(storage_nl_parser_get_type ())
#define STORAGE_NL_PARSER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), STORAGE_TYPE_NL_PARSER, StorageNlParser))
#define STORAGE_NL_PARSER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), STORAGE_TYPE_NL_PARSER, StorageNlParserClass))
#define STORAGE_IS_NL_PARSER(obj)		(GTK_TYPE_CHECK_INSTANCE_TYPE ((obj), STORAGE_TYPE_NL_PARSER))
#define STORAGE_IS_NL_PARSER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), STORAGE_TYPE_NL_PARSER))
#define STORAGE_NL_PARSER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), STORAGE_TYPE_NL_PARSER, StorageNlParserClass))

typedef struct _StorageNlParser StorageNlParser;
typedef struct _StorageNlParserClass StorageNlParserClass;
typedef struct _StorageNlParserPrivate StorageNlParserPrivate;

struct _StorageNlParser
{
  GObject parent_instance;
  StorageNlParserPrivate *priv;
};

struct _StorageNlParserClass
{
  GObjectClass parent_class;
};


GType		storage_nl_parser_get_type		(void);

GSList *        storage_nl_parser_get_matching_items    (StorageNlParser *parser,
							 const char *natural_language_phrase);
char *          storage_nl_parser_get_sql_query         (StorageNlParser *parser,
							 const char *natural_language_phrase);

StorageNlParser   *storage_nl_parser_new                ();
StorageNlParser   *storage_nl_parser_new_with_grammars  (const char *syntactic_grammar_file,
							 const char *semantic_grammar_file);

G_END_DECLS

#endif /* STORAGE_NL_PARSER_H */
