#include <storage-nl-parser.h>
#include <glib.h>
#include <stdio.h>
#include <string.h>

int
main (int argc, char **argv)
{
  g_type_init ();

  StorageNlParser *parser;
  char *sql_query;
  char nl_phrase[512];

  parser = storage_nl_parser_new();

  while (1) {
    printf("Parse: ");
    fgets(nl_phrase, 512, stdin);
    nl_phrase[strlen(nl_phrase) - 1] = '\0';
    sql_query = storage_nl_parser_get_sql_query(parser, nl_phrase);
    printf ("SQL Query: {\n    %s\n}\n", sql_query);

    g_free(sql_query);
  }
  return 0;
}
