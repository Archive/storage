#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* include this first, before NO_IMPORT_PYGOBJECT is defined */
#include <pygobject.h>
#include "storage-nl-parser.h"

void pystoragenl_register_classes (PyObject *d);
/*void pystoragenl_add_constants(PyObject *module, const gchar *strip_prefix);*/
		
extern PyMethodDef pystoragenl_functions[];

DL_EXPORT(void)
initstoragenl (void)
{
	PyObject *m, *d;

	init_pygobject ();

	m = Py_InitModule ("storagenl", pystoragenl_functions);
	d = PyModule_GetDict (m);

	pystoragenl_register_classes (d);
	/*pystoragenl_add_constants (m, "STORAGE_NL_");*/
	
	if (PyErr_Occurred ()) {
		Py_FatalError ("can't initialize module storage");
	}
}
