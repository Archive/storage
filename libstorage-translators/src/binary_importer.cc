#include "binary_importer.hh"
#include "image_importer.hh"
#include <iostream>
#include <libstorage/storage-store.h>
#define BUFFER_SIZE (32*1024*1024)

using namespace std;

const char* BinaryImporter::A_COMMENT = "comment";

BinaryImporter::BinaryImporter () : Importer::Importer() {
  this->totalWritten = 0;
}

GnomeVFSResult BinaryImporter::startImport (const char *fileName, const char *mimeType, bool toplevelNode)
{
	printf ("BinaryImporter::startImport()\n");

	this->binary_item = storage_store_add_item (defaultStore, STORAGE_ITEM_TYPE_BINARY);
	this->setToplevelItem (this->binary_item);

	Importer::startImport(fileName, mimeType, toplevelNode);

	this->writeHandle = storage_item_get_binary_handle(binary_item, STORAGE_BINARY_HANDLE_OPEN_WRITE);

	return GNOME_VFS_OK;
}

GnomeVFSResult BinaryImporter::importChunk (const char *buffer, gsize bufferSize) {
  gsize actuallyWritten;
  GnomeVFSResult result = GNOME_VFS_OK;
  gsize amountLeft = bufferSize;

  while ((amountLeft > 0) && (actuallyWritten > 0)) {
    actuallyWritten = storage_binary_handle_write(this->writeHandle, buffer, amountLeft);
    if (actuallyWritten > 0) {
      amountLeft -= actuallyWritten;
    }
  }

  this->totalWritten += bufferSize;

  set(FILE_SIZE, itos(this->totalWritten));

  if (actuallyWritten > 0) {
    return GNOME_VFS_OK;
  } else {
    return GNOME_VFS_ERROR_GENERIC;
  }
}

GnomeVFSResult BinaryImporter::finishImport () {
  printf ("BinaryImporter::finishImport()\n");
  set(FILE_SIZE, itos(this->totalWritten));
  storage_binary_handle_close (this->writeHandle);

  Importer::finishImport();

  return GNOME_VFS_OK;
}

BinaryImporter::~BinaryImporter() { }

