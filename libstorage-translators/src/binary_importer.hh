#ifndef _binary_importer_H
#define _binary_importer_H

#include "importer.hh"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libstorage/storage-item.h>
#include <libstorage/storage-binary-handle.h>

class ImageImporter;

class BinaryImporter: public Importer {
  
public:
  BinaryImporter ();
  virtual ~BinaryImporter();  

  virtual GnomeVFSResult startImport (const char *fileName, const char *mimeType, bool toplevelNode=true);
  virtual GnomeVFSResult importChunk (const char *buffer, gsize bufferSize);
  virtual GnomeVFSResult finishImport ();

  //virtual ImageImporter* setThumbnail(const std::string& filename, bool remove = false);
  //virtual void setThumbnail(StorageItem *item);

protected:
  StorageBinaryHandle *writeHandle;
  StorageItem *binary_item;

  gsize totalWritten;
  static const char* A_COMMENT;
};

#endif
