#include <stdio.h>
#include <string.h>
#include <libgnomevfs/gnome-vfs.h>

#include <string>
#include <iostream>

#include "importer_caller.hh"

#include <libpq-fe.h> /*in /usr/include26*/
#include <libpq/libpq-fs.h>

using namespace std;

/* Function prototypes */
static int check_result(GnomeVFSResult result);
gboolean WhileInDirectory(const gchar *rel_path, GnomeVFSFileInfo *info, gboolean recursing_will_loop, gpointer data, gboolean *recurse);
void RunDirectories (const char *directoryNameUri);
void CallCorrectImporter(gchar *fileNameUri); 

/*
 * check that gnome vfs result is ok
 */
static int check_result(GnomeVFSResult result) {
	if (result != GNOME_VFS_OK) {
	  g_print ("(Failed): ");
		fprintf (stderr, "%s\n", gnome_vfs_result_to_string (result));
		return -1;
	}
	return 0; /*by convention, exit status 0 is success*/
}

void RunDirectories (const char *directoryNameUri){
  GnomeVFSResult result;
  if(!gnome_vfs_initialized()){
    gnome_vfs_init();
  }
  
  /* does follow symbolic links */
  result = gnome_vfs_directory_visit((gchar *)directoryNameUri, (GnomeVFSFileInfoOptions)(GNOME_VFS_FILE_INFO_GET_MIME_TYPE | GNOME_VFS_FILE_INFO_FOLLOW_LINKS), GNOME_VFS_DIRECTORY_VISIT_LOOPCHECK, WhileInDirectory, (gpointer)directoryNameUri);
  
  check_result(result); 
}

gboolean WhileInDirectory(const gchar *rel_path, GnomeVFSFileInfo *info, gboolean recursing_will_loop, gpointer parentDirectoryNameUri, gboolean *recurse) {
  gchar *fullNameUri;
  
  fullNameUri = g_build_filename((gchar *)parentDirectoryNameUri, (gchar *)rel_path, NULL);
  
  if(info->type == GNOME_VFS_FILE_TYPE_DIRECTORY) {
    RunDirectories(fullNameUri);
  }
  else if(info->type == GNOME_VFS_FILE_TYPE_REGULAR) {
    CallCorrectImporter(fullNameUri);
  }
  
  g_free(fullNameUri);
  
  return(1);
}

void CallCorrectImporter(gchar *fileNameUri) {
  string fileNameUriStr = fileNameUri;
  int len = fileNameUriStr.length();

  char *mime_type;

  if(strcasecmp((fileNameUriStr.substr(len-5, len)).c_str(), ".mbox") == 0) {
    mime_type = "application/x-mbox";
  } else {
    GnomeVFSFileInfo fileInfo;
    GnomeVFSResult result = gnome_vfs_get_file_info (fileNameUri, &fileInfo, GNOME_VFS_FILE_INFO_GET_MIME_TYPE);
    if (result != GNOME_VFS_OK) {
      cerr << "Could not import " << fileNameUri << endl;
      cerr << gnome_vfs_result_to_string(result) << endl << endl << endl;
    }
      
    mime_type = strdup(fileInfo.mime_type);
  }

  callImporter(g_filename_from_uri(fileNameUri, NULL, NULL), mime_type, true, NULL);
  //cout << "Call importer for " << fileNameUri << " file of mime type " << mime_type << endl;
}

int main(int argc, char **argv) {
  gnome_vfs_init();

  if (argc < 3) {
    cout << "usage:\t"	<< argv[0] << " -i  	<directory or file path>" << endl;
    cout << "\t"	<< argv[0] << " -clear db" << endl;
    return -1;
  }

  string option = argv[1];

  if (option == "-i") {	
    //----------------------- convert path into file:// URI
    for (int i=2; i < argc; i++) {
      char *path;
      const char *input = argv[i];
      
      if (input[0] == '/') {
	// this is a full path
	path = g_strdup(input);
      } else {
	char *current_dir = g_get_current_dir();
	path = g_build_filename(current_dir, input, NULL);
	g_free (current_dir);
      }
      
      char  *escaped_uri = gnome_vfs_get_uri_from_local_path (path);
      g_free(path);
      
      GnomeVFSFileInfo info;
      gnome_vfs_get_file_info(escaped_uri, &info, GNOME_VFS_FILE_INFO_GET_MIME_TYPE);
      if(info.type == GNOME_VFS_FILE_TYPE_DIRECTORY) {
	RunDirectories(escaped_uri);
      } else if(info.type == GNOME_VFS_FILE_TYPE_REGULAR) {
	CallCorrectImporter(escaped_uri);
      }
    }
  } else if (option == "-clear") {
    string connectStr = "host = beauty.stanford.edu dbname = gargamel user = senior password = senior";
    PGconn *connection = PQconnectdb(connectStr.c_str());

    ConnStatusType stat = PQstatus(connection);
    char *error = PQerrorMessage(connection);
    if (PQstatus(connection) == CONNECTION_BAD)
      {
	printf("%s", PQerrorMessage(connection));
      }
    if(strlen(error) > 0){
      fprintf(stderr, "DB connection error to gargamel: %s\n", error);
      return(-1);
    }

    PGresult *results;

    string getBlobs = "SELECT attrvalue FROM numbersoup WHERE attrname = 'blobid';";
    results = PQexec(connection, getBlobs.c_str());
 
    for(int i = 0; i < PQntuples(results); i++){
      cout << ">>Unlinking Blobid: " << PQgetvalue(results, i, 0) << endl;
      lo_unlink(connection, (unsigned int)(atoi(PQgetvalue(results, i, 0))));
    }

    PQclear(results);

    string clearAttrsoup = "DELETE from attrsoup;";
    string clearNodechildren = "DELETE from nodechildren;";
    string clearDatesoup = "DELETE from datesoup;";
    string clearNumbersoup = "DELETE from numbersoup;";
    string clearAvailablerecordids = "DELETE from availablerecordids;";
    string clearPrevhighestrecordid = "DELETE from prevhighestrecordid;";
    string clearTextrecords = "DELETE from textrecords;";

    PQexec(connection, clearAttrsoup.c_str());
    PQexec(connection, clearNodechildren.c_str());
    PQexec(connection, clearDatesoup.c_str());
    PQexec(connection, clearNumbersoup.c_str());
    PQexec(connection, clearAvailablerecordids.c_str());
    PQexec(connection, clearPrevhighestrecordid.c_str());
    PQexec(connection, clearTextrecords.c_str());

    string insertStartingValue = "INSERT INTO Prevhighestrecordid (recordid) VALUES (1);";
    PQexec(connection, insertStartingValue.c_str());
    
    PQfinish(connection);
  }
  return(1);
}
