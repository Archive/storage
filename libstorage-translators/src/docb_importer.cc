#include "docb_importer.hh"

using namespace std;

DOCBImporter::DOCBImporter() : XMLImporter() {}
DOCBImporter::~DOCBImporter() {}

/*
 * Overrides parent class to use html parsing
 * May not work as planned... needs testing!
 */
xmlDocPtr DOCBImporter::parseFile(const char *filename, const char *encoding) {
  return((xmlDocPtr)docbParseFile(filename, encoding));
}

#define DOCB_TEST 1
#ifdef DOCB_TEST
int main(int argc, char** argv) {

  if(argc <= 1 || !g_path_is_absolute((gchar *)argv[1])) {
    printf("Usage: need an absolute path directory (directory or file)\n");
    return(0);
  }

  char *docnameUri = g_filename_to_uri(argv[1], NULL, NULL); /*must be an absolute path!*/
  DOCBImporter importer;
  importer.import(docnameUri, true);

  cout << "...import complete." << endl;
  //  StorageItem *item = importer.getItem();
  //GSList *attrList = storage_item_get_attributes_pairs(item);

  //GSList *curr = attrList;

  //cout << "received list. imported the following: " << endl;
  //while (curr != NULL) {
  //StorageAttrPair *data = (StorageAttrPair*) curr->data;

  //if (data == NULL) {
  //  cout << "*** LIST ERROR ***" << endl;
  //}
  //else {
  //  cout << "* " << data->name << "\t= " << data->value << endl;
  //}
  //curr = g_slist_next(curr);
  //}
  //g_slist_free(attrList);
  return 0;
}

#endif
