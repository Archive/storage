#ifndef docb_importer_HH
#define docb_importer_HH

#include "docb_importer.hh"
#include "xml_importer.hh"
#include <libxml/DOCBparser.h>

using namespace std;

class DOCBImporter: public XMLImporter {
protected:
  virtual xmlDocPtr parseFile(const char *filename, const char *encoding = NULL);

public:
  DOCBImporter();
  virtual ~DOCBImporter();
};

#endif
