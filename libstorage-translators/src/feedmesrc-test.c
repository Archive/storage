#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <gst/gst.h>
#include <gst/gsttag.h>

#include <libgnomevfs/gnome-vfs.h>

#include "feedmesrc.h"

#define USE_PULL 1
#define USE_PUSH 1

static GstData *get_next_data_chunk (const char *uri);

static void
metadata_found_tag_cb (const GstTagList *list,
		       const gchar *tag,
		       gpointer data)
{
	const GValue *val;
	const gchar *s;
	gint i;
	guint u;
	gdouble d;
	gboolean b;

	val = gst_tag_list_get_value_index (list, tag, 0);

	switch (G_VALUE_TYPE (val))
	{
		case G_TYPE_STRING:
			s = g_value_get_string (val);
			g_print ("TAG %s: %s\n", tag, s);
			break;
		case G_TYPE_BOOLEAN:
			b = g_value_get_boolean (val);
			g_print ("TAG %s: %s\n", tag, b ? "TRUE" : "FALSE");
			break;
		case G_TYPE_UINT:
			u = g_value_get_uint (val);
			g_print ("TAG %s: %u\n", tag, u);
			break;
		case G_TYPE_INT:
			i = g_value_get_int (val);
			g_print ("TAG %s: %d\n", tag, i);
			break;
		case G_TYPE_DOUBLE:
			d = g_value_get_double (val);
			g_print ("TAG %s: %f\n", tag, d);
			break;
		default:
			g_print ("Cannot handle GValue of type %s\n",
				 G_VALUE_TYPE_NAME (val));
	}
}

static void
metadata_found_tags_cb (GObject *pipeline,
			GstElement *source,
			GstTagList *tags,
			gpointer data)
{
	gst_tag_list_foreach (tags,
			      (GstTagForeachFunc) metadata_found_tag_cb,
			      NULL);
}

static void
metadata_have_type_cb (GstElement *typefind,
		       guint probability,
		       GstCaps *caps,
		       gpointer data)
{
	gchar *s;

	printf("Have type...\n");

	s = gst_caps_to_string (caps);

	g_print ("mime-type: %s\n", s);

	g_free (s);
}

static GstElement *
create_thread (GstElement** feedmesrc)
{
	GstElement *thread = NULL;
	GstElement *typefind = NULL;
	GstElement *spider = NULL;
	GstElement *fakesink = NULL;
	GstCaps *filtercaps = NULL;

	/*
	 * feedmesrc ! typefind ! spider ! application/x-gst-tags ! fakesink
	 */
	thread = gst_thread_new("thread");

	g_signal_connect (thread, "found-tag",
			  G_CALLBACK (metadata_found_tags_cb), NULL);

	*feedmesrc = g_object_new(GST_TYPE_FEEDMESRC, NULL);
#if USE_PULL
	g_object_set(G_OBJECT(*feedmesrc), "signal-when-hungry", TRUE, NULL);
#endif


	gst_object_set_name(GST_OBJECT(*feedmesrc), "feedmesrc");
	if (*feedmesrc == NULL)
	{
		g_print ("Could not make feedmesrc");
		return NULL;
	}

	typefind = gst_element_factory_make ("typefind", "typefind");
	if (typefind == NULL)
	{
		g_print ("Could not make typefind");
		return NULL;
	}
	g_signal_connect (typefind, "have-type",
			  G_CALLBACK (metadata_have_type_cb), NULL);

	spider = gst_element_factory_make ("spider", "spider");
	if (spider == NULL)
	{
		g_print ("Could not make spider");
		return NULL;
	}

	fakesink = gst_element_factory_make ("fakesink", "fakesink");
	if (fakesink == NULL)
	{
		g_print ("Could not make fakesink");
		return NULL;
	}

	gst_bin_add_many (GST_BIN (thread),
			  *feedmesrc, typefind, spider, fakesink, NULL);
	gst_element_link_many (*feedmesrc, typefind, spider, NULL);

	/* filtercaps = gst_caps_new_simple ("audio/x-raw-int", NULL); */
	filtercaps = gst_caps_new_simple ("application/x-gst-tags", NULL);
	gst_element_link_filtered (spider, fakesink, filtercaps);
	gst_caps_free (filtercaps);

	return thread;
}

static gboolean push_chunk(GstFeedMeSrc *src, GstData *data) {
  printf ("Pushing a chunk...\n");
  gst_feedmesrc_push_data(src, data);
  if (GST_IS_EVENT(data)) {
    printf ("DONE!");
    return FALSE;
  } else {
    return TRUE;
  }
}

static void
metadata_load (GstElement *thread, GstFeedMeSrc *src, const char *uri)
{
	gst_element_set_state (thread, GST_STATE_PLAYING);
#if USE_PUSH
	while (push_chunk(src, get_next_data_chunk(uri)));
#endif
}

static GstData *
get_next_data_chunk (const char *uri) {
  static int size_of_data;
  static char *data = NULL;
  static int left_to_write;
  static gboolean data_read = FALSE;
  static GStaticMutex mutex = G_STATIC_MUTEX_INIT;

  int chunk_size = 100000;
  GstData *to_return;

  g_static_mutex_lock(&mutex);
  if (!data_read) {
    GnomeVFSResult result;
    result = gnome_vfs_read_entire_file (uri, &size_of_data, &data);
    data_read = TRUE;
    left_to_write = size_of_data;
  }

  if (left_to_write <= 0) {
    static gboolean eos_sent = FALSE;
    if (eos_sent) {
      exit(0);
    } else {
      eos_sent = TRUE;
    }
    to_return = GST_DATA(gst_event_new(GST_EVENT_EOS));
  } else {
    int amount_to_write;
    char *buffer_data;
    if (left_to_write < chunk_size) {
      amount_to_write = left_to_write;
    } else {
      amount_to_write = chunk_size;
    }

    buffer_data = malloc(sizeof(char) * amount_to_write);
    memcpy(buffer_data, data + (size_of_data - left_to_write), amount_to_write);


    to_return = GST_DATA(gst_buffer_new());
    gst_buffer_set_data(GST_BUFFER(to_return), buffer_data, amount_to_write);
    left_to_write = left_to_write - amount_to_write;
  }

  g_static_mutex_unlock (&mutex);
  return to_return;
}

static GstData *
feedmesrc_hungry (GstElement *element, const char *uri) {
  return get_next_data_chunk(uri);
}


int
main (int argc,
      char **argv)
{
	GstElement *feedmesrc;
	GstElement *pipeline;

	if (argc != 2)
	{
		g_print ("Usage: %s [audio file]\n", argv[0]);
		return 1;
	}

	gst_init (&argc, &argv);
	gnome_vfs_init ();

	pipeline = create_thread (&feedmesrc);
	if (pipeline == NULL)
	{
		g_error ("Could not create pipeline.\n");
		return 1;
	}

#if USE_PULL
	g_signal_connect_after(feedmesrc, "hungry", G_CALLBACK(feedmesrc_hungry), argv[1]);
#endif

	metadata_load (pipeline, GST_FEEDMESRC(feedmesrc), argv[1]);

	gtk_main();
}
