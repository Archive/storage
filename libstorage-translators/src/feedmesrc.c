/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2000 Wim Taymans <wtay@chello.be>
 *                    2004 Seth Nickell <seth@gnome.org>
 *
 * gstfeedmesrc.c: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include "feedmesrc.h"
#include "translators-marshal.h"

GST_DEBUG_CATEGORY_STATIC (gst_feedmesrc_debug);
#define GST_CAT_DEFAULT gst_feedmesrc_debug

GstElementDetails gst_feedmesrc_details = GST_ELEMENT_DETAILS ("App Driver Source",
    "Source/Data",
    "Read from an application",
    "Seth Nickell <seth@gnome.org>");


/* Feedmesrc signals and args */
enum
{
  SIGNAL_HUNGRY,
  LAST_SIGNAL
};

enum
{
  ARG_0,
  ARG_SIGNAL_WHEN_HUNGRY
};

static guint gst_feedmesrc_signals[LAST_SIGNAL] = { 0 };

#define _do_init(bla) \
    GST_DEBUG_CATEGORY_INIT (gst_feedmesrc_debug, "feedmesrc", 0, "feedmesrc element");

GST_BOILERPLATE_FULL (GstFeedMeSrc, gst_feedmesrc, GstElement, GST_TYPE_ELEMENT,
    _do_init);

static void gst_feedmesrc_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_feedmesrc_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static GstElementStateReturn gst_feedmesrc_change_state (GstElement * element);

static GstData *gst_feedmesrc_get (GstPad * pad);

void 
gst_feedmesrc_push_data(GstFeedMeSrc *src, GstData *data) 
{
  g_assert(GST_IS_FEEDMESRC(src));
  g_assert(data != NULL);

  g_async_queue_ref(src->pushed_data);
  g_async_queue_push(src->pushed_data, data);
  g_async_queue_unref(src->pushed_data);  
}

static void
unref_pending_pushed_data(GAsyncQueue *pushed_data)
{
  gpointer value;
  GstData *data;
  while ((value = g_async_queue_try_pop(pushed_data))) {
    data = GST_DATA(value);
    gst_data_unref(data);
  }
}

static void
gst_feedmesrc_dispose (GObject *object) {
  GstFeedMeSrc *src = GST_FEEDMESRC(object);
 
  unref_pending_pushed_data(src->pushed_data);
  g_async_queue_unref(src->pushed_data);

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_feedmesrc_base_init (gpointer g_class)
{
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details (gstelement_class, &gst_feedmesrc_details);
}
static void
gst_feedmesrc_class_init (GstFeedMeSrcClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);

  gobject_class = G_OBJECT_CLASS (klass);

  g_object_class_install_property (gobject_class, ARG_SIGNAL_WHEN_HUNGRY,
				   g_param_spec_boolean ("signal-when-hungry", "Signal when hungry",
							 "Send a signal if the push queue is empty (pull data)", 
							 FALSE, G_PARAM_READWRITE));

  gst_feedmesrc_signals[SIGNAL_HUNGRY] =
      g_signal_new ("hungry", G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
		    G_STRUCT_OFFSET (GstFeedMeSrcClass, hungry), NULL, NULL,
		    translator_marshal_POINTER__VOID, G_TYPE_POINTER, 0);

  gobject_class->set_property = gst_feedmesrc_set_property;
  gobject_class->get_property = gst_feedmesrc_get_property;
  gobject_class->dispose = gst_feedmesrc_dispose;

  gstelement_class->change_state = gst_feedmesrc_change_state;
}

static void
gst_feedmesrc_init (GstFeedMeSrc * feedmesrc)
{
  feedmesrc->srcpad = gst_pad_new ("src", GST_PAD_SRC);

  gst_pad_set_get_function (feedmesrc->srcpad, gst_feedmesrc_get);

  gst_element_add_pad (GST_ELEMENT (feedmesrc), feedmesrc->srcpad);

  feedmesrc->seq = 0;
  feedmesrc->pushed_data = g_async_queue_new();
}

static GstElementStateReturn
gst_feedmesrc_change_state (GstElement * element)
{
  GstFeedMeSrc *src = GST_FEEDMESRC (element);

  switch (GST_STATE_TRANSITION (element)) {
    case GST_STATE_NULL_TO_READY:
      break;
    case GST_STATE_READY_TO_NULL:
      break;
    case GST_STATE_READY_TO_PAUSED:
      unref_pending_pushed_data(src->pushed_data);
      break;
    case GST_STATE_PAUSED_TO_READY:
      break;
    default:
      break;
  }

  if (GST_ELEMENT_CLASS (parent_class)->change_state)
    return GST_ELEMENT_CLASS (parent_class)->change_state (element);

  return GST_STATE_SUCCESS;
}


static void
gst_feedmesrc_set_property (GObject * object, guint prop_id, const GValue * value,
    GParamSpec * pspec)
{
  GstFeedMeSrc *src;

  /* it's not null if we got it, but it might not be ours */
  g_return_if_fail (GST_IS_FEEDMESRC (object));

  src = GST_FEEDMESRC (object);

  switch (prop_id) {
  case ARG_SIGNAL_WHEN_HUNGRY:
    src->signal_when_hungry = g_value_get_boolean (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    break;
  }
}

static void
gst_feedmesrc_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstFeedMeSrc *src;

  /* it's not null if we got it, but it might not be ours */
  g_return_if_fail (GST_IS_FEEDMESRC (object));

  src = GST_FEEDMESRC (object);

  switch (prop_id) {
  case ARG_SIGNAL_WHEN_HUNGRY:
    g_value_set_boolean (value, src->signal_when_hungry);
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

}

static GstData *
gst_feedmesrc_get (GstPad * pad)
{
  GstFeedMeSrc *src;
  GstData *data = NULL;
  src = GST_FEEDMESRC (gst_pad_get_parent (pad));
  /*
  if (retval == -1) {
    GST_ELEMENT_ERROR (src, RESOURCE, READ, (NULL),
        ("select on file descriptor: %s.", g_strerror (errno)));
    gst_element_set_eos (GST_ELEMENT (src));
    return GST_DATA (gst_event_new (GST_EVENT_EOS));
  */

  if (src->signal_when_hungry) {
    /* See if there's data w/o blocking */
    data = g_async_queue_try_pop(src->pushed_data);
    if (data == NULL) {
      /* Nothing on the queue, signal hunger! */
      g_signal_emit (G_OBJECT (src), gst_feedmesrc_signals[SIGNAL_HUNGRY], 0, &data);
    }
  }

  if (data == NULL) {
    /* We didn't get data in a try_pop, or by emitting a signal, wait for data */
    /*printf ("Blocking waiting for data...\n");*/
    data = g_async_queue_pop(src->pushed_data);
  }

  if (GST_IS_EVENT(data)) {
    GstEvent *event = GST_EVENT(data);
    if (event->type == GST_EVENT_EOS) {
      gst_element_set_eos(GST_ELEMENT(src));
    }
  }

  return GST_DATA (data);
}
