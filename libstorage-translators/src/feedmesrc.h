/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2000 Wim Taymans <wtay@chello.be>
 *                    2004 Seth Nickell <seth@gnome.org>
 *
 * gstfdsrc.h: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef __GST_FEEDMESRC_H__
#define __GST_FEEDMESRC_H__

#include <gst/gst.h>

G_BEGIN_DECLS


#define GST_TYPE_FEEDMESRC \
  (gst_feedmesrc_get_type())
#define GST_FEEDMESRC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_FEEDMESRC,GstFeedMeSrc))
#define GST_FEEDMESRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_FEEDMESRC,GstFeedMeSrcClass))
#define GST_IS_FEEDMESRC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_FEEDMESRC))
#define GST_IS_FEEDMESRC_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_FEEDMESRC))


typedef struct _GstFeedMeSrc GstFeedMeSrc;
typedef struct _GstFeedMeSrcClass GstFeedMeSrcClass;

struct _GstFeedMeSrc {
  GstElement element;
  /* pads */
  GstPad *srcpad;

  gboolean signal_when_hungry;

  GAsyncQueue *pushed_data;
  gulong seq;    
};

struct _GstFeedMeSrcClass {
  GstElementClass parent_class;

  /* signals */
  void (*timeout) (GstElement *element);
  GstData * (*hungry) (GstElement *element);
};

GType gst_feedmesrc_get_type(void);

void gst_feedmesrc_push_data(GstFeedMeSrc *src, GstData *data);

G_END_DECLS

#endif /* __GST_FEEDMESRC_H__ */
