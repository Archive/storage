#include "gstreamer_importer.hh"

#include <iostream>
using namespace std;

static void
metadata_found_tag_cb (const GstTagList *list,
		       const gchar *tag,
		       GStreamerImporter *importer)
{
  const GValue *val;
  val = gst_tag_list_get_value_index (list, tag, 0);
  importer->addTag(tag, val);
}

static void
metadata_found_tags_cb (GObject *pipeline,
			GstElement *source,
			GstTagList *tags,
			GStreamerImporter *importer)
{
  gst_tag_list_foreach (tags,
			(GstTagForeachFunc) metadata_found_tag_cb,
			importer);
}

static void
metadata_have_type_cb (GstElement *typefind,
		       guint probability,
		       GstCaps *caps,
		       gpointer data)
{
	gchar *s;

	s = gst_caps_to_string (caps);

	g_print ("mime-type: %s\n", s);

	g_free (s);
}

static void 
thread_eos_cb (GstElement *gstelement,
	       GStreamerImporter *importer) 
{
  importer->handleMainThreadDone();
}

GStreamerImporter::GStreamerImporter() {
  this->main_thread_done_mutex = g_mutex_new();
  this->main_thread_done_cond = g_cond_new();

  gst_init(NULL, NULL);

  this->main_thread = GST_THREAD (gst_thread_new("gst_importer_thread"));

  g_signal_connect (this->main_thread, "found-tag", G_CALLBACK (metadata_found_tags_cb), this);
  g_signal_connect (this->main_thread, "eos", G_CALLBACK (thread_eos_cb), this);

  this->feeder = GST_FEEDMESRC(g_object_new(GST_TYPE_FEEDMESRC, NULL));
  gst_object_set_name(GST_OBJECT(this->feeder), "feedmesrc");
  if (this->feeder == NULL) {
    g_print ("Could not make feedmesrc");
  }

  GstElement *typefind = gst_element_factory_make ("typefind", "typefind");
  if (typefind == NULL) {
    g_print ("Could not make typefind");
  }
	
  g_signal_connect (typefind, "have-type",
		    G_CALLBACK (metadata_have_type_cb), NULL);

  GstElement *spider = gst_element_factory_make ("spider", "spider");
  if (spider == NULL) {
    g_print ("Could not make spider");
  }

  GstElement *fakesink = gst_element_factory_make ("fakesink", "fakesink");
  if (fakesink == NULL) {
    g_print ("Could not make fakesink");
  }

  gst_bin_add_many (GST_BIN (this->main_thread), GST_ELEMENT(this->feeder), typefind, spider, fakesink, NULL);
  gst_element_link_many (GST_ELEMENT(this->feeder), typefind, spider, NULL);

  GstCaps *filtercaps = gst_caps_new_simple ("application/x-gst-tags", NULL);
  gst_element_link_filtered (spider, fakesink, filtercaps);
  gst_caps_free (filtercaps);

}

GStreamerImporter::~GStreamerImporter() { 
  g_mutex_free(this->main_thread_done_mutex);
  g_cond_free(this->main_thread_done_cond);

  gst_object_unref(GST_OBJECT(this->main_thread));
  gst_object_unref(GST_OBJECT(this->feeder));

  // FIXME: dispose of the pipeline?
}

GnomeVFSResult 
GStreamerImporter::startImport (const char *fileName, 
				const char *mimeType, 
				bool toplevelNode) {
  cout << "GStreamerImporter::startImport" << endl;
  BinaryImporter::startImport(fileName, mimeType, toplevelNode);

  this->main_thread_done = false;
  gst_element_set_state (GST_ELEMENT(this->main_thread), GST_STATE_PLAYING);

  return GNOME_VFS_OK;
}

GnomeVFSResult 
GStreamerImporter::importChunk (const char *buffer, 
				gsize bufferSize) {
  cout << "GStreamerImporter::importChunk" << endl;
  BinaryImporter::importChunk(buffer, bufferSize);

  char *buffer_data = (char *)malloc(sizeof(char) * bufferSize);
  memcpy(buffer_data, buffer, bufferSize);

  GstBuffer *gst_buffer = gst_buffer_new();
  gst_buffer_set_data(gst_buffer, (guint8*)buffer_data, bufferSize);

  gst_feedmesrc_push_data(this->feeder, GST_DATA(gst_buffer));

  return GNOME_VFS_OK;
}

GnomeVFSResult 
GStreamerImporter::finishImport () {
  cout << "GStreamerImporter::finishImport ()" << endl;
  GstEvent *event = gst_event_new(GST_EVENT_EOS);
  gst_feedmesrc_push_data(this->feeder, GST_DATA(event));

  /* Wait for the GStreamer thread to complete */
  g_mutex_lock(this->main_thread_done_mutex);
  if (!this->main_thread_done) {
    cout << "Waiting on the thread" << endl;
    g_cond_wait(this->main_thread_done_cond, this->main_thread_done_mutex);
  } else {
    cout << "The thread is already done" << endl;
  }
  g_mutex_unlock(this->main_thread_done_mutex);

  cout << "Say, I heard the thread was done" << endl;

  return BinaryImporter::finishImport();
}

void
GStreamerImporter::handleMainThreadDone () {
  cout << "The thread is done..." << endl;
  g_mutex_lock(this->main_thread_done_mutex);
  this->main_thread_done = true;
  g_cond_broadcast(this->main_thread_done_cond);
  g_mutex_unlock(this->main_thread_done_mutex);
}

void 
GStreamerImporter::addTag (const char *tag, const GValue *val) {
  string key_name = "storage:" + string(tag);
  this->set(key_name.c_str(), val);
}
