#ifndef gstreamer_importer_HH
#define gstreamer_importer_HH

#include "binary_importer.hh"

#ifdef __cplusplus
extern "C" {
#endif
#include <gst/gst.h>
#include <gst/gsttag.h>
#include "feedmesrc.h"
#ifdef __cplusplus
}
#endif

#include <map>
#include <string>

class GStreamerImporter: public BinaryImporter {

public:
  GStreamerImporter();
  virtual ~GStreamerImporter();

  virtual GnomeVFSResult startImport (const char *fileName, const char *mimeType, bool toplevelNode=true);
  virtual GnomeVFSResult importChunk (const char *buffer, gsize bufferSize);
  virtual GnomeVFSResult finishImport ();

  void addTag (const char *tag, const GValue *value);
  void handleMainThreadDone ();

protected:
  GstThread *main_thread;
  GstFeedMeSrc *feeder;

private:
  bool main_thread_done;
  GMutex *main_thread_done_mutex;
  GCond *main_thread_done_cond;
};

#endif
