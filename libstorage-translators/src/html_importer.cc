#include "html_importer.hh"

#include <storage-store.h>

HTMLImporter::HTMLImporter () : Importer::Importer ()
{
}

GnomeVFSResult
HTMLImporter::startImport (const char *host, const char *username, 
			   const char *password, bool toplevelNode)
{
	docText = g_string_new ("");
	parserCtxt = NULL;

	return GNOME_VFS_OK;
}

GnomeVFSResult
HTMLImporter::importChunk (const char *buffer, gsize bufferSize)
{
	if (parserCtxt == NULL) {
		parserCtxt = htmlCreatePushParserCtxt
			(NULL, NULL, buffer, bufferSize,
			 NULL, XML_CHAR_ENCODING_NONE);
	} else {
		htmlParseChunk (parserCtxt, buffer, bufferSize, 0);
	}

	return GNOME_VFS_OK;
}

void
HTMLImporter::ParseHead (xmlNodePtr node)
{
	while (node != NULL) {
		if (!xmlStrcmp (node->name, (xmlChar *)"title")) {
			xmlChar *title;

			title = xmlNodeGetContent (node);
			g_print ("Document title:%s\n", title);
			xmlFree (title);
		}

		node = node->next;
	}
}

void
HTMLImporter::ParseBody (xmlNodePtr node)
{
	while (node != NULL) {
		if (xmlNodeIsText (node)) {
			g_string_append (docText, (char *)node->content);
		}

		ParseBody (node->children);

		node = node->next;
	}
}

void
HTMLImporter::ParseDocument ()
{
	xmlDocPtr doc;
	xmlNodePtr node;

	doc = parserCtxt->myDoc;
	node = xmlDocGetRootElement(doc);
	node = node->children;

	while (node != NULL) {
		if (!xmlStrcmp (node->name, (xmlChar *)"head")) {
			ParseHead (node->children);
		} else if (!xmlStrcmp (node->name, (xmlChar *)"body")) {
			ParseBody (node->children);
		}

		node = node->next;
	}
}

GnomeVFSResult
HTMLImporter::finishImport ()
{
	ParseDocument ();
	xmlFreeParserCtxt (parserCtxt);

	g_print (docText->str);

	g_string_free (docText, TRUE);

	return GNOME_VFS_OK;
}

HTMLImporter::~HTMLImporter ()
{
}
