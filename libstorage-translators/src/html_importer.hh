#ifndef _HTML_IMPORTER_H
#define _HTML_IMPORTER_H

#include "importer.hh"

#include <libgnomevfs/gnome-vfs-result.h>
#include <libxml/HTMLparser.h>

class HTMLImporter: public Importer {
  
public:
	HTMLImporter ();
	virtual ~HTMLImporter();  

	GnomeVFSResult startImport  (const char *host,
				     const char *username,
				     const char *password,
				     bool toplevelNode=true);

	GnomeVFSResult importChunk  (const char *buffer,
				     gsize bufferSize);

	GnomeVFSResult finishImport ();

private:
	htmlParserCtxtPtr parserCtxt;
	GString *docText;

	void ParseDocument ();
	void ParseHead (xmlNodePtr node);
	void ParseBody (xmlNodePtr node);
};

#endif
