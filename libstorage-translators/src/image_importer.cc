#include "image_importer.hh"
#include <libstorage/storage-debug.h>
#include <iostream>

#define THUMBSIZE	64
#define MAX_ICON_SIZE	256

using namespace std;

const char* ImageImporter::A_COLORDEPTH = "pixeldepth";
const char* ImageImporter::A_COLORTYPE = "colortype";
const char* ImageImporter::A_ICONTYPE  = "is_icon";

static void        
size_prepared_callback (GdkPixbufLoader *loader,
			gint width,
			gint height,
			gpointer user_data)
{
  ImageImporter *importer = (ImageImporter *)user_data;

  importer->setDimensions(width, height);
}



ImageImporter::ImageImporter (bool createThumbnail) : BinaryImporter::BinaryImporter(), 
						      createThumbnail(createThumbnail), 
						      pixbufDimensionsLoaded(false)
{ 
}

GnomeVFSResult
ImageImporter::startImport (const char *fileName, const char *mimeType, bool toplevelNode)
{
	LOG ("ImageImporter::startImport()")
	BinaryImporter::startImport (fileName, mimeType, toplevelNode);

	this->pixbufLoader = gdk_pixbuf_loader_new ();

	g_signal_connect(G_OBJECT(this->pixbufLoader), "size-prepared", G_CALLBACK(size_prepared_callback), (gpointer)this);

	return GNOME_VFS_OK;
}

GnomeVFSResult ImageImporter::importChunk (const char *buffer, gsize bufferSize) {
  cout << "ImageImporter::importChunk()" << endl;
  BinaryImporter::importChunk(buffer, bufferSize);


  if (this->createThumbnail || (!this->pixbufDimensionsLoaded)) {
    printf ("About to do the write\n");
    //FIXME: use GError handling here
    bool success = gdk_pixbuf_loader_write (this->pixbufLoader, (const guchar *)buffer, bufferSize, NULL);
    if (!success) {
      return GNOME_VFS_ERROR_GENERIC;
    }
  } else {
    printf ("Skipping write\n");
  }

  printf ("ImageImporter::importChunk() returning\n");



  return GNOME_VFS_OK;
}

GnomeVFSResult ImageImporter::finishImport () {
  cout << "ImageImporter::finishImport()" << endl;

  if (this->createThumbnail) {
    cout << "\tcreating the thumbnail pixbuf..." << endl;
    GdkPixbuf *pixbuf;
    gboolean success;

    pixbuf = gdk_pixbuf_loader_get_pixbuf (this->pixbufLoader);
    g_object_ref(G_OBJECT(pixbuf));

    //FIXME: use GError handling here
    success = gdk_pixbuf_loader_close (this->pixbufLoader, NULL);

    char *thumbnail_data;
    gsize thumbnail_data_size;

    success = gdk_pixbuf_save_to_buffer (pixbuf, &thumbnail_data, &thumbnail_data_size,
					 "png", NULL, NULL);

    StorageItem *thumbnail_item;

    cout << "\timporting thumbnail..." << endl;
    ImageImporter thumbnailImporter(false);
    thumbnailImporter.startImport(NULL, "image/png", false);
    thumbnailImporter.importChunk(thumbnail_data, thumbnail_data_size);
    thumbnailImporter.finishImport();
    cout << "\tdone!" << endl;
    
    thumbnail_item = thumbnailImporter.getToplevelItem();

    setThumbnail(thumbnail_item);

  } else {
    gdk_pixbuf_loader_close (this->pixbufLoader, NULL);
  }

  return BinaryImporter::finishImport();
}

ImageImporter::~ImageImporter() { }

void ImageImporter::setDimensions(int width, int height) {
  set(A_WIDTH, itos(width));
  set(A_HEIGHT, itos(height));

  if (this->createThumbnail) {
    if (height >= THUMBSIZE || width >= THUMBSIZE) {
      /* Since we are primarily loading to generate a thumbnail, set
	 the scaling for our desired thumbnail size */
      
      double scale;
      if (width > height) {
	scale = ((double)THUMBSIZE) / ((double)width);
      } else {
	scale = ((double)THUMBSIZE) / ((double)height);
      }
    
      int target_width = (int)floor(width * scale);
      int target_height = (int)floor(height * scale);
      
      gdk_pixbuf_loader_set_size (this->pixbufLoader, target_width, target_height);
    }
  }

  this->pixbufDimensionsLoaded = true;
}
  
/*
void ImageImporter::import(const char *filenameURI, bool top)
{
  BinaryImporter::import(filenameURI, top);
  string thumbnail = IMAGE_THUMBNAIL;

  GError **error = NULL;
  GdkPixbuf* buf = gdk_pixbuf_new_from_file(filename.c_str(), error);
  int width = gdk_pixbuf_get_width(buf);
  int height = gdk_pixbuf_get_height(buf);
  bool isSmall = (width < MAX_ICON_SIZE && height < MAX_ICON_SIZE);
  bool isIcon = isSmall && (width == height);
  set(A_WIDTH, itos(width));
  set(A_HEIGHT, itos(height));
  set(A_COLORDEPTH, itos(gdk_pixbuf_get_bits_per_sample(buf)));
  set(A_ICONTYPE, isIcon? "T":"F");
  
  if (isSmall) {
      g_object_unref(buf);
      return;
  }

  int tHeight = (int)((double)THUMBSIZE * gdk_pixbuf_get_height(buf) / gdk_pixbuf_get_width(buf));
  GdkPixbuf* thumb = gdk_pixbuf_scale_simple(buf, THUMBSIZE, tHeight, GDK_INTERP_BILINEAR); 

  if (gdk_pixbuf_save(thumb, thumbnail.c_str(), "png", error)) {
      setThumbnail(thumbnail, true);
  }
  g_object_unref(buf);
  g_object_unref(thumb);
}
*/


