#ifndef image_importer_HH
#define image_importer_HH

#include "binary_importer.hh"
#include <gdk-pixbuf/gdk-pixbuf.h>


class ImageImporter: public BinaryImporter {

public:
  ImageImporter(bool createThumbnail=true);
  virtual ~ImageImporter();

  virtual GnomeVFSResult startImport (const char *fileName, const char *mimeType, bool toplevelNode=true);
  virtual GnomeVFSResult importChunk (const char *buffer, gsize bufferSize);
  virtual GnomeVFSResult finishImport ();

  virtual void setDimensions(int width, int height);

protected:
  static const char* A_COLORTYPE;
  static const char* A_COLORDEPTH;
  static const char* A_ICONTYPE;

  bool createThumbnail;

  GdkPixbufLoader *pixbufLoader;

private:
  bool pixbufDimensionsLoaded;

};

#endif
