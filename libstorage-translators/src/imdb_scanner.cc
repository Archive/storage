#include "imdb_scanner.hh"

#define QUERY_CMD "./imdbpost -f "
#define TITLE_CMD "./imdbpost -t "
#define QUERY_OUT "/tmp/imdb_query"
#define TITLE_OUT "/tmp/imdb_title"

#define END_COMMENT "END OF SUCKY HARD CODED LINK BLOCK"
#define SEARCH_PAGE_ICON "IMDb name and title search"
#define TITLE_BEGIN "Titles (Approx Matches)"

#define ATTRIB_LIMIT 1234
#define BUFFER_SIZE 65536

using namespace std;

typedef string::size_type size_type;

IMDbScanner::IMDbScanner() {}
IMDbScanner::~IMDbScanner() {}


string IMDbScanner::execute(string& command, const char* tmp_path)
{
  string output;
  char buffer[BUFFER_SIZE] = "";

  system(command.c_str());
  ifstream temp;
  temp.open(tmp_path, ios::in);

  if (!temp.is_open()) {
    cout << ">>> error reading " << tmp_path << endl;
    exit(1);
  }

  temp.read(buffer, BUFFER_SIZE);
  temp.close();
  output = buffer;

  /*string delcmd = "rm ";
  delcmd += tmp_path;
  system(delcmd.c_str()); */
  
  return output;
}


vector<pair<string, string> > IMDbScanner::scan(const char *movieTitle)
{
  title = movieTitle;
  size_type index;
  string command;

  do {	// replace any spaces with +
    index = title.rfind(' ');
    if (index != string::npos)
    	title[index] = '+';
  } while (index > string::npos);

  command = QUERY_CMD;
  command += QUERY_OUT;
  command += " q=" + title;
  string searchResults = execute(command, QUERY_OUT);

  if (searchResults.find(SEARCH_PAGE_ICON) != string::npos) {
      // this is layout-specific. if IMDb changes...
      index = searchResults.find("/Title?", index) + 7;
      string titleID = searchResults.substr(index, IMDbTitleIDLength);
      // --------------------
      command = TITLE_CMD;
      command += TITLE_OUT;
      command += " " + titleID;
      info = execute(command, TITLE_OUT);
  } else { // they gave us a title page directly.... i hope...
      info = searchResults;
  }

  // make headers into a list of field delimiters on the page.
  index = 0;
  while (index != string::npos) {
    index = info.find("blackcatheader",index+1);
    if (index != string::npos)
      headers.push_back(index);
  }
  index = 0;
  while (index != string::npos) {
    index = info.find("class=\"ch\"", index+1);
    if (index != string::npos)
      headers.push_back(index);
  }
  sort(headers.begin(), headers.end());
  
  // title stuff probably won't change...
  size_type titleIndex = info.find("<title>",0) + 7;
  size_type yearIndex = info.find("</title>",0);
  yearIndex = info.rfind("(", yearIndex) + 1;
  title = info.substr(titleIndex, yearIndex - titleIndex - 2); 
  string year = info.substr(yearIndex, 4);
  pair<string,string> titleField("title", title);
  pair<string,string> yearField("year", year);
  attributes.push_back(titleField);
  attributes.push_back(yearField); 
  // ---------------------

  getURLs("director", "Directed by");
  getURLs("writer", "Writing credits");
  getURLs("genre", "Genre");
  getURLs("actor", "Cast overview, first billed only");
  getURLs("actor", "Complete credited cast");
  return attributes;
}
  

  				
// well, the good stuff is inside 
void IMDbScanner::getURLs(const char *attrib, const char *prompt)
{
  //cout << ">>> " << attrib << " (\"" << prompt << "\")" << endl;
  string values;
  string token;
  size_type start = info.find(prompt, 0);
  size_type stop = info.size();
  size_type end = start;

  if (start == string::npos) {
      cout << ">>> '" << prompt << "' not in file." << endl;
      return;
  }
  
  for (stop = 0; headers[stop] < start; stop++);
  stop = headers[stop];

  while (start < stop) {
    start = info.find("<a ", end) + 1;
    start = info.find(">", start) + 1;
    end = info.find("</a>", start);

    token = info.substr(start, end-start);
    if (token == "(more)" || token[0] == '<') break;
    if (token == "WGA") continue;
    if (token.size() + values.size() + 2 > ATTRIB_LIMIT) continue;
    if (values != "") values += ", ";
    values += token;
  }
  pair<string,string> attribValuePair(attrib, values);
  attributes.push_back(attribValuePair);
}


#if 0

int main(int argc, char** argv)
{
  if (argc < 2) {
    cout << "usage: " << argv[0] << " <movie title>" << endl;
    return 0;
  }
  IMDbScanner s;
  
  string title = argv[1];
  for (int i = 2; i < argc; i++) {
    title += " ";
    title += argv[i];
  }

  vector<pair<string,string> > info = s.scan(title.c_str());
  vector<pair<string,string> >::iterator iter;

  for (iter = info.begin(); iter != info.end(); ++iter) {
    cout << iter->first << "    \t= " << iter->second << endl;
  }

}

#endif
