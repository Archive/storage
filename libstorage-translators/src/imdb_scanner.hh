#ifndef _imdb_scanner_HH
#define _imdb_scanner_HH

#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <iostream>
#include <fstream>

class IMDbScanner {

 private:
  static const std::string::size_type IMDbTitleIDLength = 7;
  std::string title;
  std::string info;
  std::vector<std::pair<std::string, std::string> > attributes;

  std::vector<std::string::size_type> headers;
  std::string execute(std::string& command, const char* temp_file);
  void getURLs(const char*, const char*);
  
 public:  
  IMDbScanner();
  ~IMDbScanner();
  std::vector<std::pair<std::string, std::string> > scan(const char *movieTitle);
  

};

#endif
