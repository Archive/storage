#include <WWWLib.h>
#include <WWWInit.h>
#include <iostream>
#include <fstream>

#define FIND_URI "http://us.imdb.com/find"
#define QUERY_URI "http://us.imdb.com/Tsearch"
#define TITLE_URI "http://us.imdb.com/Title?"
#define APPNAME "Mozilla"
#define APPVERSION "5.0 (X11; U; Linux i686) Gecko/20030314 Galeon/1.3.4"

using namespace std;

static HTChunk *result = NULL;
static char *outfile = NULL;

int finish(HTRequest *request, HTResponse *response, void *param, int status) 
{
    if (result) { // status == HT_LOADED &&  
        ofstream file;
	file.open(outfile);
        file << HTChunk_data(result);
	file.close();
	HTChunk_delete(result);
    }
    else  cout << ">>> request FAILED." << endl;
    HTRequest_delete(request);
    HTEventList_stopLoop();
    HTProfile_delete();
    return 0;
}



int main (int argc, char ** argv)
{
    if (argc < 4) {
        cout << "usage:\timdbpost -[f|q|t] <output> <search terms>" << endl;
        cout << "\timdbpost <uri> <output> <search terms>" << endl;
        return 0;
    }
    
    const char *uri;
    outfile = argv[2];
    bool post = false;
    HTAssocList *formfields = HTAssocList_new();

    if (argv[1][1] == 't') {
      string u = TITLE_URI;
      u += argv[3];
      uri = u.c_str();
    }
    else if (argv[1][1] == 'f') {
      post = true;
      uri = FIND_URI;
    }
    else if (argv[1][1] == 'q') {
      post = true;
      uri = QUERY_URI;
    }
    else if (argv[1][0] == '-') {
       return 0;
    }
    else uri = argv[1];

    for (int i=3; i<argc; i++) {
          char *s = argv[i];
          HTParseFormInput(formfields, s);
    }
    
    cout << ">>> " << uri << endl;

    HTProfile_newNoCacheClient("IMDbSearch", "1.0");
    //HTSetTraceMessageMask("soghl"); // Get trace messages
    HTNet_addAfter(finish, NULL, NULL, HT_ALL, HT_FILTER_LAST);
    HTLib_setAppName(APPNAME);
    HTLib_setAppName(APPVERSION);
    HTHost_setEventTimeout(15000);

    HTRequest *request = HTRequest_new();
    HTAnchor *anchor = HTAnchor_findAddress(uri);
    HTRequest_setOutputFormat(request, WWW_SOURCE); // default output = "asis"

    if (post)    
	result = HTPostFormAnchorToChunk(formfields, anchor, request);
    else
        result = HTLoadAnchorToChunk(anchor, request);

    HTAssocList_delete(formfields);
    HTEventList_loop(request);
    return 0;
}

