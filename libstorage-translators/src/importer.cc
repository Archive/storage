#include "importer.hh"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>

#define BUFFER_SIZE	(16*1024)
#define DELIM		":,"

using namespace std;

const char* Importer::USER_CREATE = "creator";
const char* Importer::USER_OWN	  = "owner";
const char* Importer::USER_UID	  = STORAGE_FILE_UID;
const char* Importer::USER_GID	  = STORAGE_FILE_GID;
const char* Importer::MIME_TYPE   = STORAGE_FILE_MIME;
const char* Importer::FILE_SIZE   = STORAGE_FILE_SIZE;
const char* Importer::FILE_NAME   = STORAGE_FILE_NAME;
const char* Importer::FILE_TEXT   = STORAGE_FILE_TEXT;

const char* Importer::DATE_CREATE = STORAGE_FILE_CREATION;
const char* Importer::DATE_MODIFY = STORAGE_FILE_MODIFICATION;

const char* Importer::A_THUMBNAIL = STORAGE_FILE_THUMBNAIL;
const char* Importer::A_PATH_INFO = "directories";

const char* Importer::A_ARTIST	= "artist";
const char* Importer::A_ALBUM	= "album";
const char* Importer::A_TITLE	= "title";
const char* Importer::A_YEAR 	= "year";
const char* Importer::A_AUTHOR	= "author";

const char* Importer::A_HEIGHT	= STORAGE_FILE_HEIGHT;
const char* Importer::A_WIDTH	= STORAGE_FILE_WIDTH;
const char* Importer::A_BITRATE	= STORAGE_FILE_BITRATE;

const char* Importer::A_TIME_LENGTH = STORAGE_FILE_DURATION;
const char* Importer::A_PAGES = STORAGE_FILE_PAGES;

GSList *Importer::getQueryResults (const char *soup, 
				   const char *attr, const char *val)
{
	return storage_store_get_items_from_ids(defaultStore,
						storage_store_get_matches (defaultStore, soup, attr, val));
}

Importer::Importer() {
  this->import_in_progress = false;

  defaultStore = storage_get_default_store ();
  toplevel_item = NULL;

  gnome_vfs_init();
}

Importer::~Importer () { 
  if (this->import_in_progress) {
    fprintf (stderr, "WARNING: deleting Importer object before calling Importer::finishImport()\n");
    this->finishImport();
  }

  if (toplevel_item)
  {
    g_object_unref (toplevel_item);
  }
}

string Importer::itos(unsigned int i)
{
  stringstream s;
  s << i;
  return s.str();
}

inline void Importer::set(const char *attrib, const char *val)
{ 
  if (attrib && val) {
    storage_item_set_attribute(toplevel_item, attrib, val);
  }
  cout << storage_item_get_id(toplevel_item) << ": " << attrib << "\t= " << val << endl;
}

inline void Importer::set(const char *attrib, const string val)
{
  set(attrib, val.c_str());
}

inline void Importer::set(const char *attrib, const GValue *val) {
  cout << "Settings " << attrib << endl;
  if (attrib && val) {
    storage_item_set_attribute_gvalue(toplevel_item, attrib, val);
  }
  cout << storage_item_get_id(toplevel_item) << ": " << attrib << "\t= GValue" << endl;
}

inline void Importer::set(const AttrVal p)
{
  set(p.first.c_str(), p.second.c_str());
}

void Importer::set(const vector<AttrVal>& info)
{
  for (vector<AttrVal>::const_iterator i = info.begin(); i != info.end(); ++i)
  	set(*i);
}

inline bool Importer::sanityCheck(GnomeVFSResult result, const char* name)
{
  if(result != GNOME_VFS_OK) {
    cerr << "GnomeVFS got angry at " << name << endl;
    cerr << "Error: " << gnome_vfs_result_to_string(result) << endl;
    return false;
  }
  return true;
}


void Importer::setThumbnail(StorageItem *thumb)
{
  storage_item_insert_child(this->toplevel_item, thumb, 0);
  set(A_THUMBNAIL, storage_item_get_id(thumb));
}

void Importer::setToplevelItem (StorageItem *item)
{
	if (toplevel_item)
	{
		g_object_unref (toplevel_item);
		toplevel_item = NULL;
	}

	toplevel_item = STORAGE_ITEM (g_object_ref (item));
}

GnomeVFSResult Importer::startImport (const char *fileName, const char *mimeType, bool toplevelNode)
{
  this->import_in_progress = true;

  set(FILE_NAME, fileName);
  set(MIME_TYPE, mimeType);
  set(STORAGE_FILE_TOP, toplevelNode?"T":"F");
  return GNOME_VFS_OK;
}

GnomeVFSResult Importer::finishImport () {
  this->import_in_progress = false;
  
  return GNOME_VFS_OK;
}
