#ifndef _gargamel_importer_HH_
#define _gargamel_importer_HH_

#include <libstorage/storage-store.h>
#include <libstorage/storage-item.h>
#include <libgnomevfs/gnome-vfs.h>
#include <vector>
#include <string>
#include <utility>

#define CLI_SPACE	"\\ "
#define URI_SPACE	"%20"

typedef std::pair<std::string,std::string> AttrVal;

class Importer {

 public:
  virtual ~Importer(); 

  virtual GnomeVFSResult startImport (const char *fileName, const char *mimeType, bool toplevelNode=true);
  virtual GnomeVFSResult importChunk (const char *buffer, gsize bufferSize) = 0;
  virtual GnomeVFSResult finishImport ();

  inline StorageItem *  getToplevelItem ()                   { return toplevel_item; }

 protected:
  // Variables

  StorageStore *defaultStore;

  bool import_in_progress;
  
  // Methods

  Importer();

  void    setToplevelItem  (StorageItem *item);
  void    setThumbnail     (StorageItem *thumb);
  static  std::string itos (unsigned int i);
  GSList *getQueryResults  (const char *soup, const char *attr, const char *val);

  virtual void set(const char* attr, const char* val);
  virtual void set(const char* attr, const std::string val);
  virtual void set(const AttrVal p);
  virtual void set(const std::vector<AttrVal>& list);
  virtual void set(const char *attrib, const GValue *val);

  virtual bool sanityCheck(GnomeVFSResult result, const char* name); 

  static const char* USER_CREATE;
  static const char* USER_OWN;
  static const char* USER_UID;
  static const char* USER_GID;
  static const char* FILE_NAME;
  static const char* FILE_SIZE;
  static const char* FILE_TEXT;
  static const char* DATE_CREATE;
  static const char* DATE_MODIFY;
  static const char* MIME_TYPE;
  static const char* A_PATH_INFO;
  static const char* A_ARTIST;
  static const char* A_ALBUM;
  static const char* A_TITLE;
  static const char* A_YEAR;
  static const char* A_AUTHOR;
  static const char* A_HEIGHT;
  static const char* A_WIDTH;
  static const char* A_BITRATE;
  static const char* A_TIME_LENGTH;
  static const char* A_THUMBNAIL;
  static const char* A_PAGES;

private:
  StorageItem *toplevel_item;

};

#endif
