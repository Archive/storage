#include "importer_caller.hh"
using namespace std;

/*
 * Calls the correct importer for a given mime_type.
 * If parent is NULL (or not specified), the Importer assumes
 * it is the top_node.
 *
 * Returns the top level node of the imported file if successful, NULL if not
 */
Importer *callImporter(const char *abs_filename, const char *mimeType, bool fullImport, Importer *parent){
  gnome_vfs_init();
  char *path;
  Importer *importer = NULL;

  path = g_strdup(abs_filename);
  if(!g_path_is_absolute((gchar *)path)){
    cout << "Error: Pathname to callImporter is not absolute\n";
    return(importer);
  }

  char  *escaped_uri = gnome_vfs_get_uri_from_local_path (path);
  cout << "Escaped URI: " << escaped_uri << endl;
  g_free(path);



  if (strcasecmp(mimeType, "application/x-mbox") == 0) {
    cout << "New MboxImporter\n";
    importer = new MboxImporter;
  } else if (strcasecmp(mimeType, "audio/mpeg") == 0) {
    cout << "New mp3Importer\n";
    importer = new MPEGAudioImporter;
  } else if (strcasecmp(mimeType, "application/pdf") == 0) {
    importer = new PDFImporter;
    cout << "New PDFImporter\n";
  } else if (strcasecmp(mimeType, "text/xml") == 0) {
    importer = new XMLImporter;
  } else if (strcasecmp(mimeType, "text/html") == 0) {
    importer = new HTMLImporter;
  } else if (strcasecmp(mimeType, "video/mpeg") == 0) {
    importer = new MPEGVideoImporter;
    cerr << "Not handled, need to fix the header file... what defines mpeg.h ???" << endl;
  } else if (strncasecmp(mimeType, "text/", 5) == 0) {
    importer = new TextImporter;
  } else if (strncasecmp(mimeType, "image/", 6) == 0) {
    importer = new ImageImporter;
  } else if (strncasecmp(mimeType, "video/", 6) == 0) {
    importer = new VideoImporter;
  } else {
    cout << "New binary importer\n";
    importer = new BinaryImporter;
  }

  if(!fullImport && (strncasecmp(mimeType, "text/", 5) == 0)) {    
    importer->create(escaped_uri);
  } else {
    importer->import(escaped_uri);
  }

  g_free(escaped_uri);

  if(parent != NULL) {
    storage_item_insert_child(parent->getItem(), importer->getItem(), -1); //-1 appends the last child
    importer->set(STORAGE_FILE_TOP, false ? "T":"F"); //the importer probably set it as a top_level, but we're adding as a child
  }

  cout << "Successfully imported type " << mimeType << endl;
  return(importer);
}
