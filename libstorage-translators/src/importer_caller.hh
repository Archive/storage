#ifndef _gargamel_importer_caller_HH_
#define _gargamel_importer_caller_HH_

#include "binary_importer.hh"
#include "image_importer.hh"
#include "pdf_importer.hh"  
#include "xml_importer.hh"
#include "mpegaudio_importer.hh"
#include "text_importer.hh"
#include "html_importer.hh"   
#include "mpegvideo_importer.hh"
#include "video_importer.hh"

// Not yet handled...
#include "mbox_importer.hh"     
//#include "docb_importer.hh"    

#include <libgnomevfs/gnome-vfs.h>

Importer *callImporter(const char *abs_filename, const char *mimeType, bool fullImport = true, Importer *parent = NULL);

#endif
