#include "mbox_importer.hh"
#include "importer_caller.hh"
#include <iostream>

using namespace std;

const char *MboxImporter::A_REC_TO	= "to";
const char *MboxImporter::A_REC_CC	= "cc";
const char *MboxImporter::A_REC_BCC	= "bcc";
const char *MboxImporter::A_FROM	= "from";
const char *MboxImporter::A_SUBJECT     = "subject";
const char *MboxImporter::A_REPLY_TO    = "reply to";
const char *MboxImporter::A_DATE_SENT   = STORAGE_FILE_DATE;
const char *MboxImporter::A_ATTACHMENT  = "attachment";
const char *MboxImporter::A_BODY  = "message body";
const char *MboxImporter::MSG_MIME_TYPE = "message/rfc822";
const char *MboxImporter::MBOX_MIME_TYPE = "application/x-mbox";

#define PLAIN_TEXT_MIME "TEXT/PLAIN"

#define TEMP_FILE_LOCATION "/tmp/"


/*
 * The mbox importer is designed to have a top_level mbox node. We now don't want that,
 * so basically I'm doing everything as though we did, but not inserting children to parent,
 * setting the messages to top-level, too, and then deleting the mbox node.
 */


MboxImporter::MboxImporter() : Importer() { }
MboxImporter::~MboxImporter() { }

void MboxImporter::import(const char *filenameURI, bool top)
{
  Importer::import(filenameURI, STORAGE_ITEM_TYPE_NODE, top);
  set(MIME_TYPE, MBOX_MIME_TYPE);
  g_mime_init(0); 

  int msgCount = 0; 
  GMimeStream *mbox = g_mime_stream_fs_new(open(filename.c_str(), O_RDONLY));
  GMimeParser *parser = g_mime_parser_new_with_stream(mbox);
  g_mime_parser_set_scan_from(parser, true);

  while (!g_mime_parser_eos(parser)) { //this tells us when we are done reading the messages
    appendMessage(g_mime_parser_construct_message(parser));
    cout << ">>> finished message #" << ++msgCount << endl;
  }
  
  //delete the mbox node
  storage_item_remove(item);
  cout << ">>> =-=- mailbox finished" << endl;
 
}

void MboxImporter::appendMessage(GMimeMessage *msg)
{
  if (msg == NULL) return;
  cout << ">>> message loaded." << endl;
  
  InternetAddressList *to, *cc, *bcc;
  const char *sender, *subj, *replyto, *sent;
  to = g_mime_message_get_recipients(msg, GMIME_RECIPIENT_TYPE_TO);
  cc = g_mime_message_get_recipients(msg, GMIME_RECIPIENT_TYPE_CC);
  bcc = g_mime_message_get_recipients(msg, GMIME_RECIPIENT_TYPE_BCC);
  subj = g_mime_message_get_subject(msg);
  sender = g_mime_message_get_sender(msg);
  replyto = g_mime_message_get_reply_to(msg);

  time_t sent_time_t;
  int sent_gmt_offset;
  g_mime_message_get_date(msg, &sent_time_t, &sent_gmt_offset);
  sent_time_t += sent_gmt_offset*60*60/100; //adds the appropriate number of seconds on
  tm *sent_time_tm = gmtime(&sent_time_t);
  sent = asctime(sent_time_tm);
  //g_mime_message_get_date_string(msg)

  Importer messageNode;
  messageNode.create(STORAGE_ITEM_TYPE_NODE, true);
  //no mbox top_level node -- don't insert child
  //storage_item_insert_child(item, messageNode.getItem(), -1);

  if(to != NULL) {
    messageNode.set(A_REC_TO, internet_address_list_to_string(to, false));
    cout << A_REC_TO << ": " << internet_address_list_to_string(to, false) << endl;
  }
  if(cc != NULL) { 
    messageNode.set(A_REC_CC, internet_address_list_to_string(cc, false));
    cout << A_REC_CC << ": " << internet_address_list_to_string(cc, false) << endl;
  }
  if(bcc != NULL) {
    messageNode.set(A_REC_BCC, internet_address_list_to_string(bcc, false));
    cout << A_REC_BCC << ": " << internet_address_list_to_string(bcc, false) << endl;
  }
  if(subj != NULL) {  
    messageNode.set(A_SUBJECT, subj);
    cout << A_SUBJECT << ": " << subj << endl;
  }
  if(sender != NULL) {
    messageNode.set(A_FROM, sender);
    cout << A_FROM << ": " << sender << endl;
  }
  if(replyto != NULL) {
    messageNode.set(A_REPLY_TO, replyto);
    cout << A_REPLY_TO << ": " << replyto << endl;
  }
  if(sent != NULL) {
    messageNode.set(A_DATE_SENT, sent);
    cout << A_DATE_SENT << ": " << sent << endl;
  }  
  messageNode.set(MIME_TYPE, MSG_MIME_TYPE);
  cout << MIME_TYPE << ": " << MSG_MIME_TYPE << endl;

  //since we don't have an mbox top_level node, set the PATH attribute of the message to be the mbox filename
  if(storage_item_has_attribute(item, FILE_NAME)) {
    messageNode.set(A_PATH_INFO, storage_item_get_attributes_value(item, FILE_NAME));
    cout << A_PATH_INFO << ": " << storage_item_get_attributes_value(item, FILE_NAME) << endl;
  }

  //delete [] to;
  //delete [] cc;
  //delete [] bcc;
  //delete [] subj;
  //delete [] sender;
  //delete [] replyto;
  //delete [] sent;
  g_mime_message_foreach_part(msg, handleMimePart, (gpointer)&messageNode); 
}

void MboxImporter::handleMimePart(GMimeObject *obj, gpointer p)
{
  Importer *parentImporter = (Importer*) p;

  //A "part" that comes in to this section is either a GMIME_PART or a GMIME_MULTIPART... and it must be handled accordingly
  //We're going to attach the subnode DIRECTLY beneath the main email message node
  if(GMIME_IS_PART(obj)) {
    cout << ">>>Message part is part\n";

    Importer *partNode;
    //partNode.create(STORAGE_ITEM_TYPE_NODE, false);

    GMimePart *mimePart = GMIME_PART(obj);
 
    const char *mime_type, *filename;
    mime_type = g_mime_content_type_to_string(g_mime_object_get_content_type(obj));
    filename = g_mime_part_get_filename(mimePart);
    if(filename == NULL) {
      filename = strdup(A_BODY);
    }

    if(mime_type == NULL) {
      mime_type = "binary";
    }

    cout << "Calling the importer_caller for " << mime_type << endl;
    size_t content_len;
    const char * raw_content = g_mime_part_get_content(mimePart, &content_len); //NOT null-terminated!
    cout << "After got raw_content\n";
    if(raw_content != NULL) {
      cout << "Content length of " << content_len << endl;
      string tempFilename(TEMP_FILE_LOCATION);
      tempFilename += filename;
      ofstream file(tempFilename.c_str());
      for(size_t i=0; i < content_len; i++) {
	file << raw_content[i];
      }
      file.close();
      cout << "File " << tempFilename << " should have been created\n";
      partNode = callImporter(tempFilename.c_str(), mime_type, false, parentImporter);
      string removeFile = "rm -f \"" + tempFilename + "\"";
      system(removeFile.c_str());

      //these should already be set, but no hurt setting them again
      if(mime_type != NULL) {
	partNode->set(MIME_TYPE, mime_type);
	cout << MIME_TYPE << ": " << mime_type << endl;
      }
      partNode->set(FILE_NAME, filename);
      cout << FILE_NAME << ": " << filename << endl;
    } else {
      cout << "Raw content was NULL!!\n";
    }


    //have to insert to parent
    //storage_item_insert_child(parentImporter->getItem(), partNode.getItem(), -1); //-1 appends the last child

    //delete [] mime_type;
    //delete [] filename;

  }
  else if(GMIME_IS_MULTIPART(obj)) {
    //I'm not exactly sure what this means... it may just be a wrapper part for multiple sub-parts?
    cout << ">>>Message part is multipart\n";
  }
}


void MboxImporter::printMessage(StorageItem *item)
{
  //print out current attribute listing
  GSList *attrPairs = storage_item_get_attributes_pairs(item);
  while(attrPairs != NULL) {
    StorageAttrPair *pair = (StorageAttrPair *)(attrPairs->data);
    cout << pair->name << ": " << pair->value <<endl;
    attrPairs = g_slist_next(attrPairs);
  }

  cout << "===---===---===---===---===" << endl;
  GSList* kids = storage_item_get_children(item);
  GSList* kidsLeft = kids;
  while (kidsLeft) {
    printMessage((StorageItem *)kidsLeft->data);
    kidsLeft = g_slist_next(kidsLeft);
  }
  g_free(kids);
}


void MboxImporter::printMailbox(char *recordid_str)
{
  //StorageItem *item = (StorageItem *)storage_item_new_from_recordid(recordid_str);
  //if(item == NULL) return;
  //printMessage(item);   //recursively query children
}



#if 0
int main(int argc, char** argv)
{
  if (argc < 3) {
    cout << "usage:\t"	<< argv[0] << " -i  	<mailbox file>" << endl;
    cout << "\t"	<< argv[0] << " -[e|rm]  <record id>" << endl;
    return 0;
  }
  string option = argv[1];
  char *input = argv[2];
  char *path;

  if (option == "-i") {		// import
    if (input[0] == '/') {
      path = g_strdup(input);
    }
    else {
      char *current_dir = g_get_current_dir();
      path = g_build_filename(current_dir, input, NULL);
      g_free(current_dir);
    }
    char *escaped_uri = gnome_vfs_get_uri_from_local_path(path);
    g_free(path);
    
    MboxImporter importer;
    importer.import(escaped_uri);
    //g_free(escaped_uri);
  }
  else if (option == "-e") {	// extract
    MboxImporter importer;
    importer.printMailbox(input);
  }
  else if (option == "-rm") {	// remove
    gnome_vfs_init();
    //StorageItem* item = (StorageItem *)storage_item_new_from_recordid(input);
    //if (item != NULL)
    //storage_item_remove(item);
  }
  return 0;
}
#endif


