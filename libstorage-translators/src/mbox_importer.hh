#ifndef _mbox_importer_HH
#define _mbox_importer_HH

#include "binary_importer.hh"
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <gmime/gmime.h>

class MboxImporter: public Importer {
 protected:
  virtual void appendMessage(GMimeMessage *msg);
  virtual void printMessage(StorageItem *item);
  static  void handleMimePart(GMimeObject* obj, gpointer parent);
  
 public:
  MboxImporter();
  virtual ~MboxImporter();
  /*  virtual void import(const char *filenameURI, bool top = true);*/
  // A simple test method to print out to stdout what is stored in the
  // database for a mailbox. Intended only for debugging.
  virtual void printMailbox(char *recordid_str);

  static const char * A_REC_TO;
  static const char * A_REC_CC;
  static const char * A_REC_BCC;
  static const char * A_FROM;
  static const char * A_SUBJECT;
  static const char * A_REPLY_TO;
  static const char * A_DATE_SENT;
  static const char * A_ATTACHMENT;
  static const char * A_BODY; //the attribute value in the db that signifies the body of the text
  static const char * MSG_MIME_TYPE;	// the MIME type to use when searching for messages in DB
  static const char * MBOX_MIME_TYPE;	// the MIME type we use for the mbox node

};

#endif
