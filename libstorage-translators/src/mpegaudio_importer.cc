#include "mpegaudio_importer.hh"
#include <cstdlib>

// for manually querying DB
#include <libpq-fe.h>		//in /usr/include
#include <libpq/libpq-fs.h>
// ------------------------

#define ID3_TEMP_PICTURE	"/tmp/id3_thumbnail"
#define ID3_FRAME_IMAGE		"APIC"

using namespace std;

const char* thumbList[] = {"Folder.jpg", "AlbumArtSmall.jpg", "Cover.jpg", "cover.jpg"};
const int thumbListSize = 4;

const char* MPEGAudioImporter::A_TRACKNUM = "track";
const char* MPEGAudioImporter::A_GENRE =    "genre";
const char* MPEGAudioImporter::A_LYRICS = STORAGE_FILE_TEXT;

MPEGAudioImporter::MPEGAudioImporter() : BinaryImporter() {}
MPEGAudioImporter::~MPEGAudioImporter() { }




static bool exists(const string& filename) {
   struct stat s;
   return stat(filename.c_str(), &s) == 0; 
}

string MPEGAudioImporter::getFrame(const char *frameName, struct id3_tag *tag)
{
  struct id3_frame *frame = id3_tag_findframe(tag, frameName, 0);
  if (!frame) return "";
  id3_field *field = &frame->fields[1];
  string value;

  int numStrings = id3_field_getnstrings(field);
  for (int i = 0; i < numStrings; i++) {
      char *info = (char *) id3_ucs4_latin1duplicate(id3_field_getstrings(field, i));
      if (i > 0)
          value += ", ";
      value += info;
      free(info);
  }
  int index = atoi(value.c_str());
  if (frameName == ID3_FRAME_GENRE && isdigit(value[0]) && index < ID3_NR_OF_V1_GENRES) {
      value = ID3_v1_genre_description[index];
  }
  return value;
}


void MPEGAudioImporter::import(const char *filenameURI, bool top)
{
  BinaryImporter::import(filenameURI, top);
  string thumbnail;
   
  int fd = open(filename.c_str(), O_RDONLY);
  if (fd == -1) {
      cout << strerror(errno) << endl;
      return;
  }
  struct id3_file *id3file = id3_file_fdopen(fd, ID3_FILE_MODE_READONLY);
  struct id3_tag *tagMAD = id3_file_tag(id3file);
  struct id3_frame *imgFrame = id3_tag_findframe(tagMAD, ID3_FRAME_IMAGE, 0);

  string artist= getFrame(ID3_FRAME_ARTIST,tagMAD);
  string album = getFrame(ID3_FRAME_ALBUM, tagMAD);

  ofstream file(ID3_TEMP_PICTURE, ofstream::trunc | ofstream::binary);

  if (imgFrame) cout << "Image frame exists" << endl;

  if (imgFrame && file.is_open()) {
    cout << "This file has an embedded image" << endl;
      id3_length_t imgSize = 0;
      const id3_byte_t* data = id3_field_getbinarydata(&(imgFrame->fields[4]), &imgSize);
      file.write((const char*)data, (unsigned long)imgSize);
      thumbnail = ID3_TEMP_PICTURE;
  } else {
      for (int i = 0; i < thumbListSize; ++i) {
	cout << "Looking at " << path + thumbList[i] << " for the thumbnail" << endl;
          if (exists(path + thumbList[i])) {
              thumbnail = path + thumbList[i];
	      cout << "Using " << thumbnail << " for the thumbnail" << endl;
	      break;
	  }
      }
  }
  file.close();



#if 1
  if (thumbnail != ID3_TEMP_PICTURE) {
      // first generate list of ids
      GSList *results = getQueryResults("AttrSoup", A_ALBUM, album.c_str()); 

      for (GSList *i = results; i != NULL; i = g_slist_next(i)) {      	  
          StorageItem *hit = STORAGE_ITEM(i->data);
	  string hitMime = storage_item_get_attribute_value(hit, MIME_TYPE);
	  string hitArtist = storage_item_get_attribute_value(hit, A_ARTIST); 

	  if (strcasecmp(artist.c_str(), hitArtist.c_str()) != 0)
	       continue;

	  if (hitMime.find("image/") != string::npos) {
	      cout << ">>> thumbnailing storage:///" << storage_item_get_id(hit);
	      setThumbnail(hit);
	      thumbnail = "";	// so it doesn't import one later.
	      break;
	  }
      }
      g_slist_free(results);
  }
#endif

  ImageImporter *img = setThumbnail(thumbnail); // returns NULL on ""

  if (img) {
    img->set(A_ARTIST,  artist);
    img->set(A_ALBUM,   album);
  }

  set(A_TITLE,   getFrame(ID3_FRAME_TITLE, tagMAD));
  set(A_YEAR,    getFrame(ID3_FRAME_YEAR,  tagMAD));
  set(A_TRACKNUM,getFrame(ID3_FRAME_TRACK, tagMAD));
  set(A_GENRE,   getFrame(ID3_FRAME_GENRE, tagMAD));
  set(A_ARTIST,  artist);
  set(A_ALBUM,   album);
 
  id3_file_close(id3file);
  if (img) delete img;

  ID3_Tag t(filename.c_str());	// since libid3tag doesn't do mpeg header info
  const Mp3_Headerinfo *info = t.GetMp3HeaderInfo();
  if (info) {			// in case this is actually an ogg
    set(A_TIME_LENGTH, itos(info->time));
    if (info->vbr_bitrate > 0)
        set(A_BITRATE, itos(info->vbr_bitrate));
    else
        set(A_BITRATE, itos(info->bitrate));
  }

}
