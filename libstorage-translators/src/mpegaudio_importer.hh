#ifndef mpegaudio_importer_HH
#define mpegaudio_importer_HH

#include "binary_importer.hh"
#include "image_importer.hh"
#include <id3tag.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#ifdef HAVE_CONFIG_H
#undef HAVE_CONFIG_H
#include <id3/tag.h>
#include <id3/misc_support.h>
#endif


class MPEGAudioImporter: public BinaryImporter {

 private:
  std::string getFrame(const char *frameName, struct id3_tag *tag);
  
 public:
  MPEGAudioImporter();
  virtual ~MPEGAudioImporter();
  //virtual void import(const char *filenameURI, bool top = true);
  
  static const char* A_TRACKNUM;
  static const char* A_GENRE;
  static const char* A_LYRICS;

};

#endif
