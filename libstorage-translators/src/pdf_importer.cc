#include "pdf_importer.hh"
#include "text_importer.hh"

#define BUFFER_SIZE (16*1024)


using namespace std;

PDFImporter::PDFImporter() : BinaryImporter() {}
PDFImporter::~PDFImporter() {}

void PDFImporter::import(const char *filenameURI, bool top) {

  BinaryImporter::import(filenameURI, top);
  ripMetadata(); 
  ripText();
  grabThumbnail();
}


void PDFImporter::grabThumbnail() {
  //save image to "blah.pdf.info", will parse out later
  string pdfCommand = "pdftopbm -l 1 -r 12 \"" + filename + "\" \"" + filename + "\"";
  if (system(pdfCommand.c_str()) != 0) {
  	cout << "thumbnailing error" << endl;
	return;
  }
  string imageFilename = filename + "-000001.pbm";
  setThumbnail(imageFilename, true);
}



void PDFImporter::ripMetadata() {
  //print out metadata to "blah.pdf.info", will parse out later
  string infoFilename = filename + ".info";
  string command = "pdfinfo \"" + filename + "\" > \"" + infoFilename + "\"";
  if (system(command.c_str())) {  // i.e. non-zero return value
    cout << "metadata error" << endl;
    return;
  }

  string metadata;/*
  ifstream infoFile(infoFilename.c_str(), ios::in);
  if (!infoFile) {
      cout << ">>> metadata error" << endl;
      return;
  }

  char *buffer = new char[BUFFER_SIZE];
  int length;

  while (infoFile.good()) {
     length = infoFile.readsome(buffer, BUFFER_SIZE-1);
     buffer[length] = '\0';
     metadata += buffer;
  }
  delete [] buffer;
  */

  /* info page has the comments in the following order:
   * Title:
   * Author:
   * Creator:
   * Producer:
   * CreationDate:
   * ModDate:
   * Tagged:
   * Pages:
   * Encrypted:
   * Page size:
   * File size:
   * Optimized:
   * PDF version:
   * for now, we're only going to concern ourselves with 
   * the title, author, creationDate, modificationDate, 
   * and number of pages
   */
  
  vector<string> lineTokens;
  vector<string> tokens;
  tokenize(metadata, lineTokens, "\n"); //get each of the lines, which we know will follow the above info line format
  
  for (vector<string>::iterator it = lineTokens.begin(); it != lineTokens.end(); ++it) {
      tokenize(*it, tokens, " ");
      string dataName = tokens[0];
      string dataValue;
      for(unsigned int i = 1; i < tokens.size(); i++) {
          if(i > 1)
	      dataValue += ", ";
          dataValue += tokens[i];
      }
    insertChosenAttributes(dataName, dataValue);
    tokens.clear();
  }

  system(("rm -f " + infoFilename).c_str());
}

void PDFImporter::insertChosenAttributes(const string name, const string value) {
  //gotta do this better
  if(name == "Title:")
    set(A_TITLE, value);
  else if(name == "Author:")
    set(A_AUTHOR, value); 
  else if(name == "CreationDate:")
    set(DATE_CREATE, value);
  else if(name == "ModDate:")
    set(DATE_MODIFY, value); 
  else if(name == "Pages:")
    set(A_PAGES, value);
}

void PDFImporter::ripText() {
  
  string textFilename = filename + ".txt";
  string command = "pdftotext \"" + filename + "\" \"" + textFilename + "\"";

  if (system(command.c_str()) != 0) {
      cout << ">> error extracting PDF text" << endl;
      return;
  } 

  TextImporter textChild;
  textChild.import(g_filename_to_uri(textFilename.c_str(), NULL, NULL), false);
  string removeCommand = "rm -f " + textFilename;
  system(removeCommand.c_str());
  storage_item_insert_child(item, textChild.getItem(), -1);
}

/*
 * The following tokenizer code is from Alavoor Vasudevan's C++ Programming HOW-TO online
 * document, distributable under the GNU license.
 */
void PDFImporter::tokenize(const string& str, vector<string>& tokens, const string& delimiters) {
  //Skip delimiters at beginning
  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  //Find first "non-delimiter"
  string::size_type pos = str.find_first_of(delimiters, lastPos);

  while(string::npos != pos || string::npos != lastPos) {
    //found a token, add to the vector
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    //Skip delimiters. Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    //Find the next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }
}

