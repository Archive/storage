#ifndef pdf_importer_HH
#define pdf_importer_HH

#include "binary_importer.hh"
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <unistd.h>

using namespace std;

class PDFImporter: public BinaryImporter {
 private:
  virtual void ripMetadata();
  virtual void ripText();
  virtual void grabThumbnail();
  virtual void tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ");
  virtual void insertChosenAttributes(const string name, const string value);
  
 public:
  PDFImporter();
  virtual ~PDFImporter();
  //virtual void import(const char *filenameURI, bool top = true);
};

#endif
