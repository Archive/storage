#include "../libstorage/libstorage/storage-item.h"
#include <stdio.h>
#include <string.h>


int main(int argc, char** argv) {
	StorageStore *store;
	StorageItem *item;

	g_type_init();

	if (argc <= 1) {
		g_print ("Usage: remove <recordID of top level node>\n");
		return 0;
	}

	store = storage_get_default_store ();
	item = (StorageItem *)storage_item_new (store, argv[1]);
	storage_item_remove(item);
	g_object_unref (item);

	g_print ("...removal complete\n");

	return 0;
}
