/*
 * storage_translator.c: Implements a Bonobo StorageTranslator server
 *
 * Authors:
 *   Seth Nickell <snickell@redhat.com>
 *   Miguel de Icaza (miguel@ximian.com)
 *
 */
#include <config.h>
#include <libbonobo.h>

#include <string>

#include "importer.hh"

#include "GNOME_StorageTranslator.h"
#include "storage-translator.h"


struct _StorageTranslatorPrivate {
  Importer *importer;
};


/*
 * A pointer to our parent object class
 */
static GObjectClass *storage_translator_parent_class;

void storage_translator_set_importer (StorageTranslator *translator, void *importer) {
  translator->priv->importer = (Importer *)importer;
}

void storage_translator_set_exporter (StorageTranslator *translator, void *exporter) {

}

/*
 * Implemented GObject::finalize
 */
static void
storage_translator_object_finalize (GObject *object)
{
	storage_translator_parent_class->finalize (object);
}

static void
impl_storage_translator_startImport (PortableServer_Servant  servant,
				     const CORBA_char *fileName,
				     const CORBA_char *mimeType,
				     CORBA_Environment      *ev)
{
  StorageTranslator *storage_translator = STORAGE_TRANSLATOR (bonobo_object (servant));

  printf ("impl_storage_translator_startImport()\n");

  storage_translator->priv->importer->startImport(fileName, mimeType);
}

static void
impl_storage_translator_startExport (PortableServer_Servant  servant,
				     CORBA_Environment      *ev)
{
}

static void
impl_storage_translator_finishImport (PortableServer_Servant  servant,
				     CORBA_Environment *ev)
{
  StorageTranslator *storage_translator = STORAGE_TRANSLATOR (bonobo_object (servant));

  printf ("impl_storage_translator_finishImport()\n");

  storage_translator->priv->importer->finishImport();
}

static void
impl_storage_translator_finishExport (PortableServer_Servant  servant,
				     CORBA_Environment *ev)
{

}

static CORBA_long
impl_storage_translator_importChunk (PortableServer_Servant  servant,
				     const GNOME_StorageTranslator_iobuf *buffer,
				     CORBA_Environment      *ev)
{
	StorageTranslator *storage_translator = STORAGE_TRANSLATOR (bonobo_object (servant));
			
	printf ("impl_storage_translator_importChunk()\n");

	storage_translator->priv->importer->importChunk ((const char *)buffer->_buffer, buffer->_length);
						 
	return TRUE;
}

static CORBA_long
impl_storage_translator_exportChunk (PortableServer_Servant  servant,
				     GNOME_StorageTranslator_iobuf **buffer,
				     CORBA_Environment      *ev)
{
  /* FIXME: implement export
  CORBA_octet     *data;
  *buffer = GNOME_StorageTranslator_iobuf__alloc ();
  StorageTranslator *storage_translator = STORAGE_TRANSLATOR (bonobo_object (servant));
    data = CORBA_sequence_CORBA_octet_allocbuf (count);
                                                                                                                                  
                (*buffer)->_buffer = data;
                (*buffer)->_length = bytes_read;

  */

  return TRUE;
}


static void
storage_translator_class_init (StorageTranslatorClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;
	POA_GNOME_StorageTranslator__epv *epv = &klass->epv;

	printf ("Class is initializing\n");

	storage_translator_parent_class = (GObjectClass *)g_type_class_peek_parent (klass);

	object_class->finalize = storage_translator_object_finalize;

	epv->startImport  = impl_storage_translator_startImport;
	epv->startExport  = impl_storage_translator_startExport;
	epv->importChunk  = impl_storage_translator_importChunk;
	epv->exportChunk  = impl_storage_translator_exportChunk;
	epv->finishImport = impl_storage_translator_finishImport;
	epv->finishExport = impl_storage_translator_finishExport;

}

static void
storage_translator_init (StorageTranslator *storage_translator)
{
  printf ("Initializing a storage translator\n");
  storage_translator->priv = g_new0(StorageTranslatorPrivate, 1);
  storage_translator->priv->importer = NULL;
  //storage_translator->exporter = NULL;
}

BONOBO_TYPE_FUNC_FULL (
	StorageTranslator,                /* Glib class name */
	GNOME_StorageTranslator,  /* CORBA interface name */
	BONOBO_TYPE_OBJECT,  /* parent type */
	storage_translator)                /* local prefix ie. 'storage_translator'_class_init */
