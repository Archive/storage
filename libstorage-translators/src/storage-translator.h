/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _STORAGE_TRANSLATOR_H_
#define _STORAGE_TRANSLATOR_H_

#include <bonobo/bonobo-object.h>
#include "GNOME_StorageTranslator.h"

G_BEGIN_DECLS

#define STORAGE_TRANSLATOR_TYPE         (storage_translator_get_type ())
#define STORAGE_TRANSLATOR(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), STORAGE_TRANSLATOR_TYPE, StorageTranslator))
#define STORAGE_TRANSLATOR_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), STORAGE_TRANSLATOR_TYPE, StorageTranslatorClass))
#define STORAGE_TRANSLATOR_IS_OBJECT(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), STORAGE_TRANSLATOR_TYPE))
#define STORAGE_TRANSLATOR_IS_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), STORAGE_TRANSLATOR_TYPE))
#define STORAGE_TRANSLATOR_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), STORAGE_TRANSLATOR_TYPE, StorageTranslatorClass))

typedef struct _StorageTranslatorPrivate StorageTranslatorPrivate;
typedef struct _StorageTranslator StorageTranslator;

struct _StorageTranslator {
	BonoboObject parent;

	StorageTranslatorPrivate *priv;
};

typedef struct {
	BonoboObjectClass parent_class;

	POA_GNOME_StorageTranslator__epv epv;
} StorageTranslatorClass;

void storage_translator_set_importer (StorageTranslator *translator, void *importer);
void storage_translator_set_exporter (StorageTranslator *translator, void *exporter);

GType storage_translator_get_type (void);

G_END_DECLS

#endif /* _STORAGE_TRANSLATOR_H_ */
