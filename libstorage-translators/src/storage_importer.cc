#include "binary_importer.hh"
#include "image_importer.hh"
#include "pdf_importer.hh"  
#include "xml_importer.hh"
#include "mpegaudio_importer.hh"
#include "text_importer.hh"
#include "html_importer.hh"   
#include "mpegvideo_importer.hh"
#include "video_importer.hh"

// Not yet handled...
//#include "mbox_importer.hh"     
//#include "docb_importer.hh"    

#include <libgnomevfs/gnome-vfs.h>

int main(int argc, char** argv)
{

  gnome_vfs_init();

  if (argc < 2) {
    cout << "usage: " << argv[0] << " <filename> [\"binary\"]" << endl;
    return -1;
  }

  //----------------------- convert path into file:// URI
  
  for (int i=1; i < argc; i++) {

    char *path;
    const char *input = argv[i];

    if (input[0] == '/') {
      // this is a full path
      path = g_strdup(input);
    } else {
      char *current_dir = g_get_current_dir();
      path = g_build_filename(current_dir, input, NULL);
      g_free (current_dir);
    }

    char  *escaped_uri = gnome_vfs_get_uri_from_local_path (path);
    g_free(path);

    Importer *importer = NULL;
    
    if(argc > 2 && strcmp(argv[2], "binary") == 0){
      importer = new BinaryImporter;
    }
    else{
      
      GnomeVFSFileInfo fileInfo;
      GnomeVFSResult result = gnome_vfs_get_file_info (escaped_uri, &fileInfo,
						       GNOME_VFS_FILE_INFO_GET_MIME_TYPE);
      
      if (result != GNOME_VFS_OK) {
	cerr << "Could not import " << escaped_uri << endl;
	cerr << gnome_vfs_result_to_string(result) << endl << endl << endl;
      continue;
      }
      
      string mimeType = string(fileInfo.mime_type);
      
      
      if (mimeType == "audio/mpeg") {
	importer = new MPEGAudioImporter;
      } else if (mimeType == "application/pdf") {
	importer = new PDFImporter;
      } else if (mimeType == "text/xml") {
	importer = new XMLImporter;
      } else if (mimeType == "text/html") {
	importer = new HTMLImporter;
      } else if (mimeType == "video/mpeg") {
	//importer = new MPEGVideoImporter;
	cerr << "Not handled, need to fix the header file... what defines mpeg.h ???" << endl;
      } else if (mimeType.substr(0,5) == "text/") {
	importer = new TextImporter;
      } else if (mimeType.substr(0, 6) == "image/") {
	importer = new ImageImporter;
      } else if (mimeType.substr(0, 6) == "video/") {
	importer = new VideoImporter;
      } else {
	importer = new BinaryImporter;
      }
    }
    
    importer->import(escaped_uri);
    g_free(escaped_uri);
    
    StorageItem *item = importer->getItem();
    GSList *attrList = storage_item_get_attributes_pairs(item);
    
    GSList *curr = attrList;
    
    cout << "received list. imported the following: " << endl;
    while (curr != NULL) {
      StorageAttrPair *data = (StorageAttrPair*) curr->data;
      
      if (data == NULL) {
	cout << "*** LIST ERROR ***" << endl;
      }
      else {
	cout << "* " << data->name << "\t= " << data->value << endl;
      }
      curr = g_slist_next(curr);
    }
    g_slist_free(attrList);
  }

  return 0;
}
