#include "text_importer.hh"

#include <iostream>

using namespace std;

TextImporter::TextImporter() : Importer() { }
TextImporter::~TextImporter() { }

GnomeVFSResult
TextImporter::startImport (const char *fileName, const char *mimeType, bool toplevelNode)
{
	StorageItem *item;

	item = storage_store_add_item (defaultStore, STORAGE_ITEM_TYPE_TEXT);
	this->setToplevelItem (item);
	g_object_unref (item);

	text = "";

	return Importer::startImport (fileName, mimeType, toplevelNode);
}

GnomeVFSResult
TextImporter::importChunk (const char *buffer, gsize bufferSize)
{
	text.append (buffer, bufferSize);

	return GNOME_VFS_OK;
}

GnomeVFSResult
TextImporter::finishImport ()
{
	set (FILE_TEXT, text);
	
	Importer::finishImport ();

	return GNOME_VFS_OK;
}
