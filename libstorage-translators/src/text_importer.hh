#ifndef _text_importer_HH
#define _text_importer_HH

#include "importer.hh"

#include <string>

class TextImporter: public Importer {
  
public:
	TextImporter();
	virtual ~TextImporter();

	GnomeVFSResult startImport  (const char *fileName,
				     const char *mimeType,
				     bool toplevelNode=true);

	GnomeVFSResult importChunk  (const char *buffer,
				     gsize bufferSize);

	GnomeVFSResult finishImport ();

private:

	std::string text;
};

#endif
