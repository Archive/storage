#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <bonobo/bonobo-shlib-factory.h>
#include <bonobo-activation/bonobo-activation.h>
#include "GNOME_StorageTranslator.h"
#include "storage-translator.h"
#include "importer.hh"
#include "image_importer.hh"
#include "html_importer.hh"
#include "text_importer.hh"
#include "gstreamer_importer.hh"

static BonoboObject *
translator_factory (BonoboGenericFactory *factory,
	       const char           *component_id,
	       gpointer              closure)
{
  StorageTranslator *translator = STORAGE_TRANSLATOR(g_object_new (STORAGE_TRANSLATOR_TYPE, NULL));
  Importer *importer = NULL;

  gnome_vfs_init();

  if (!strcmp (component_id, "OAFIID:GNOME_StorageTranslator_Image")) {
    importer = new ImageImporter();
  }
  else if (!strcmp (component_id, "OAFIID:GNOME_StorageTranslator_Text")) {
    importer = new TextImporter();
  }
  else if (!strcmp (component_id, "OAFIID:GNOME_StorageTranslator_HTML")) {
    importer = new HTMLImporter();
  }
  else if (!strcmp (component_id, "OAFIID:GNOME_StorageTranslator_GStreamer")) {
    importer = new GStreamerImporter();
  }
  else if (!strcmp (component_id, "OAFIID:GNOME_StorageTranslator_Binary")) {
    importer = new BinaryImporter();
  }

  printf ("Setting importer\n");
  storage_translator_set_importer (translator, importer);

  printf ("Returning object\n");
  return BONOBO_OBJECT (translator);
}

extern "C" {

  static Bonobo_Unknown                                                         
  make_factory (PortableServer_POA poa, const char *iid, gpointer impl_ptr,     
		CORBA_Environment *ev)                                          
  {                                                                             
    return bonobo_shlib_factory_std ("OAFIID:GNOME_StorageTranslator_Main_Factory", poa, impl_ptr, 
				     translator_factory, NULL, ev);
  }                 
                                                            
  static BonoboActivationPluginObject plugin_list[] = {{"OAFIID:GNOME_StorageTranslator_Main_Factory", make_factory}, { NULL } };
  BonoboActivationPlugin Bonobo_Plugin_info = { plugin_list, N_("Storage Translator factory") };
}

