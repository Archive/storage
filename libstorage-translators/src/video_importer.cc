#include "video_importer.hh"
#include <vector>

#define VIDEO_TEMP_PICTURE	"/tmp/vid_thumbnail"
#define VIDEO_THUMBNAIL_APP	"totem-video-thumbnailer"

using namespace std;

typedef string::size_type size_type;

VideoImporter::VideoImporter() : BinaryImporter() { }

VideoImporter::~VideoImporter() { }

void VideoImporter::setTitleAndHeaderInfo()
{
  // try to guess from the filename.
  size_type start = filename.rfind("/") + 1;
  size_type end = filename.rfind(".");
  title = filename.substr(start, end-start);

  while ((start = title.find("_",0)) != string::npos) {
    title[start] = ' ';
  }
}

void VideoImporter::import(const char *filenameURI, bool top)
{
  BinaryImporter::import(filenameURI);
  setTitleAndHeaderInfo();

  vector<AttrVal> info = scanner.scan(title.c_str());
  set(info);
  cout << ">>> IMDb attributes saved." << endl;

  string command = VIDEO_THUMBNAIL_APP;
  string thumbnail = VIDEO_TEMP_PICTURE;

  command += " \"" + filename + "\" \"" + thumbnail + "\"";
  cout << "Running " << command << endl;
  system(command.c_str());

  setThumbnail(thumbnail, true);
}
