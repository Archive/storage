#ifndef _video_importer_H
#define _video_importer_H

#include "binary_importer.hh"
#include "image_importer.hh"
#include "imdb_scanner.hh"

class VideoImporter: public BinaryImporter {

 protected:
  IMDbScanner scanner;
  std::string title;
  
 public:
  VideoImporter();
  //virtual void import(const char *filenameURI, bool top);
  virtual void setTitleAndHeaderInfo();
  virtual ~VideoImporter();
};

#endif
