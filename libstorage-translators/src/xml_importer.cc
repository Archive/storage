#include "xml_importer.hh"

using namespace std;

const char* XMLImporter::A_XML_ELEMENT_TYPE = "element type";
const char* XMLImporter::A_XML_VERSION = "version";

static const char* XML_DTD_TYPE = "DTD Type";
static const char* XML_DTD_INTERNAL_SUBSET = "intSubset";
static const char* XML_DTD_EXTERNAL_SUBSET = "extSubset";
static const char* XML_DTD_EXTERNAL_ID = "ExternalID";
static const char* XML_DTD_SYSTEM_ID = "SystemID";

#define DEFAULT_XML_VERSION "1.0"

XMLImporter::XMLImporter() : Importer() {}
XMLImporter::~XMLImporter() {}

//just to test builds... should remove when done testing
void XMLImporter::test(){
  xmlDocPtr doc;
  doc = xmlNewDoc((xmlChar *)DEFAULT_XML_VERSION);

  xmlNodePtr newNode =  xmlNewDocRawNode(doc, NULL,(xmlChar *)"root node", NULL); //not sure if xmlNewDocRawNode is the right function to be using...
  xmlNodePtr childNode = xmlNewTextChild(newNode, NULL,(xmlChar *) "child node", NULL);
  xmlNewTextChild(childNode, NULL,(xmlChar *) "text", (xmlChar *)"Bill Martin");

  xmlDocSetRootElement(doc, newNode);

  xmlSaveFormatFile("-", doc, 1);
}

void XMLImporter::import(const char *filenameURI, bool top) {
  Importer::import(filenameURI, STORAGE_ITEM_TYPE_NODE, top); //root node represents the xmlDoc

  string filename = g_filename_from_uri(filenameURI, NULL, NULL);

  xmlDocPtr doc = parseFile(filename.c_str());
  if (doc == NULL ) {
    fprintf(stderr,"Document not parsed successfully. \n");
    return;
  }

  //set the version attribute on the top-level node
  if(doc->version != NULL) {
    set(A_XML_VERSION, (char *)doc->version);
  }

  importTree(doc->children, this, filenameURI);

  //xmlSaveFormatFile("-", doc, 1);

  grabThumbnail(filename);

  xmlFreeDoc(doc);
}

/*
 * An XML exporter: simply exports the XML to an in-memory XMLDoc;
 * does not write it to a file.
 *
 * The user is responsible for freeing the memory associated with the xmlDocPtr!
 *
 * If an error for any reason, will return NULL.
 */
xmlDocPtr XMLImporter::xml_export(const char *recordid_str){
  xmlDocPtr doc;
  StorageItem *item = (StorageItem *)
  		storage_item_new_from_recordid(
			(char *)recordid_str, HOSTNAME, USERNAME, PASSWORD);

  if (item == NULL) {
    return(NULL);
  }

  if (storage_item_has_attribute(item, strdup(A_XML_VERSION))) {
    doc = xmlNewDoc((xmlChar *)storage_item_get_attributes_value(item, strdup(A_XML_VERSION)));
  } else {
    doc = xmlNewDoc((xmlChar *)DEFAULT_XML_VERSION);
  }

  GSList* children = storage_item_get_children(item);
  while(children != NULL) {
    appendDocNodes((StorageItem *)children->data, doc);
    children = g_slist_next(children);
  }
  return(doc);
}

/*
 * Prints out doc tree to stdout
 */
void XMLImporter::printDoc(const char *recordid_str) {
  xmlSaveFormatFile("-", xml_export(recordid_str), 1);
}

void XMLImporter::saveDoc(const char *filename, const char *recordid_str) {
  xmlSaveFormatFile(filename, xml_export(recordid_str), 1);
}

void XMLImporter::saveDocToFILE(FILE *f, const char *recordid_str) {
  xmlDocFormatDump(f, xml_export(recordid_str), 1);
}

/*
 * Appends the Nodes to the document root... also serves
 * as the wrapper for the recursive appendNodes
 */
void XMLImporter::appendDocNodes(StorageItem *item, xmlDocPtr doc) {
  if(item == NULL) { //shouldn't happen, but we'll check anyway
    cout << "ERROR: NULL result when appending node to document!\n";
    return;
  }

  xmlNodePtr newNode;
  int type = atoi((char *)storage_item_get_attributes_value(item, strdup(A_XML_ELEMENT_TYPE)));
  //  cout << "...node " << storage_item_get_id(item) << " is of type " << type << endl;
  if(type == XML_TEXT_NODE) {
    cout << "...attempted to add text node to doc\n";
    newNode = xmlNewDocText(doc, (xmlChar *)storage_item_get_attribute_value(item, strdup(FILE_TEXT)));
    //kind of a hack way of doing it, but it works...
    if(xmlDocGetRootElement(doc) == NULL) {
      xmlDocSetRootElement(doc, newNode); //there's actually no error-checking to ensure that the root is a node type!
    } else { //just add it as a sibling to the root
      xmlAddSibling(xmlDocGetRootElement(doc), newNode);
    }
  } else if(type == XML_COMMENT_NODE) {
    cout << "...attempted to add comment node to doc\n";
    //newNode = xmlNewDocComment(doc, (xmlChar *)storage_item_get_attribute_value(item, strdup(FILE_TEXT)));
    newNode = xmlNewComment((xmlChar *)storage_item_get_attribute_value(item, strdup(FILE_TEXT)));
    //kind of a hack way of doing it, but it works...
    if(xmlDocGetRootElement(doc) == NULL) {
      xmlDocSetRootElement(doc, newNode); //there's actually no error-checking to ensure that the root is a node type!
    } else { //just add it as a sibling to the root
      xmlAddSibling(xmlDocGetRootElement(doc), newNode);
    }
  } else if(type == XML_ELEMENT_NODE) {
    cout << "...attempted to add root node to doc\n";
    //cout << "...appending node named " << storage_item_get_attribute_value(item, (char *)FILE_NAME) << " to doc root\n";
    newNode =  xmlNewDocRawNode(doc, NULL, (xmlChar *)storage_item_get_attribute_value(item, strdup(FILE_NAME)), NULL); //not sure if xmlNewDocRawNode is the right function to be using...
    cout << "...setting root element named " << storage_item_get_attribute_value(item, strdup(FILE_NAME)) << endl;
    xmlDocSetRootElement(doc, newNode);

    //recursively reconstruct the tree below this node
    appendNodes(storage_item_get_children(item), newNode);
  } else if(type == XML_DTD_NODE) {
    exportDTDNode(doc, item);
  } else {
    cout << "...Unhandled Node Type: " << type << endl;
  }  
}

/*
 * Reconstructs the XML tree
 */
void XMLImporter::appendNodes(GSList *currentList, xmlNodePtr parentNode) {
  GSList* dtdChildren;
  if(currentList == NULL || parentNode == NULL) { //end the recursion
    //cout << "...in appendNodes, current list is NULL, return\n";
    return;
  }

  //cout << "...attempting to get the storage item from the GSList\n";
  StorageItem *item = (StorageItem *)currentList->data;
  //  cout << "...obtained node " << storage_item_get_id(item) << " from GSList\n";
  int type;
  if(storage_item_has_attribute(item, (char *)A_XML_ELEMENT_TYPE)) {
    type = atoi((char *)storage_item_get_attribute_value(item, strdup(A_XML_ELEMENT_TYPE)));
    //cout << "...obtained node " << storage_item_get_id(item) << " of type " << type << " from GSList\n";
  } else {
    cout << "ERROR: No element type stored for this node!\n";
    return;
  }

  xmlNodePtr curNode = NULL;
  switch(type) {
  case XML_ELEMENT_NODE:
    curNode = xmlNewNode(NULL, (xmlChar *)storage_item_get_attribute_value(item, strdup(FILE_NAME)));
    break;
  case XML_ATTRIBUTE_NODE:
    break;
  case XML_TEXT_NODE:
    //   cout << "...attempting to add text node " << storage_item_get_id(item) << " to the tree\n";
    //   cout << "...FILE_NAME is " << storage_item_get_attribute_value(item, (char *)FILE_NAME) << endl;
    //   cout << "...FILE_TEXT is " << storage_item_get_attribute_value(item, (char *)FILE_TEXT) << endl;
    curNode = xmlNewText((xmlChar *)storage_item_get_attribute_value(item, strdup(FILE_TEXT)));
    break;
  case XML_CDATA_SECTION_NODE:
    break;
  case XML_ENTITY_REF_NODE:
    break;
  case XML_ENTITY_NODE:
    break;
  case XML_PI_NODE:
    break;
  case XML_COMMENT_NODE:
    curNode = xmlNewComment((xmlChar *)storage_item_get_attribute_value(item, strdup(FILE_TEXT)));
    break;
  case XML_DOCUMENT_NODE:
    break;
  case XML_DOCUMENT_TYPE_NODE:
    break;
  case XML_DOCUMENT_FRAG_NODE:
    break;
  case XML_NOTATION_NODE:
    break;
  case XML_HTML_DOCUMENT_NODE:
    break;
  case XML_DTD_NODE:
    dtdChildren = storage_item_get_children(item);
    while(dtdChildren != NULL) {
      StorageItem *dtdItem = (StorageItem *)dtdChildren->data;
      char *dtdType = storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_TYPE));
      xmlDtdPtr dtdPtr;
      if(strcmp(dtdType, XML_DTD_EXTERNAL_SUBSET)==0) {
	dtdPtr = xmlNewDtd(parentNode->doc, (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(FILE_NAME)), (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_EXTERNAL_ID)), (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_SYSTEM_ID)));
      }
      else if(strcmp(dtdType, XML_DTD_INTERNAL_SUBSET)==0) {
	dtdPtr = xmlCreateIntSubset(parentNode->doc, (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(FILE_NAME)), (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_EXTERNAL_ID)), (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_SYSTEM_ID)));
      }
      dtdChildren = g_slist_next(dtdChildren);
    }
    break;    
  case XML_ELEMENT_DECL:
  case XML_ATTRIBUTE_DECL:
  case XML_ENTITY_DECL:
  case XML_NAMESPACE_DECL:
  case XML_XINCLUDE_START:
  case XML_XINCLUDE_END:
#ifdef LIBXML_DOCB_ENABLED
  case XML_DOCB_DOCUMENT_NODE:
#endif
  default:
    break;
  }

  //append any children
  appendNodes(storage_item_get_children(item), curNode);
  //append siblings
  appendNodes(g_slist_next(currentList), parentNode);

  addAttributes(curNode, item);
  xmlAddChild(parentNode, curNode); //this call needs to come after we've appended the children and siblings to ensure proper ordering
}

/*
 * Adds the attributes when reconstructing the doc structure
 */
void XMLImporter::addAttributes(xmlNodePtr curNode, StorageItem *item) {
  //add attributes to node
  GSList *attrPairs = storage_item_get_attribute_pairs(item);
  while(attrPairs != NULL) {
    StorageAttrPair *pair = (StorageAttrPair *)(attrPairs->data);

    //cout << "...attr pair name: " << pair->name << " :ended\n";

    if(strcmp(pair->name, A_XML_ELEMENT_TYPE) != 0 && strcmp(pair->name, FILE_NAME) != 0) {//we don't want to include the element type as an attribute, nor the node name
      xmlNewProp(curNode, (xmlChar *)pair->name, (xmlChar *)pair->value);
    }  
    attrPairs = g_slist_next(attrPairs);
  }
}


/*
 * Simply parses the file... in a separate function to allow easier overriding.
 * For XML files, encoding is just NULL (whatever value is there will be ignored)
 */
xmlDocPtr XMLImporter::parseFile(const char *filename, const char *encoding) {
  return(xmlParseFile(filename));
}

/*
 * Takes a node that has not yet been added to its parent as a child.
 * Walks tree in preorder fashion
 */
void XMLImporter::importTree(xmlNodePtr curNodePtr, Importer *parentImporter, const char *filenameURI) {
  if(curNodePtr == NULL) {
    return; //this is the only "error checking" for now
  }

  //first thing: determine the type of node we have
  xmlElementType type = curNodePtr->type;
  StorageItemType node_type = STORAGE_ITEM_TYPE_NODE; //default is node type

  switch(type) {
  case XML_ELEMENT_NODE:
    node_type = STORAGE_ITEM_TYPE_NODE;
    break;
  case XML_ATTRIBUTE_NODE:
    break;
  case XML_TEXT_NODE:
    node_type = STORAGE_ITEM_TYPE_TEXT;
    break;
  case XML_CDATA_SECTION_NODE:
    break;
  case XML_ENTITY_REF_NODE:
    break;
  case XML_ENTITY_NODE:
    break;
  case XML_PI_NODE:
    break;
  case XML_COMMENT_NODE:
    node_type = STORAGE_ITEM_TYPE_TEXT;
    break;
  case XML_DOCUMENT_NODE:
    break;
  case XML_DOCUMENT_TYPE_NODE:
    break;
  case XML_DOCUMENT_FRAG_NODE:
    break;
  case XML_NOTATION_NODE:
    break;
  case XML_HTML_DOCUMENT_NODE:
    break;
  case XML_DTD_NODE:
    (int)node_type = -1;
    parseDTDNode(curNodePtr, parentImporter, filenameURI);
    break;
  case XML_ELEMENT_DECL:
    break;
  case XML_ATTRIBUTE_DECL:
    break;
  case XML_ENTITY_DECL:
    break;
  case XML_NAMESPACE_DECL:
    break;
  case XML_XINCLUDE_START:
    break;
  case XML_XINCLUDE_END:
    break;
#ifdef LIBXML_DOCB_ENABLED
  case XML_DOCB_DOCUMENT_NODE:
    break;
#endif
  default:
    break;
  }

  //now create importer, add to parent's child list, as long as node_type != -1 (our signal that it has already been taken care of)
  if(node_type != -1) {
    Importer curImporter;
    curImporter.create(node_type, false);
    storage_item_insert_child(parentImporter->getItem(), curImporter.getItem(), -1); //-1 appends the last child

    addNodeInfo(curNodePtr, &curImporter);

    //follow the tree down--must only happen if we have created an importer here
    if(curNodePtr->children != NULL) {
      importTree(curNodePtr->children, &curImporter, filenameURI);
    }
  }

  //follow the siblings to the right of the tree
  if(curNodePtr->next != NULL) {
    importTree(curNodePtr->next, parentImporter, filenameURI);
  }
}

void XMLImporter::parseDTDNode(xmlNodePtr curNodePtr, Importer *parentImporter, const char *filenameURI) {
  Importer dtdImporter;
  dtdImporter.create(STORAGE_ITEM_TYPE_NODE, false);
  storage_item_insert_child(parentImporter->getItem(), dtdImporter.getItem(), -1); //dtd node added

  //name the dtd parent node to the root element name
  if(curNodePtr->name != NULL) {
    dtdImporter.set(FILE_NAME, (char *)curNodePtr->name);
  }
  //give the dtd parent node its appropriate element type
  dtdImporter.set(A_XML_ELEMENT_TYPE, itos((int)curNodePtr->type).c_str());

  //now things get interesting... so, a DTD can, in this structure of an XMLDoc, have an internal subset DTD and/or an external subset DTD
  if(curNodePtr->doc->extSubset != NULL) {
    Importer dtdExt;
    dtdExt.create(STORAGE_ITEM_TYPE_NODE, false);
    storage_item_insert_child(dtdImporter.getItem(), dtdExt.getItem(), -1);
    dtdExt.set(XML_DTD_TYPE, XML_DTD_EXTERNAL_SUBSET);
    if(curNodePtr->doc->extSubset->name != NULL) {
      dtdExt.set(FILE_NAME, (char *)curNodePtr->doc->extSubset->name);
    }
    if(curNodePtr->doc->extSubset->SystemID != NULL) {
      dtdExt.set(XML_DTD_SYSTEM_ID, (char *)curNodePtr->doc->extSubset->SystemID);
    }
    if(curNodePtr->doc->extSubset->ExternalID != NULL) {
      dtdExt.set(XML_DTD_EXTERNAL_ID, (char *)curNodePtr->doc->extSubset->ExternalID);
    }
    //should check on other info, too
  }

  //check for internal subset, too...
  if(curNodePtr->doc->intSubset != NULL) {
    Importer dtdInt;
    dtdInt.create(STORAGE_ITEM_TYPE_NODE, false);
    storage_item_insert_child(dtdImporter.getItem(), dtdInt.getItem(), -1);
    dtdInt.set(XML_DTD_TYPE, XML_DTD_INTERNAL_SUBSET);
    if(curNodePtr->doc->intSubset->name != NULL) {
      dtdInt.set(FILE_NAME, (char *)curNodePtr->doc->intSubset->name);
    }
    if(curNodePtr->doc->intSubset->SystemID != NULL) {
      dtdInt.set(XML_DTD_SYSTEM_ID, (char *)curNodePtr->doc->intSubset->SystemID);
    }
    if(curNodePtr->doc->intSubset->ExternalID != NULL) {
      dtdInt.set(XML_DTD_EXTERNAL_ID, (char *)curNodePtr->doc->intSubset->ExternalID);
    }
    //should check on other info, too
  } 
}

void XMLImporter::exportDTDNode(xmlDocPtr doc, StorageItem *item) {
  GSList *dtdChildren = storage_item_get_children(item);
  while(dtdChildren != NULL) {
    StorageItem *dtdItem = (StorageItem *)dtdChildren->data;
    char *dtdType = storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_TYPE));
    xmlDtdPtr dtdPtr;
    if(strcmp(dtdType, XML_DTD_EXTERNAL_SUBSET)==0) {
      dtdPtr = xmlNewDtd(doc, (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(FILE_NAME)), (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_EXTERNAL_ID)), (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_SYSTEM_ID)));
    }
    else if(strcmp(dtdType, XML_DTD_INTERNAL_SUBSET)==0) {
      dtdPtr = xmlCreateIntSubset(doc, (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(FILE_NAME)), (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_EXTERNAL_ID)), (xmlChar *)storage_item_get_attribute_value(dtdItem, strdup(XML_DTD_SYSTEM_ID)));
    }
    dtdChildren = g_slist_next(dtdChildren);
  }
}

void XMLImporter::addNodeInfo(xmlNodePtr curNodePtr, Importer *importer) {
  xmlElementType type = curNodePtr->type;

  //store the element type in the db for future retrieval
  importer->set(A_XML_ELEMENT_TYPE, itos((int)type).c_str());

  //name the db record name to the node name
  if(curNodePtr->name != NULL) {
    importer->set(FILE_NAME, (char *)curNodePtr->name);
  }

  //if not a node, insert text
  if(type == XML_TEXT_NODE || type == XML_COMMENT_NODE) {
    if(curNodePtr->content != NULL) {
      cout << "XML: Set text context\n";
      importer->set(FILE_TEXT, (char *)curNodePtr->content);
    }    
  }

  //add attributes
  xmlAttrPtr attribute = curNodePtr->properties;
  while(attribute != NULL) {
    if((attribute->name != NULL) && (attribute->children->content != NULL)) {
      cout << "XML: Attempting to add attribute " << (char *)attribute->name <<endl;
      importer->set((char *)attribute->name, (char *) attribute->children->content);
    }
    attribute = attribute->next;
  }
}

/*
 * May at some point in time grab a thumbnail of the first page to display
 */
void XMLImporter::grabThumbnail(string filename) {
}

