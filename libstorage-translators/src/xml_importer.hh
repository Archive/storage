#ifndef xml_importer_HH
#define xml_importer_HH

#include "binary_importer.hh"
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

using namespace std;

class XMLImporter: public Importer {
protected:
  virtual void importTree(xmlNodePtr curNodePtr, Importer *parentImporter, const char *filenameURI);
  virtual void addNodeInfo(xmlNodePtr curNodePtr, Importer *importer);
  virtual void grabThumbnail(string filename);
  virtual xmlDocPtr parseFile(const char *filename, const char *encoding = NULL);
  virtual void appendDocNodes(StorageItem *item, xmlDocPtr doc);
  virtual void appendNodes(GSList *currentList, xmlNodePtr parentNode);
  virtual void addAttributes(xmlNodePtr curNode, StorageItem *item);
  virtual void parseDTDNode(xmlNodePtr curNodePtr, Importer *parentImporter, const char *filenameURI);
  virtual void exportDTDNode(xmlDocPtr doc, StorageItem *item); 

public:
  XMLImporter();
  virtual ~XMLImporter();
  //virtual void import(const char *filenameURI, bool top = true);
  virtual xmlDocPtr xml_export(const char *recordid_str);
  virtual void printDoc(const char *recordid_str);
  virtual void saveDoc(const char *filename, const char *recordid_str);
  virtual void saveDocToFILE(FILE *f, const char *recordid_str);
  virtual void test();
  
  static const char* A_XML_ELEMENT_TYPE;
  static const char* A_XML_VERSION;
};

#endif
