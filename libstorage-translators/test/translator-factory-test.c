#include <config.h>
#include <libbonobo.h>
#include <libgnomevfs/gnome-vfs.h>

#include "GNOME_StorageTranslator.h"

void 
readFromURI (const char *uri, 
	     const char *file_name,
	     const char *mime_type, 
	     GNOME_StorageTranslator translator) 
{
	CORBA_Environment  ev;
	GnomeVFSResult   result;
	GnomeVFSFileSize bytes_read;
	CORBA_octet     *data;
	GnomeVFSHandle *handle;
	GNOME_StorageTranslator_iobuf *buffer;

	result = gnome_vfs_open(&handle, uri, GNOME_VFS_OPEN_READ);

	if (result != GNOME_VFS_OK) {
	  printf ("Opening URI %s failed with error: %s\n", uri, gnome_vfs_result_to_string(result));
	  return;
	}

	/* Cue that a new Import is coming down the line */
	CORBA_exception_init (&ev);
	GNOME_StorageTranslator_startImport (translator, file_name, mime_type, &ev);
	if (BONOBO_EX (&ev)) {
		char *err = bonobo_exception_get_text (&ev);
		g_warning (_("An exception occured '%s'"), err);
		g_free (err);
	}
	CORBA_exception_free (&ev);

	/* Loop through all the file reads passing each chunk off
	   to the translator as we read it */
	do {
	  buffer = GNOME_StorageTranslator_iobuf__alloc ();
	  CORBA_sequence_set_release (buffer, TRUE);
	  data = CORBA_sequence_CORBA_octet_allocbuf (100000);
	  	  
	  result = gnome_vfs_read (handle, data, 100000, &bytes_read);

	  buffer->_buffer = data;
	  buffer->_length = bytes_read;

	  CORBA_exception_init (&ev);
	  GNOME_StorageTranslator_importChunk (translator, buffer, &ev);
	  if (BONOBO_EX (&ev)) {
	    char *err = bonobo_exception_get_text (&ev);
	    g_warning (_("An exception occured '%s'"), err);
	    g_free (err);
	  }
	  CORBA_exception_free (&ev);

	} while (result == GNOME_VFS_OK);

	CORBA_exception_init (&ev);
	GNOME_StorageTranslator_finishImport (translator, &ev);
	if (BONOBO_EX (&ev)) {
		char *err = bonobo_exception_get_text (&ev);
		g_warning (_("An exception occured '%s'"), err);
		g_free (err);
	}
	CORBA_exception_free (&ev);

	if (result != GNOME_VFS_ERROR_EOF) {
	  printf ("Reading URI %s failed with error: %s\n", uri, gnome_vfs_result_to_string(result));
	  return;
	}
}

int 
main (int argc, char *argv [])
{
	CORBA_Environment  ev;
	CORBA_Object translator_object;
	GNOME_StorageTranslator translator;
	GnomeVFSFileInfo info;
	GnomeVFSResult result;
	char *query;
	const char *uri;
	char *mime_type;
	char *file_name;
	GnomeVFSURI *vfsuri;

	if (argc != 2) {
	  printf ("Usage: import-file URI\n");
	  return -1;
	} else {
	  uri = argv[1];
	}

	gnome_vfs_init();

	/*
	 * Initialize bonobo.
	 */
	if (!bonobo_init (&argc, argv))
		g_error (_("I could not initialize Bonobo"));
	
	/*
	 * Enable CORBA/Bonobo to start processing requests
	 */
	bonobo_activate ();

	result = gnome_vfs_get_file_info(uri, &info, GNOME_VFS_FILE_INFO_GET_MIME_TYPE);

	if (result != GNOME_VFS_OK) {
	  printf ("Getting type of URI %s failed with error: %s\n", uri, gnome_vfs_result_to_string(result));
	  return -1;
	} else if (info.mime_type == NULL) {
	  printf ("Getting type of URI %s failed\n", uri);
	  return -1;
	}

	mime_type = g_strdup(info.mime_type);
	g_free(info.mime_type);

	query = g_strdup_printf ("repo_ids.has ('IDL:GNOME/StorageTranslator:1.0') AND bonobo:supported_mime_types.has ('%s')", mime_type);
	g_print (query);

	CORBA_exception_init (&ev);
	translator_object = bonobo_activation_activate (query, NULL, Bonobo_ACTIVATION_RESULT_OBJECT, NULL, &ev);

	g_free(query);

	/* Check for exceptions */
	if (BONOBO_EX (&ev)) {
		char *err = bonobo_exception_get_text (&ev);
		g_warning (_("An exception occured '%s'"), err);
		g_free (err);
	}
	CORBA_exception_free (&ev);

	if (translator_object == CORBA_OBJECT_NIL) {
		g_warning (_("Could not create an instance of the translator"));
		return bonobo_debug_shutdown ();
	}

	/* Send a message */
	CORBA_exception_init (&ev);

	translator = Bonobo_Unknown_queryInterface (translator_object, "IDL:GNOME/StorageTranslator:1.0", &ev);
	
	/* Check for exceptions */
	if (BONOBO_EX (&ev)) {
		char *err = bonobo_exception_get_text (&ev);
		g_warning (_("An exception occured '%s'"), err);
		g_free (err);
	}
	CORBA_exception_free (&ev);

	vfsuri = gnome_vfs_uri_new(uri);
	file_name = gnome_vfs_uri_extract_short_name(vfsuri);
	gnome_vfs_uri_unref(vfsuri);

	readFromURI(uri, file_name, mime_type, translator);

	g_free(file_name);

	g_free(mime_type);

	bonobo_object_release_unref (translator_object, NULL);
	
	return bonobo_debug_shutdown ();
}
