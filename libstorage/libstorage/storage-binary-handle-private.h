#ifndef STORAGE_BINARY_HANDLE_PRIVATE_H
#define STORAGE_BINARY_HANDLE_PRIVATE_H

#include <libpq-fe.h>
#include <libpq/libpq-fs.h>

#include "storage-binary-handle.h"
#include "storage-store.h"

StorageBinaryHandle *storage_binary_handle_open (StorageStore *store, Oid blobID, StorageBinaryHandleOpenMode mode);

#endif
