
#include "storage-store-private.h"

#include "storage-binary-handle.h"
#include "storage-binary-handle-private.h"


struct _StorageBinaryHandle {
  int lo_fd;
  PGconn *connection;
  StorageStore *store;
};

StorageBinaryHandle *
storage_binary_handle_open (StorageStore *store, Oid blobID, StorageBinaryHandleOpenMode mode) {
  StorageBinaryHandle *handle;
  int pg_mode = 0;

  handle = (StorageBinaryHandle *) g_malloc0 (sizeof(StorageBinaryHandle));
  handle->store = store;
  handle->connection = _storage_store_get_connection (handle->store);

  if (mode && STORAGE_BINARY_HANDLE_OPEN_READ) {
    pg_mode = pg_mode | INV_READ;
  }

  if (mode && STORAGE_BINARY_HANDLE_OPEN_WRITE) {
    pg_mode = pg_mode | INV_WRITE;
  }

  PQclear(PQexec(handle->connection, "BEGIN"));

  handle->lo_fd = lo_open(handle->connection, blobID, pg_mode);

  return handle;
}

/**
 * storage_binary_handle_write:
 * @handle: the binary handle, opened with mode STORAGE_BINARY_HANDLE_OPEN_WRITE
 * @buffer: the data to write to the blob
 * @buffer_size: the size of buffer in bytes
 *
 * Writes the data in @buffer to the blob specified by @handle.
 *
 * Return value: the number of bytes actually written
 **/
gsize
storage_binary_handle_write (StorageBinaryHandle *handle, const char *buffer, gsize buffer_size) {
  /* FIXME: lo_read docs claim the argument is const char *, but header is actually char *...
     it doesn't actually munge the data does it ?!? */
  return lo_write (handle->connection, handle->lo_fd, (char *)buffer, buffer_size);
}

/**
 * storage_binary_handle_read:
 * @handle: the binary handle, opened with mode STORAGE_BINARY_HANDLE_OPEN_READ
 * @buffer: a pre-allocated buffer to read data into
 * @buffer_size: the size of the pre-allocated buffer
 *
 * Reads @buffer_size bytes from the blob into @buffer
 *
 * Return value: the number of bytes actually written into @buffer
 **/
gsize 
storage_binary_handle_read (StorageBinaryHandle *handle, char *buffer, gsize buffer_size) {
  return lo_read (handle->connection, handle->lo_fd, buffer, buffer_size);
}

/**
 * storage_binary_handle_close:
 * @handle: the binary handle to close
 *
 * Closes @handle and frees the memory
 **/
void
storage_binary_handle_close (StorageBinaryHandle *handle) {
  lo_close (handle->connection, handle->lo_fd);
  PQclear(PQexec(handle->connection, "END"));
  _storage_store_release_connection(handle->store, handle->connection);
  g_free (handle);
}
