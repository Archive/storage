#ifndef STORAGE_BINARY_HANDLE_H
#define STORAGE_BINARY_HANDLE_H

#include <glib-object.h>

G_BEGIN_DECLS

typedef struct _StorageBinaryHandle StorageBinaryHandle;

typedef enum
{
  STORAGE_BINARY_HANDLE_OPEN_READ = 1 << 0,
  STORAGE_BINARY_HANDLE_OPEN_WRITE = 1 << 1
} StorageBinaryHandleOpenMode;

gsize  storage_binary_handle_write (StorageBinaryHandle *handle, 
				    const char *buffer, 
				    gsize buffer_size);
gsize  storage_binary_handle_read  (StorageBinaryHandle *handle, 
				    char *buffer, 
				    gsize buffer_size);
void   storage_binary_handle_close (StorageBinaryHandle *handle);

G_END_DECLS


#endif
