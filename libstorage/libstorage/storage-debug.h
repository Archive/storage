/*
 *  Copyright (C) 2003 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef STORAGE_DEBUG_H
#define STORAGE_DEBUG_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>

G_BEGIN_DECLS

#ifndef GNOME_ENABLE_DEBUG
#define DISABLE_LOGGING
#define DISABLE_PROFILING
#endif

#ifdef DISABLE_LOGGING
#define LOG(msg, args...)
#else
#define LOG(msg, args...)						\
g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG,				\
       "[ %s ] " msg,						\
       __FILE__ , ## args);
#endif

#ifdef DISABLE_PROFILING
#define START_PROFILER(name)
#define STOP_PROFILER(name)
#else
#define START_PROFILER(name)	\
storage_profiler_start (name, __FILE__);
#define STOP_PROFILER(name)	\
storage_profiler_stop (name);
#endif

typedef struct
{
	GTimer *timer;
	char *name;
	char *module;
} StorageProfiler;

void		storage_debug_init	(void);

#ifndef DISABLE_PROFILING

void		storage_profiler_start	(const char *name,
					 const char *module);

void		storage_profiler_stop	(const char *name);

#endif

G_END_DECLS

#endif
