#include "storage-item.h"
#include "storage-store.h"
#include "storage-store-private.h"
#include "storage-query-maker.h"
#include "storage-binary-handle-private.h"

#include <stdlib.h>
#include <string.h>

#include <libpq-fe.h>
#include <libpq/libpq-fs.h>

static GObjectClass *parent_class = NULL;

struct _StorageItemPrivate {
	StorageStore *store;
	char *recordid_str;
	GHashTable *attr_cache;
	/* only used if a node */
	int numChildren;
};

enum
{
	PROP_0,
	PROP_STORE,
	PROP_ID
};

#define DEBUG_STORAGE 1

StorageDataType
storage_get_type_from_attribute (char *attrname)
{
	/* here's the list of attrnames to tables */
	if ((strcasecmp(attrname, STORAGE_FILE_ACCESS) == 0) ||
	    (strcasecmp(attrname, STORAGE_FILE_MODIFICATION) == 0) ||
	    (strcasecmp(attrname, STORAGE_FILE_CREATION) == 0) ||
	    (strcasecmp(attrname, STORAGE_FILE_DATE) == 0)) {
		return storageTimeType;
	}

	if ((strcasecmp(attrname, STORAGE_FILE_SIZE) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_BLOB) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_PERM) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_UID) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_GID) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_LENGTH) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_WIDTH) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_HEIGHT) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_BITRATE) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_DURATION) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_PAGES) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_YEAR) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_TRACK) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_THUMBNAIL) == 0) ||
     	    (strcasecmp(attrname, STORAGE_FILE_PIXELDEPTH) == 0)) {
		return storageIntType;
	} 

	return storageStringType;
}

static void
init (StorageItem *rec)
{
#if DEBUG_STORAGE
	printf ("Initializing\n");
#endif
	rec->priv = g_new (StorageItemPrivate, 1);
	rec->priv->recordid_str = NULL;
	rec->priv->numChildren = 0;
}

static void
storage_item_cache_soup (StorageItem *item, const char *soup)
{
	PGresult * results;
	int i;

	results = storage_dbl_query_maker (item->priv->store, GetAllAttrs, 
					   soup, item->priv->recordid_str);
  
	for(i = 0; i < PQntuples(results); ++i) {
#if DEBUG_STORAGE
		printf("caching %s %s: %s = %s\n", soup, item->priv->recordid_str,
    			PQgetvalue(results, i, 0),PQgetvalue(results, i, 1));
#endif
	        if (g_hash_table_lookup(item->priv->attr_cache,
		    PQgetvalue(results, i, 0)) == NULL) {
			g_hash_table_insert (item->priv->attr_cache,
			                     g_strdup (PQgetvalue (results, i, 0)),
                                             g_strdup (PQgetvalue (results, i, 1)));
		}
  	}

	PQclear(results);
}

/**
 * storage_item_cache_all_attributes:
 * @item: a #StorageItem
 *
 * Cache all attributes of an item to make them
 * fastly accessible.
 **/
void
storage_item_cache_all_attributes (StorageItem *item)
{
	storage_item_cache_soup(item, ATTR_SOUP);
	storage_item_cache_soup(item, NUMBER_SOUP);
	storage_item_cache_soup(item, DATE_SOUP);
}

/**
 * storage_item_new:
 * @id: record identifier as string
 *
 * Creates a new #StorageItem object from the record identifier.
 *
 * Return value: a new #StorageItem.
 **/
GObject *
storage_item_new (StorageStore *store, const char *id)
{
	StorageItem *item;

	g_assert(STORAGE_IS_STORE(store));

	item = g_object_new (STORAGE_TYPE_ITEM,
		             "store", G_OBJECT(store),
		             "record_id", id,
		             NULL);

	return G_OBJECT(item);
}

/**
 * storage_item_get_n_parents:
 * @item: a #StorageItem
 *
 * Returns the number of the item parents.
 *
 * Return value: the number of the item parents.
 **/
guint
storage_item_get_n_parents(StorageItem *item)
{
	PGresult * results;
	int num;

	results = storage_query_maker (item->priv->store, NumParents,
				       item->priv->recordid_str);
	num = atoi (PQgetvalue(results, 0, 0));
	PQclear (results);

	return num;
}

/**
 * storage_item_get_parents:
 * @item: a #StorageItem
 *
 * Returns item parents.
 *
 * Return value: a #GSList of #StorageItem.
 **/
GSList *
storage_item_get_parents (StorageItem *item)
{
	PGresult * results;
	int i;
	GSList *parentList = NULL;
	char *recordid_str;

	results = storage_query_maker (item->priv->store, GetParents,
				       item->priv->recordid_str);

	for(i = PQntuples(results) - 1; i >= 0; i--) {
		StorageItem *parent_item;

		recordid_str = PQgetvalue (results, i, 0);
		parent_item = STORAGE_ITEM (storage_item_new(item->priv->store, recordid_str));
		parentList = g_slist_append (parentList, parent_item);
	}

	PQclear(results);

	return parentList;
}

/**
 * storage_item_get_approx_size:
 * @item: a #StorageItem
 *
 * Returns the approximative size of the item.
 *
 * Return value: approximative size in bytes.
 **/
guint
storage_item_get_approx_size (StorageItem *item)
{
	PGresult * results;
	int size;

	results = storage_query_maker (item->priv->store, GetSize,
				       item->priv->recordid_str);
	if(PQntuples(results) < 1) {
		size = atoi(PQgetvalue(results, 0, 0));
	} else {
		g_print ("No size attribute for recordid=%s\n",
			 item->priv->recordid_str);
		size = 0;
	}

	PQclear(results);

	return size;
}

/**
 * storage_item_get_id:
 * @item: a #StorageItem
 *
 * Returns the item identifier.
 *
 * Return value: the record identifier as a string.
 **/
char *
storage_item_get_id (StorageItem *item)
{
	return item->priv->recordid_str;
}

/**
 * storage_item_get_uri:
 * @item: a #StorageItem
 *
 * Returns the item uri.
 *
 * Return value: the item uri.
 **/
char *
storage_item_get_uri (StorageItem *item)
{
	char *store_uri, *uri;

	store_uri = storage_store_get_uri (item->priv->store);

	uri = g_strconcat (store_uri, "items/", item->priv->recordid_str, NULL);

	g_free (store_uri);

	return uri;
}

/**
 * storage_item_write_entire_binary:
 * @item: a #StorageItem
 * @buffer: bytes to be written to the item's blob, not NULL terminated
 * @buffer_size: amount of data to read from @buffer
 **/
void
storage_item_write_entire_binary (StorageItem *item, char *buffer, gsize buffer_size)
{
  PGconn *connection;
  char *blobid_str = NULL;
  char *end_blobid = NULL;
  Oid blobid;
  int fd;

  connection = _storage_store_get_connection (item->priv->store);

  PQclear(PQexec(connection, "BEGIN"));

  blobid_str = storage_item_get_attribute(item, STORAGE_FILE_BLOB);
  blobid  = strtol(blobid_str, &end_blobid, 10);
  
  if(end_blobid == blobid_str + strlen(blobid_str)) {
    g_print ("Found an existing blob, unlinking\n");
    lo_unlink(connection, blobid);
  }

  blobid = lo_creat(connection, INV_READ | INV_WRITE);

  fd = lo_open(connection, blobid, INV_WRITE);

  /* FIXME: do error checking on return values? */
  lo_write(connection, fd, buffer, buffer_size);
  lo_close(connection, fd);

  PQclear(PQexec(connection, "END"));

  g_free(blobid_str);
}

/**
 * storage_item_read_entire_binary:
 * @item: a #StorageItem
 *
 * Read the whole item data.
 *
 * Return value: An array of bytes.
 **/
char *
storage_item_read_entire_binary (StorageItem *item, gsize *binary_size)
{
	PGconn *connection;
	Oid blobid;
	int fd;
	int result;
	char *buffer = NULL;
	char *blobid_str = NULL;
	char *end_blobid = NULL;

	g_print ("Trying to read binary\n");
  
	if (binary_size != NULL) *binary_size = 0;

 	connection = _storage_store_get_connection (item->priv->store);

	blobid_str = storage_item_get_attribute(item, STORAGE_FILE_BLOB);
	blobid  = strtol(blobid_str, &end_blobid, 10);

	if(end_blobid == blobid_str + strlen(blobid_str)) {
 		g_print ("Reading from blobid %d\n", blobid);
 		PQclear(PQexec(connection, "BEGIN"));
		fd = lo_open(connection, blobid, INV_READ);
    
		result = lo_lseek(connection, fd, 0, SEEK_END);
		if (result >= 0) {
			int size = result;

			result = lo_tell(connection, fd);
			if (result >= 0) {
				result = lo_lseek(connection, fd, 0, SEEK_SET);
				if (result >= 0) {
					g_print ("Reading %d bytes\n", size);
	  
					buffer = malloc(sizeof(char) * size);
					result = lo_read(connection, fd, buffer, size);
					if (result >= 0) {
						if (binary_size != NULL) *binary_size = result;
					}
				}
			}
		}
    
		PQclear(PQexec(connection, "END"));
	}

	_storage_store_release_connection(item->priv->store, connection);

	g_free(blobid_str);

	return buffer;
}

/**
 * storage_item_get_binary_handle:
 * @item: a #StorageItem, must be of type STORAGE_ITEM_TYPE_BINARY
 *
 * Returns a #StorageBinaryHandle associated with @item
 *
 * Return value: a #StorageBinaryHandle which must be close with storage_binary_handle_close()
 **/
StorageBinaryHandle *
storage_item_get_binary_handle (StorageItem *item, StorageBinaryHandleOpenMode mode) {
  Oid blobid;
  char *blobid_str = NULL;
  char *end_blobid = NULL;	
  StorageBinaryHandle *returnval = NULL;

  blobid_str = storage_item_get_attribute(item, STORAGE_FILE_BLOB);
  blobid  = strtol(blobid_str, &end_blobid, 10);

  if(end_blobid == blobid_str + strlen(blobid_str)) {
    returnval = storage_binary_handle_open (item->priv->store, blobid, mode);
  }

  g_free (blobid_str);

  return returnval;
}

/**
 * storage_item_get_vfs_handle:
 * @item: a #StorageItem
 *
 * Returns a #GnomeVFSHandle to the item data. Useful if you do not
 * want to read the whole binary at once.
 *
 * Return value: the #GnomeVFSHandle.
 **/
GnomeVFSHandle *
storage_item_get_vfs_handle (StorageItem *item, GnomeVFSOpenMode mode)
{
	char *uri;
	GnomeVFSResult result;
	GnomeVFSHandle *handle;

	if (!gnome_vfs_init ()) {
		g_print ("Cannot initialize the GNOME Virtual File System.\n");
		return NULL;
	}

	uri = storage_item_get_uri(item);

	result = gnome_vfs_open (&handle, uri, mode);

	/* FIXME: disregard checking */

	return handle;
}

/**
 * storage_item_n_children:
 * @item: a #StorageItem
 *
 * Returns the number of the item children.
 *
 * Return value: the number of the item children.
 **/
guint
storage_item_n_children (StorageItem *item)
{
	return item->priv->numChildren;
}

/**
 * storage_item_get_children:
 * @item: a #StorageItem
 *
 * Returns item parents.
 *
 * Return value: a #GSList of #StorageItem.
 * The list should be freed with g_list_free() and the items
 * with g_object_unref().
 **/
GSList *
storage_item_get_children (StorageItem *item)
{
	PGresult * results;
	int i;
	GSList *childList = NULL;
	char *recordid_str;

	results = storage_dbl_query_maker (item->priv->store,GetSortedChildren,
					   item->priv->recordid_str, "");

	for(i = PQntuples(results) - 1; i >= 0; i--) {
		StorageItem *child_item;

		recordid_str = PQgetvalue(results, i, 0);
		child_item = STORAGE_ITEM (storage_item_new(item->priv->store, recordid_str));
 		childList = g_slist_append(childList, child_item);
	}

	PQclear(results);

	return childList;
}

/**
 * storage_item_get_nth_child:
 * @item: a #StorageItem
 * @childNum: the child number
 *
 * Returns a child by number
 *
 * Return value: a #StorageItem.
 **/
StorageItem *
storage_item_get_nth_child (StorageItem *item, guint childNum)
{
	PGresult * results;
	char numBuff[50];
	StorageItem *child;

	sprintf (numBuff, "%d", childNum);

	results = storage_dbl_query_maker (item->priv->store,GetNthChild,
					   item->priv->recordid_str, numBuff);

	if(PQntuples(results) < 1) {
		g_print ("Tried to access the %d child in the future\n", childNum);
		return NULL;
	}

	child = STORAGE_ITEM (storage_item_new(item->priv->store, numBuff));

	PQclear(results);

	return child;
}

/**
 * storage_item_sever_nth_child:
 * @item: a #StorageItem
 * @childNum: the child number
 *
 * Unlink a child from one of his parents.
 *
 * Return value:
 **/
int
storage_item_sever_nth_child (StorageItem *item, int childNum)
{
	char numBuf[50];

	if(childNum == -1) {
		childNum = item->priv->numChildren;
	}

	if (childNum < 0 || childNum >= item->priv->numChildren) {
		g_warning ("Can't sever childnum=%d in recordid=%s\n",
			   childNum, item->priv->recordid_str);
 		return -1;
	}

	sprintf(numBuf, "%d", childNum);

	PQclear(storage_dbl_query_maker(item->priv->store, SeverNthChild,
		item->priv->recordid_str, numBuf));
	PQclear(storage_dbl_query_maker(item->priv->store,
		DecrementLinkNumsGreaterThan, item->priv->recordid_str, numBuf));
  
	item->priv->numChildren--;
	return 0;
}

/**
 * storage_item_sever_nth_child:
 * @item: a #StorageItem
 * @childNum: the child number
 *
 * Insert an item child.
 *
 * Return value:
 **/
int 
storage_item_insert_child (StorageItem *item, StorageItem *child, int childNum)
{
	/* FIXME: checks to make sure the record structure in the DB is still a DAG */
	char numBuf[50];

	if(childNum == -1) {
		childNum = item->priv->numChildren;
	}

	if (childNum < 0 || childNum > item->priv->numChildren) {
		g_print ("Can't insert childnum=%d in recordid=%s\n",
			 childNum, item->priv->recordid_str);
		return -1;
	}

	sprintf(numBuf, "%d", childNum);

	PQclear (storage_dbl_query_maker (item->priv->store, IncrementLinkNumsAsBigAs,
					  item->priv->recordid_str, numBuf));
	PQclear (storage_quad_query_maker (item->priv->store,ConnectNthChild,
					   item->priv->recordid_str, 
				           child->priv->recordid_str, numBuf, ""));
  
	item->priv->numChildren++;

	return 0;
}

/**
 * storage_item_n_attributes:
 * @item: a #StorageItem
 *
 * Returns the number of the item attributes.
 *
 * Return value: the number of the item attributes.
 **/
guint
storage_item_n_attributes (StorageItem *item)
{
	PGresult * results;
	int num = 0;

	results = storage_dbl_query_maker (item->priv->store, GetNumAttrs,
					   DATE_SOUP, item->priv->recordid_str);
	num += atoi (PQgetvalue (results, 0, 0));
	PQclear (results);

	results = storage_dbl_query_maker (item->priv->store, GetNumAttrs,
					   NUMBER_SOUP, item->priv->recordid_str);
	num += atoi (PQgetvalue (results, 0, 0));
	PQclear(results);

	results = storage_dbl_query_maker (item->priv->store, GetNumAttrs,
					   ATTR_SOUP, item->priv->recordid_str);
	num += atoi (PQgetvalue (results, 0, 0));
	PQclear(results);

	return num;
}

/**
 * storage_item_get_attributes_pairs:
 * @item: a #StorageItem
 *
 * Returns all the attributes pairs for an item.
 *
 * Return value: a list of #StorageAttrPair.
 * The list should be freed with g_list_free().
 **/
GSList *
storage_item_get_attribute_pairs (StorageItem *item)
{
	PGresult * results;
	int i;
	StorageAttrPair *pair;
	GSList *attrList  = NULL;

	results = storage_dbl_query_maker (item->priv->store, GetAllAttrs,
					   DATE_SOUP, item->priv->recordid_str);
	for(i = PQntuples (results) - 1; i >= 0; i--) {
		pair = g_new (StorageAttrPair, 1);
		pair->name = strdup (PQgetvalue(results, i, 0));
		pair->value = strdup (PQgetvalue (results, i, 1));
		attrList = g_slist_append(attrList,pair);
	}
	PQclear(results);

	results = storage_dbl_query_maker (item->priv->store, GetAllAttrs,
					   NUMBER_SOUP, item->priv->recordid_str);
	for (i = PQntuples(results) - 1; i >= 0; i--) {
		pair = g_new (StorageAttrPair, 1);
	 	pair->name = strdup (PQgetvalue (results, i, 0));
		pair->value = strdup (PQgetvalue (results, i, 1));
		attrList = g_slist_append (attrList,pair);
	}
	PQclear(results);

	results = storage_dbl_query_maker (item->priv->store, GetAllAttrs,
					   ATTR_SOUP, item->priv->recordid_str);
	for(i = PQntuples(results) - 1; i >= 0; i--) {
		pair = g_new (StorageAttrPair, 1);
 		pair->name = strdup (PQgetvalue (results, i, 0));
		pair->value = strdup (PQgetvalue (results, i, 1));
		attrList = g_slist_append(attrList,pair);
	}
	PQclear(results);

	return attrList;
}

static char *
storage_get_table_from_type (StorageDataType type)
{
	switch (type) {
	case storageTimeType:
		return strdup (DATE_SOUP);
	case storageIntType:
		return strdup (NUMBER_SOUP);
	case storageStringType:
		return strdup (ATTR_SOUP);
	default:
		g_warning ("Can't get table for type %d\n", (int)type);
	}

	return strdup (ATTR_SOUP);
}

/**
 * storage_item_has_attribute:
 * @item: a #StorageItem
 * @attrName: the attribute name
 *
 * Returns %TRUE if an item has the attribute.
 *
 * Return value: %TRUE if the item has the attribute.
 **/
gboolean
storage_item_has_attribute (StorageItem *item, const char *attrName)
{
	PGresult * results;
	int count, size;
	char *attrNameEsc, *tableName;

	/* escape the passed in string */
	size = strlen (attrName);
	attrNameEsc = g_new (gchar,2*size + 1);
	PQescapeString (attrNameEsc, attrName, size);

	tableName = storage_get_table_from_type
		(storage_get_type_from_attribute (attrNameEsc));
	results = storage_tri_query_maker (item->priv->store,CountAttr, tableName,
					   attrNameEsc, item->priv->recordid_str);
	count = atoi(PQgetvalue(results, 0, 0));
	PQclear(results);

	g_free(attrNameEsc);

	return (count != 0);
}

void 
storage_item_set_attribute_gvalue (StorageItem *item,
				   const char *attrName,
				   const GValue *val)
{
  char *string_value = NULL;
  gint i;
  guint u;
  gdouble d;
  gboolean b;

  switch (G_VALUE_TYPE (val)) {
  case G_TYPE_STRING:
      string_value = g_strdup(g_value_get_string (val));
      break;
  case G_TYPE_BOOLEAN:
    b = g_value_get_boolean (val);
    if (b == TRUE) {
      string_value = g_strdup("T");
    } else {
      string_value = g_strdup("F");
    }
    break;
  case G_TYPE_UINT:
    u = g_value_get_uint(val);
    string_value = g_strdup_printf("%u", u);
    break;
  case G_TYPE_INT:
    i = g_value_get_int (val);
    string_value = g_strdup_printf("%d", i);
    break;
  case G_TYPE_DOUBLE:
    d = g_value_get_double (val);
    string_value = g_strdup_printf("%f", d);
    break;
  default:
    g_print ("Cannot handle GValue of type %s\n",
	     G_VALUE_TYPE_NAME (val));
  }

  if (string_value != NULL) {
    storage_item_set_attribute(item, attrName, string_value);
    g_free(string_value);
  }
}

/**
 * storage_item_get_attribute:
 * @item: a #StorageItem
 * @attrName: the attribute name
 *
 * Returns the value of an attribute.
 *
 * Return value: the attribute value as a string.
 * It should be freed with g_free().
 **/
char *
storage_item_get_attribute (StorageItem *item, const char *attrName)
{
	char *attrValue;
  
	attrValue = g_hash_table_lookup (item->priv->attr_cache, attrName);
	if (attrValue != NULL) {
		attrValue = g_strdup (attrValue);
		return attrValue;
	}
  
	return NULL;
}

/**
 * storage_item_set_attribute:
 * @item: a #StorageItem
 * @attrName: the attribute name
 * @new_attrValue: the new attribute value
 *
 * Set the value of an attribute.
 *
 **/
void
storage_item_set_attribute (StorageItem *item, const gchar *attrName,
				  const gchar *new_attrValue)
{
	PGresult * results;
  	char *attrNameEsc, *new_attrValueEsc, *tableName;
	int nameSize, valueSize;

	/*  escape the passed in value */
	valueSize = strlen (new_attrValue);
	new_attrValueEsc = g_new (gchar, 2*valueSize + 1);
	PQescapeString (new_attrValueEsc, new_attrValue, valueSize);
    
	/* see if attribute has special formatting */
	if(strcmp (attrName, STORAGE_FILE_TOP) == 0) {
		if ((new_attrValueEsc[0] == 't') ||
		    (new_attrValueEsc[0] == 'T') ||
		    (new_attrValueEsc[0] == 'f') ||
		    (new_attrValueEsc[0] == 'F')) {
			attrNameEsc = g_strdup(attrName);
			new_attrValueEsc[1] = '\0';
		} else {
			g_print ("Can't write a non-boolean top_level attribute for recordid=%s\n",
				 item->priv->recordid_str);
			g_free(new_attrValueEsc);
			return;
		}

	} else {
		/* escape the passed in name */
		nameSize = strlen (attrName);
 		attrNameEsc = g_new (gchar,2*nameSize + 1);
		PQescapeString (attrNameEsc, attrName, nameSize);
	}

	/* try and just set it in the beginning */
	tableName = storage_get_table_from_type
		(storage_get_type_from_attribute (attrNameEsc));
	results = storage_quad_query_maker (item->priv->store, SetAttr,
					    tableName, new_attrValueEsc,
					    attrNameEsc, item->priv->recordid_str);
  
	if (atoi (PQcmdTuples(results)) < 1) {
		/* didn't previously exist, so insert */
		PQclear(results);
		PQclear (storage_quint_query_maker (item->priv->store, InsertNewAttr,
						    tableName, item->priv->recordid_str,
						    attrNameEsc, new_attrValueEsc,""));
	}

	g_hash_table_insert (item->priv->attr_cache,
			     g_strdup(attrName),
			     g_strdup(new_attrValue));

	g_free(attrNameEsc);
	g_free(new_attrValueEsc);
}

/**
 * storage_item_delete_attribute:
 * @item: a #StorageItem
 * @attrName: the attribute name
 *
 * Delete an item attribute.
 *
 **/
void
storage_item_delete_attribute (StorageItem *item, const gchar *attrName)
{
	PGresult * results;
	int size;
	char *attrNameEsc, *tableName;

	/* escape the passed in name */
	size = strlen (attrName);
	attrNameEsc = g_new (gchar, 2 * size + 1);
	PQescapeString (attrNameEsc, attrName, size);

	tableName = storage_get_table_from_type
		(storage_get_type_from_attribute (attrNameEsc));
	results = storage_tri_query_maker (item->priv->store, DelAttr,
					   tableName, attrNameEsc,
					   item->priv->recordid_str);
	PQclear(results);
}

/* function we use for freeing the keys and data in the hash before
   we destroy the hash */
static void
free_key_value (gpointer key, gpointer value, gpointer user_data)
{
        g_free(key);
        g_free(value);
}

static void
finalize (GObject *object)
{
	StorageItem *rec;

	rec = STORAGE_ITEM(object);

	g_return_if_fail (rec->priv != NULL);

	if (rec->priv->recordid_str) {
		g_free (rec->priv->recordid_str);
	}

	if (rec->priv->attr_cache) {
		g_hash_table_foreach (rec->priv->attr_cache, free_key_value, NULL);
		g_hash_table_destroy (rec->priv->attr_cache);
	}

	g_free (rec->priv);
	rec->priv = NULL;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
storage_item_set_store (StorageItem *item, GObject *store)
{
	item->priv->store = STORAGE_STORE (store);
}

static void
storage_item_set_id (StorageItem *item, const char *recordid)
{
	PGresult * results;

	item->priv->recordid_str = g_strdup (recordid);
	item->priv->attr_cache = g_hash_table_new (g_str_hash, g_str_equal);

	/* FIXME we can probably avoid this if we are creating a new item */
	/* if its a node we need to set its numChildren */
	results = storage_query_maker (item->priv->store, IsNode, item->priv->recordid_str);

	if (atoi (PQgetvalue (results, 0, 0)) > 0) {
		PQclear(results);
 		results = storage_query_maker (item->priv->store, NumChildren,
					       item->priv->recordid_str);
    		item->priv->numChildren = atoi (PQgetvalue(results, 0, 0));
		PQclear (results);
	} else {
		PQclear (results);
	}
}

static void
storage_item_set_property (GObject *object,
                           guint prop_id,
                           const GValue *value,
                           GParamSpec *pspec)
{
        StorageItem *item = STORAGE_ITEM (object);
                                                                                                                             
        switch (prop_id) {
                case PROP_STORE:
			storage_item_set_store (item, g_value_get_object (value));
                        break;
		case PROP_ID:
			storage_item_set_id (item, g_value_get_string (value));
			break;
        }
}

static void
storage_item_get_property (GObject *object,
                           guint prop_id,
                           GValue *value,
                           GParamSpec *pspec)
{
        StorageItem *item = STORAGE_ITEM (object);
                                                                                                                             
        switch (prop_id) {
                case PROP_STORE:
                        g_value_set_object (value, item->priv->store);
                        break;
		case PROP_ID:
			g_value_set_string (value, item->priv->recordid_str);
			break;
	}
}

static void
class_init (StorageItemClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = finalize;
	object_class->set_property = storage_item_set_property;
	object_class->get_property = storage_item_get_property;

	parent_class = g_type_class_peek_parent (klass);

	g_object_class_install_property (object_class,
					 PROP_ID,
					 g_param_spec_string ("record_id",
							      "Record Id",
							      "The record identifier",
							      NULL,
							      G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
                                         PROP_STORE,
                                         g_param_spec_object ("store",
                                                              "StorageStore",
                                                              "The storage store object",
							      STORAGE_TYPE_STORE,
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT_ONLY));
}

GType
storage_item_get_type (void)
{
        static GType type = 0;

        if (type == 0) {
                GTypeInfo info = {
                        sizeof (StorageItemClass),
                        NULL, /* GBaseInitFunc */
			NULL, /* GBaseFinalizeFunc */
			(GClassInitFunc) class_init, 
			NULL, /* GClassFinalizeFunc */
			NULL, /* user supplied data */
                        sizeof (StorageItem), 
			0, /* n_preallocs */
			(GInstanceInitFunc) init,
			NULL
                };

                type = g_type_register_static (G_TYPE_OBJECT, "StorageItem", &info, (GTypeFlags)0);
        }

        return type;
}

GType
storage_item_type_get_type (void)
{
	static GType etype = 0;

	if (etype == 0) {
		static const GEnumValue values[] = {
			{ STORAGE_ITEM_TYPE_NODE, "STORAGE_ITEM_TYPE_NODE", "node" },
			{ STORAGE_ITEM_TYPE_TEXT, "STORAGE_ITEM_TYPE_TEXT", "text" },
			{ STORAGE_ITEM_TYPE_BINARY, "STORAGE_ITEM_TYPE_BINARY", "binary" },
			{ 0, NULL, NULL }
		};
 		etype = g_enum_register_static ("StorageItemType", values);
	}
	return etype;
}

GType
storage_data_type_get_type (void)
{
	static GType etype = 0;
	if (etype == 0) {
		static const GEnumValue values[] = {
			{ storageStringType, "STORAGE_STRING_TYPE", "string" },
			{ storageIntType, "STORAGE_INT_TYPE", "int" },
			{ storageTimeType, "STORAGE_TIME_TYPE", "time" },
			{ 0, NULL, NULL }
		};
		etype = g_enum_register_static ("StorageDataType", values);
	}
	return etype;
}
