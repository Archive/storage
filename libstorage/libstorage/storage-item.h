#ifndef STORAGE_ITEM_H
#define STORAGE_ITEM_H

#include <glib/gerror.h>
#include <glib-object.h>
#include <libgnomevfs/gnome-vfs.h>
  
#include <libstorage/storage-binary-handle.h>

G_BEGIN_DECLS

/* predefined list of attributes */
#define STORAGE_FILE_NAME "storage:title"
#define STORAGE_FILE_SIZE "storage:size"
#define STORAGE_FILE_TEXT "storage:content"
#define STORAGE_FILE_MIME "storage:mimetype"
#define STORAGE_FILE_BLOB "storage:blobid"
#define STORAGE_FILE_PERM "storage:permissions"
#define STORAGE_FILE_ACCESS "storage:access_time"
#define STORAGE_FILE_MODIFICATION "storage:modification_time"
#define STORAGE_FILE_CREATION "storage:creation_time"

#define STORAGE_FILE_DATE "storage:date"
#define STORAGE_FILE_LENGTH "storage:length"
#define STORAGE_FILE_WIDTH "storage:width"
#define STORAGE_FILE_HEIGHT "storage:height"
#define STORAGE_FILE_BITRATE "storage:bitrate"
#define STORAGE_FILE_DURATION "storage:duration"
#define STORAGE_FILE_PAGES "storage:pages"
#define STORAGE_FILE_YEAR "storage:year"
#define STORAGE_FILE_TRACK "storage:track"
#define STORAGE_FILE_THUMBNAIL "storage:thumbnail"
#define STORAGE_FILE_PIXELDEPTH "storage:pixeldepth"

#define STORAGE_TYPE_ITEM		(storage_item_get_type ())
#define STORAGE_ITEM(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), STORAGE_TYPE_ITEM, StorageItem))
#define STORAGE_ITEM_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), STORAGE_TYPE_ITEM, StorageItemClass))
#define STORAGE_IS_ITEM(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), STORAGE_TYPE_ITEM))
#define STORAGE_IS_ITEM_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), STORAGE_TYPE_ITEM))
#define STORAGE_ITEM_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), STORAGE_TYPE_ITEM, StorageItemClass))

typedef struct _StorageItemPrivate StorageItemPrivate;

typedef struct _StorageAttrPair StorageAttrPair;
typedef struct _StorageItem StorageItem;
typedef struct _StorageItemClass StorageItemClass;

typedef enum
{
  STORAGE_ITEM_TYPE_NODE,
  STORAGE_ITEM_TYPE_TEXT,
  STORAGE_ITEM_TYPE_BINARY
} StorageItemType;

GType storage_item_type_get_type (void);

#define STORAGE_TYPE_ITEM_TYPE (storage_item_type_get_type ())

typedef enum
{
  storageStringType,
  storageIntType,
  storageTimeType
} StorageDataType;

GType storage_data_type_get_type (void);

#define STORAGE_TYPE_DATA_TYPE (storage_data_type_get_type ())

struct _StorageAttrPair
{
  char *name;
  char *value;
};

struct _StorageItem
{
  GObject parent_instance;
  StorageItemPrivate *priv;
};

struct _StorageItemClass
{
  GObjectClass parent_class;
};

#include <libstorage/storage-store.h>

GType		storage_item_get_type		  (void);

GObject        *storage_item_new		  (StorageStore *store,
						   const char *id);

char           *storage_item_get_uri		  (StorageItem *item);
char           *storage_item_get_id		  (StorageItem *item);

guint           storage_item_get_n_parents	  (StorageItem *item);
GSList*         storage_item_get_parents	  (StorageItem *item);
int             storage_item_insert_child	  (StorageItem *item,
					           StorageItem *child,
					           int childNum);
guint           storage_item_n_children		  (StorageItem *item);
GSList         *storage_item_get_children	  (StorageItem *item);
StorageItem    *storage_item_get_nth_child	  (StorageItem *item,
					 	   guint childNum);
int             storage_item_sever_nth_child      (StorageItem *item,
					           int childNum);

guint		storage_item_n_attributes	  (StorageItem *item);
GSList         *storage_item_get_attribute_pairs (StorageItem *item);
gboolean	storage_item_has_attribute	  (StorageItem *item,
					 	   const char *attrName);
char	       *storage_item_get_attribute        (StorageItem *item,
					 	   const char *attrName);
void		storage_item_set_attribute        (StorageItem *item,
					 	   const char *attrName,
					 	   const char *new_attrValue);
void            storage_item_set_attribute_gvalue (StorageItem *item,
						   const char *attrName,
						   const GValue *new_attrValue);
void		storage_item_delete_attribute	  (StorageItem *item,
					 	   const char *attrName);
void		storage_item_cache_all_attributes (StorageItem *item);

guint           storage_item_get_approx_size	  (StorageItem *item);
void            storage_item_write_entire_binary  (StorageItem *item, 
						   char *buffer, 
						   gsize buffer_size);
char           *storage_item_read_entire_binary   (StorageItem *item,
					           gsize *result_binary_size);
StorageBinaryHandle *storage_item_get_binary_handle (StorageItem *item, 
						     StorageBinaryHandleOpenMode mode);
GnomeVFSHandle *storage_item_get_vfs_handle	  (StorageItem *item, 
						   GnomeVFSOpenMode mode);

StorageDataType storage_get_type_from_attribute   (char *attrname);

/* Some stuff we are unsure. Do not use */

#define STORAGE_FILE_TOP "storage:top_level" /* must pass it a value of "[T|t].*" or "[F|f].*" */
#define STORAGE_FILE_UID "storage:uid"
#define STORAGE_FILE_GID "storage:gid"

G_END_DECLS

#endif /* GNOME_STORAGE_INTERFACE_H */
