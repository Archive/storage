#include "storage-query-maker.h"

#include "storage-store-private.h"

#include <libpq-fe.h>
#include <string.h>

/* 1 part queries */
#define STORAGE_IS_NODE_PREFIX "SELECT COUNT(*) FROM NodeChildren WHERE parent = "
#define STORAGE_IS_TEXT_PREFIX "SELECT COUNT(*) FROM TextRecords WHERE recordID = "

#define STORAGE_FIND_PARENTS_PREFIX "SELECT parent FROM NodeChildren WHERE child = "
#define STORAGE_NUM_PARENTS_PREFIX "SELECT COUNT(*) FROM NodeChildren WHERE child = ";

#define STORAGE_NUM_CHILDREN_PREFIX "SELECT COUNT(*) FROM NodeChildren WHERE parent = ";
#define STORAGE_GET_CHILDREN_PREFIX "SELECT child FROM NodeChildren WHERE parent = "

#define STORAGE_DEL_CHILD_LINKS_PREFIX "DELETE FROM NodeChildren WHERE parent = "
#define STORAGE_DEL_PARENT_LINKS_PREFIX "DELETE FROM NodeChildren WHERE child = "
#define STORAGE_DEL_TEXT_PREFIX "DELETE FROM TextRecords WHERE recordid = "

#define STORAGE_GETBLOBID_PREFIX "SELECT attrValue FROM NumberSoup WHERE attrName = '" STORAGE_FILE_BLOB "' AND recordID = "

#define STORAGE_IS_TOP_LEVEL_PREFIX "SELECT attrValue FROM AttrSoup WHERE attrName = '" STORAGE_FILE_TOP "' AND recordID = "
#define STORAGE_SIZE_PREFIX "SELECT attrValue FROM NumberSoup WHERE attrName = '" STORAGE_FILE_SIZE "' AND recordID = "


#define STORAGE_UPDATE_ACCESS_PREFIX "UPDATE DateSoup SET attrValue = CURRENT_TIMESTAMP WHERE attrName = '" STORAGE_FILE_ACCESS "' AND recordID = "
#define STORAGE_UPDATE_MODIFICATION_PREFIX "UPDATE DateSoup SET attrValue = CURRENT_TIMESTAMP WHERE attrName = '" STORAGE_FILE_MODIFICATION "' AND recordID = "

#define STORAGE_DEL_AVAILABLE_PREFIX "DELETE FROM AvailableRecordIDs WHERE recordID = "

/* 2 part queries */
#define STORAGE_GET_SORTED_CHILDREN_PREFIX1 "SELECT child from NodeChildren WHERE parent =  "
#define STORAGE_GET_SORTED_CHILDREN_PREFIX2 " ORDER BY linkNum"

#define STORAGE_GET_NTH_CHILD_PREFIX1 "SELECT child FROM NodeChildren WHERE parent = "
#define STORAGE_GET_NTH_CHILD_PREFIX2 " AND linkNum = "

#define STORAGE_SEVER_NTH_CHILD_PREFIX1 "DELETE FROM NodeChildren WHERE parent = "
#define STORAGE_SEVER_NTH_CHILD_PREFIX2 " AND linkNum = "

#define STORAGE_INC_LINKNUMS_ABA_PREFIX1 "UPDATE NodeChildren SET linkNum = linkNum + 1 WHERE parent = "
#define STORAGE_INC_LINKNUMS_ABA_PREFIX2 " AND linkNum >= "//some linkNum

#define STORAGE_DEC_LINKNUMS_GT_PREFIX1 "UPDATE NodeChildren SET linkNum = linkNum - 1 WHERE parent = "
#define STORAGE_DEC_LINKNUMS_GT_PREFIX2 " AND linkNum > "//some linkNum


#define STORAGE_INSERT_NEW_TEXT_PREFIX1 "INSERT INTO TextRecords (recordid) VALUES (" //recordid
#define STORAGE_INSERT_NEW_TEXT_PREFIX2 ")"

#define STORAGE_SET_TEXT_PREFIX1 "UPDATE TextRecords SET content = '"
#define STORAGE_SET_TEXT_PREFIX2 "' WHERE recordID = "

#define STORAGE_NUM_ATTRS_PREFIX1 "SELECT COUNT(*) FROM "
#define STORAGE_NUM_ATTRS_PREFIX2 " WHERE recordId = "

#define STORAGE_GET_ALL_ATTRS_PREFIX1  "SELECT attrName, attrValue FROM "
#define STORAGE_GET_ALL_ATTRS_PREFIX2  " WHERE recordID = "

#define STORAGE_ATTRS_DELETE_PREFIX1 "DELETE FROM "
#define STORAGE_ATTRS_DELETE_PREFIX2 " WHERE recordID = "

/* 3 part queries */
#define STORAGE_GET_ATTR_PREFIX1 "SELECT attrValue FROM "
#define STORAGE_GET_ATTR_PREFIX2 " WHERE AttrName = '"
#define STORAGE_GET_ATTR_PREFIX3 "' and recordID = "

#define STORAGE_COUNT_ATTR_PREFIX1 "SELECT COUNT(*) FROM "
#define STORAGE_COUNT_ATTR_PREFIX2 " WHERE AttrName = '"
#define STORAGE_COUNT_ATTR_PREFIX3 "' and recordID = "

#define STORAGE_DEL_ATTR_PREFIX1 "DELETE FROM "
#define STORAGE_DEL_ATTR_PREFIX2 " WHERE AttrName = '"
#define STORAGE_DEL_ATTR_PREFIX3 "' and recordID = "

/* 4 part queries */
#define STORAGE_UPDATE_ATTR_PREFIX1 "UPDATE " //eg AttrSoup
#define STORAGE_UPDATE_ATTR_PREFIX2 " SET AttrValue = '"
#define STORAGE_UPDATE_ATTR_PREFIX3 "' WHERE AttrName = '"
#define STORAGE_UPDATE_ATTR_PREFIX4 "' and recordID = "

#define STORAGE_CONNECT_NTH_CHILD_PREFIX1 "INSERT INTO NodeChildren VALUES(" //parent
#define STORAGE_CONNECT_NTH_CHILD_PREFIX2 "," //child
#define STORAGE_CONNECT_NTH_CHILD_PREFIX3 "," //linkNum
#define STORAGE_CONNECT_NTH_CHILD_PREFIX4 ")" // ""

/* 5 part queries */
#define STORAGE_INSERT_NEW_ATTR_PREFIX1 "INSERT INTO " //eg AttrSoup
#define STORAGE_INSERT_NEW_ATTR_PREFIX2 " VALUES (" //recordid
#define STORAGE_INSERT_NEW_ATTR_PREFIX3 ",'" //attrname
#define STORAGE_INSERT_NEW_ATTR_PREFIX4 "','" //attrvalue
#define STORAGE_INSERT_NEW_ATTR_PREFIX5 "')" // ""

PGresult *
storage_quint_query_maker (StorageStore *store, QuintQuery type, 
			   const char *arg1, const char *arg2,
			   const char *arg3, const char *arg4,
			   const char *arg5)
{
	PGresult * results;
	char *query_prefix1, *query_prefix2, *query_prefix3, *query_prefix4, *query_prefix5,
	  *full_query, *error_msg;
	PGconn *dbConn;

	switch(type){
	case InsertNewAttr:
	  query_prefix1 = STORAGE_INSERT_NEW_ATTR_PREFIX1;
	  query_prefix2 = STORAGE_INSERT_NEW_ATTR_PREFIX2;
	  query_prefix3 = STORAGE_INSERT_NEW_ATTR_PREFIX3;
	  query_prefix4 = STORAGE_INSERT_NEW_ATTR_PREFIX4;
	  query_prefix5 = STORAGE_INSERT_NEW_ATTR_PREFIX5;
	  break;
	default:
		fprintf(stderr, "Invalid quint query type=%d\n", (int)type);
		return NULL;
	}

	//1 for the ';' and 1 for the '\0'
	full_query = g_new(char, strlen(query_prefix1) + strlen(arg1) +
			   strlen(query_prefix2) + strlen(arg2) +
			   strlen(query_prefix3) + strlen(arg3) +
			   strlen(query_prefix4) + strlen(arg4) +
			   strlen(query_prefix5) + strlen(arg5) + 1 + 1);
	full_query[0] = '\0';

	strcat(full_query, query_prefix1);
	strcat(full_query, arg1);
	strcat(full_query, query_prefix2);
	strcat(full_query, arg2);
	strcat(full_query, query_prefix3);
	strcat(full_query, arg3);
	strcat(full_query, query_prefix4);
	strcat(full_query, arg4);
	strcat(full_query, query_prefix5);
	strcat(full_query, arg5);
	strcat(full_query, ";");

#if DEBUG_STORAGE
	printf("About to query for [%s]\n", full_query);
#endif
	dbConn = _storage_store_get_connection(store);
	results = PQexec(dbConn, full_query);
	_storage_store_release_connection(store, dbConn);

#if DEBUG_STORAGE
	printf("...done\n");
#endif

	error_msg = PQresultErrorMessage(results);
	if(strlen(error_msg) > 0){
		fprintf(stderr, "error[%s] from query[%s]\n", error_msg, full_query);
	}

	g_free(full_query);

	return results;
}

PGresult *
storage_quad_query_maker (StorageStore *store, QuadQuery type, 
			  const char *arg1, const char *arg2,
			  const char *arg3, const char *arg4)
{
	PGresult * results;
	char *query_prefix1, *query_prefix2, *query_prefix3, *query_prefix4,
	  *full_query, *error_msg;
	PGconn *dbConn;

	switch(type){
	case SetAttr:
	  query_prefix1 = STORAGE_UPDATE_ATTR_PREFIX1;
	  query_prefix2 = STORAGE_UPDATE_ATTR_PREFIX2;
	  query_prefix3 = STORAGE_UPDATE_ATTR_PREFIX3;
	  query_prefix4 = STORAGE_UPDATE_ATTR_PREFIX4;
	  break;
	case ConnectNthChild:
	  query_prefix1 = STORAGE_CONNECT_NTH_CHILD_PREFIX1;
	  query_prefix2 = STORAGE_CONNECT_NTH_CHILD_PREFIX2;
	  query_prefix3 = STORAGE_CONNECT_NTH_CHILD_PREFIX3;
	  query_prefix4 = STORAGE_CONNECT_NTH_CHILD_PREFIX4;
	  break;
	default:
		fprintf(stderr, "Invalid quad query type=%d\n", (int)type);
		return NULL;
	}

	//1 for the ';' and 1 for the '\0'
	full_query = g_new(char, strlen(query_prefix1) + strlen(arg1) +
			   strlen(query_prefix2) + strlen(arg2) +
			   strlen(query_prefix3) + strlen(arg3) +
			   strlen(query_prefix4) + strlen(arg4) + 1 + 1);
	full_query[0] = '\0';

	strcat(full_query, query_prefix1);
	strcat(full_query, arg1);
	strcat(full_query, query_prefix2);
	strcat(full_query, arg2);
	strcat(full_query, query_prefix3);
	strcat(full_query, arg3);
	strcat(full_query, query_prefix4);
	strcat(full_query, arg4);
	strcat(full_query, ";");

#if DEBUG_STORAGE
	printf("About to query for [%s]\n", full_query);
#endif
	dbConn = _storage_store_get_connection(store);
	results = PQexec(dbConn, full_query);
	_storage_store_release_connection(store, dbConn);

#if DEBUG_STORAGE
	printf("...done\n");
#endif

	error_msg = PQresultErrorMessage(results);
	if(strlen(error_msg) > 0){
		fprintf(stderr, "error[%s] from query[%s]\n", error_msg, full_query);
	}

	g_free(full_query);

	return results;
}

PGresult *
storage_tri_query_maker (StorageStore *store, TriQuery type, const char *arg1,
			 const char *arg2, const char *arg3)
{
	PGresult * results;
	char *query_prefix1, *query_prefix2, *query_prefix3, *full_query, *error_msg;

	switch(type){
	case GetAttr:
	  query_prefix1 = STORAGE_GET_ATTR_PREFIX1;
	  query_prefix2 = STORAGE_GET_ATTR_PREFIX2;
	  query_prefix3 = STORAGE_GET_ATTR_PREFIX3;
	  break;
	case CountAttr:
	  query_prefix1 = STORAGE_COUNT_ATTR_PREFIX1;
	  query_prefix2 = STORAGE_COUNT_ATTR_PREFIX2;
	  query_prefix3 = STORAGE_COUNT_ATTR_PREFIX3;
	  break;
	case DelAttr:
	  query_prefix1 = STORAGE_DEL_ATTR_PREFIX1;
	  query_prefix2 = STORAGE_DEL_ATTR_PREFIX2;
	  query_prefix3 = STORAGE_DEL_ATTR_PREFIX3;
	  break;
	default:
		fprintf(stderr, "Invalid tri query type=%d\n", (int)type);
		return NULL;
	}

	full_query = g_new(char, strlen(query_prefix1) + strlen(arg1) +
			   strlen(query_prefix2) + strlen(arg2) +
			   strlen(query_prefix3) + strlen(arg3) + 1 + 1);//1 for the ';' and 1 for the '\0'
	full_query[0] = '\0';

	strcat(full_query, query_prefix1);
	strcat(full_query, arg1);
	strcat(full_query, query_prefix2);
	strcat(full_query, arg2);
	strcat(full_query, query_prefix3);
	strcat(full_query, arg3);
	strcat(full_query, ";");

	results = make_query(store, full_query);
#if DEBUG_STORAGE
	printf("...done\n");
#endif

	error_msg = PQresultErrorMessage(results);
	if(strlen(error_msg) > 0){
		fprintf(stderr, "error[%s] from query[%s]\n", error_msg, full_query);
	}

	g_free(full_query);

	return results;
}

PGresult *
storage_dbl_query_maker (StorageStore *store, DblQuery type,
			 const char *arg1, const char *arg2)
{
	PGresult * results;
	char *query_prefix1, *query_prefix2, *full_query, *error_msg;

	switch(type){
	case GetSortedChildren:
	  query_prefix1 = STORAGE_GET_SORTED_CHILDREN_PREFIX1;
	  query_prefix2 = STORAGE_GET_SORTED_CHILDREN_PREFIX2;
	  break;
	case GetNthChild:
	  query_prefix1 = STORAGE_GET_NTH_CHILD_PREFIX1;
	  query_prefix2 = STORAGE_GET_NTH_CHILD_PREFIX2;
	  break;
	case SeverNthChild:
	  query_prefix1 = STORAGE_SEVER_NTH_CHILD_PREFIX1;
	  query_prefix2 = STORAGE_SEVER_NTH_CHILD_PREFIX2;
	  break;
	case IncrementLinkNumsAsBigAs:
	  query_prefix1 = STORAGE_INC_LINKNUMS_ABA_PREFIX1;
	  query_prefix2 = STORAGE_INC_LINKNUMS_ABA_PREFIX2;
	  break;
	case DecrementLinkNumsGreaterThan:
	  query_prefix1 = STORAGE_DEC_LINKNUMS_GT_PREFIX1;
	  query_prefix2 = STORAGE_DEC_LINKNUMS_GT_PREFIX2;
	  break;
	case InsertNewText:
	  query_prefix1 = STORAGE_INSERT_NEW_TEXT_PREFIX1;
	  query_prefix2 = STORAGE_INSERT_NEW_TEXT_PREFIX2;
	  break;
	case SetText:
	  query_prefix1 = STORAGE_SET_TEXT_PREFIX1;
	  query_prefix2 = STORAGE_SET_TEXT_PREFIX2;
	  break;
	case GetNumAttrs:
	  query_prefix1 = STORAGE_NUM_ATTRS_PREFIX1;
	  query_prefix2 = STORAGE_NUM_ATTRS_PREFIX2;
	  break;
	case GetAllAttrs:
	  query_prefix1 = STORAGE_GET_ALL_ATTRS_PREFIX1;
	  query_prefix2 = STORAGE_GET_ALL_ATTRS_PREFIX2;
	  break;
	case DelAttrs:
	  query_prefix1 = STORAGE_ATTRS_DELETE_PREFIX1;
	  query_prefix2 = STORAGE_ATTRS_DELETE_PREFIX2;
	  break;
	default:
		fprintf(stderr, "Invalid dbl query type=%d\n", (int)type);
		return NULL;
	}

	full_query = g_new(char, strlen(query_prefix1) + strlen(arg1) +
			   strlen(query_prefix2) + strlen(arg2) + 1 + 1);//1 for the ';' and 1 for the '\0'
	full_query[0] = '\0';

	strcat(full_query, query_prefix1);
	strcat(full_query, arg1);
	strcat(full_query, query_prefix2);
	strcat(full_query, arg2);
	strcat(full_query, ";");

	results = make_query(store, full_query);

#if DEBUG_STORAGE
	printf("...done\n");
#endif

	error_msg = PQresultErrorMessage(results);
	if(strlen(error_msg) > 0){
		fprintf(stderr, "error[%s] from query[%s]\n", error_msg, full_query);
	}

	g_free(full_query);

	return results;
}

PGresult *
storage_query_maker (StorageStore *store, Query type, const char *recordid_str)
{
        PGresult * results;
        gchar *query_prefix, *full_query;

	switch(type){
	case IsNode:
		query_prefix = STORAGE_IS_NODE_PREFIX;
		break;
	case IsText:
		query_prefix = STORAGE_IS_TEXT_PREFIX;
		break;	
	case DelAvailable:
		query_prefix = STORAGE_DEL_AVAILABLE_PREFIX;
		break;
	case DelText:
		query_prefix = STORAGE_DEL_TEXT_PREFIX;
		break;
	case GetParents:
		query_prefix = STORAGE_FIND_PARENTS_PREFIX;
		break;
	case GetChildren:
		query_prefix = STORAGE_GET_CHILDREN_PREFIX;
		break;
	case NumParents:
		query_prefix = STORAGE_NUM_PARENTS_PREFIX;
		break;
	case NumChildren:
		query_prefix = STORAGE_NUM_CHILDREN_PREFIX;
		break;
	case DelChildLinks:
		query_prefix = STORAGE_DEL_CHILD_LINKS_PREFIX;
		break;
	case DelParentLinks:
		query_prefix = STORAGE_DEL_PARENT_LINKS_PREFIX;
		break;
	case IsTopLevel:
		query_prefix = STORAGE_IS_TOP_LEVEL_PREFIX;
		break;
	case UpdateAccess:
		query_prefix = STORAGE_UPDATE_ACCESS_PREFIX;
		break;
	case UpdateModification:
		query_prefix = STORAGE_UPDATE_MODIFICATION_PREFIX;
		break;
	case GetBLOBID:
		query_prefix = STORAGE_GETBLOBID_PREFIX;
		break;
	case GetSize:
		query_prefix = STORAGE_SIZE_PREFIX;
		break;
	default:
		fprintf(stderr, "Invalid query type=%d\n", (int)type);
		return NULL;
	}

	full_query = g_new(char, strlen(query_prefix) + strlen(recordid_str) + 1 + 1);//1 for the ';' and 1 for the '\0'
	full_query[0] = '\0';

	strcat(full_query, query_prefix);
	strcat(full_query, recordid_str);
	strcat(full_query, ";");

	results = make_query(store, full_query);

	g_free(full_query);

	return results;
}

PGresult *
make_query(StorageStore *store, const char *query)
{
  char *error_msg;
  PGconn *dbConn = _storage_store_get_connection (store);
  PGresult * results  = PQexec(dbConn, query);
  _storage_store_release_connection(store, dbConn);

#if DEBUG_STORAGE
  printf("About to query for [%s]\n", query);
#endif
  error_msg = PQresultErrorMessage(results);
  if(strlen(error_msg) > 0){
    fprintf(stderr, "error[%s] from query[%s]\n", error_msg, query);
  }

  return results;
}
