#ifndef STORAGE_QUERY_MAKER_H
#define STORAGE_QUERY_MAKER_H

#include "storage-store.h"

#include <libpq-fe.h>

G_BEGIN_DECLS

#define STORAGE_GET_AVAILABLE_NUMBER "SELECT recordID FROM AvailableRecordIDs LIMIT 1"
#define STORAGE_GET_HIGHEST_NUMBER "SELECT recordID FROM PrevHighestRecordID"
#define STORAGE_INC_HIGHEST_NUMBER "UPDATE PrevHighestRecordId SET recordID = recordID + 1"

typedef enum {
	IsNode,
	IsText, 
	DelAvailable,
	DelText,
	GetParents,
	GetChildren,
	NumParents,
	NumChildren, 
	DelChildLinks,
	DelParentLinks,
	IsTopLevel,
	UpdateAccess,
	UpdateModification,
	GetBLOBID, 
	GetSize,
	NumStorageQueries
} Query;

typedef enum {
	GetSortedChildren, 
	GetNthChild,
	SeverNthChild,
	IncrementLinkNumsAsBigAs,
	DecrementLinkNumsGreaterThan,
	InsertNewText,
	SetText,
	GetNumAttrs,
	GetAllAttrs,
	DelAttrs,
	NumDblQueries
} DblQuery;

typedef enum {
	GetAttr,
	CountAttr,
	DelAttr,
	NumTriQueries
} TriQuery;

typedef enum {
	SetAttr,
	ConnectNthChild,
	NumQuadQueries
} QuadQuery;

typedef enum {
	InsertNewAttr,
	NumQuintQueries
} QuintQuery;

PGresult 	*storage_query_maker		(StorageStore *store,
						 Query type,
						 const char *recordid_str);

PGresult 	*storage_dbl_query_maker	(StorageStore *store,
						 DblQuery type,
						 const char *arg1,
						 const char *arg2);

PGresult 	*storage_tri_query_maker	(StorageStore *store,
						 TriQuery type,
						 const char *arg1,
						 const char *arg2, 
						 const char *arg3);

PGresult 	*storage_quad_query_maker	(StorageStore *store,
						 QuadQuery type,
						 const char *arg1,
						 const char *arg2,
						 const char *arg3,
						 const char *arg4);

PGresult 	*storage_quint_query_maker	(StorageStore *store,
						 QuintQuery type,
						 const char *arg1,
						 const char *arg2,
						 const char *arg3,
						 const char *arg4,
						 const char *arg5);

PGresult 	*make_query			(StorageStore *store,
						 const char *query);
G_END_DECLS

#endif
