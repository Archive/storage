#ifndef STORAGE_STORE_PRIVATE
#define STORAGE_STORE_PRIVATE

#include "storage-store.h"
#include <libpq-fe.h>

G_BEGIN_DECLS

PGconn	       *_storage_store_get_connection	  (StorageStore *store);

void		_storage_store_release_connection (StorageStore *store,
						   PGconn *conn);

#endif
