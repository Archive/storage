#include "storage-store.h"
#include "storage-store-private.h"

#include "storage-debug.h"
#include "storage-query-maker.h"

#include <libpq-fe.h>
#include <libpq/libpq-fs.h>
#include <string.h>
#include <stdlib.h>

static GObjectClass *parent_class = NULL;

struct _StorageStorePrivate {
	char *hostname;
	char *username;
	char *password;
        char *port;

	GQueue *connections;
	GMutex *conn_lock;
	GCond *avail_cond;
};

enum
{
	PROP_0,
	PROP_USERNAME,
	PROP_HOSTNAME,
	PROP_PASSWORD,
	PROP_PORT
};

#define NUM_CONNECTIONS 5

static void
storage_remove_helper (StorageStore *store, char *child, gboolean dontTouchTopLevelRecs,
		       gboolean dontTouchMultiParentChildren);

static void
storage_store_init (StorageStore *store)
{
	store->priv = g_new (StorageStorePrivate, 1);
	store->priv->username = NULL;
	store->priv->password = NULL;
	store->priv->hostname = NULL;
	store->priv->port     = NULL;
	store->priv->connections = NULL;
	store->priv->avail_cond = g_cond_new();
	store->priv->conn_lock = g_mutex_new();
}

static void
storage_store_finalize (GObject *object)
{
	StorageStore *store;
                                                                                                                        
	store = STORAGE_STORE(object);
	g_return_if_fail(store != NULL);

	g_free(store->priv->username);
	g_free(store->priv->password);
	g_free(store->priv->hostname);
	g_free(store->priv->port);
	g_cond_free (store->priv->avail_cond);
	g_mutex_free (store->priv->conn_lock);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
storage_store_set_username (StorageStore *store, const char *username)
{
	g_free (store->priv->username);
	store->priv->username = g_strdup (username);
}

static void
storage_store_set_hostname (StorageStore *store, const char *hostname)
{
	g_free (store->priv->hostname);
	store->priv->hostname = g_strdup (hostname);
}

static void
storage_store_set_port (StorageStore *store, const char *port)
{
	g_free (store->priv->port);
	store->priv->port = g_strdup (port);
}

static void
storage_store_set_password (StorageStore *store, const char *password)
{
	g_free (store->priv->password);
	store->priv->password = g_strdup (password);
}

static void
storage_store_set_property (GObject *object,
                            guint prop_id,
                            const GValue *value,
                            GParamSpec *pspec)
{
        StorageStore *store = STORAGE_STORE (object);
                                                                                                                             
        switch (prop_id) {
                case PROP_USERNAME:
			storage_store_set_username (store, g_value_get_string (value));
                        break;
                case PROP_HOSTNAME:
			storage_store_set_hostname (store, g_value_get_string (value));
                        break;
                case PROP_PASSWORD:
			storage_store_set_password (store, g_value_get_string (value));
			break;
	        case PROP_PORT:
		        storage_store_set_port (store, g_value_get_string (value));
			break;
        }
}

static void
storage_store_get_property (GObject *object,
                            guint prop_id,
                            GValue *value,
                            GParamSpec *pspec)
{
        StorageStore *store = STORAGE_STORE (object);
                                                                                                                             
        switch (prop_id) {
                case PROP_USERNAME:
                        g_value_set_string (value, store->priv->username);
                        break;
                case PROP_PASSWORD:
                        g_value_set_string (value, store->priv->password);
                        break;
                case PROP_HOSTNAME:
                        g_value_set_string (value, store->priv->hostname);
                        break;
                case PROP_PORT:
                        g_value_set_string (value, store->priv->port);
                        break;
	}
}

static void
storage_store_class_init (StorageStoreClass *klass)
{
	GObjectClass *object_class;

	if (!g_thread_supported ()) g_thread_init (NULL);
	storage_debug_init ();

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = storage_store_finalize;
	object_class->set_property = storage_store_set_property;
	object_class->get_property = storage_store_get_property;

	parent_class = g_type_class_peek_parent (klass);

	g_object_class_install_property (object_class,
					 PROP_USERNAME,
					 g_param_spec_string ("username",
							      "Username",
							      "The database user name",
							      NULL,
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (object_class,
					 PROP_HOSTNAME,
					 g_param_spec_string ("hostname",
							      "Hostname",
							      "The database host name",
							      NULL,
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (object_class,
					 PROP_PORT,
					 g_param_spec_string ("port",
							      "Port",
							      "The database port number, as a string",
							      NULL,
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (object_class,
					 PROP_PASSWORD,
					 g_param_spec_string ("password",
							      "Password",
							      "The database password",
							      NULL,
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT_ONLY));
}

GType
storage_store_get_type (void)
{
        static GType type = 0;

        if (type == 0) {
                GTypeInfo info = {
                        sizeof (StorageStoreClass),
                        NULL, /* GBaseInitFunc */
			NULL, /* GBaseFinalizeFunc */
			(GClassInitFunc) storage_store_class_init, 
			NULL, /* GClassFinalizeFunc */
			NULL, /* user supplied data */
                        sizeof (StorageStore), 
			0, /* n_preallocs */
			(GInstanceInitFunc) storage_store_init,
			NULL
                };

                type = g_type_register_static (G_TYPE_OBJECT, "StorageStore", &info, (GTypeFlags)0);
        }

        return type;
}

static PGconn *
new_connection (StorageStore *store) 
{
	GString *dsn;
	PGconn *connection;
	char *error;
	ConnStatusType stat;

	dsn = g_string_new("");

	g_string_append_printf (dsn, "dbname = %s ", STORAGE_DB_NAME);

	if (store->priv->hostname) {
		g_string_append_printf (dsn, "host = %s ", store->priv->hostname);
	}

	if (store->priv->port) {
		g_string_append_printf (dsn, "port = %s ", store->priv->port);
	} else {
		g_string_append_printf (dsn, "port = %s ", STORAGE_DB_PORT);
	}

	if (store->priv->password) {
		g_string_append_printf (dsn, "password = %s ", store->priv->password);
	}

	if (store->priv->username) {
		g_string_append_printf (dsn, "username = %s ", store->priv->username);
	}

	connection = PQconnectdb(dsn->str); 

	g_string_free(dsn, TRUE);

	stat = PQstatus(connection);
	error = PQerrorMessage(connection);
	if (PQstatus(connection) == CONNECTION_BAD) {
		g_warning ("%s", PQerrorMessage(connection));
	}
	if (strlen(error) > 0) {
		g_warning ("DB connection error to " STORAGE_DB_NAME ": %s", error);
		return NULL;
	}

	return connection;
}

static GQueue *
create_connections (StorageStore *store)
{
	GQueue *connections = g_queue_new();
	
	int i;

	for (i = 0; i < NUM_CONNECTIONS; i++) {
		PGconn *connection = new_connection (store);
		g_queue_push_head(connections, connection);
	}

	return connections;
}

static PGconn * 
get_connection (StorageStore *store) 
{
	PGconn *connection;

	g_mutex_lock(store->priv->conn_lock);

	if (store->priv->connections == NULL) {
		store->priv->connections = create_connections (store);
	}

	while(g_queue_is_empty(store->priv->connections)) {
		g_cond_wait(store->priv->avail_cond, store->priv->conn_lock);
	}

	connection = g_queue_pop_head (store->priv->connections);

	g_mutex_unlock(store->priv->conn_lock);

	return connection;
}

static void 
release_connection (StorageStore *store, PGconn *connection)
{
	g_return_if_fail (connection != NULL);

	/* first free it in the table */
	g_mutex_lock(store->priv->conn_lock);
	g_queue_push_tail(store->priv->connections, connection);
	g_mutex_unlock(store->priv->conn_lock);

	g_cond_broadcast(store->priv->avail_cond);
}

StorageStore *
storage_store_new (void)
{
	StorageStore *store;
                                                                                                                             
        store = STORAGE_STORE (g_object_new (STORAGE_TYPE_STORE, NULL));
                                                                                                                             
        return store;
}

StorageStore *
storage_store_new_with_login (const char *hostname,
			      const char *port,
			      const char *username,
			      const char *password)
{
	StorageStore *store;
                                               

	store = STORAGE_STORE (g_object_new (STORAGE_TYPE_STORE,
					     "hostname", hostname,
					     "port", port,
					     "password", password,
					     "username", username,
					     NULL));                                                               
	return store;
}

StorageItem *
storage_store_add_item (StorageStore *store, StorageItemType type)
{
	char buffer[50];
	Oid blobid;
	PGconn *dbConn;
	int fd;
	PGresult *results;
	char *id;
	StorageItem *item;

	results = make_query (store, STORAGE_GET_AVAILABLE_NUMBER);

	if(PQntuples(results) > 0) {
		id = PQgetvalue(results, 0, 0);
		PQclear(results);
		PQclear(storage_query_maker(store,DelAvailable, id));
	}
	else {
		int num;

		PQclear(results);

		results =  make_query (store, STORAGE_GET_HIGHEST_NUMBER);
    
		if (PQntuples(results) > 0) {
			char *value = NULL;

			value = PQgetvalue(results, 0, 0);
			num = atoi(value) + 1;
		} else {
			num = 0;
			g_warning ("Couldn't get a free Storage ID number, using 0");
		}

		PQclear(results);

		sprintf(buffer, "%d", num);
		id = g_strdup(buffer);
    
		PQclear(make_query(store, STORAGE_INC_HIGHEST_NUMBER));
	}

	item = storage_store_get_item_from_id (store, id);

	//now actually insert
	switch(type) {
	case STORAGE_ITEM_TYPE_TEXT:
		//insert into the text table
		PQclear(storage_dbl_query_maker(store, InsertNewText, id, ""));
		break;
	case STORAGE_ITEM_TYPE_BINARY:
		//insert an attrvalu pair into attrsoup with the blobid
		dbConn = get_connection (store);
		PQexec(dbConn, "begin");
		blobid = lo_creat(dbConn, INV_READ | INV_WRITE);

		//now update the blob attr
		sprintf(buffer, "%d", (int)blobid);
		storage_item_set_attribute (item, STORAGE_FILE_BLOB, buffer);

		fd = lo_open(dbConn, blobid, INV_WRITE);
    
		PQexec(dbConn, "end");
	 	release_connection(store, dbConn);
	break;
	case STORAGE_ITEM_TYPE_NODE:
		//nothing... no attrs yet
	break;
	default:
		g_warning ("Can't make item of type=%d", type);
	break;
	}

	LOG ("Item added to the store, id is %s", id)

	return item;
}

static void
storage_store_remove_item_ext_int (StorageStore *store, const char *recordid_str, 
			           gboolean dontTouchChildren, 
			           gboolean dontTouchTopLevelRecs, 
			           gboolean dontTouchMultiParentChildren)
{
	PGresult * results;
	int db_result, i, blobID;
	PGconn *dbConn;
  
	/* remove all parents pointing to me */
	PQclear(storage_query_maker(store, DelParentLinks, recordid_str));

	/* remove my info */
	results = storage_query_maker(store,GetBLOBID, recordid_str);

	if(PQntuples(results) > 0) {
		blobID = atoi(PQgetvalue(results, 0, 0));
		PQclear(results);

		/* unlink the binary */
		/* FIXME: unlinking takes a long time. if you make the unlink call
		   on a non-blocking connetion, will it still block?
		   int PQsetnonblocking(dbConn, 1);
		*/
	
		dbConn = get_connection (store);
		PQclear(PQexec(dbConn, "begin"));
		db_result = lo_unlink(dbConn, blobID);
		PQclear(PQexec(dbConn, "end"));
		release_connection(store, dbConn);

		if(db_result < 0) {
			g_warning("DBFS BLOB unlink error on recordid=%s, error code=%d\n",
				  recordid_str, db_result);
		}

	}
	else {
		PQclear(results);
	}

	results = storage_query_maker(store,IsText, recordid_str);
	if(atoi(PQgetvalue(results, 0, 0))> 0) {
		PQclear(results);  
		PQclear(storage_query_maker(store,DelText, recordid_str));
	}
	else {
		PQclear(results);  
	}

	/* now its a Node */
	PQclear(storage_dbl_query_maker(store,DelAttrs, DATE_SOUP, recordid_str));
	PQclear(storage_dbl_query_maker(store,DelAttrs, NUMBER_SOUP, recordid_str));
	PQclear(storage_dbl_query_maker(store,DelAttrs, ATTR_SOUP, recordid_str));

	results = storage_query_maker(store,GetChildren, recordid_str);

	/* remove connections to me. */
	if(!dontTouchChildren) {
		for(i = PQntuples(results) - 1; i >= 0; i--) {
			storage_remove_helper(store, PQgetvalue(results, i, 0),
					      dontTouchTopLevelRecs, dontTouchMultiParentChildren);
		}
	}

	PQclear(storage_query_maker(store,DelChildLinks, recordid_str));
	PQclear(results);
}

static void
storage_remove_helper (StorageStore *store, char *child, gboolean dontTouchTopLevelRecs,
		       gboolean dontTouchMultiParentChildren)
{
	PGresult * results;
	char *ans;

	if(dontTouchTopLevelRecs) {
		results = storage_query_maker(store,IsTopLevel, child);
 		if((PQntuples(results) > 0) &&
		   (ans = PQgetvalue(results, 0, 0), (ans[0] == 't' || ans[0]=='T'))) {
			PQclear(results);
			return;
		}
		PQclear(results);
	}

	if(dontTouchMultiParentChildren) {
		results = storage_query_maker(store,NumParents, child);
		if(atoi(PQgetvalue(results, 0, 0)) > 0) {
			PQclear(results);
			return;
 		}
		PQclear(results);
	}

	/* otherwise remove it */
	storage_store_remove_item_ext_int(store, child, TRUE, dontTouchTopLevelRecs,
				          dontTouchMultiParentChildren);
}

void
storage_store_remove_item (StorageStore *store, StorageItem *item)
{
	const char *id;

	id = storage_item_get_id (item);
	storage_store_remove_item_ext_int (store, id, FALSE, TRUE, TRUE);
}

void 
storage_store_remove_item_ext (StorageStore *store,
			       StorageItem *item, 
			       gboolean dontTouchChildren, 
			       gboolean dontTouchTopLevelRecs, 
			       gboolean dontTouchMultiParentChildren)
{
  const char *id;

  id = storage_item_get_id (item);
  storage_store_remove_item_ext_int (store, id, dontTouchChildren,
			             dontTouchTopLevelRecs, dontTouchMultiParentChildren);
}

StorageItem *
storage_store_get_item_from_id  (StorageStore *store,
				 const char *id)
{
	return STORAGE_ITEM (storage_item_new (store, id));
}

char *
storage_store_get_uri (StorageStore *store)
{
	GString *uri;
	char *result;

	uri = g_string_new("storage://");

	if (store->priv->username)
	{
		g_string_append (uri, store->priv->username);

		if (store->priv->password)
		{
			g_string_append (uri, ":");
			g_string_append (uri, store->priv->username);
		}
	}

	if (store->priv->hostname)
	{
		g_string_append (uri, "@");
		g_string_append (uri, store->priv->hostname);
	}

	if (store->priv->port)
	{
		g_string_append (uri, ":");
		g_string_append (uri, store->priv->port);
	}	  

	g_string_append (uri, "/");

	result = uri->str;

	g_string_free (uri, FALSE);
			 
	return result;
}

PGconn *
_storage_store_get_connection (StorageStore *store)
{
	return (gpointer) get_connection(store);
}

void
_storage_store_release_connection (StorageStore *store,
				   PGconn *connection)
{
	release_connection (store, connection);
}

StorageStore *
storage_get_default_store (void)
{
	static StorageStore *store = NULL;

	if (store == NULL)
	{
		/* FIXME for now we just use the local db constructor */
		store = storage_store_new ();
	}

	return store;
}

GSList *
storage_store_get_matches (StorageStore *store,
			   const char *soup,
			   const char *attrib,
			   const char *val)
{
	int i;
	char *recordid, *query;
	GSList * itemList = NULL; 
	PGresult * results;

	query = g_strdup_printf (
		"SELECT recordID FROM %s WHERE attrName = '%s' AND attrValue = '%s'", 
		soup, attrib, val);

	results = make_query (store, query);

	for (i = 0; i < PQntuples(results); ++i) {
		recordid = PQgetvalue(results, i, 0);
 		itemList = g_slist_prepend(itemList, recordid);
	}

	return itemList;
}

static char *
build_recordid_list_string(GSList *recordidList) 
{
  char *currentList = strdup("");
  char *oldList;
  GSList *i;

  for (i = recordidList; i != NULL; i = g_slist_next (i)) {
    oldList = currentList;
    if (i == recordidList) {
      currentList = g_strdup_printf("%s", (char *)i->data);
    } else {

      currentList = g_strdup_printf("%s, %s", oldList, (char *)i->data);
    }
    g_free(oldList);
  }

  return currentList;
}

GSList *
storage_store_get_items_from_ids (StorageStore *store, GSList* recordidList)
{
	GSList *itemList = NULL;
	GHashTable *hash;
	char *soups[NUM_SOUPS] = {ATTR_SOUP, NUMBER_SOUP, DATE_SOUP};
	char *recordid;
	char *recordidListString;
	int j, k;
	PGresult *results[NUM_SOUPS];
	StorageItem *item;

	hash = g_hash_table_new (g_str_hash, g_str_equal);

	if (g_slist_length (recordidList) <= 0) return NULL;

	recordidListString = build_recordid_list_string(recordidList);

	for (j = 0; j < NUM_SOUPS; ++j) {	
	        char *query;

		query = g_strdup_printf ("SELECT * FROM %s WHERE recordid in (%s)", soups[j], recordidListString);
		results[j] = make_query (store, query);
		g_free (query);

		for (k = 0; k < PQntuples (results[j]); ++k) {
			recordid = PQgetvalue (results[j], k, 0);
			item = g_hash_table_lookup (hash, recordid);

			if (item == NULL) {
				item = STORAGE_ITEM (storage_item_new (store, recordid));
				g_hash_table_insert (hash, recordid, item);
				itemList = g_slist_prepend (itemList, item);
				storage_item_cache_all_attributes (item);
			}
		}
	}

	g_free(recordidListString);

	for (j = 0; j < NUM_SOUPS; ++j) {
		PQclear (results[j]);
	}

	g_hash_table_destroy (hash);

	return itemList;
}

