#ifndef STORAGE_STORE_H
#define STORAGE_STORE_H

#include <glib/gerror.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define STORAGE_TYPE_STORE		(storage_store_get_type ())
#define STORAGE_STORE(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), STORAGE_TYPE_STORE, StorageStore))
#define STORAGE_STORE_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), STORAGE_TYPE_STORE, StorageStoreClass))
#define STORAGE_IS_STORE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), STORAGE_TYPE_STORE))
#define STORAGE_IS_STORE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), STORAGE_TYPE_STORE))
#define STORAGE_STORE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), STORAGE_TYPE_STORE, StorageStoreClass))

#define DATE_SOUP "DateSoup"
#define NUMBER_SOUP "NumberSoup"
#define ATTR_SOUP "AttrSoup"
#define NUM_SOUPS 3

#define STORAGE_DB_NAME "storage"
#define STORAGE_DB_PORT "11592"

typedef struct _StorageStore StorageStore;
typedef struct _StorageStoreClass StorageStoreClass;
typedef struct _StorageStorePrivate StorageStorePrivate;

struct _StorageStore
{
  GObject parent_instance;
  StorageStorePrivate *priv;
};

struct _StorageStoreClass
{
  GObjectClass parent_class;
};

#include <libstorage/storage-item.h>

GType		storage_store_get_type		(void);

StorageStore   *storage_get_default_store       (void);

StorageStore   *storage_store_new		(void);

StorageStore   *storage_store_new_with_login	(const char *hostname,
						 const char *port,
					 	 const char *username,
					 	 const char *password);

StorageItem    *storage_store_add_item		(StorageStore *store,
						 StorageItemType type);

void		storage_store_remove_item	(StorageStore *store,
						 StorageItem *item);

void 		storage_store_remove_item_ext	(StorageStore *store,
			       			 StorageItem *item, 
			       			 gboolean dontTouchChildren, 
			       			 gboolean dontTouchTopLevelRecs, 
			       			 gboolean dontTouchMultiParentChildren);

StorageItem    *storage_store_get_item_from_id  (StorageStore *store,
						 const char *recordID);

GSList         *storage_store_get_items_from_ids (StorageStore *store,
						  GSList *recordIDs);


char           *storage_store_get_uri           (StorageStore *store);

GSList         *storage_store_get_matches       (StorageStore *store,
			   			 const char *soup,
			   			 const char *attrib,
			   			 const char *val);


G_END_DECLS

#endif /* STORAGE_STORE_H */
