#include <storage-store.h>
#include <storage-item.h>
#include <assert.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <libpq-fe.h> //in /usr/include
#include <libpq/libpq-fs.h>
#include <time.h>
#include <string.h>

#define BUF_SIZE 50
#define TEST_ULINK 1

static void
attrListPrint (gpointer data, gpointer user_data)
{
  StorageAttrPair* realData = (StorageAttrPair*) data;
    printf("\t[%s, %s]\n", realData->name, realData->value);

}

static void
itemListPrint (gpointer data, gpointer user_data)
{
  StorageItem* realData = (StorageItem*) data;
    printf("\t[%s]\n", storage_item_get_id(realData));
}

static void
testNodes()
{
  StorageStore *store;
  StorageItem *itemText, *itemNode, *itemNode2, *itemNode3;
  char *value;
  GSList *list;

  store = storage_store_new ();

  itemText = storage_store_add_item(store, STORAGE_ITEM_TYPE_TEXT);
  printf("Created text with id=%s\n", storage_item_get_id(itemText));

  storage_item_set_attribute(itemText, STORAGE_FILE_NAME, "textTitle");
  storage_item_set_attribute(itemText, STORAGE_FILE_TEXT,  "textText");
  storage_item_set_attribute(itemText, STORAGE_FILE_MIME, "text/plain");
  storage_item_set_attribute(itemText, STORAGE_FILE_TOP, "true");
  storage_item_set_attribute(itemText, STORAGE_FILE_SIZE, "123");
  storage_store_remove_item(store, itemText);

  printf ("Testing the Attributes\n");
  itemNode = storage_store_add_item(store, STORAGE_ITEM_TYPE_NODE);
  printf("Created top node with id=%s\n", storage_item_get_id(itemNode));


  assert(storage_item_has_attribute(itemNode, "fill") == FALSE);
  assert(storage_item_n_attributes(itemNode) == 0);

  storage_item_set_attribute(itemNode, "fill", "yes");
  value = storage_item_get_attribute(itemNode, "fill");
  assert(strcmp(value, "yes") == 0);
  assert(storage_item_has_attribute(itemNode, "fill") == TRUE);
  assert(storage_item_n_attributes(itemNode) == 1);
  storage_item_set_attribute(itemNode, "fill", "no");


  storage_item_set_attribute(itemNode, "fill2", "yes2");

  printf("Should be equivalent to \n\t[fill, no]\n\t[fill2, yes2]\n\n");
  list = storage_item_get_attribute_pairs(itemNode);
  g_slist_foreach(list, attrListPrint, NULL);
  g_slist_free(list);
  printf("\n");

  storage_item_delete_attribute(itemNode, "fill2");

  //testing node functions
  assert(storage_item_n_children(itemNode) == 0);
  itemNode2 = storage_store_add_item(store, STORAGE_ITEM_TYPE_NODE);
  printf("Created node2 with id=%s\n", storage_item_get_id(itemNode2));
  storage_item_insert_child(itemNode, itemNode2, -1);
  assert(storage_item_n_children(itemNode) == 1);
  
  itemNode3 = storage_store_add_item(store, STORAGE_ITEM_TYPE_NODE);
  printf("Created node3 with id=%s\n", storage_item_get_id(itemNode3));
  assert(storage_item_get_n_parents(itemNode3) == 0);
  storage_item_insert_child(itemNode, itemNode3, 0);
  assert(storage_item_get_n_parents(itemNode3) == 1);
  
  list = storage_item_get_children(itemNode);
  printf("should be exactly increasing by one\n");
  g_slist_foreach(list, itemListPrint, NULL);
  g_slist_free(list);
  printf("\n");

  list = storage_item_get_parents(itemNode2);
  printf("parents should be exactly [%s]\n", storage_item_get_id(itemNode));
  g_slist_foreach(list, itemListPrint, NULL);
  g_slist_free(list);
  printf("\n");

  storage_item_sever_nth_child(itemNode, 0);//removed 3

  storage_store_remove_item(store, itemNode);
  storage_store_remove_item(store, itemNode3);
 

  itemNode = storage_store_get_item_from_id(store, "4");
  //guint storage_item_get_approx_size(StorageItem *item);
}

#define WRITE_SIZE 4
static void
testBinary()
{  
  StorageItem *itemBin;
  char *data="hello my children\n", buffer[BUF_SIZE];
  int completed, size, sizeToWrite;
  //needs to be a FileSize because ints are too small and it will
  //overwrite other values.
  GnomeVFSFileSize num1;
  GnomeVFSHandle *handle;
  GnomeVFSResult res;
  StorageStore *store;

  store = storage_store_new ();

  itemBin = storage_store_add_item(store, STORAGE_ITEM_TYPE_BINARY);
  printf("Created binary with id=%s\n", storage_item_get_id(itemBin));
  handle = storage_item_get_vfs_handle(itemBin, GNOME_VFS_OPEN_WRITE);

  

  //test the writing

  printf("writting this to the file:\n%s\n", data);
  completed = 0;
  size = strlen(data)+1;
  do{
    sizeToWrite = (WRITE_SIZE < (size - completed)) ? WRITE_SIZE : (size-completed);
    res=  gnome_vfs_write(handle, data + completed, 
			  sizeToWrite, &num1);
    completed += num1;
    if(res != GNOME_VFS_OK){
      printf("Error on write %s\n", gnome_vfs_result_to_string(res));
      break;
    }
    else{
      printf("Wrote %d bytes(more)\n", (int)num1);
    }
 }while((num1 > 0) && (completed < size)); 

  //erase data
  for(completed = 0; completed < BUF_SIZE; completed++)
    buffer[completed] = (char)0;
 
  res = gnome_vfs_seek(handle, GNOME_VFS_SEEK_START, 0);
  if(res != GNOME_VFS_OK){printf("Error on seek %s\n", gnome_vfs_result_to_string(res));}
  
  res = gnome_vfs_seek(handle, GNOME_VFS_SEEK_END, 0);
  if(res != GNOME_VFS_OK){printf("Error on seek2 %s\n", gnome_vfs_result_to_string(res));}
  
  res= gnome_vfs_tell(handle, &num1);
  if(res != GNOME_VFS_OK){printf("Error on tell %s\n", gnome_vfs_result_to_string(res));}
  printf("new size=%d\n", (int)num1);
  size = num1;

  res = gnome_vfs_seek (handle, GNOME_VFS_SEEK_START, 0);
  if(res != GNOME_VFS_OK){printf("Error on seek %s\n", gnome_vfs_result_to_string(res));}

  handle = storage_item_get_vfs_handle(itemBin, GNOME_VFS_OPEN_READ);

  completed = 0;
  do{
    res = gnome_vfs_read(handle, buffer + completed, 
		   size -completed, &num1);
    completed += num1;
    if(res != GNOME_VFS_OK){
      printf("Error on read, read %d num bytes and go this error %s\n",
	     (int)num1, gnome_vfs_result_to_string(res));
      break;
    }
    else{
      printf("Read %d bytes(more)\n", (int)num1);
    }
  }while((num1 > 0) && (completed < size));

  gnome_vfs_close(handle);

  printf("read back this from the file:\n%s\n", buffer);
#if TEST_ULINK
  storage_store_remove_item (store, itemBin);//
#endif
}

int
main (int argc, char **argv)
{
  g_type_init ();

  testBinary();
  testNodes();

  return -1;
}
