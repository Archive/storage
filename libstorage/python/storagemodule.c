#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* include this first, before NO_IMPORT_PYGOBJECT is defined */
#include <pygobject.h>
#include "storage-item.h"

void pystorage_register_classes (PyObject *d);
void pystorage_add_constants(PyObject *module, const gchar *strip_prefix);
		
extern PyMethodDef pystorage_functions[];

DL_EXPORT(void)
initstorage (void)
{
	PyObject *m, *d;

	init_pygobject ();

	m = Py_InitModule ("storage", pystorage_functions);
	d = PyModule_GetDict (m);

	pystorage_register_classes (d);
	pystorage_add_constants (m, "STORAGE_");
	
	if (PyErr_Occurred ()) {
		Py_FatalError ("can't initialize module storage");
	}
}
