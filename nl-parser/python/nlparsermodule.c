#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* include this first, before NO_IMPORT_PYGOBJECT is defined */
#include <pygobject.h>
#include "nlparser.h"

void pynlparser_register_classes (PyObject *d);
void pynlparser_add_constants(PyObject *module, const gchar *strip_prefix);
		
extern PyMethodDef pynlparser_functions[];

DL_EXPORT(void)
initnlparser (void)
{
	PyObject *m, *d;

	init_pygobject ();

	m = Py_InitModule ("nlparser", pynlparser_functions);
	d = PyModule_GetDict (m);

	pynlparser_register_classes (d);
	//pynlparser_add_constants (m, "NLPARSER_");
	
	if (PyErr_Occurred ()) {
		Py_FatalError ("can't initialize module nlparser");
	}
}
