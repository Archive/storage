#include "entity-resolver.hh"
#include "k2yobject-utils.hh"
#include "lambda-generator.hh"
#include "lambda.hh"
#include "debug-out.hh"
#include "storage-item.h"

#include <string>
#include <list>
#include <iostream>
#include <glib-2.0/glib.h>
#include <values.h>


using namespace std;
using namespace PET;

SQLQuery EntityResolver::getQuery () {
  return this->query;
}

char *tableFromType(StorageDataType t){
  switch(t){

  case storageIntType:
    return "NumberSoup";

  case storageTimeType:
    return "DateSoup";

  default:
  case storageStringType:
    return "AttrSoup";
  }
}
  
EntityResolver::EntityResolver (K2YObject *sentence, list<K2YObject *> &objects, Generator *generator) {
  this->generator = generator;

  score = -MAXFLOAT;
  if(!sentence) return;

  unFarkedVar = new Variable();
  unFarkedVar->setName("RemeberRemoveMe");
  list<Generated *> generated;
  
  list<K2YObject*>::iterator objIt;

  // Print the tree roots
  cout << endl << "Tree roots:" << endl;
  cout << objectListToString(objects);

  //keep some statistics
  int numOtherRels = 0, numLeaves = 0, numUnknownOtherRels = 0, numUnknownLeaves = 0;
  int numFuckedPhrases = 0;

  // Generate over the tree roots:
  map<Variable*, int> farkList;
  for(objIt = objects.begin(); objIt != objects.end(); objIt++){
    if(!objectInSomeArgs(*objIt, objects)){
#if DEBUG_NL || DEBUG_GRAMMAR
      cout << endl << "Parsing tree root: " << objectToString(*objIt, true) << endl;
#endif
      Generated * ge = generator->generate(*objIt);
      //cout << "woot[" << (int)ge <<"]" << endl;
      LambdaGasGenerated *gas = dynamic_cast<LambdaGasGenerated*>(ge);
      assert(gas != NULL);//assume for now this is the only Generator

      if(!gas->expr){
#if DEBUG_NL || DEBUG_GRAMMAR
	cerr << "Generated: an empty expr parsing " << objectToString(*objIt, true) << endl;
#endif
	continue;
      }

      //keep the statistics going
      numOtherRels += gas->numOtherRels;
      numLeaves += gas->numLeaves;
      numUnknownOtherRels += gas->numUnknownOtherRels;
      numUnknownLeaves += numUnknownLeaves;

      LambdaExpression *exp = dynamic_cast<LambdaExpression*>(gas->expr);
      if(!exp){
	cout << "Should've gotten a LambdaExpression back, but here's an expr" << endl;
	cout << gas->expr->toString() << endl;
	assert(0);
      }
      assert(exp);

#if DEBUG_NL
      cout <<endl << "Generated: " << exp->toString() << endl << endl;
#endif

      list<Expression*> *finalList = QuantClearer(exp, NULL);
      exp->expressionList = finalList;
      //FIXME: we leak like a seeeeeve.
#if DEBUG_NL
      //cout << "De-quantified and flattened: " << exp->toString() << endl << endl;
#endif

      //pull out all the FARKS
      GatherFarks(exp, farkList);
      //queryStr = RemoveFarks(queryStr, farkList);
      
#if DEBUG_NL
      //cout << "Stripped: " << queryStr << endl;
      //cout << "Stripped and renamed: " << exp->toString()<< endl << endl;
#endif

      PostProcess(exp);

#if DEBUG_NL
      //cout << "Stripped: " << queryStr << endl;
      //cout << "PostProcessed: " << exp->toString()<< endl << endl;
#endif

      string queryStr = TableTranslate(exp, farkList, numFuckedPhrases);
#if DEBUG_NL
      //cout << "Translated Objects: " << queryStr << endl << endl;
#endif


      AddQuantifiedNounsToFromClause(farkList);
      query.addWhereConjunction(queryStr);
    }

  }

  query.setTableName("AttrSoup");
  //score for the percentage of phrases we knew (penalized for left over variables)
  score = double(numLeaves + numOtherRels - numFuckedPhrases)/
    (numLeaves + numOtherRels + numUnknownLeaves+ numUnknownOtherRels);

  if(query.getQuery().find('%') != string::npos)
    score += 2;

  K2YMainVerb *verb       = (K2YMainVerb *)(sentence->args[0]);
  K2YQuantifier *quantifier = (K2YQuantifier *)(verb->args[1]);
  K2YObject *directObject = quantifier->quantified;
  if(!directObject || !directObject->getRelation()){
#if DEBUG_NL || DEBUG_GRAMMAR
    cout << "We couldn't get a Direct Object" << endl;
#endif
    score = -MAXFLOAT;
    return;
  }
  query.setDOName( g_strdup_printf("x%d", directObject->getRelation()->id()));  
  if(!WeFarkedTheDO(directObject->getRelation()->id(), farkList)){
#if DEBUG_NL || DEBUG_GRAMMAR
    cout << "We didn't connect up the Direct Object" << endl;
#endif
    score = -MAXFLOAT;
  }
}

bool EntityResolver::WeFarkedTheDO(int doID, map<Variable*,int> &farkList){
  map<Variable*, int>::iterator it, endIt = farkList.end();
  for(it = farkList.begin(); it != endIt; it++){
    if(it->second == doID)
      return true;
  }
  return false;
}

void EntityResolver::AddQuantifiedNounsToFromClause(map<Variable*, int> &farkList){
  map<Variable*, int>::iterator it, endIt = farkList.end();
  for(it = farkList.begin(); it != endIt; it++){
    string varName = g_strdup_printf("x%d", it->second);
    
    query.addFromClause(varName);
  }
}

bool EntityResolver::isAndExpr(Expression *expr){
  StringExpression *strexpr = dynamic_cast<StringExpression*>(expr);
  return (strexpr && 
	  (strexpr->getName() == "AND" || 
	   strexpr->getName() == " AND " ||
	   strexpr->getName() == "and" || 
	   strexpr->getName() == " and " ));
}


void EntityResolver::PostProcess(LambdaExpression *exp){
  //remove from and back ands
  if((exp->expressionList->size() > 0) && 
     isAndExpr(exp->expressionList->front()))
    exp->expressionList->pop_front();
  
  if((exp->expressionList->size() > 0) && 
     isAndExpr(exp->expressionList->back()))
    exp->expressionList->pop_back();
  

  if(exp->expressionList->size() < 2) return;

  list<Expression *>::iterator it, pastIt, endIt;
  endIt = exp->expressionList->end();
  pastIt = exp->expressionList->begin();
  it = exp->expressionList->begin();
  it++;
  for(; it != endIt; it++, pastIt++){
    if(isAndExpr(*it) && isAndExpr(*pastIt))
      exp->expressionList->erase(pastIt);

  }
}

//pull out all the faks
void EntityResolver::GatherFarks(LambdaExpression *lexp, map<Variable*, int> &farkList){
  cout << "Gathering farks on " << lexp->toString() << endl;
  list<Expression *>::iterator it, endIt = lexp->expressionList->end();
  for(it = lexp->expressionList->begin(); it != endIt; it++){
    Expression *expr = *it;
    LambdaExpression *lam = dynamic_cast<LambdaExpression*>(expr);
    if(lam){
      GatherFarks(lam, farkList);
      continue;
    }
    StringExpression *str = dynamic_cast<StringExpression*>(expr);
    if(str && str->getName() == "fark"){
      list<Expression*>::iterator prev = it;
      prev--;
      
      //remove the fark
      lexp->expressionList->erase(it);
      it = prev;
      it++;
      assert(it != endIt);

      //get the paren
      StringExpression *strexpr = dynamic_cast<StringExpression*>(*it);
      assert(strexpr);
      assert(strexpr->getName() == "(");
      
      //remove the paren
      lexp->expressionList->erase(it);
      it = prev;
      it++;
      assert(it != endIt);

      //get the var
      Variable *var = dynamic_cast<Variable*>(*it);
      string varName = "";
      if(var){
	varName = var->getName();
      }
      else{
	LambdaExpression *lam = dynamic_cast<LambdaExpression*>(*it);
	if(!lam)
	  continue;
	//assert(lam);
	//if we are nested at all
	LambdaExpression *childLam;
	while(lam->arguments.empty() && 
	      lam->expressionList->size() == 1 &&
	      (childLam = dynamic_cast<LambdaExpression*>(lam->expressionList->front()))){
	  lam = childLam;
	}

	assert(lam->arguments.size() > 0);
	varName = lam->arguments.front()->getName();
      }


      //remove the var or quantifie expression
      lexp->expressionList->erase(it);
      it = prev;
      it++;
      assert(it != endIt);

      //get the id
      strexpr = dynamic_cast<StringExpression*>(*it);
      assert(strexpr);
      int id = atoi(strexpr->getName().c_str());

      //remove the id
      lexp->expressionList->erase(it);
      it = prev;
      it++;
      assert(it != endIt);

      //get the last paren
      strexpr = dynamic_cast<StringExpression*>(*it);
      assert(strexpr);
      // FIXME: make this not happen
      if (strexpr->getName() != ")") {
	score = -MAXFLOAT;
	return;
      }
      assert(strexpr->getName() == ")");

      //remove the last paren
      lexp->expressionList->erase(it);
      it = prev;

      //add it to the farkList
      farkList[var] = id;
      var->setName(g_strdup_printf("x%d",id));
    }

  }
}

list<Expression*> * EntityResolver::QuantClearer(LambdaExpression *lexpr, Variable **var){
  list<Expression*> *newList = new list<Expression*>();
  list<Expression*>::iterator it, drag, endIt = lexpr->expressionList->end();

  if(lexpr->arguments.size() > 0 && 
     lexpr->arguments.front()->getName().find("uantifie") != string::npos &&
     var){
    *var = lexpr->arguments.front();
    //cout << "setting quant to " << lexpr->arguments.front()->getName() << endl;
  }
  Variable *childVar = NULL;
  for(it = lexpr->expressionList->begin(), drag = it; it != endIt; it++){
    LambdaExpression * lamChild = dynamic_cast<LambdaExpression*>(*it);
    if(lamChild){

      bool before = false;
      StringExpression* dragStr= dynamic_cast<StringExpression*>(*drag);
      if(dragStr){
	string str = dragStr->toString();
	if(str[str.size() - 1] == '=')
	  before = true;
      }

      Variable *quantVarName = NULL;
      list<Expression*> *childList = QuantClearer(lamChild, &quantVarName);
      if(quantVarName)
	childVar = quantVarName;

      if(!before)
	newList->insert(newList->end(), childList->begin(), childList->end());


      list<Expression*>::iterator cursor = it;
      cursor++;
      if(cursor != endIt){ //look for the '.'
	StringExpression *nextStr = dynamic_cast<StringExpression*>(*cursor);
	if(nextStr && nextStr->toString()[0] == '.'){
	  it++;

	  if(!before)
	    newList->push_back( new StringExpression(" AND "));

	  if(quantVarName)
	    newList->push_back( quantVarName);
	  else{
	    newList->push_back( unFarkedVar);//this way it'll remove it later
	  }

	  newList->push_back(nextStr);

	  //if they are seperated
	  list<Expression*>::iterator itWow = cursor;
	  itWow++;
	  StringExpression *wowStr = NULL;
	  if(nextStr->toString().size() == 1 && 
	     itWow != endIt && 
	     (wowStr = dynamic_cast<StringExpression*>(*itWow))){
	    newList->push_back(wowStr);
	    it++;
	  }

	  if(before)
	    newList->push_back( new StringExpression(" AND "));
	    
	}
      }

      if(before)
	newList->insert(newList->end(), childList->begin(), childList->end());
	
      continue;
    }

    
    newList->push_back(*it);
    drag = it;
  }

  //set the 
  if(var && *var == NULL)
    *var = childVar;
  return newList;
}

static void printList (list<Expression*> *expressions) {
  if (expressions->begin() == expressions->end()) {
    cerr << "LIST IS EMPTY" << endl;
    return;
  }
  list<Expression*>::iterator it = expressions->begin();
  while (it != expressions->end()) {
    Expression *expr = *it;
    if (dynamic_cast<Variable *>(expr)) {
      cout << "      type is var" << endl;
    } else if (dynamic_cast<LambdaExpression *>(expr)) {
      cout << "      type is lambda" << endl;
    } else if (dynamic_cast<StringExpression *>(expr)) {
      cout << "      type is string" << endl;

    }
    cout << "  pl: " << expr->toString() << endl;
    it++;
  }
}

//also removes the lambdas
string EntityResolver::TableTranslate(LambdaExpression *lexpr, 
				      map<Variable*, int> &farkList, 
				      int &numPhrasesFucked){


  //read through expression list
  list<Expression*> *expressionList = lexpr->expressionList;
  list<Expression*>::iterator it, endIt = expressionList->end();
  bool rollOver;
  
  for(it = expressionList->begin(); it != endIt; (rollOver) ? it++ : it){
    Expression *expr = *it;
    rollOver = true;

    StringExpression *strexp = dynamic_cast<StringExpression*>(expr);
    if(strexp){
      continue;
    }
    
    Variable *var = dynamic_cast<Variable*>(expr);
    if(var){
      map<Variable*, int>::iterator findIt;
      if((findIt = farkList.find(var)) == farkList.end()){
	//delete this phrase
	numPhrasesFucked++;


#if DEBUG_NL || DEBUG_GRAMMAR
	cout << "Variable : " << var->getName()  << " not quantified over" << endl;
	cout << "\tin " << lexpr->toString() << endl << endl;
#endif
	it = removeConjunction(expressionList, it);
      }
      else{
	//make sure the next coming up is a string
	list<Expression*>::iterator prevIt = it;

	it++;
	expressionList->erase(prevIt);//erase the varname
	prevIt = it;
	assert(it != expressionList->end());
	StringExpression *dotStr = dynamic_cast<StringExpression*>(*it);
	assert(dotStr);
	assert(dotStr->getName()[0] == '.');

	it++;
	expressionList->erase(prevIt);//erase the dotstr
	string attrName;

	if(dotStr->getName().size() > 1){
	  attrName = dotStr->getName().substr(1);
	}
	else{
	  prevIt = it;
	  assert(it != expressionList->end());
	  StringExpression *attrStr = dynamic_cast<StringExpression*>(*it);
	  assert(attrStr);
	  attrName = attrStr->getName();

	  it++;
	  expressionList->erase(prevIt);//erase the attrName
	}


	string insertString;
	const char *attrcstr = attrName.c_str();
	if(strcasecmp(attrcstr, "attrName") == 0){
	  insertString = var->getName() + ".attrName";
	}
	else if(strcasecmp(attrcstr, "attrValue") == 0){
	  insertString = var->getName() + ".attrValue";
	}
	else if(strcasecmp(attrcstr, "recordid") == 0){
	  insertString = var->getName() + ".recordid";
	}
	else{ //change the variablename
	  //create a new entry
	  string newVarName = var->getName()+ "_" + attrName;
	  query.addWhereConjunction(var->getName() + ".recordid = " + newVarName + ".recordid ");

	  query.addFromClause(newVarName, tableFromType(storage_get_type_from_attribute(strdup(attrName.c_str()))));

	  insertString = newVarName + ".attrName = '" + attrName + "'"; 
	  expressionList->insert(it, new StringExpression(insertString));
	  insertString = " AND ";
	  expressionList->insert(it, new StringExpression(insertString));
	  insertString = newVarName + ".attrValue";
	  
	}
	expressionList->insert(it, new StringExpression(insertString));
      }
      rollOver = false;
      continue;
    }
    
    LambdaExpression *lexp = dynamic_cast<LambdaExpression*>(expr);
    assert(lexp);
    StringExpression * lamReplace = new StringExpression(TableTranslate(lexp, farkList, numPhrasesFucked));
    list<Expression*>::iterator delIt = it;
    it++;
    expressionList->erase(delIt);
    expressionList->insert(it, lamReplace);
    rollOver = false;
  }
  
  //now build up the query
  string query;
  endIt = expressionList->end();
  for(it = expressionList->begin(); it != endIt; it++){
    query += (*it)->toString()+ ' ';
  }
  return query;
}


//returns the position in the expressionList
list<Expression*>::iterator EntityResolver::removeConjunction(list<Expression*> *expressionList, 
							      list<Expression*>::iterator it){

  //printList(expressionList);
  
  //refind the andIt in here
  list<Expression*>::iterator currIt, andIt = expressionList->begin();
  for(currIt = expressionList->begin(); currIt != expressionList->end(); currIt++){
    if(isAndExpr(*currIt)){
      andIt = currIt;
    }
    if(currIt == it)
      break;
  }
  
#if DEBUG_NL||DEBUG_GRAMMAR	
  cout << "\tTherefore removing [";cout.flush();
#endif

  list<Expression*>::iterator cursor = andIt;
  cursor++;
  bool firstTime = true;
  bool passed = false;
  do{
    if(!passed && (andIt == it)) 
      passed = true;
    if(firstTime){//always get rid of the first one
#if DEBUG_NL||DEBUG_GRAMMAR
      cout << (*andIt)->toString() << ' '; cout.flush();
#endif
      expressionList->erase(andIt);
      andIt = cursor;
      if(andIt == expressionList->end())
	break;
      cursor++;
      firstTime = false;
      continue;
    }

    StringExpression * possibleAnd = dynamic_cast<StringExpression*>(*andIt);
    if(possibleAnd && 
       ((strcasecmp(possibleAnd->getName().c_str(),"AND") == 0 ) ||
	 (strcasecmp(possibleAnd->getName().c_str(), " AND ") == 0))){
      if(andIt == expressionList->begin()){
	//cout << "deleted the later AND" << endl;
#if DEBUG_NL|| DEBUG_GRAMMAR	
	cout << possibleAnd->getName(); cout.flush();
#endif
	expressionList->erase(andIt);
	andIt = cursor;
      }
      //else{
	//cout << "found and AND but wasn't at the beginning";
	//}
      if(passed)
	break;
    }
#if DEBUG_NL|| DEBUG_GRAMMAR
    cout << (*andIt)->toString() << " "; cout.flush();
#endif	  
    expressionList->erase(andIt);
    andIt = cursor;
    cursor++;
  }while(andIt != expressionList->end());
	
  cout << "]" << endl;
  return andIt;
}
