#ifndef ENTITY_RESOLVER_HH_
#define ENTITY_RESOLVER_HH_

class EntityResolver;

#include <pet-parser.hh>

#include <sql-query.hh>
#include <generator.hh>

#include "lambda.hh"

class EntityResolver {
public:
  EntityResolver (PET::K2YObject *sentence, 
		  std::list<PET::K2YObject *> &objects, 
		  Generator *generator);

  SQLQuery getQuery();
  float score;

protected:
  SQLQuery resolve (PET::K2YEntity *entity, std::list<PET::K2YObject *> &objects);
  //void processEntitiesLater (std::vector<PET::K2YObject *> &entityList);
  std::string TableTranslate(LambdaExpression *lexpr, 
			     std::map<Variable*, int> &farkList, 
			     int &numPhrasesFucked);
  void GatherFarks(LambdaExpression *lexp, std::map<Variable*, int> &farkList);
  //std::string ApplyFarks(std::string queryStr, 
  //			 std::map<std::string, int> &farkList);
  Expression* LambdaComputeTree(PET::K2YObject *obj);
  void AddQuantifiedNounsToFromClause(std::map<Variable*, int> &farkList);
  std::list<Expression*>::iterator removeConjunction(std::list<Expression*> *expressionList, 
						     std::list<Expression*>::iterator it);
  list<Expression*> *  QuantClearer(LambdaExpression *lexpr, Variable **var);
  bool WeFarkedTheDO(int doID, map<Variable*,int> &farkList);
  void PostProcess(LambdaExpression *exp);
  void printOutTable(list<Expression*> *list);
  bool isAndExpr(Expression *expr);

  //data
  Generator *generator;
  std::list<PET::K2YEntity *> entities;
  SQLQuery query;
  Variable *unFarkedVar; //used as a place holder.. i dunno its weird.
};

#endif
