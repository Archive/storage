#ifndef GENERATOR_HH_
#define GENERATOR_HH_

class Generator;

#include <pet-parser.hh>

#include "generated.hh"

class Generator {
public:
  virtual Generated *generate (PET::K2YObject *object) = 0;
};

#endif
