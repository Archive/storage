#include "k2yobject-utils.hh"
#include <iostream.h>

using namespace PET;

string objectListToString (list<PET::K2YObject *> &objects) {
  string result;
  list<PET::K2YObject *>::iterator objIt;	
  for(objIt = objects.begin(); objIt != objects.end(); objIt++){
    if(!objectInSomeArgs(*objIt, objects)){
      result += "   " + objectToString(*objIt, true) + "\n";
    }
  }

  return result;
}

// Returns true if "object" is in the args of possibleParent
bool objectInArgs (K2YObject *object, K2YObject *possibleParent) {
  //cout << "Looking at: " << possibleParent->getRelationName() << " with " << possibleParent->args.size() << " args" << endl;
  vector<PET::K2YObject *>::iterator i = possibleParent->args.begin();

  while (i != possibleParent->args.end()) {
    PET::K2YObject *possibleMatch = *i;
    //cout << "  comparing " << object->getRelationName() << " with " << possibleMatch->getRelationName() << endl;
    //FIXME: this is probably wrong.... are ids matching really goin to make
    // positively sure these are the same node? Damn you K2Y
    if (possibleMatch->getRelation()->id() == object->getRelation()->id()) {
      return true;
    }

    i++;
  }

  return false;
}

string objectToString (PET::K2YObject *object, bool objectTree) {
  vector<PET::K2YObject *>::iterator i = object->args.begin();

  char tmp[3] = "a:";
  tmp[0] += object->getRelation()->id();
  string output = string(tmp) + object->getRelationName() + " (";
  while (i != object->args.end()) {
    PET::K2YObject *child = *i;
    if (objectTree) {
      output += objectToString(child, true) + " ";
    } else {
      char tmp[3] = "a:";
      tmp[0] += child->getRelation()->id();
      output += string(tmp) + child->getRelationName() + " ";
    }

    i++;
  }

  output += ")";

  return output;
}


bool objectInSomeArgs (PET::K2YObject *object, list<PET::K2YObject *> &possibleParents) {
  list<PET::K2YObject *>::iterator i = possibleParents.begin();

  while (i != possibleParents.end()) {
    PET::K2YObject *possibleParent = *i;

    if (objectInArgs (object, possibleParent)) {
      return true;
    }

    i++;
  }

  return false;
}
