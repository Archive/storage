#ifndef K2YOBJECT_UTILS_HH_
#define K2YOBJECT_UTILS_HH_

#include <pet-parser.hh>

// Returns true if "object" is in the args of possibleParent
bool objectInArgs (PET::K2YObject *object, PET::K2YObject *possibleParent);
string objectToString (PET::K2YObject *object, bool printTree = false);
bool objectInSomeArgs (PET::K2YObject *object, list<PET::K2YObject *> &possibleParents);
string objectListToString (list<PET::K2YObject *> &objects);
#endif
