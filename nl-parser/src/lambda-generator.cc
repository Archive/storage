#include "lambda-generator.hh"
#include "k2yobject-utils.hh"
#include "debug-out.hh"

#include <vector>
#include <iostream>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>

#include "macros.hh"

using namespace PET;

LambdaGenerator::LambdaGenerator (string semanticTranslationFile) {

  registerMacros();

  // FIXME: we leak like crazy in here
  xmlDocPtr doc;
  xmlNodePtr relNode;
  const char *name;

  doc = xmlParseFile (semanticTranslationFile.c_str());   

  if (doc == NULL
      || doc->xmlRootNode == NULL
      || doc->xmlRootNode->name == NULL) {
    xmlFreeDoc(doc);
    cerr << "WARNING: could not read " << semanticTranslationFile << endl;
    return;
  }

  for (relNode = doc->xmlRootNode->xmlChildrenNode; relNode != NULL; relNode = relNode->next) {
    if (strcmp ((const char *)relNode->name, "rel") == 0) {
      // This is a relation definition
      name = (const char *)xmlGetProp (relNode, (const xmlChar *)"name");
      if (name == NULL || relNode->type != XML_ELEMENT_NODE) continue;
      
      xmlNodePtr lambdaNode = relNode->xmlChildrenNode;
      //FIXME: check node type here
      /*if (relNode->type != XML_TEXT_NODE) {
	printf ("Hit invalid node reading %s, expected textual lambda expression but got something else\n", name);
	}*/
      char *lambdaString = (char *)xmlNodeGetContent(lambdaNode);
      
      list<string> *expressionStrs = findExpression(name);
      if (expressionStrs == NULL) {
	expressionStrs = new list<string>;
	relToLambdaStrings.push_back(pair<string, list<string> *>(string(name), expressionStrs));
      }
      expressionStrs->push_back (lambdaString);

    } else if (strcmp ((const char *)relNode->name, "typeshift") == 0) {
      // This is a typeshift definition
      xmlNodePtr lambdaNode = relNode->xmlChildrenNode;
      char *typeshiftString = (char *)xmlNodeGetContent(lambdaNode);      
      //cout << "Typeshift: " << typeshiftString << endl;
      Expression * typeExpr = Expression::parseExpression(typeshiftString);
      Expression::RegisterTypeShift(typeExpr);
    } else if (strcmp ((const char *)relNode->name, "macro") == 0) {
      name = (const char *)xmlGetProp (relNode, (const xmlChar *)"name");
      if (name == NULL || relNode->type != XML_ELEMENT_NODE) continue;
      
      xmlNodePtr macroNode = relNode->xmlChildrenNode;
      char *macroString = (char *)xmlNodeGetContent(macroNode);

      if (macroString) {
	Expression::RegisterStringMacro(name, macroString);
	free (macroString);
      }
    }
  }
}

list<string> *LambdaGenerator::findExpression (string rel) {
  list<pair<string, list<string> *> >::iterator it, itEnd = relToLambdaStrings.end();

  for (it = relToLambdaStrings.begin(); it != itEnd; it++) {
    if ((*it).first == rel) return (*it).second;
  }
  /*cout << "couldn't find " << rel << "in" << endl;
    for(i = relToLambdaStrings.begin(); i != relToLambdaStrings.end(); i++) {
    cout << "[" << it->first << "," << it->second << "]" << endl;
    }
    cout << endl;//*/

  return NULL;
}

LambdaGenerator::~LambdaGenerator () {

}

Expression* LambdaGenerator::LambdaComputeQuantifier (K2YQuantifier *quantifier, LambdaGasGenerated *gas) {
  string expression = g_strdup_printf("\\[e]Quantifier%d[]:[", quantifier->getRelation()->id());

  bool first = true;
  string meaning;

  for(int i = quantifier->args.size() -1 ; i >= 0 ; i--) {
    Expression *childLambda = LambdaComputeTree(quantifier->args[i], gas);

    if (first) {
      meaning = childLambda->meaning;
    } else {
      meaning += ", " + childLambda->meaning;
    }

    if (childLambda != NULL) {
      string childExpression = childLambda->toString(true);

      if (!first) {
	expression += " AND ";
      } else {
	first = false;
      }
      expression += childExpression + g_strdup_printf(" Quantifier%d ", quantifier->getRelation()->id());
    }
  }
  expression += " ]";

  Expression *expr = Expression::parseExpression(expression.c_str());
  dynamic_cast<LambdaExpression*>(expr)->setIAmQuant(true);
  expr->meaning = "(" + quantifier->getRelationName() + " " + meaning + ")";
  cout << "[[" << expr->meaning << "]] = " << expr->toPrettyString() << endl;
  return expr;
}

Expression* LambdaGenerator::LambdaComputeTree(K2YObject *object, LambdaGasGenerated *gas){
  if (object->getRole() == K2Y_QUANTIFIER) {
    return LambdaComputeQuantifier ((K2YQuantifier *)object, gas);
  }

  string relname = object->getRelationName();
  string expressionStr;
  Expression *expr = NULL, *expr2;

  list<string> *expressionStrs = findExpression(relname);
  if(!expressionStrs && object->args.size() < 1)
    expressionStrs = findExpression("_thing_rel");
  
  if (expressionStrs) {
#if DEBUG_NL
    cout << "Found expressions for " << objectToString(object) << endl;
#endif
    //FIXME: we assume only one valid one
    expressionStr = expressionStrs->front();
    expr = Expression::parseExpression(expressionStr.c_str(), object);
    if (expr) expr->meaning = relname;

    //update stats
    if(object->args.size() > 0) gas->numOtherRels++;
    else gas->numLeaves++;
  }
  else{
#if DEBUG_NL
    cout << "Couldn't find expressions for " << objectToString(object) << endl;
#endif
    //update stats
    if(object->args.size() > 0)  gas->numUnknownOtherRels++; 
    else  gas->numUnknownLeaves++;
  }
  

  for(int i = object->args.size() -1 ; i >= 0 ; i--){
    Expression *childLambda = LambdaComputeTree(object->args[i], gas);

    if(!childLambda){
      continue;
    }
    if(expr == NULL){
      expr = childLambda;
      expr->meaning = "(" + relname + " " + childLambda->meaning + ")";

      cout << "[[" << expr->meaning << "]] = " << expr->toPrettyString() << endl << endl;
    }
    else{

      string process = "Applying " + expr->toString() + "\n\tand  " + childLambda->toString() +"\n";
      expr2 = Expression::Apply(expr, childLambda);
      process += "\tand got  " + (expr2 ? expr2->toString() : "an empty expr") + "\n";
     
#if DEBUG_NL
      cout << process;
#endif
 
      if(!expr2){

#if DEBUG_GRAMMAR
	cout << "Failed to apply: " << expr->meaning << endl << 
	  "\t and " << childLambda->meaning << endl << endl;
#endif
	continue;
      }
      //keep around the meaning
      string prev_meaning = expr->meaning;
      expr = expr2;
      expr->meaning = "(" + prev_meaning + " " + childLambda->meaning + ")";

#if DEBUG_GRAMMAR
      cout << "[[" << expr->meaning << "]] = " << expr->toPrettyString() << endl;
      if(expr->weirdness.size() > 1) cout << expr->weirdness << endl;
      cout << endl;
#endif

    }
  }
  //else {
  //cout << "Couldn't find expression for " << objectToString(object) << endl;
    //query.predicatesWithoutMeaning.push_back(objectToString(object));
  //}

  return expr;
}



Generated*  LambdaGenerator::generate(K2YObject *object) {
  LambdaGasGenerated *gas = new LambdaGasGenerated();
  //#if DEBUG_GRAMMAR
  //cout << "Working on root " <<  objectToString(object) << endl;
  //#endif
  gas->expr = LambdaComputeTree(object, gas);
  //cout << "Generated: " << (gas->expr ? gas->expr->toString() : "an empty expr") << endl;
  return gas;
}
