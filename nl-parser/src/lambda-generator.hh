#ifndef Lambda_GENERATOR_HH_
#define Lambda_GENERATOR_HH_

class LambdaGenerator;

#include <string>
#include <list>

#include <pet-parser.hh>

#include "generator.hh"
#include "lambda.hh"

class LambdaGasGenerated : public Generated{
public:
  LambdaGasGenerated():numOtherRels(0), numLeaves(0), numUnknownOtherRels(0), numUnknownLeaves(0){}
  Expression *expr;

  //stats
  int numOtherRels;
  int numLeaves;
  int numUnknownOtherRels; 
  int numUnknownLeaves;
};

class LambdaGenerator : public Generator {
 public:
  LambdaGenerator (string semanticTranslationFile);
  virtual ~LambdaGenerator();  

  virtual Generated* generate (PET::K2YObject *object);

 protected:
  Expression* LambdaComputeTree(PET::K2YObject *object, LambdaGasGenerated *gas);
  Expression* LambdaComputeQuantifier (PET::K2YQuantifier *quantifier, LambdaGasGenerated *gas);

  std::list<std::pair <std::string, std::list<std::string> *> > relToLambdaStrings;
  std::list<std::string> *findExpression (std::string rel);
};

#endif
