#include "lambda.hh"
#include "debug-out.hh"
#include <iostream>

using namespace std; 
using namespace PET;

Expression::MyFuncMap * Expression::functionTable = new Expression::MyFuncMap();
Expression::MyStringMap * Expression::stringSubstitutionTable = new Expression::MyStringMap();
vector<LambdaExpression *> *Expression::typeShifts = new vector<LambdaExpression*>();

/* Note about GScanner
 * if the scanner's return value is less than 256, it a single character (must use return value
 * instead of value->v_char.
 */

//FIXME: the cloning stuff is crap, you can't generated references to variables that are outside your scope. Stop using but make sure its safe.

//Maybe right?
#define THIS_SUCKS 1

void parseCheck(int expected, GScanner *cat, string& prefix){
  GTokenType gtt =  g_scanner_get_next_token(cat);
  if(gtt == G_TOKEN_ERROR){
    cerr << "GScanner errored w/ prefix: " << prefix << endl;
    assert(gtt != G_TOKEN_ERROR);
  }
  if(gtt != expected){
    cerr << "Parse Error, expected " << (char)expected << " here: " << prefix << endl;
    assert(gtt == expected);
  }
  prefix += (char)gtt;
}



string parseToken(GScanner *cat, string& prefix){
  string token;
  GTokenType gtt =  g_scanner_get_next_token(cat);
  if(gtt == G_TOKEN_ERROR){
    cerr << "GScanner errored w/ prefix: " << prefix << endl;
    assert(gtt != G_TOKEN_ERROR);
  }
  if(gtt < 256){
    token = (char)gtt;
  }
  else if (gtt == G_TOKEN_INT){
    token = g_strdup_printf("%d", cat->value.v_int);
  }
  else{
    token = cat->value.v_string;
  }
  prefix += ' ' + token;
  return token;
}

bool Type::equals(Type *other) {


  ComplexType *usComplex    = dynamic_cast<ComplexType*>(this);
  ComplexType *otherComplex = dynamic_cast<ComplexType*>(other);

  //see if either is a string object
  if((usComplex && !usComplex->to && !usComplex->from) ||
     (otherComplex && !otherComplex->to && !otherComplex->from))
    return false;

  //check for the null types
  if(usComplex && 
     usComplex->from->isNullType() && 
     usComplex->to->equals(other))
    return true;

  if(otherComplex && 
     otherComplex->from->isNullType() && 
     otherComplex->to->equals(this))
    return true;

  //now do standard comparisons
  if(otherComplex && usComplex)
    return otherComplex->from->equals(usComplex->from) && otherComplex->to->equals(usComplex->to);
  
  ConcreteType *usConcrete    = dynamic_cast<ConcreteType*>(this);
  ConcreteType *otherConcrete = dynamic_cast<ConcreteType*>(other);
  
  if (usConcrete && otherConcrete)
    return (usConcrete->toString(false) == usConcrete->toString(false));
  
  //they are different types
  return false;
}

bool Type::isNullType() {
  ConcreteType *type = dynamic_cast<ConcreteType*>(this);

  if (!type) return false;

  return this->toString(false) == string("[]");
}

Type * Type::parseType(GScanner * cat, string& prefix){
  parseCheck(G_TOKEN_LEFT_BRACE, cat, prefix);
  
  //get the first argument
  Type *first;
  bool haveSecond = getArg(cat, first, prefix);
  
  if(haveSecond){
    Type *second;
    parseCheck(G_TOKEN_COMMA, cat, prefix);
    getArg(cat, second, prefix);
    first = new ComplexType(first, second);
  }
 
  parseCheck(G_TOKEN_RIGHT_BRACE, cat, prefix);
  return first;
}

bool Type::getArg(GScanner * cat, Type *& t, string& prefix){
  GTokenType gtt;
  gtt = g_scanner_peek_next_token(cat);
  if(gtt == G_TOKEN_LEFT_BRACE){
    t = parseType(cat, prefix);
    return true;
  }
  else if(gtt == G_TOKEN_RIGHT_BRACE){
    t = new ConcreteType("");
    return false;
  }
  else{
    t = new ConcreteType(parseToken(cat, prefix));
    gtt = g_scanner_peek_next_token(cat);
    return (G_TOKEN_COMMA == gtt);
  }
}

ComplexType::ComplexType(Type *first, Type *second):
  from(first),
  to(second){}
  
string ComplexType::toString(bool forClone){
  string retString = "[";
  ConcreteType *conc;

  if((conc = dynamic_cast<ConcreteType*>(from)))
    retString += conc->value;
  else
    retString += from->toString(forClone);

  retString += ",";

  if((conc = dynamic_cast<ConcreteType*>(to)))
    retString += conc->value;
  else
    retString += to->toString(forClone);

  retString += "]";
  return retString;
}

ComplexType* ComplexType::clone(){
  return new ComplexType(from->clone(), to->clone());
}


ConcreteType::ConcreteType(string name):
  value(name){
}

string ConcreteType::toString(bool forClone){
  return "[" + value + "]";
}

ConcreteType* ConcreteType::clone(){
  return new ConcreteType(value);
}

//Variable
Variable::Variable(GScanner * cat, string& prefix){
  name = parseToken(cat, prefix);
  type = Type::parseType(cat, prefix);
}

string Variable::toString(bool forClone){
  string retval = name;
#if LAMBDA_DEBUG
  //ONLY IF you're crazy
  //if(!forClone){
  //  retval += g_strdup_printf("<%d>", (int)this);
  //}
#endif
  return retval;
}

//FIXME: don't use, your references won't work any more!!!!
Variable* Variable::clone(){
  Variable *var = new Variable();
  var->name = name;
  var->type = type->clone();
  var->id = id;
  return var;
}

//

string Expression::toPrettyString() {
  return this->toString(false);
}

string Expression::parseMacro(GScanner*cat, K2YObject *obj, string& prefix){
  parseCheck('{', cat, prefix);
  string name = parseToken(cat, prefix);

  string argument;
  GTokenType gtt =  g_scanner_peek_next_token(cat);
  if(gtt != '}'){
    argument = parseToken(cat, prefix);
  }
  
  string retValue;
  MyFuncMap::iterator it = functionTable->find(name);
  MyStringMap::iterator it2 = stringSubstitutionTable->find(name);
  if(it != functionTable->end()) {
    retValue = (*(it->second))(obj, argument);
  } else if  (it2 != stringSubstitutionTable->end()){
    retValue = it2->second;
  } else{
    cout << "Couldn't find a function for macro name " << name << endl;
  }
  while(parseToken(cat, prefix)[0] != '}');
  return retValue;
}
void Expression::RegisterMacro(std::string macroName, MyMacroFunc func){
  (*Expression::functionTable)[macroName] = func;
}

void Expression::RegisterStringMacro(std::string macroName, std::string macroContents){
  (*Expression::stringSubstitutionTable)[macroName] = macroContents;
}

void Expression::RegisterTypeShift(Expression *expr){
  LambdaExpression *lexp;
  if((lexp = dynamic_cast<LambdaExpression*>(expr)) != NULL){
    typeShifts->push_back(lexp);
  }
  else{
    printf("Error: TypeShifting Expression not a LambdaFunction\n");
  }
}

//class Expression
list<Expression*> *
Expression::parseExpressionList(GScanner* cat, MyMap * varMap, 
				LambdaExpression *parentLambda, string& prefix){
  parseCheck(G_TOKEN_LEFT_BRACE, cat, prefix);

  list<Expression*> *exprList = new list<Expression*>();

  while(true){
    GTokenType gtt = g_scanner_peek_next_token(cat);
      
    if(gtt == '\\'){
      exprList->push_back(new LambdaExpression(cat, prefix, varMap, parentLambda->obj, parentLambda));
      continue;
    }
    if(gtt == '{'){
      string macroReturn = Expression::parseMacro(cat, parentLambda->obj, prefix);
      exprList->push_back(new StringExpression(macroReturn));
      continue;
    }
    //if(gtt == '*'){
    //  g_scanner_get_next_token(cat);
    //  exprList->push_back(new Variable(cat));
    //  continue;
    //}
    string temp = parseToken(cat, prefix);
    if(temp == "]"){
      break;
    }
    MyMap::iterator it = varMap->find(temp);
    if(it == varMap->end()){
      //printf("Got a string %s\n", ident);
      exprList->push_back(new StringExpression(temp));
    }
    else{
      //printf("Found Var %s, %d\n", ident, (int)it->second);
      exprList->push_back(it->second);
    }
    
  }
  
  return exprList;
}

static bool firstTime = true;
Expression *Expression::parseExpression(const char * expression,  K2YObject *obj){
  /*
  if(firstTime){
    firstTime = false;
    Expression *e1 = parseExpression("\\[t]Var[e,t],Var3[e,t]:[yes Var]");
    Expression *e2 = parseExpression("\\[t]Var3[e]:[\"yes Me\"]");
    //Expression * shift = parseExpression("\\[[e,t],[e,t]]Iv[e,t]:[\\[e,t]Fv[e,t]:[\\[t]Yv[e]:[Fv Yv AND Iv Yv]]]");
    //noun (DO)
    //Expression *e1 = parseExpression("\\[t]Var[e]:[Var * mimetype in {music}]");

    //Expression *e2 = parseExpression("\\[e,t]Var2[e]:[\\[t]Var3[e]:[Var3 owns Var2]]");
    //Expression * shift = parseExpression("\\[[e,[e,t]],[e,[e,t]]]Iv[e,t]:[\\[e,[e,t]]Fv[e,[e,t]]:[\\[e,t]Yv[e]:[\\[t]Xv[e]:[Fv Xv Yv AND Iv Xv]]]]");
    cout << e1->toString() << endl
	 << e2->toString() << endl 
      // << shift->toString() << endl
      ;
    //Expression::RegisterTypeShift(shift);
    Expression *e3 = Expression::Apply(e1, e2);
    if(e3){
      cout << e3->toString() << endl;
    }
    else{
      cout << "I hate you" << endl;
    }
    assert(0);
  }
  else
    firstTime = false;//*/

  if(expression[0] == '\\'){
    string prefix;
    LambdaExpression *expr = new LambdaExpression(expression, prefix, NULL, obj, NULL);
    expr->Simplify();
    return expr;
  }
  else{
    return new StringExpression(string(expression));
  }
}

Expression* Expression::Apply(Expression *one, Expression *two){
  Expression* expr =  ApplyInt(one, two, true);
  //see if we can behead.
  LambdaExpression *lam = dynamic_cast<LambdaExpression*>(expr);
  if(lam){
    //since the ones they are applying have to be fully qualified
    lam->parentLambda = NULL;
    if(lam->arguments.empty() && lam->expressionList->size() == 1){
      expr = lam->expressionList->front();
      //again see if we should make sure the parent node is cut.
      lam = dynamic_cast<LambdaExpression*>(expr);
      if(lam) lam->parentLambda = NULL;
    }
  }
  return expr;
}

Expression *Expression::OrderedApply(Expression *first, Expression *second){
  LambdaExpression *lexpr = dynamic_cast<LambdaExpression*>(first);
  if(lexpr){
    return lexpr->RealApply(second, true, true);
  }
  return NULL;
}

Expression* Expression::ApplyInt(Expression *one, Expression *two, bool debug){
  Expression *finalExpr = NULL;
  LambdaExpression *lexpr1 = dynamic_cast<LambdaExpression*>(one);

  //try w/o shifting
  if(lexpr1 && (finalExpr = lexpr1->RealApply(two, debug, false)))
    return finalExpr;

  LambdaExpression *lexpr2 = dynamic_cast<LambdaExpression*>(two);
  if(lexpr2 && (finalExpr = lexpr2->RealApply(one, debug, false)))
    return finalExpr;

  //now try shifting
  if(lexpr1 && (finalExpr = lexpr1->RealApply(two, debug, true))){
    finalExpr->weirdness = "upshifted the left one";
    return finalExpr;
  }

  if(lexpr2 && (finalExpr = lexpr2->RealApply(one, debug, true))){
    finalExpr->weirdness = "upshifted the right one";
    return finalExpr;
  }

  return finalExpr;
}

Expression *LambdaExpression::RealApply(Expression *expr, bool debug, bool canshift){
#if THIS_SUCKS
  if(iAmQuant) return NULL;
#endif
  if(!arguments.empty()){
    Variable *goner = arguments.front();
    if (expr->getType()->equals(goner->getType())){
      LambdaExpression *newThis = clone();
      Expression *newExpr;
      //didn't used to have these lines
      if(dynamic_cast<LambdaExpression*>(expr))
	newExpr = expr->clone();
      else
	newExpr = expr;
      //newExpr = expr->clone();

      //reget the goner var
      goner = newThis->arguments.front();
      newThis->arguments.pop_front();
      newThis->variableMap.erase(goner->getName());
      newThis->Replace(goner, newExpr);
      
      //behead
      if(newThis->arguments.empty() && 
	 newThis->expressionList->size() == 1){
#if LAMBDA_DEBUG
	if(debug) cout << "beheading this:" << newThis->toString() << endl <<
		    "\tto this:" << newThis->expressionList->front()->toString() << endl << endl;
#endif
	return newThis->expressionList->front();
      }
      return newThis;
    }
    
    if(canshift){
#if LAMBDA_DEBUG 
      //if(debug)cout << "Trying to shift " << toString() << endl;
#endif
      vector<LambdaExpression*>::iterator shiftIt, shiftItEnd = typeShifts->end();
      for(shiftIt = typeShifts->begin(); shiftIt != shiftItEnd; shiftIt++){
	LambdaExpression *typeShift = *shiftIt;

	//FIXME: typeshifts can only be of nested variable stuff
	if(typeShift->arguments.front()->getType()->equals(expr->getType()) &&
	   (typeShift->resultType->equals(arguments.front()->getType()))){
#if LAMBDA_DEBUG 
	  if(debug)cout << "Trying to shift " << toString() << endl;
	  if(debug)cout <<  "\twith this: " << typeShift->toString() << endl;
#endif
	  Expression *shiftedExpr = typeShift->RealApply(expr, false, false);
	  if(!shiftedExpr){
	    cerr << "Shift looked like it would work, but didnt'" << endl;
	    continue;
	  }
	  LambdaExpression *shiftedLamExpr = dynamic_cast<LambdaExpression*>(shiftedExpr);
	  if(!shiftedLamExpr){
	    cerr << "Shift didn't return a lambdaExpression" << endl;
	    continue;
	  }
	  LambdaExpression *newThis = clone();
	  Expression * final = newThis->RealApply(shiftedLamExpr, false, false);
	  if(!final){
	    cerr << "Shifted expr looked like it would work, but didnt'" << endl;
	    continue;
	  }
#if LAMBDA_DEBUG 
	if(debug)cout <<  "\tand got this: " << final->toString() << endl;
#endif
	  return final;
	}
      }
#if LAMBDA_DEBUG 
      //if(debug)cout << "\tbut couldn't find any good shifts" << endl << endl;
#endif
    }

  }

  return NULL;
}

//constructs a new typer from the args and the result on the fly
//FIXME: if not on the fly then re-determine everytime some is applied.
Type *LambdaExpression::getType(){
  ComplexType * head = NULL, *parent = NULL;

  list<Variable*>::iterator it, endIt = arguments.end();
  for(it = arguments.begin(); it != endIt; it++){ 
    Type *argType = (*it)->getType();

    //see if its null
    if(argType->isNullType()) continue;
    
    ComplexType *comp = new ComplexType(argType, NULL);
    if(!parent){
      head = comp;
    }
    else{
      parent->to = comp;
    }

    parent = comp;
  }

  if(!head)
    return resultType;
  

  //FIXME: later don't assume that the result type isn't null.
  parent->to = resultType;
  return head;
}

//StringExpression

StringExpression::StringExpression(string name){
  this->name = name;
}

//keeps the string constants together when rescanning.
string StringExpression::toString(bool forClone){
  if(forClone)
    return '\"'+name+'\"';

  return name;
}


StringExpression* StringExpression::clone(){
  return new StringExpression(name);
}


//class LambdaExpression
LambdaExpression::LambdaExpression(const char *lambda_line, string& prefix, MyMap* outsideBindings,  
				   K2YObject *obj, LambdaExpression * parentLambda){ 
  GScanner* cat = g_scanner_new(NULL);
  //char bland[1000];

  //set it up so "." "/" and "-" work
  //bland[0] = '\0';
  //strcat(bland, G_CSET_a_2_z);
  //strcat(bland, "_0123456789");
  //strcat(bland, G_CSET_A_2_Z);
  //strcat(bland, G_CSET_LATINS);
  //strcat(bland, G_CSET_LATINC);
  //strcat(bland, "./-");//my own additives
  cat->config->cset_identifier_nth = g_strdup_printf("%s%s", 
						     cat->config->cset_identifier_nth, "/-");
  cat->config->cset_identifier_first = g_strdup_printf("%s%s", 
						     cat->config->cset_identifier_first, ".");

  cat->input_name = "lambda expression";
  g_scanner_input_text(cat, lambda_line, strlen(lambda_line));
  
  init(cat, prefix, outsideBindings, obj, parentLambda);
}

LambdaExpression::LambdaExpression(GScanner* cat, string& prefix, MyMap* outsideBindings,  
				   K2YObject *obj, LambdaExpression * parentLambda){
  init(cat, prefix, outsideBindings, obj, parentLambda);
}

void LambdaExpression::init(GScanner* cat, string& prefix, MyMap* outsideBindings, 
			    K2YObject *obj, LambdaExpression * parentLambda){
  this->obj = obj;
  this->parentLambda = parentLambda;
  iAmQuant = false;

  //parse out the args
  parseCheck('\\', cat, prefix);
  
  GTokenType gtt = g_scanner_peek_next_token(cat);

  resultType = Type::parseType(cat, prefix);
  Variable *var;
  if((gtt = g_scanner_peek_next_token(cat)) != ':'){
    var = new Variable(cat, prefix);
    arguments.push_back(var);
    variableMap[var->getName()] = var;
  }
  while(G_TOKEN_COMMA == (gtt = g_scanner_peek_next_token(cat))){    
    parseToken(cat, prefix);

    var = new Variable(cat, prefix);
    arguments.push_back(var);
    variableMap[var->getName()] = var;
  }

  parseCheck(':', cat, prefix);

  bool weDelete;
  if(outsideBindings == NULL){
    weDelete = true;
    outsideBindings = new MyMap();
  }
  else{
    weDelete = false;
  }

  //copy over the existing values with my (closer binding)
  MyMap::iterator end = variableMap.end();
  for(MyMap::iterator it = variableMap.begin(); it != end; it++){
    (*outsideBindings)[(*it).first] = (*it).second;
  }
  expressionList = Expression::parseExpressionList(cat, outsideBindings, this, prefix);

  if(weDelete){
    delete outsideBindings;
  }

}

LambdaExpression::~LambdaExpression(){
  delete expressionList;
  //and delete other stuff, beware of free the variables
  //from the expressionList and the arguments list.
}

void LambdaExpression::EnsureDiffVarsThanParents(){
  if(!parentLambda) return;
  
  list<Variable*>::iterator argit, argendIt = arguments.end();
  for(argit = arguments.begin(); argit != argendIt; argit++){//for each of this lambda's variables
    Variable *tobeExecuted = *argit;

    bool found;
    string toAdd;
    do{//keep renaming if we have to
      found = false;
      LambdaExpression *par = parentLambda;
      while(par != NULL && !found){//search up through all the parents

	//and through all their arguments
	list<Variable*>::iterator parargit, parargendIt = par->arguments.end();
	for(parargit = par->arguments.begin(); parargit != parargendIt; parargit++){
	  if((*parargit)->getName() == (tobeExecuted->getName() + toAdd)){
	    found = true;
	    break;
	  }
	}
	
	par = par->parentLambda;
      }

      //here's the renaming algorithm
      if(found){
	if(toAdd.empty() || toAdd[toAdd.size() - 1]=='z'){
	  toAdd += 'a';
	}
	else{
	  char let = toAdd[toAdd.size() - 1];
	  let++;
	  toAdd[toAdd.size() - 1] = let;
	}
      }
    }while(found);
    if(!toAdd.empty()){
#if LAMBDA_DEBUG
      cout << endl << "In: " << toString() << endl << 
	" Renamed: " << tobeExecuted->getName() << " to " << tobeExecuted->getName() + toAdd << endl;
#endif
      tobeExecuted->setName(tobeExecuted->getName() + toAdd);
    }
  }
  
  //recurse down
  list<Expression*>::iterator expit, expendIt = expressionList->end();
  for(expit = expressionList->begin(); expit != expendIt; expit++){
    LambdaExpression *lexpr = dynamic_cast<LambdaExpression*>(*expit);
    if(lexpr){
      lexpr->EnsureDiffVarsThanParents();
    }
  }
  
}

void LambdaExpression::Simplify() {
  //recurse down and then 
  list<Expression*>::iterator expIt, expEnd = expressionList->end();
  for(expIt = expressionList->begin(); expIt != expEnd; expIt++){
    LambdaExpression *childLambda = dynamic_cast<LambdaExpression*>(*expIt);
    if(childLambda)
      childLambda->Simplify();
  }
  

  //then try and simplify the things in the expressions
  //by applying and pulling out from empty lambdas
  bool foundOne;
  do{
    foundOne = false;

    if(expressionList->empty()) break;

    expEnd = expressionList->end();

    list<Expression*>::iterator beforeExpEnd = expEnd;
    beforeExpEnd--;

    for(expIt = expressionList->begin(); expIt != expEnd; expIt++){
      list<Expression*>::iterator expItNext = expIt;

      //try to pull stuff out if we can
      LambdaExpression *lexp = dynamic_cast<LambdaExpression*>(*expIt);
      if(lexp && lexp->arguments.empty()){
	foundOne = true;
	expressionList->insert(expIt, lexp->expressionList->begin(), lexp->expressionList->end());
	expIt--;
	expressionList->erase(expItNext);
	break;
      }

      if(expIt == beforeExpEnd)
	continue;

      expItNext++;
      LambdaExpression *lamLeft;
      Expression *simplified;
      if((lamLeft = dynamic_cast<LambdaExpression*>(*expIt)) && 
	 (simplified = lamLeft->RealApply(*expItNext, true, true))){
	foundOne = true;
	LambdaExpression *newLam = dynamic_cast<LambdaExpression*>(simplified);
	if(newLam)
	  newLam->Simplify();
#if LAMBDA_DEBUG
	set<Expression*> bracketedTerms;
	bracketedTerms.insert(lamLeft);
	bracketedTerms.insert(*expItNext);
	cout << "Simplifying " << bracketToString(&bracketedTerms) << endl;
#endif
	expressionList->insert(expIt, simplified);
	//erase original left
	expressionList->erase(expIt);
	//erase original right
	expressionList->erase(expItNext);
#if LAMBDA_DEBUG
	set<Expression*>newBracketedTerms;
	newBracketedTerms.insert(simplified);
	cout << "\tto " << bracketToString(&newBracketedTerms) << endl << endl;
#endif
	break;
      }
    }
    
  }while(foundOne);

}

void LambdaExpression::Replace(Variable *iffree, Expression *newExpr){
  //rename bound variables coming in that are already bound
  LambdaExpression * newLExpr = dynamic_cast<LambdaExpression*>(newExpr);

  string origString = toString();
  bool replaced = false;
  set<Expression*> replacedTerms;
  replacedTerms.insert(newExpr);
  list<Expression*>::iterator expIt, expEnd = expressionList->end();
  for(expIt = expressionList->begin(); expIt != expEnd; expIt++){
    Expression* expr = (*expIt);
    Variable *var;
    LambdaExpression *lexpr;
    if((var = dynamic_cast<Variable*>(expr))){
      if( var == iffree){
	replaced = true;
	if(newLExpr){
	  newLExpr = newLExpr->clone();
	  newLExpr->parentLambda = this;
	  newLExpr->EnsureDiffVarsThanParents();
	  expressionList->insert(expIt, newLExpr);
	}
	else{
	  expressionList->insert(expIt, newExpr);
	}

	list<Expression*>::iterator deleteIt = expIt;
	expIt--;
	expressionList->erase(deleteIt);
      }
    }
    else if((lexpr = dynamic_cast<LambdaExpression*>(expr))){
      assert(lexpr);
      lexpr->Replace(iffree, newExpr);
    }
  }
#if LAMBDA_DEBUG
  if(replaced)
    cout << endl << "Before replacing: " << newExpr->toString() << " in: " << origString << endl << 
      "\tafter replace" << bracketToString(&replacedTerms) << endl << endl;
#endif
  
  this->Simplify();
  
}


string LambdaExpression::toPrettyString(){
  string retString;
  
  
  if(arguments.size() > 0 && arguments.front()->getType()->isNullType()){
    retString = "all ";
  } else {
    retString = "\\";
  }

  retString += resultType->toString(false);

  bool first = true;
  list<Variable*>::iterator it, varEnd = arguments.end();
  for(it = arguments.begin(); it != varEnd; it++){
    if(!first)
      retString += ',';
    first = false;
    retString += (*it)->toPrettyString();
    retString += (*it)->getType()->toString(false);
  }
  
  retString += ":[";

  first = true;
  list<Expression*>::iterator exprEnd = expressionList->end();
  for(list<Expression*>::iterator it = expressionList->begin(); 
      it != exprEnd; it++){
    if(!first)
      retString += ' ';
    first = false;
    retString += (*it)->toPrettyString();
  }
  retString += "]";

  return retString;
}

string LambdaExpression::contextToString(set<Expression*> *bracketedTerms){
  if(!parentLambda)
    return bracketToString(bracketedTerms);

  //FIXME: doesn't quite work, parents are a bit messed up.
  //cout << "have parent, but I would've said " << bracketToString(bracketedTerms) << endl;
  return parentLambda->contextToString(bracketedTerms);
}


string LambdaExpression::bracketToString(set<Expression*> *bracketedTerms){
  string retString = "\\";
  retString += resultType->toString();
  
  bool first = true;
  list<Variable*>::iterator varEnd = arguments.end();
  for(list<Variable*>::iterator it = arguments.begin(); 
      it != varEnd; it++){
    if(!first)
      retString += ',';
    first = false;
    retString += (*it)->toString();
    //retString += (*it)->getType()->toString();
  }
  
  retString += ":";
  //}
  
  retString +="[";
  
  first = true;
  list<Expression*>::iterator it, exprEnd = expressionList->end();
  for(it = expressionList->begin(); it != exprEnd; it++){
    if(!first)
      retString += ' ';
    first = false;

    if(bracketedTerms->find(*it) != bracketedTerms->end()) retString += '<';

    LambdaExpression *l = dynamic_cast<LambdaExpression*>(*it);
    if(l)
      retString += l->bracketToString(bracketedTerms);
    else
      retString += (*it)->toString();

    if(bracketedTerms->find(*it) != bracketedTerms->end()) retString += '>';
  }

  retString += "]";
  return retString;
}

string LambdaExpression::toString(bool forClone){
  string retString;
  //if(arguments.size() > 0){
    retString = "\\";
    retString += resultType->toString(forClone);
    
    bool first = true;
    list<Variable*>::iterator it, varEnd = arguments.end();
    for(it = arguments.begin(); it != varEnd; it++){
      if(!first)
	retString += ',';
      first = false;
      retString += (*it)->toString(forClone);
      retString += (*it)->getType()->toString(forClone);
    }

    retString += ":";
    //}
  
  retString +="[";

  first = true;
  list<Expression*>::iterator exprEnd = expressionList->end();
  for(list<Expression*>::iterator it = expressionList->begin(); 
      it != exprEnd; it++){
      if(!first)
	retString += ' ';
      first = false;
    retString += (*it)->toString(forClone);
  }

  retString += "]";
  return retString;
}

LambdaExpression* LambdaExpression::clone(){
  MyMap outsideVars;
  LambdaExpression *ancestor;
  for(ancestor = parentLambda; ancestor != NULL; ancestor = ancestor->parentLambda){
    //copy this parent's vars into this map;
    MyMap::iterator it, endIt = ancestor->variableMap.end();
    for(it = ancestor->variableMap.begin(); it != endIt; it++){
      outsideVars[it->first] = it->second;
    }
  }
  //this way the vars are created with references to outside lambda's correctly.
  string prefix;
  LambdaExpression *lexpr =  new LambdaExpression(toString(true).c_str(), prefix, 
						  &outsideVars, obj, parentLambda);
  return lexpr;
}
