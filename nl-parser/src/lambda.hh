#ifndef _LAMBDA_H_
#define _LAMBDA_H_

#include "generated.hh"

#include <string> 
#include <assert.h> 
#include <glib-2.0/glib.h>

#include <list>
#include <map>

#include <pet-parser.hh>


class Variable;
typedef std::map<std::string, Variable*> MyMap;

/* Type
 * The base class for the types in this system.
 * Types are either just entitities [e] (ConcreteType)
 * or from one type to another [t1,t2] (ComplexType).
 */
class Type{
public:
  //[e], or [e,t]
  virtual ~Type(){}
  
  /* parseType
   * ----------
   * Assumes you giving it something valid of the form 
   * [randomType]junkAfterwards
   * and it will consume everything between (and including)
   * the braces and leave you junkAfterwards.
   */
  static Type *parseType(GScanner * cat, string& prefix);
  

  bool equals(Type *other);
  virtual std::string toString(bool forClone = false) = 0;
  virtual Type* clone() = 0;
  bool isNullType();

protected:
  static bool getArg(GScanner * cat, Type *& t, string& prefix);
};

class ComplexType : public Type{
  friend class Type;
  friend class ConcreteType;
 public:
  std::string toString(bool forClone = false);
  ComplexType* clone();
  Type * getFrom(){ return from; }
  Type * getTo(){ return to; }
  Type * from, *to;
  ComplexType(Type *first, Type *second);

 protected:
  ~ComplexType(){}
};

class ConcreteType : public Type{
  //public:
  friend class Type;
  friend class ComplexType;
 public:
  std::string toString(bool forClone = false);
  ConcreteType* clone();
  ConcreteType(std::string name);
 protected:
  ~ConcreteType(){}
  std::string value;
};


/* Expression
 * The base type for all expressions, there are the following productions rules
 * Expression := string
 * Expression := LambdaExpression
 */
class LambdaExpression;
class Expression : public Generated {
  friend class LambdaExpression;
public:
  typedef std::string(*MyMacroFunc)(PET::K2YObject *obj, std::string arg);
  typedef std::map<std::string, MyMacroFunc> MyFuncMap;
  typedef std::map<std::string, std::string> MyStringMap;

  virtual ~Expression(){}

  /* parseExpressionList
   * ---------------
   * Parses an list of expressions of the form
   * [Var1 str2 lambdaExp3 str7 ...]JunkAfterwards
   * and leaves you with JunkAfterwards.
   */
  static Expression *parseExpression(const char * expression, PET::K2YObject *obj = NULL);
  
  static std::list<Expression*>* 
  parseExpressionList(GScanner* cat, MyMap* varMap, LambdaExpression *parent, string& prefix);
  
  static Expression *OrderedApply(Expression *first, Expression *second);
  //either one or two needs to be a LambdaExpression
  //if neither can be applied NULL is returned
  //returned value might be another LambdaExpression.
  static Expression* Apply(Expression *one, Expression *two);
  static void RegisterMacro(std::string macroName, MyMacroFunc func); 
  static void RegisterStringMacro(std::string macroName, std::string macroContents);
  static void RegisterTypeShift(Expression *expr);
  

  virtual Type* getType() = 0;
  virtual std::string toString(bool forClone = false) = 0;
  virtual std::string toPrettyString();
  std::string meaning;

  virtual Expression* clone() = 0;
  std::string weirdness;
private:
  static Expression* ApplyInt(Expression *one, Expression *two, bool debug = false);
  static std::string parseMacro(GScanner*cat, PET::K2YObject *obj, string& prefix);
  static MyFuncMap *functionTable;
  static MyStringMap *stringSubstitutionTable;

  static std::vector<LambdaExpression *> *typeShifts;
};

/* has a name and a type
 */
class Variable : public Expression{
  friend class LambdaExpression;
  friend class Expression;
 public:
  std::string toString(bool forClone = false);
  void setName(std::string newName){ name = newName; }

  std::string getName(){ return name; }

  Variable(){}
private:

  /* Variable
   * -----------
   * Assumes you give it something of the form
   * VarName[randomType]junkAfterwards
   * and it will leave you with junkAfterwards
   */
  Variable(GScanner * cat, string& prefix);
  Type * getType(){ return type; }
  Variable* clone();

  //data
  std::string name;
  Type *type;
  int id;
};

class StringExpression: public Expression{
public:
  StringExpression(std::string name);
  ~StringExpression(){}

  std::string toString(bool forClone = false);
  StringExpression* clone();
  std::string getName(){ return name; }
  //FIXME: this is how we deal with objects taht we never want to apply
  Type * getType() { return new ComplexType(NULL, NULL); }

private:
  std::string name;
};

/* LambdaExpression
 * \X1[type1],X2[type2]:[expression]
 * and expression is defined above.
 */
class LambdaExpression: public Expression{
  friend class Expression;
public:
  /* LambdaExpression
   * ------------------
   * Assumes you give it something of the form
   * \Var1[type1],Var2[type2]:[expressionlist] junkAfterwards
   * It will leave you with junkAfterwards.
   * later it may contain 0 arguments if they have been all
   * applied.
   */
  LambdaExpression(GScanner* cat, string& prefix, MyMap* outsideBindings = NULL,  
		   PET::K2YObject *obj = NULL, LambdaExpression * parent = NULL);
  LambdaExpression(const char *lambda_line, string& prefix, MyMap* outsideBindings = NULL,  
		   PET::K2YObject *obj = NULL, LambdaExpression * parent = NULL);
  void EnsureDiffVarsThanParents();
  std::string contextToString(std::set<Expression*> *bracketedTerms);
  std::string bracketToString(std::set<Expression*> *bracketedTerms);
  ~LambdaExpression();
  std::string toString(bool forClone = false);
  std::string LambdaExpression::toPrettyString();

  LambdaExpression* clone();
  Expression *RealApply(Expression *expr, bool debug, bool typeshift = false);
  
  void Replace(Variable *iffree, Expression *newExpr);
  Type *getType();

  Type *resultType;
  std::list<Variable*> arguments;
  MyMap variableMap;//localVars
  std::list<Expression*> *expressionList;
  void setIAmQuant(bool newQuant){ iAmQuant = newQuant; }

private:
  LambdaExpression(){}
  PET::K2YObject *obj;
  void init(GScanner* cat, string& prefix, MyMap* outsideBindings, 
	    PET::K2YObject *obj, LambdaExpression * parent);

  void Simplify();

  LambdaExpression *parentLambda;
  string name;
  bool iAmQuant;
};


#endif //_LAMBDA_H_
