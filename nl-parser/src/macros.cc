#include "macros.hh"
#include "lambda.hh"
#include "pet-parser.hh"

#include <glib.h>

#include <string>

using namespace PET;

static string time_tToString (time_t time_t_val) {
  tm *time = gmtime(&time_t_val);
  char *date = g_strdup_printf ("'%d-%d-%d %d:%d:%d-00'", time->tm_year + 1900, time->tm_mon + 1, time->tm_mday, time->tm_hour, time->tm_min, time->tm_sec);
  string date_string = string(date);
  g_free(date);
  return date_string;
}

static string farkFunc(K2YObject *obj, string /*uselessArg*/){
  char buffer[50];
  sprintf(buffer, "%d", obj->getRelation()->id());
  return buffer;
}

static string getUnquotedValue(K2YObject *obj, string key) {
  fs &objectFeatureStructure = obj->getRelation()->get_fs();
  //objectFeatureStructure.print(stdout);
  //cout << endl;
  fs value = objectFeatureStructure.get_path_value(key.c_str());
  string name = string(value.name()).substr(1);
  //cout << "Getting value |" << name << "| for key " << key << endl;
  return name;
}

static string getValue(K2YObject *obj, string key){
  string name = getUnquotedValue(obj, key);
  return "\'%" + name + "%\'";
}


static string getWord(K2YObject *obj, string) {
  return getValue(obj, "CONST_VALUE");
}

static string getCValue(K2YObject *obj, string) {
  char *number;
  if (obj->has_cvalue) {
    number = g_strdup_printf("%d", obj->cvalue);
    cout << "Object has a CValue... its " << number << endl;
  } else {
    cerr << "WARNING: object has no CValue" << endl;
    number = g_strdup("0");
  }
  return number;
}

static string getDayOfWeek(K2YObject *obj, string) {
  string dayofw = getUnquotedValue(obj, "CONST_VALUE");

  int daynum;
  if (dayofw == "sun") {
    daynum = 0;
  } else if (dayofw == "mon") {
    daynum = 1;
  } else if (dayofw == "tue") {
    daynum = 2;
  } else if (dayofw == "wed") {
    daynum = 3;
  } else if (dayofw == "thu") {
    daynum = 4;
  } else if (dayofw == "fri") {
    daynum = 5;
  } else if (dayofw == "sat") {
    daynum = 6;
  } else {
    cerr << "Invalid day " << dayofw << endl;
    daynum = 0;
  }

  time_t date = time(NULL);
  tm *time = gmtime(&date);
  
  int current_day = time->tm_wday;

  if (daynum >= current_day) {
    // The day is after the current day, subtract a week (in seconds)
    date -= 7 * 24 * 60 * 60;
  }

  string date_string = time_tToString (date);
  cout << "Returning date " << date_string << endl;
  return date_string;
}

static string dateToday(K2YObject *obj, string) {
  time_t date = time(NULL);
  string date_string = time_tToString(date);
  cout << "Returning today as: " << date_string;
  return date_string;
}

static string printObject(K2YObject *obj, string) {
  fs &objectFeatureStructure = obj->getRelation()->get_fs();
  objectFeatureStructure.print(stdout); 
  cout << endl;
  return string("");
}

static string getGenericValue(K2YObject *obj, string) {
  fs &objectFeatureStructure = obj->getRelation()->get_fs();  
  //objectFeatureStructure.print(stdout); 
  //cout << endl;
  fs first = objectFeatureStructure.get_path_value("LABEL.FIRST");
  int id = first.type() - 1;
  //cout << "The id is " << id << endl;
  map<int, string>::iterator foo = PET::id_to_string.find(id);
  if (foo != PET::id_to_string.end()) {
    string inside = foo->second;
    if(inside[0] == '^')
      inside = inside.substr(1);
    //remove all the spaces
    int pos;
    while((pos = inside.find('^')) != string::npos){
      inside[pos] = ' ';
    }
    return "\'%" + inside + "%\'";
  } else {
    cerr << "Couldn't find value" << endl;
    return "\'\'";
  }
}

void registerMacros () {
  Expression::RegisterMacro("fark", farkFunc); 
  Expression::RegisterMacro("get_cvalue", getCValue);
  Expression::RegisterMacro("get_value", getValue);
  Expression::RegisterMacro("get_word", getWord);
  Expression::RegisterMacro("get_dayofweek", getDayOfWeek);
  Expression::RegisterMacro("date_today", dateToday);
  Expression::RegisterMacro("print_object", printObject);
  Expression::RegisterMacro("get_generic_value", getGenericValue);
}
