#include "parser.hh"
#include "parse-results.hh"
#include "lambda-generator.hh"

#include <iostream.h>

int main (int argv, char **argc) {
  char *grammar = NULL;
  char *semantics = NULL;

  if (argv == 1) {
    grammar = strdup(GRAMMAR_FILE);
    semantics = strdup(SEMANTICS_FILE);
  } else if (argv != 3) {
    cout << "Usage: nl-parser GRAMMAR_FILE.grm SEMANTICS.xml" << endl;
    grammar = strdup(argc[1]);
    semantics = strdup(argc[2]);
    return -1;
  }

  LambdaGenerator *generator = new LambdaGenerator(semantics);
  PetParser *parser = new PetParser (grammar, generator);



  while (true) {
    //cout << endl << endl << endl << "========================================================================" << endl << endl << endl << endl;
    cout << endl << "Parse: ";

    string toParse;

    getline(cin, toParse);

    ParseResults *results = parser->parse(toParse);
    
    if (results != NULL) {
      SQLQuery query = results->getQuery();
      //cout << endl << endl << "And the best query is..." << endl;
      //query.print();
    } else {
      cout << endl << "No valid parses found!" << endl;
    }

  }

  return 0;
}

