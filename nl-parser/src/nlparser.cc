#include "nlparser.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <config.h>

#include "parser.hh"
#include "parse-results.hh"
#include "lambda-generator.hh"


#include <string>

static GObjectClass *parent_class = NULL;

struct _NlInterfacePrivate{
  LambdaGenerator *generator;
  PetParser *parser;
};

char *
nl_parser_get_query (NlParser *parser, const char *parse_string)
{
  cout << endl << ("INITIALIZING NLPARSER THREADS") << endl;

  if (!g_thread_supported ()) g_thread_init (NULL);

  cout << ("DONE") << endl << endl;

  ParseResults *results = parser->privy->parser->parse(string(parse_string));

  if (results != NULL) {
    SQLQuery query = results->getQuery();
    return g_strdup(query.getQuery().c_str());
  } else {
    return g_strdup("NO VALID QUERY FOUND");
  }
}

void
init (NlParser *rec)
{
  rec->privy = g_new (NlInterfacePrivate, 1);
  rec->privy->generator = new LambdaGenerator(SEMANTICS_FILE);
  rec->privy->parser    = new PetParser(GRAMMAR_FILE, rec->privy->generator);
}

GObject *nl_parser_new(){
  return G_OBJECT(g_object_new(NL_TYPE_PARSER, NULL));
}

void
finalize (GObject *object)
{
  NlParser *rec;

  rec = NL_PARSER(object);

  if (rec->privy == NULL) {
    return;
  }

  if (rec->privy->parser) delete (rec->privy->parser);
  if (rec->privy->generator) delete (rec->privy->generator);

  g_free (rec->privy);
  rec->privy = NULL;

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

void
class_init (NlParserClass *klass)
{
	GObjectClass *object_class;

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = finalize;

	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
}

GType
nl_parser_get_type (void)
{
        static GType type = 0;

        if (type == 0) {
                GTypeInfo info = {
                        sizeof (NlParserClass),
                        NULL, /* GBaseInitFunc */
			NULL, /* GBaseFinalizeFunc */
			(GClassInitFunc) class_init, 
			NULL, /* GClassFinalizeFunc */
			NULL, /* user supplied data */
                        sizeof (NlParser), 
			0, /* n_preallocs */
			(GInstanceInitFunc) init,
			NULL
                };

		printf ("Registering type\n");
                type = g_type_register_static (G_TYPE_OBJECT, "NlParser", &info, (GTypeFlags)0);
        }

        return type;
}

