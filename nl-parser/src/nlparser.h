#ifndef NL_PARSER_H
#define NL_PARSER_H

#include <glib/gerror.h>
#include <glib-object.h>
  
G_BEGIN_DECLS

#define NL_TYPE_PARSER		(nl_parser_get_type ())
#define NL_PARSER(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), NL_TYPE_PARSER, NlParser))
#define NL_PARSER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), NL_TYPE_PARSER, NlParserClass))
#define NL_IS_PARSER(obj)		(GTK_TYPE_CHECK_INSTANCE_TYPE ((obj), NL_TYPE_PARSER))
#define NL_IS_PARSER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), NL_TYPE_PARSER))
#define NL_PARSER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), NL_TYPE_PARSER, NlParserClass))


typedef struct _NlInterfacePrivate NlInterfacePrivate;

typedef struct _NlParser NlParser;
typedef struct _NlParserClass NlParserClass;


struct _NlParser
{
  GObject parent_instance;
  NlInterfacePrivate *privy;
};

struct _NlParserClass
{
  GObjectClass parent_class;
};



GObject* nl_parser_new		(void);
GType    nl_parser_get_type	(void);
char *   nl_parser_get_query    (NlParser *parser, const char *parse_string);

G_END_DECLS

#endif /* GNOME_NL_INTERFACE_H */

