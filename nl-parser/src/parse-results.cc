#include "parse-results.hh"

using namespace PET;


ParseResults::ParseResults (K2YObject *sentence, K2YMainVerb *mainVerb, K2YEntity *directObject, list<K2YObject *> objects) {
  this->sentence = sentence;
  this->mainVerb = mainVerb;
  this->directObject = directObject;
  this->objects = objects;
}

K2YObject *        
ParseResults::getSentence () {
  return this->sentence;
}

K2YMainVerb *        
ParseResults::getMainVerb () {
  return this->mainVerb;
}

PET::K2YEntity *
ParseResults::getDirectObject () {
  return this->directObject;
}

list<K2YObject *>
ParseResults::getObjects () {
  return this->objects;
}

void ParseResults::print (K2YObject *object, int indent_level) {
  vector<PET::K2YObject *>::iterator i = object->args.begin();

  for (int j=0; j < indent_level; j++) printf ("    ");
  printf ("%s\n", object->getRelation()->name());

  while (i != object->args.end()) {
    PET::K2YObject *object = *i;

    this->print(object, indent_level + 1);

    i++;
  }
}

void ParseResults::print () {
  list<PET::K2YObject *>::iterator i = this->objects.begin();

  while (i != this->objects.end()) {
    printf ("Object {\n");
    PET::K2YObject *object = *i;

    this->print(object, 1);

    printf("}\n\n");
    i++;
  }
  //print(getSentence(), 0);
}

void ParseResults::setQuery (SQLQuery query) {
  this->query = query;
}

SQLQuery ParseResults::getQuery () {
  return this->query;
}
