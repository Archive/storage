/* parse-results.hh
 * Copyright (C) 2002,2003 Seth Nickell
 */

#ifndef PARSE_RESULTS_HH
#define PARSE_RESULTS_HH

class ParseResults;

#include <pet-parser.hh>

#include "sql-query.hh"

class ParseResults {
public:
  ParseResults (PET::K2YObject *sentence, PET::K2YMainVerb *main_verb, PET::K2YEntity *direct_object, list<PET::K2YObject *> objects);

  PET::K2YObject *        getSentence ();
  PET::K2YMainVerb *      getMainVerb ();
  PET::K2YEntity *        getDirectObject (); 
  list<PET::K2YObject *>  getObjects  ();

  void print ();
  void print (PET::K2YObject *object, int indent_level);

  void setQuery (SQLQuery query);
  SQLQuery getQuery ();

protected:
  PET::K2YObject   *sentence;
  PET::K2YMainVerb *mainVerb;
  PET::K2YEntity   *directObject;
        
  list<PET::K2YObject *> objects;
        
  SQLQuery          query;
};

#endif
