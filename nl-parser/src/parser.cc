#include "parser.hh"
#include "k2yobject-utils.hh"
#include "entity-resolver.hh"

#include <cheap/config.h>
#include <pet-system.h>
#include <chart.h>
#include <inputchart.h>
#include <tsdb++.h>
#include <parse.h>
#include <options.h>

#include <mrs.hh>

#include <string>
#include <values.h>

#define DEBUG_NL 1

namespace PET {
  extern int opt_nth_meaning;

  FILE *ferr, *fstatus, *flog;

  grammar *Grammar;
  settings *cheap_settings;
}

static PET::K2YObject *popSentence (list<PET::K2YObject *>&semantic_objects);
static bool validFindSentence (PET::K2YObject *sentence, PET::K2YObject *mainVerb, list<PET::K2YObject *>&semantic_objects);

PetParser::PetParser (string grammar_file, Generator *generator) {
  char *grammar_file_cstr = strdup (grammar_file.c_str());

  this->generator = generator;
  this->grammar_id = 1;

  /* PET likes to write to files for some perverse reason */
  PET::ferr = stderr;
  PET::fstatus = stderr;
  PET::flog = (FILE *)NULL;

  /* Load the settings file corresponding to the grammar */
  cout << "Reading settings from " << PET::settings::basename(grammar_file_cstr);
  this->cheapSettings = New PET::settings(PET::settings::basename(grammar_file_cstr), grammar_file_cstr, "reading");

  PET::cheap_settings = this->cheapSettings;

  /* FIXME: why does it use the YY tokenizer when we uncomment this? */
  PET::options_from_settings(this->cheapSettings);

  //PET::init_options();

  PET::opt_default_les = true;
  PET::pedgelimit = 10000;
  PET::memlimit = 100 * 1024 * 1024;

  PET::verbosity = 0;

  /* Attempt to load the grammar file */
  try { 
    this->grammar = New PET::grammar(grammar_file_cstr);
    PET::Grammar = this->grammar;
    cout << endl << endl;
    cout << PET::ntypes << " types loaded" << endl;
  } catch(PET::error &e) {
    fprintf(stderr, "\naborted\n");
    e.print(stderr);
    delete this->grammar;
    delete this->cheapSettings;
  }

  // FIXME: needed? 
  PET::initialize_version();
}

ParseResults *
PetParser::parse (string phrase) {

  ParseResults *results = NULL;
  ParseResults *bestResults = NULL;

  using namespace PET;

  PET::opt_yy = false;
  //opt_nth_meaning = 1;

  //replace ' ' with '_'
  bool insideQuote = false;
  for(unsigned int i = 0; i < phrase.size(); i++){
    if(phrase[i] == '\"'){
      if(!insideQuote){
	phrase[i] = '^';
      }
      else{
	phrase = phrase.substr(0, i) + phrase.substr(i+1);
      }
      insideQuote = !insideQuote;
      i--;//since we removed a char
    }
    else{
      if(phrase[i] == ' ' && insideQuote){
	phrase[i] = '^';
      }
    }
   
  }
  //cout << "New parse: " << phrase << endl;

  // Run the parser over "phrase"

  int numResults = 0;
  int parseNum = 0;
  float bestScore = -MAXFLOAT;

  chart *Chart = 0;
  try {
    fs_alloc_state FSAS;
    input_chart i_chart(New end_proximity_position_map);

    list<error> errors;
    analyze(i_chart, phrase, Chart, FSAS, errors, this->grammar_id);

    if(!errors.empty()) cerr << "Errors found" << endl;


    // Iterate through the possible parses
    for(vector<item *>::iterator iter = Chart->readings().begin(); iter != Chart->readings().end(); ++iter) {
      
      item *it = *iter;
      parseNum++;
      
      MRS::MRS *mrs = new MRS::MRS(*it);
      cout << mrs->toString();

      ScopeFinder scoper;
      scoper.findScope(mrs);

    }
