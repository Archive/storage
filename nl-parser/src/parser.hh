/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/* pet-parser.hh
 * Copyright (C) 2002,2003 Seth Nickell
 */

#ifndef PET_PARSER_HH
#define PET_PARSER_HH

class PetParser;

#include <pet-parser.hh>

#include "parse-results.hh"
#include "generator.hh"

class PetParser
{
 public:        
        PetParser (string grammar_file, Generator *generator);

        /* Returns the K2YObjects representing the semantics
           of the string passed into "phrase" */
        ParseResults *parse (string phrase);

 protected:
        PET::grammar *grammar;
        PET::settings *cheapSettings;
        Generator *generator;

 private:
        /* A unique number associated with this->Grammar */
        int grammar_id;
};


#endif /* PET_PARSER_HH */
