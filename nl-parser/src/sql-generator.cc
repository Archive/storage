#include "sql-generator.hh"
#include "k2yobject-utils.hh"

#include <vector>
#include <iostream>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>

using namespace PET;


SQLGenerator::SQLGenerator (string semanticTranslationFile) {
  // FIXME: we leak like crazy in here
  xmlDocPtr doc;
  xmlNodePtr relNode;
  const char *name;

  doc = xmlParseFile (semanticTranslationFile.c_str());   

  if (doc == NULL
      || doc->xmlRootNode == NULL
      || doc->xmlRootNode->name == NULL) {
    xmlFreeDoc(doc);
    cout << "WARNING: could not read " << semanticTranslationFile << endl;
    return;
  }

  for (relNode = doc->xmlRootNode->xmlChildrenNode; relNode != NULL; relNode = relNode->next) {
    name = (const char *)xmlGetProp (relNode, (const xmlChar *)"name");
    if (name == NULL || relNode->type != XML_ELEMENT_NODE) continue;

    list<SQLRelation *> *relations = new list<SQLRelation *>();

    for (xmlNodePtr operatorNode = relNode->xmlChildrenNode; operatorNode != NULL; operatorNode = operatorNode->next) {
      if (operatorNode->name == NULL || operatorNode->type != XML_ELEMENT_NODE) continue;

      string operatorNodeName((const char *)operatorNode->name);
      string operatorString;

      if (operatorNodeName == "equal") {
	operatorString = "=";
      } else if (operatorNodeName == "lessthan") {
	operatorString = "<";
      } else if (operatorNodeName == "greaterthan") {
	operatorString = ">";
      } else if (operatorNodeName == "like") {
	operatorString = "LIKE";
      }

      list<string> attributes;
      list<int> argumentNumbers;

      for (xmlNodePtr argNode = operatorNode->xmlChildrenNode; argNode != NULL; argNode = argNode->next) {
	if (argNode->name == NULL || argNode->type != XML_ELEMENT_NODE) continue;

	string attribute((const char *)xmlGetProp (argNode, (const xmlChar *)"attribute"));
	string argName((const char *)argNode->name);
	
	if (argName == "constant") {
	  // Constant value in the XML file
	  attributes.push_back(attribute);
	  argumentNumbers.push_back(-1);
	} else {
	  // Argument value in the XML file
	  if (strlen((const char *)argNode->name) != 4) cerr << "WARNING: invalid arg node " << argName << endl;
	  
	  int argumentNumber = atoi(argName.c_str() + 3);
	  
	  attributes.push_back(attribute);
	  argumentNumbers.push_back(argumentNumber);
	}
      }

      relations->push_back (new SQLRelation (attributes, argumentNumbers, operatorString));
    }

    relToSQLRelations.push_back(pair<string, list<SQLRelation *> *>(string(name), relations));
  }
}

list<SQLRelation *> *SQLGenerator::findSQLRelation (string rel) {
  list<pair<string, list<SQLRelation *> *> >::iterator i = relToSQLRelations.begin();

  while (i != relToSQLRelations.end()) {
    if ((*i).first == rel) return (*i).second;
    i++;
  }

  return NULL;
}

SQLGenerator::~SQLGenerator () {

}

SQLQuery  SQLGenerator::generate(K2YObject *object) {
  SQLQuery query;

  list<SQLRelation *> *sqlRelations = findSQLRelation(object->getRelationName());
  
  if (sqlRelations) {
    list<SQLRelation *>::iterator relationIter = sqlRelations->begin();
    while (relationIter != sqlRelations->end()) {
      SQLRelation *relation = *relationIter;
      
      query += relation->makeConjunction(object);
      
      relationIter++;
    }
  } else {
    query.predicatesWithoutMeaning.push_back(objectToString(object));
  }

  return query;
}
