#ifndef SQL_GENERATOR_HH_
#define SQL_GENERATOR_HH_

class SQLGenerator;

#include <string>
#include <list>

#include <pet-parser.hh>

#include "generator.hh"
#include "sql-query.hh"
#include "sql-relation.hh"

class SQLGenerator : public Generator {
 public:
  SQLGenerator (string semanticTranslationFile);
  virtual ~SQLGenerator();

  virtual SQLQuery generate (PET::K2YObject *object);

 protected:
  list<pair <string, list<SQLRelation *> * > > relToSQLRelations;
  list<SQLRelation *> *findSQLRelation (string rel);
};

#endif
