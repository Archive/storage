
#include "sql-query.hh"

#include <iostream.h>

SQLQuery::SQLQuery () {
  tableName = "AttrSoup";//default
}

SQLQuery::SQLQuery (const SQLQuery &query) {
  this->whereConjunctions = query.whereConjunctions;
  this->fromClauses = query.fromClauses;
  this->tableName = query.tableName;
  this->doName = query.doName;
}

static void append (list<string> &target, const list<string> &toAddRef) {
  list<string> toAdd = toAddRef;
  target.splice (target.end(), toAdd);
}


void SQLQuery::setDOName(string doName){
  this->doName = doName;
}

void SQLQuery::operator+=(const SQLQuery &rhs) {
  //cout << "We have " << predicatesWithoutMeaning.size() << " predicates, merging in " << rhs.predicatesWithoutMeaning.size();
  append (this->whereConjunctions, rhs.whereConjunctions);

  // We have to iterate through these because we trim duplicates
  map<string,string>::const_iterator i = rhs.fromClauses.begin();
  while (i != rhs.fromClauses.end()) {
    this->addFromClause(i->first, i->second);
    i++;
  }

  append (this->predicatesWithoutMeaning, rhs.predicatesWithoutMeaning);
  append (this->unusedPredicates, rhs.unusedPredicates);
  //cout << " ... ending up with " << predicatesWithoutMeaning.size() << endl;
}

void SQLQuery::addWhereConjunction (string conjunct) {
  if(conjunct.size() < 1) return;
  //also check here for dups
  bool foundWhereConj = false;
  list<string>::iterator i, iEnd = this->whereConjunctions.end();
  for(i = whereConjunctions.begin(); i != iEnd; i++) {
    if (*i == conjunct) {
      foundWhereConj = true;
      break;
    }
  }

  if (!foundWhereConj) {
    this->whereConjunctions.push_back(conjunct);
  }
}

void SQLQuery::addFromClause (string var, string altTableName) {
  // Only add a var if its not already there

  map<string, string>::iterator it = fromClauses.find(var);
  if(it != fromClauses.end() && altTableName == tableName)
    return;

  fromClauses[var] = altTableName;
}

void SQLQuery::addFromClause (string var) {
  addFromClause(var, tableName);
}

void SQLQuery::print () {
  string query = this->getQuery();
  cout << "\n\n------------------------------------------------\n";

  cout << "SQLQuery {\n   " << query << "\n}" << endl;

  cout << "Size of meaningless is " << this->predicatesWithoutMeaning.size() << endl;

  if (this->predicatesWithoutMeaning.size() > 0) {
    cout << "Predicates to whom no meaning was assigned:" << endl;
    list<string>::iterator i = this->predicatesWithoutMeaning.begin();
    while (i != this->predicatesWithoutMeaning.end()) {
      string predicate = *i;
      cerr << "   " << predicate << endl;
      i++;
    }
  }

  if (this->unusedPredicates.size() > 0) {
    cout << "Predicates who were not linked into the tree:" << endl;
    list<string>::iterator i = this->unusedPredicates.begin();
    while (i != this->unusedPredicates.end()) {
      string predicate = *i;
      cerr << "   " << predicate << endl;
      i++;
    }
  }
  cout << "\n------------------------------------------------\n\n";
}

string SQLQuery::getQuery () {

  string query = "SELECT DISTINCT " + doName + ".recordid FROM ";

  string firstClause;

  map<string, string>::iterator i = this->fromClauses.begin();
  bool first = true;
  while (i != this->fromClauses.end()) {    
    string clause = i->second + ' ' + i->first;

    if(first)
      first = false;
    else
      query += ", ";

    query +=  clause;

    i++;
  }

  //query += " WHERE e1.recordid=" + doName + ".recordid ";
  if(!this->whereConjunctions.empty())
    query += " WHERE ";

  list<string>::iterator it = this->whereConjunctions.begin();
  first = true;
  while (it != this->whereConjunctions.end()) {
    string queryConjunct = *it;

    if(first)
      first = false;
    else
      query += " AND ";

    query += queryConjunct;

    it++;
  }

  return query;
}

void SQLQuery::setTableName (string tableName) {
  this->tableName = tableName;
}
