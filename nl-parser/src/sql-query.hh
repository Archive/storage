#ifndef SQL_QUERY_HH_
#define SQL_QUERY_HH_

class SQLQuery;

#include <string>
#include <list>
#include <map>

#include <generated.hh>

using namespace std;

class SQLQuery : public Generated {
public:
  SQLQuery ();
  SQLQuery (const SQLQuery &query);

  void operator+=(const SQLQuery &rhs);

  void addFromClause (string var);//uses default table
  void addFromClause (string var, string altTableName);
  void addWhereConjunction (string conjunct);

  void setTableName (string tableName);
  void setDOName(string doName);
  //return default table name
  string getTableName(){ return tableName; }

  void print ();

  string getQuery ();

  list<string> predicatesWithoutMeaning;
  list<string> unusedPredicates;

protected:
  list<string> whereConjunctions;

  string tableName;
  string doName;
  map<string, string> fromClauses;
};

#endif
