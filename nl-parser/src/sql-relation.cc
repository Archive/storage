#include "sql-relation.hh"

using namespace PET;

SQLRelation::SQLRelation (list<string> attributes, list<int> argumentNumbers, string relationOperator) {
  this->attributes = attributes;
  this->argumentNumbers = argumentNumbers;
  this->relationOperator = relationOperator;
}

string SQLRelation::buildIdentifier (K2YObject *object) {
  char buffer[1024];
  sprintf (buffer, "G%d", object->getRelation()->id());
  return string(buffer);
}

SQLQuery SQLRelation::makeConjunction (K2YObject *parent) {
  vector<K2YObject *> arguments = parent->args;
  list<string>::iterator attributeIter = this->attributes.begin();
  list<int>::iterator argumentNumberIter = this->argumentNumbers.begin();

  int argumentsSize = arguments.size();

  SQLQuery query;

  string relationString;
  bool alreadyBuiltOne = false;

  while (attributeIter != this->attributes.end()) {
    string attribute = *attributeIter;
    int argumentNumber = *argumentNumberIter;
    
    if (argumentNumber > argumentsSize) {
      cerr << "Warning: tried to read an argument past the end\n";
      continue;
    }

    if (alreadyBuiltOne) {
      relationString += this->relationOperator;
    } else {
      alreadyBuiltOne = true;
    }

    if (argumentNumber < 0) {
      // This is a constant
      relationString += " " + attribute + " ";
    } else if (argumentNumber == 0) {
      // This is referring to itself, probably an entity
      string identifier = this->buildIdentifier(parent);

      query.addFromClause(identifier);

      relationString += " " + identifier + "." + attribute + " ";
    } else {
      // This is a valid argument, 1 indexed
      K2YObject *object = arguments[argumentNumber - 1];
      string identifier = this->buildIdentifier(object);

      query.addFromClause(identifier);

      relationString += " " + identifier + "." + attribute + " ";
    }

    attributeIter++;
    argumentNumberIter++;
  }

  if (relationString == "") cout << "AIEEE! Trying to print an empty one" << endl;

  query.addWhereConjunction(relationString);

  return query;
}
