#ifndef SQL_RELATION_HH_
#define SQL_RELATION_HH_

class SQLRelation;

#include <pet-parser.hh>

#include <vector>
#include <list>
#include <string>

#include <sql-query.hh>

using namespace std;

class SQLRelation {
public:
  SQLRelation (list<string> attributes, list<int> argumentNumbers, string relationOperator);

  SQLQuery makeConjunction (PET::K2YObject *parent);

protected:
  string buildIdentifier (PET::K2YObject *object);

  string relationOperator;

  // There should be one to one correspondence here
  list<string> attributes;
  list<int>    argumentNumbers;
};

#endif
