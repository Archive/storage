#include "semantics.h"


namespace PET {

void create_args_lists (list<K2YObject *> &objects) {
  // Connect the arguments together
  list<PET::K2YObject *>::iterator i = objects.begin();
  while (i != objects.end()) {
    PET::K2YObject *object = *i;

    object->createArgsList(objects);
    
    i++;
  }

  // Now connect in the postargs, these are from argof relations
  i = objects.begin();
  while (i != objects.end()) {
    PET::K2YObject *object = *i;

    list<PET::K2YObject *>::iterator j = object->postargs.begin();

    while (j != object->postargs.end()) {
      PET::K2YObject *child = *j;

      object->args.push_back(child);

      j++;
    }
    
    i++;
  }
}

static bool objectInArgs (K2YObject *object, K2YObject *possibleParent) {
  vector<PET::K2YObject *>::iterator i = possibleParent->args.begin();

  while (i != possibleParent->args.end()) {
    PET::K2YObject *possibleMatch = *i;
    if (possibleMatch->getRelation()->id() == object->getRelation()->id()) {
      return true;
    }

    i++;
  }

  return false;
}

void relink_quantifiers (list<K2YObject *> &objects, mrs &m) {
  list<PET::K2YQuantifier *> quantifiers;

  list<PET::K2YObject *>::iterator i = objects.begin();
  while (i != objects.end()) {
    PET::K2YObject *object = *i;
    
    if (object->getRole() == K2Y_QUANTIFIER) {
      K2YQuantifier *quantifier = (K2YQuantifier *)object;
      quantifier->swallowItemsInArgsHandle(m, objects);
      quantifiers.push_back(quantifier);
    }

    i++;
  }

  list<K2YQuantifier *>::iterator quantifier_iter = quantifiers.begin();
  while (quantifier_iter != quantifiers.end()) {
    K2YQuantifier *quantifier = *quantifier_iter;
    quantifier_iter++;

    int quantifiedID = quantifier->quantified->getRelation()->id();

    // Go through all the objects, and if they have quantified in their
    // arguments, point them at quantifier instead
    i = objects.begin();
    while (i != objects.end()) {
      K2YObject *object = *i;
      i++;

      // We don't want to substitute things that are already
      // in our args and create a recursive structure
      if (objectInArgs(object, quantifier)) continue;

      // Heh, don't update our own entry and create a recursive structure ;-)
      if (object == quantifier) continue;

      for (int j = 0; j < object->args.size(); j++) {
	if (object->args[j]->getRelation()->id() == quantifiedID) {
	  // This thing used to point to the quantified, redirect
	  // to point at us now
#if DEBUG_QUANTIFIER
	  cout << "Pointing " << object->getRelationName();
	  cout << " at quantifier " << quantifier->getRelationName();
	  cout << " instead of " << quantifier->quantified->getRelationName() << endl;
#endif
	  object->args[j] = quantifier;
	}
      }

    }

  }
}

K2YObject *K2YObject::resolveID (int id, list<K2YObject *> &objects) {
  list<PET::K2YObject *>::iterator i = objects.begin();

  while (i != objects.end()) {
    PET::K2YObject *object = *i;

    if (object->rel->id() == id) {
      return object;
    }

    i++;
  }

  //printf ("WARNING: didn't find a reference for id %d\n", id);
  return NULL;
}
  
K2YObject *new_auto_k2y_object (mrs_rel &rel, k2y_role role, int clause, fs index) {
  switch (role) {
  case K2Y_MAINVERB: return new K2YMainVerb (rel, role, clause, index);
  case K2Y_SUBJECT:
  case K2Y_DOBJECT:
  case K2Y_IOBJECT:  return new K2YEntity   (rel, role, clause, index);
  case K2Y_SENTENCE: return new K2YSentence (rel, role, clause);
  default:      printf ("In semantics.cpp falling back to default for %s\n", rel.name());
                return new K2YObject   (rel, role, clause, index);
  }
}

void K2YQuantifier::swallowItemsInArgsHandle(mrs &m, list<K2YObject *> &objects) {
#if DEBUG_QUANTIFIER
  cout << "Swallowing args in quantifier " << this->getRelationName() << endl;
#endif

  if (this->args.size() > 0) {
    K2YObject *object = this->args[0];
    this->quantified = object;
  } else {
    cerr << "WARNING: Trying to process a quantifier with no arguments!!!" << endl;
    return;
  }

  // We have the ID of the handel that is "under" this quantifier
  int handel_id = m.hcons(this->getRelation()->value(k2y_role_name("k2y_restr")));

  // We have the list of IDs under handel
  list<int> ids_under_handel = m.rels(k2y_role_name("k2y_hndl"), handel_id);

  if (ids_under_handel.size() < 1) {
    cerr << "WARNING: creating conjunction for " << this->getRelationName() << ", found no ids under handle " << handel_id << endl;
  }

  int quantifiedClause = this->quantified->getClause();
  this->args.clear();

  list<int>::iterator i = ids_under_handel.begin();
  while (i != ids_under_handel.end()) {
    int id = *i;

    K2YObject *object = this->resolveID(id, objects);

    // Only add objects from this clause
    if (object && object->getClause() == quantifiedClause) {
#if DEBUG_QUANTIFIER
      cout << "  Adding " << object->getRelationName() << "(" << object->getRelation()->id() << ") to the quantifier" << endl;
#endif
      this->args.push_back(object);
    }

    i++;
  }
}


K2YObject::K2YObject (mrs_rel &r, k2y_role role, int clause, fs index) {
  this->index = index;
  this->role = role;
  this->rel = new mrs_rel(r);
  this->clause = clause;
  this->visited = false;

  if (r.cvalue() >= 0) {
    this->has_cvalue = true;
    this->cvalue = r.cvalue();
  } 

  this->relationName = r.name();
}

K2YObject::~K2YObject () {
}

k2y_role K2YObject::getRole() {
  return this->role;
}

mrs_rel* K2YObject::getRelation() {
  return this->rel;
}

string K2YObject::getRelationName() {
  return relationName;
}

void K2YObject::createArgsList (list<K2YObject *> &allObjects) {
  //printf ("  %s has no args\n", getRelationName().c_str());
}

int K2YObject::getClause() {
  return this->clause;
}

K2YModifier::K2YModifier (mrs_rel &r, k2y_role role, int clause, int arg) : K2YObject (r, role, clause) {
  this->arg  = arg;
}

void K2YModifier::createArgsList (list<K2YObject *> &allObjects) {
  //printf ("  %s is a modifier, has one arg\n", getRelationName().c_str());
  K2YObject *object = this->resolveID(this->arg, allObjects);
  
  if (object != NULL) {
    this->args.push_back(object);
  }
}

K2YIntArg::K2YIntArg (mrs_rel &r, k2y_role role, int clause, mrs_rel argof) : K2YObject (r, role, clause) {
  this->argof = argof;
}

void K2YIntArg::createArgsList (list<K2YObject *> &allObjects) {
  K2YObject *object = this->resolveID(this->argof.id(), allObjects);
  
  if (object != NULL) {
    object->postargs.push_back(this);
  }
}

K2YQuantifier::K2YQuantifier (mrs_rel &r, k2y_role role, int clause, int var) : K2YObject (r, role, clause) {
  this->var  = var;
}

void K2YQuantifier::createArgsList (list<K2YObject *> &allObjects) {
  //printf ("  %s is a quantifier, has one arg\n", getRelationName().c_str());
  K2YObject *object = this->resolveID(this->var, allObjects);
  
  if (object != NULL) {
    this->args.push_back(object);
  }
}

K2YConj::K2YConj (mrs_rel &r, k2y_role role, int clause, list<int> conjs) : K2YObject (r, role, clause) {
  this->conjs = conjs;
}

void K2YConj::createArgsList (list<K2YObject *> &allObjects) {
  //printf ("  %s is a conj, has %d args\n", getRelationName().c_str(), this->conjs.size());

  list<int>::iterator i = this->conjs.begin();

  while (i != this->conjs.end()) {
    int id = *i;
    
    K2YObject *object = this->resolveID(id, allObjects);
  
    if (object != NULL) {
      this->args.push_back(object);
    }

    i++;
  }
}

K2YEntity::K2YEntity (mrs_rel &r, k2y_role role, int clause, fs index) : K2YObject (r, role, clause, index) {
  if (index == 0) return;

  fs f;
  
  // Get the plurality/voice
  f = r.get_fs().get_path_value("INST.PNG.PN");
  if (f.valid()) this->pn = f.name();
  
  // Get the gender
  f = r.get_fs().get_path_value("INST.PNG.GEN");
  if (f.valid()) this->gender = f.name();
}

K2YMainVerb::K2YMainVerb (mrs_rel &r, k2y_role role, int clause, fs index) : K2YObject (r, role, clause, index) {
  if (index == 0) return;

  fs f;

  // Get the tense
  f = index.get_path_value("E.TENSE");
  if(f.valid()) this->tense = f.name();

  // Get the aspect
  f = index.get_path_value("E.ASPECT");
  if(f.valid()) this->aspect = f.name();

  // Get the mood
  f = index.get_path_value("E.MOOD");
  if(f.valid()) this->mood = f.name();
}

void K2YMainVerb::createArgsList (list<K2YObject *> &allObjects) {
  //printf ("  %s is a verb, has 2 or 3 args\n", getRelationName().c_str());

  K2YObject *subject = NULL;
  K2YObject *directObject = NULL;
  K2YObject *indirectObject = NULL;

  list<PET::K2YObject *>::iterator i = allObjects.begin();

  int verbClause = this->getClause();

  while (i != allObjects.end()) {
    PET::K2YObject *object = *i;

    if (object->getClause() == verbClause) {
      bool found = false;

      if (object->getRole() == PET::K2Y_SUBJECT) {
	subject = object;
      } else if (object->getRole() == PET::K2Y_DOBJECT) {
	directObject = object;
      } else if (object->getRole() == PET::K2Y_IOBJECT) {
	if (indirectObject) {
	  //printf ("WARNING: VERB %s HAS MORE THAN ONE INDIRECT OBJECT\n", this->getRelationName().c_str());
	}
	indirectObject = object;
      }
    }

    i++;
  }
  
  if (subject) {
    this->args.push_back(subject);
  } else {
    //printf ("WARNING: VERB %s HAS NO SUBJECT, treating as passive\n", this->getRelationName().c_str());
    this->relationName = this->relationName + "_passive";
  }

  if (directObject) {
    this->args.push_back(directObject);
  } else {
    //printf ("WARNING: VERB %s HAS NO DIRECT OBJECT\n", this->getRelationName().c_str());
  }

  if (indirectObject) {
    this->args.push_back(indirectObject);
  }

}

K2YParticle::K2YParticle (mrs_rel &r, k2y_role role, int clause) : K2YObject (r, role, clause) {
}

K2YSentence::K2YSentence (mrs_rel &r, k2y_role role, int clause) : K2YObject (r, role, clause) {

}

void K2YSentence::createArgsList (list<K2YObject *> &allObjects) {
  //printf ("  %s is a sentence, has 1 args (main verb)\n", getRelationName().c_str());

  list<PET::K2YObject *>::iterator i = allObjects.begin();

  int sentenceClause = this->getClause();

  while (i != allObjects.end()) {
    PET::K2YObject *object = *i;

    if ((object->getRole() == PET::K2Y_MAINVERB) && (object->getClause() == sentenceClause)) {
      this->args.push_back(object);
      return;
    }

    i++;
  }  

  //printf ("WARNING: couldn't find a verb for sentence\n");

}

}
