#ifndef SEMANTICS_H
#define SEMANTICS_H

namespace PET {
  class K2YObject;
}

#include <string>

#include "k2y.h"
#include "mrs.h"

namespace PET {

K2YObject *new_auto_k2y_object (mrs_rel &rel, k2y_role role, int clause, fs index);

void create_args_lists (list<K2YObject *> &objects);
void relink_quantifiers (list<K2YObject *> &objects, mrs &m);

class K2YObject {
 public:
  K2YObject (mrs_rel &r, k2y_role role, int clause, fs index = 0);
  virtual ~K2YObject();

  k2y_role  getRole();
  mrs_rel*  getRelation();
  int       getClause();
  string    getRelationName();

  fs index;

  int cvalue;
  bool has_cvalue;

  vector<K2YObject *> args;
  list<K2YObject *> postargs;

  virtual void createArgsList (list<K2YObject *> &allObjects);

  bool tree_root;

  // Free for use by caller, defaults to false
  bool visited;

 protected:
  k2y_role role;
  mrs_rel *rel;
  int clause;

  string relationName;

  K2YObject *resolveID (int id, list<K2YObject *> &objects);
};

class K2YEntity : public K2YObject {
public:
  K2YEntity (mrs_rel &r, k2y_role role, int clause, fs index);

  string pn;
  string gender;
};

class K2YMainVerb : public K2YObject {
public:
  K2YMainVerb (mrs_rel &r, k2y_role role, int clause, fs index);

  string tense;
  string aspect;
  string mood;

  virtual void createArgsList (list<K2YObject *> &allObjects);
};

class K2YModifier : public K2YObject {
 public:
  K2YModifier (mrs_rel &r, k2y_role role, int clause, int arg); 

  int arg;

  virtual void createArgsList (list<K2YObject *> &allObjects);
};

class K2YIntArg : public K2YObject {
 public:
  K2YIntArg (mrs_rel &r, k2y_role role, int clause, mrs_rel argof); 

  virtual void createArgsList (list<K2YObject *> &allObjects);

  mrs_rel argof;
};

class K2YQuantifier : public K2YObject {
 public:
  K2YQuantifier (mrs_rel &r, k2y_role role, int clause, int var); 

  void swallowItemsInArgsHandle(mrs &m, list<K2YObject *> &objects);

  int var;

  K2YObject *quantified;

  virtual void createArgsList (list<K2YObject *> &allObjects);
};

class K2YConj : public K2YObject {
 public:
  K2YConj (mrs_rel &r, k2y_role role, int clause, list<int> conjs); 

  list<int> conjs;

  virtual void createArgsList (list<K2YObject *> &allObjects);
};

class K2YParticle : public K2YObject {
public:
  K2YParticle (mrs_rel &r, k2y_role role, int clause);
};

class K2YSentence : public K2YObject {
public:
  K2YSentence (mrs_rel &r, k2y_role role, int clause);

  virtual void createArgsList (list<K2YObject *> &allObjects);
};

}

#endif
