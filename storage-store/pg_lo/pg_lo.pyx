# -*- Mode: Python -*-

cdef extern from "libpq-fe.h":

    ctypedef struct PGconn
    ctypedef unsigned int Oid
    ctypedef unsigned int size_t
    ctypedef struct PGresult
    
    cdef int lo_open(PGconn *conn, Oid lobjId, int mode)
    cdef int lo_close(PGconn *conn, int fd)
    cdef int lo_read(PGconn *conn, int fd, char *buf, size_t len)
    cdef int lo_write(PGconn *conn, int fd, char *buf, size_t len)
    cdef int lo_lseek(PGconn *conn, int fd, int offset, int whence)
    cdef Oid lo_creat(PGconn *conn, int mode)
    cdef int lo_tell(PGconn *conn, int fd)
    cdef int lo_unlink(PGconn *conn, Oid lobjId)
    cdef Oid lo_import(PGconn *conn, char *filename)
    cdef int lo_export(PGconn *conn, Oid lobjId, char *filename)

    cdef PGconn *PQconnectdb(char *conninfo)
    cdef void PQclear(PGresult *res)
    cdef PGresult *PQexec(PGconn *conn, char *query)
    
cdef extern from "stdlib.h":
    cdef void *malloc(size_t size)
    cdef void free(void *ptr)
    cdef void *calloc(size_t nmemb, size_t size)

cdef extern from "Python.h":
    void Py_XINCREF (object)
    void Py_XDECREF (object)
    object PyString_FromStringAndSize(char *, int)

cdef class Handle
cdef class Connection


cdef class Handle:
    cdef int lo_handle
    cdef PGconn *conn
    
    cdef __cinit__(self, PGconn *_conn, int lo_handle):
        self.conn = _conn
        self.lo_handle = lo_handle

    def read(self, num_bytes):
        cdef char *buffer
        buffer = <char *>malloc(num_bytes * sizeof(char *))    
        read_bytes = lo_read(self.conn, self.lo_handle, buffer, num_bytes)
        python_string = PyString_FromStringAndSize(buffer, read_bytes)
        return python_string

    def close(self):
        retval = lo_close(self.conn, self.lo_handle)
        PQclear(PQexec(self.conn, "END"));
        return retval

cdef class Connection:
    cdef PGconn *conn

    def __init__(self, host=None, port=None):
        dsn = "dbname = storage "
        if host:
            dsn = dsn + "host = " + str(host)
        if port:
            dsn = dsn + "port = " + str(port)
            
        self.conn = PQconnectdb(dsn)

    cdef PGconn *getPGconn(self):
        return self.conn

    def open(self, object_id, mode):
        cdef int handle_int
        cdef Handle handle
        cdef Oid oid

        oid = object_id
        PQclear(PQexec(self.conn, "BEGIN"));    
        handle_int = lo_open(self.conn, oid, mode)
        handle = Handle()
        handle.__cinit__(self.conn, handle_int)
        return handle
