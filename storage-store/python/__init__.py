import dbus

hi = "bar"

import storage.renderer_factory

class Method:
    def __init__(self, target_method, store):
        self._target_method = target_method
        self._store = store
        
    def _items_to_paths(self, potential_items):
        if type(potential_items) == list:
            result = [ ]
            for potential_item in potential_items:
                result.append(self._items_to_paths(potential_item))
        elif potential_items.__class__ == Item:
            result = potential_items.get_item_path()
        else:
            result = potential_items

        return result

    def _paths_to_items(self, potential_paths):
        if type(potential_paths) == list:
            result = [ ]
            for potential_path in potential_paths:
                result.append(self._paths_to_items(potential_path))
        elif potential_paths.__class__ == dbus.ObjectPath:
            result = Item(potential_paths, self._store)
        else:
            result = potential_paths

        return result

    def __call__(self, *args):
        converted_args = self._items_to_paths(args)
        key = self._store.get_key()
        results = self._target_method(key, *converted_args)
        return self._paths_to_items(results)
            

class Item:
    def __init__(self, item_path, store):
        self._item_path = item_path
        self._store = store
        self._item = store.get_dbus_service().get_object(item_path, "org.gnome.Storage.Item")
        self._item.ref(store.get_key())


    def get_item_path(self):
        return self._item_path

    def get_renderer(self):
        return renderer_factory.get_renderer_for_item(self)

    def get_store(self):
        return self._store

    def __del__(self):
        try:
            self._item.unref()
        except Exception:
            pass

    def __getattr__(self, member):
        if member[0] != '_':
            return Method(self._item.__getattr__(member), self._store)
        else:
            raise AttributeError

class Store:
    def __init__(self, client_description):
        self._client_description = client_description
        
        bus = dbus.SystemBus()
        self._storage_service = bus.get_service("org.gnome.Storage")
        self._store = self._storage_service.get_object("/org/gnome/Storage", "org.gnome.Storage")
        self._key = self._store.register(client_description)

    def get_dbus_service(self):
        return self._storage_service

    def get_key(self):
        return self._key

    def __del__(self):
        try:
            self._storage_service.unregister(self._connection_key)
        except Exception:
            pass
        
    def __getattr__(self, member):
        if member[0] != '_':        
            return Method(self._store.__getattr__(member), self)
        else:
            raise AttributeError        

