import storage.renderers
import glob

_mime_to_render_class = { }

def _do_import(name):
    mod = __import__(name, globals(), locals(), [])
    components = name.split('.')
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod

def _get_renderer_modules(base_module):
    module_files = [ ]
    for path in base_module.__path__:
        module_files.extend(glob.glob(path + "/*py"))
    modules = [ ]
    for module_file in module_files:
        components = module_file.split('/')
        module_name = (components[len(components) - 1]).split('.')[0]
        modules.append(_do_import(base_module.__name__ + '.' + module_name))
    return modules

def _setup_mime_to_render_class(base_module):
    global _mime_to_render_class
    
    modules = _get_renderer_modules(base_module)

    for module in modules:
        try:
            mime_types = module.mime_types
            renderer_class = module.renderer_class

            for mime_type in mime_types:
                _mime_to_render_class[mime_type.lower()] = renderer_class
        except Exception, e:
            pass

def get_renderer_for_item(item):
    global _mime_to_render_class

    renderer = None
    mimetype = item.getAttribute("storage:mimetype")
    mimetype = mimetype.lower()

    try:
        rendererClass = _mime_to_render_class[mimetype]
        renderer = rendererClass(item)
    except KeyError:
        renderer = renderer.Renderer(item)
    
    return renderer

_setup_mime_to_render_class(storage.renderers)


