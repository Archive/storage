import gtk
from storage.renderers import renderer

class ImageRenderer(renderer.Renderer):
    def __init__(self, item):
        renderer.Renderer.__init__(self, item)

    def _render_pixbuf(self):
        try:
            binary_pixbuf_data = self.item.readBinary()

            loader = gtk.gdk.PixbufLoader()
            loader.write(binary_pixbuf_data)
            loader.close()
            
            pixbuf = loader.get_pixbuf()
        except Exception,e:
            message = "*Applet*: Error reading image: %s" % str(e)
            print (message)
            raise RenderFailedException(message)
                
        return pixbuf

    def _get_metadata_text(self):
        width = self._get_attribute("storage:width")
        height = self._get_attribute("storage:height")

        if width != None and width != "" and height != None and height != "":
            return str(width) + "x" + str(height)
        else:
            return None

renderer_class = ImageRenderer
mime_types = ["image/png", "image/jpeg", "image/gif", "image/x-portable-bitmap"]
