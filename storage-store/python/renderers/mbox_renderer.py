import gtk

from storage.renderers import renderer

class MboxRenderer(renderer.Renderer):
    def __init__(self, item):
        renderer.Renderer.__init__(self, item)

    def _get_title_text(self):
        return self._get_attribute("subject")

    def _get_metadata_text(self):
        frm = self._get_attribute("from")
        to = self._get_attribute("to")

        return "From: %s\nTo: %s" % (frm, to)

renderer_class = MboxRenderer
mime_types = ["message/rfc822", "application/x-mbox"]
