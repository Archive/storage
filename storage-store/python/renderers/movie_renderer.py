import gtk

from storage.renderers import renderer

class MovieRenderer(renderer.Renderer):
    def __init__(self, item):
        renderer.Renderer.__init__(self, item)

    def _get_metadata_text(self):
        return self._get_attribute("year")

renderer_class = MovieRenderer
mime_types = ["video/mpeg", "video/x-msvideo", "video/quicktime", "video/x-avi", "video/x-ms-asf", "video/x-ms-wmv", "application/vnd.rn-realmedia"]
