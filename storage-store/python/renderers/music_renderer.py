import gtk

from storage.renderers import renderer

class MusicRenderer(renderer.Renderer):
    def __init__(self, item):
        renderer.Renderer.__init__(self, item)
        self.loader = gtk.gdk.gdk_pixbuf_loader_new()

    def _get_metadata_text(self):
        metadata = ""
        artist = self._get_attribute("artist")
        #duration = self.item.get_attribute("duration")

        if artist != None and artist != "":
            metadata = "by " + artist

        return metadata

renderer_class = MusicRenderer
mime_types = ["audio/mpeg", "application/ogg"]
