import gtk

class NoThumbnailException(Exception):
    def __init__(self):
        Exception.__init__(self, "No thumbnail found")

class RenderFailedException(Exception):
    def __init__(self, description=None):
        Exception.__init__(self, description)
    
class Renderer:
    def __init__(self, item):
        self.item = item
    
    def render_thumbnail(self):
        try:
            pixbuf = self._get_thumbnail_pixbuf()
        except NoThumbnailException:
            try:
                pixbuf = self._render_pixbuf()
            except RenderFailedException:
                pixbuf = self._get_default_pixbuf()

        return self._scale_pixbuf_to_thumbnail(pixbuf)

    def render_text(self):
        title = self._get_title_text()
        metadata = self._get_metadata_text()

        if metadata == None:
            return title
        else:
            return title + "\n" + metadata

    def _get_attribute(self, attribute_name):
        return self.item.getAttribute(attribute_name)

    def _get_title_text(self):
        title = self._get_attribute("storage:title")
        if title == None:
            title = "Untitled"
        return title

    def _get_metadata_text(self):
        return None

    def _render_pixbuf(self):
        raise RenderFailedException()

    def _get_default_pixbuf(self):
        return default_thumb

    def _get_thumbnail_pixbuf(self):
        thumbid = self._get_attribute('storage:thumbnail')
        
        if thumbid == None:
            raise NoThumbnailException

        store = self.item.get_store()

        thumb_item = store.getItem(thumbid)
        renderer = thumb_item.get_renderer()
        thumbnail = renderer.render_thumbnail()
        
        return thumbnail

    def _scale_pixbuf_to_thumbnail(self, pixbuf):
        width = float(pixbuf.get_width())
        height = float(pixbuf.get_height())
        if width > height:
            height = (height / width) * 96.0
            width = 96.0
        if height > 64:
            width = (width / height) * 64.0
            height = 64.0

        return pixbuf.scale_simple(int(width), int(height), gtk.gdk.INTERP_BILINEAR)

renderer_class = Renderer
mime_types = []
