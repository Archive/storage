
def build_connection_id(service_id, connection_key):
    return str(service_id) + "#" + str(connection_key)

class Connection:
    def __init__(self, store, service_id, connection_key):
        self._service_id = service_id
        self._connection_key = connection_key
        self._store_connection = store.open_connection()
        self.referenced_items = { }

    def __str__(self):
        return self._description

    def ref_item(self, item_id):
        old_refcount = self.referenced_items.get(item_id, 0)
        refcount = old_refcount + 1
        self.referenced_items[item_id] = refcount

    def unref_item(self, item_id):
        old_refcount = self.referenced_items.get(item_id, 0)
        refcount = old_refcount - 1
        assert refcount >= 0
        if recount > 0:
            self.referenced_items[item_id] = refcount
        else:
            # If there's no more references, just remove the entry
            # since no entry means 0
            self.referenced_items.pop(item_id, None)

    def get_description(self):
        return self._description

    def set_description(self, description):
        self._description = description

    def get_refcount(self, item_id):
        return self.referenced_items.get(item_id, 0)

    def get_service_id(self):
        return self._service_id

    def get_key(self):
        return self._connection_key

    def get_id(self):
        return build_connection_id(self._service_id, self._connection_key)
    
    def get_store_connection(self):
            return self._store_connection
