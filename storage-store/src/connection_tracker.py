import random
import sys

random.seed()

from connection import Connection, build_connection_id

class NoSuchConnection(Exception):
    def __init__(message=None):
        Exception.__init__(self, message)


class ConnectionTracker:
    def __init__(self, store):
        self._connection_id_to_connection = { }
        self._service_id_to_connections = { }
        self._store = store
        
    def open_connection(self, service_id):
        connection_key = self._generate_connection_key(service_id)
        connection = Connection(self._store, service_id, connection_key)
        connection_id = connection.get_id()

        self._connection_id_to_connection[connection_id] = connection

        if not self._service_id_to_connections.has_key(service_id):
            self._service_id_to_connections[service_id] = []

        self._service_id_to_connections[service_id].append(connection)
        
        return connection

    def close_connection(self, connection):
        connection_id = connection.get_id()
        popped_connection = self._connection_id_to_connection.pop(connection_id, None)
        if popped_connection == None:
            raise NoSuchConnectionException

        try:
            self._service_id_to_connections[service_id].remove(connection)
            # FIXME: if there's no more connections to service, remove the connection list from the hash
        except KeyError:
            pass
        
    def get_connection(self, service_id, connection_key):
        connection_id = build_connection_id(service_id, connection_key)
        try:
            return self._connection_id_to_connection[connection_id]
        except KeyError:
            raise NoSuchConnectionException, "Could not find a connection for service ID '" + service_id + "' with the given key."

    def get_connection_from_id(self, connection_id):
        try:
            return self._connection_id_to_connection[connection_id]
        except KeyError:
            raise NoSuchConnectionException, "Could not find connection '" + connection_id + "'"

    def get_connections(self):
        return self._connection_id_to_connection.values()

    def get_connections_from_service(self, service_id):
        try:
            return self._service_id_to_connections[service_id]
        except KeyError:
            return []

    def _generate_connection_key(self, service_id):
        while(1):
            #FIXME: for security we probably should use a better random number generator
            connection_key = random.randint(1, sys.maxint)
            connection_id = build_connection_id(service_id, connection_key)
            if not self._connection_id_to_connection.has_key(connection_id):
                break

        return connection_key


