class Connection(dbus.Object):
    def __init__(self, store, service_id, connection_key, description):
        self._service_id = service_id
        self._description = description
        self._connection_key = connection_key
        self._store_connection = store.open_connection()
        self.referenced_items = { }

    def unregister(self, message, connection_key):
        """Unregister this connection"""
        self._unregister(message.get_sender(), connection_key, complain_if_unregistered=True)

    def getItemsMatchingQuery(self, message, connection_key, query):
        connection = self.get_connection(message.get_sender(), connection_key)
        node_ids = sql_helper.getIDsMatchingQuery(connection.get_store_connection(), query)
        return _storage_ids_to_object_paths(node_ids)

    def getToplevelItems(self, message, connection_key):
        connection = self.get_connection(message.get_sender(), connection_key)            
        node_ids = sql_helper.getToplevelNodes(connection.get_store_connection())
        return _storage_ids_to_object_paths(node_ids)

    def getServiceDescription(self, message, service_id):
        client = self.get_client(service_id)
        return str(client)


    # Local methods

    def __str__(self):
        return self._description

    def ref_item(self, item_id):
        old_refcount = self.referenced_items.get(item_id, 0)
        refcount = old_refcount + 1
        self.referenced_items[item_id] = refcount

    def unref_item(self, item_id):
        old_refcount = self.referenced_items.get(item_id, 0)
        refcount = old_refcount - 1
        assert refcount >= 0
        if recount > 0:
            self.referenced_items[item_id] = refcount
        else:
            # If there's no more references, just remove the entry
            # since no entry means 0
            self.referenced_items.pop(item_id, None)

    def get_refcount(self, item_id):
        return self.referenced_items.get(item_id, 0)

    def get_service_id(self):
        return self._service_id

    def get_store_connection(self):
            return self._store_connection
