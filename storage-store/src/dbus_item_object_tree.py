import dbus
from storage_store import sql_helper

def _storage_id_to_object_path(id):
    return dbus.ObjectPath("/org/gnome/Storage/items" + "/" + str(id))

def _storage_ids_to_object_paths(id_list):
    paths = [ ]
    for id in id_list:
        paths.append(_storage_id_to_object_path(id[0]))
    return paths

class ItemObjectTree(dbus.ObjectTree):
    def __init__(self, item_base_path, service, connection_tracker):
        dbus.ObjectTree.__init__(self, item_base_path, service,
                                 dbus_methods = [ self.getAttributes,
                                                  self.getAttribute,
                                                  self.getChildren,
                                                  self.getParents,
                                                  self.touch,
                                                  self.getConnections,
                                                  self.ref,
                                                  self.unref,
                                                  self.readBinary ])

        self._base_path = item_base_path
        self._connection_tracker = connection_tracker
        self._service = service

    def _storage_id_from_item_path(self, item_path):
        return item_path[1:]

    def _add_attribute_value_pairs_to_dict(self, attribute_value_pairs, attributeDict):
        for av_pair in attribute_value_pairs:
            attributeDict[av_pair[0]] = av_pair[1]

    def getAttributes(self, message, item_path, connection_key):
        connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)
        item_id = self._storage_id_from_item_path(item_path)

        soups = ["AttrSoup", "NumberSoup", "DateSoup"]

        attributeDict = { }

        for soup in soups:
            av_pairs = sql_helper.getAttributeValuePairs(item_id, connection.get_store_connection(), soup)
            self._add_attribute_value_pairs_to_dict(av_pairs, attributeDict)

        return attributeDict

    def getAttribute(self, message, item_path, connection_key, attribute_name):
        connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)            
        item_id = self._storage_id_from_item_path(item_path)

        attribute_value = None

        soups = ["AttrSoup", "NumberSoup", "DateSoup"]

        for soup in soups:
            values = sql_helper.getAttribute(item_id, connection.get_store_connection(), soup, attribute_name)
            if len(values) > 0:
                attribute_value = values[0][0]
                break

        return attribute_value

    def _set_attribute(store_connection, attribute_name, attribute_value):
        if type(attribute_value) in [int, float]:
            soup = "NumberSoup"
        else:
            soup = "AttrSoup"

        #FIXME: deal with DateSoup
        
        sql_helper.setAttribute(item_id, soup, attribute_name, attribute_value)

    def setAttribute(self, message, item_path, connection_key, attribute_name, attribute_value):
        connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)            
        item_id = self._storage_id_from_item_path(item_path)
        self._set_attribute(connection.get_store_connection(), attribute_name, attribute_value)
                                
    def readBinary(self, message, item_path, connection_key):
        connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)

        blobid = self.getAttribute(message, item_path, connection_key, 'storage:blobid')

        handle = connection.get_store_connection().getBinaryHandle(blobid)

        result = ""

        read = handle.read(100000)
        while len(read) > 0:
            print ("reading... size " + str(len(read)))
            result = result + read
            read = handle.read(100000)

        print ("Got a result size " + str(len(result)))

        return dbus.ByteArray(result)


    def getChildren(self, message, item_path, connection_key):
        connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)
        item_id = self._storage_id_from_item_path(item_path)            
        child_ids = sql_helper.getChildren(item_id, connection.get_store_connection())
        return _storage_ids_to_object_paths(child_ids)

    def getParents(self, message, item_path, connection_key):
        connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)                        
        item_id = self._storage_id_from_item_path(item_path)
        parent_ids = sql_helper.getParents(item_id, connection.get_store_connection())
        return _storage_ids_to_object_paths(parent_ids)

    def touch(self, message, item_path):
        self.emit_signal("org.gnome.Storage.Item", "changed", item_path)

    def getConnections(self, message, item_path, connection_key):
        my_connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)                        
        item_id = self._storage_id_from_item_path(item_path)
        
        connections = self._connection_tracker.get_connections()

        service_ids = { }

        for connection in connections:
            if connection.get_refcount(item_id) > 0:
                service_ids[connection.get_id()] = True

        return service_ids.keys()

    def ref(self, message, item_path, connection_key):
        connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)
        item_id = self._storage_id_from_item_path(item_path)
        connection.ref_item(item_id)

    def unref(self, message, item_path, connection_key):
        connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)
        item_id = self._storage_id_from_item_path(item_path)
        connection.unref_item(item_id)            

