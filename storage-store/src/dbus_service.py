import dbus

from dbus_item_object_tree import ItemObjectTree
from dbus_storage_object import StorageObject
from connection_tracker import ConnectionTracker

class DBusService(dbus.Service):

    def __init__(self, store):
        dbus.Service.__init__(self, "org.gnome.Storage", bus=dbus.SystemBus())

        connection_tracker = ConnectionTracker(store)
        store_object = StorageObject("/org/gnome/Storage", self, connection_tracker)
        item_object_tree = ItemObjectTree("/org/gnome/Storage/items", self, connection_tracker)

