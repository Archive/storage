import dbus

from storage_store import sql_helper
from connection_tracker import ConnectionTracker

def _storage_id_to_object_path(id):
    return dbus.ObjectPath("/org/gnome/Storage/items" + "/" + str(id))
            
def _storage_ids_to_object_paths(id_list):
    paths = [ ]
    for id in id_list:
        paths.append(_storage_id_to_object_path(id[0]))
    return paths

class StorageObject(dbus.Object):

    def __init__(self, object_path, service, connection_tracker):
        dbus.Object.__init__(self, object_path, service, [self.getToplevelItems,
                                                          self.getItem,
                                                          self.getItemsMatchingQuery,
                                                          self.register,
                                                          self.unregister,
                                                          self.getConnectionDescription])
        self._connection_tracker = connection_tracker

        # Register to receive the ServiceDeleted signal so we know when clients leave
        dbus_bus_service = service.get_bus().get_service("org.freedesktop.DBus")
        dbus_bus_object  = dbus_bus_service.get_object("/org/freedesktop/DBus", "org.freedesktop.DBus")
        dbus_bus_object.connect_to_signal("ServiceDeleted", self.service_deleted_handler)
        
        # DBus Methods
        
    def register(self, message, description):
        """Register yourself as accessing the store"""
        service_id = message.get_sender()
        connection = self._connection_tracker.open_connection(service_id)
        connection.set_description(description)
        print "Registered '" + description + "'"
        return connection.get_key()

    def unregister(self, message, connection_key):
        """Unregister this connection"""
        connection = self._connection_tracker.get_connection(message.get_sender(), connection_key)
        self._connection_tracker.close_connection(connection)
        print "Connection " + str(client) + " unregistered"

    def getItem(self, message, connection_key, item_id):
        return _storage_id_to_object_path(item_id)

    def getItemsMatchingQuery(self, message, connection_key, query):
        connection = self.get_connection(message.get_sender(), connection_key)
        node_ids = sql_helper.getIDsMatchingQuery(connection.get_store_connection(), query)
        return _storage_ids_to_object_paths(node_ids)

    def getToplevelItems(self, message, connection_key):
        connection = self.get_connection(message.get_sender(), connection_key)            
        node_ids = sql_helper.getToplevelNodes(connection.get_store_connection())
        return _storage_ids_to_object_paths(node_ids)

    def getConnectionDescription(self, message, connection_key, connection_id):
        connection = self._connection_tracker.get_connection_from_id(connection_id)
        return connection.get_description()

    def commit(self, message, connection_key):
        """Commit any outstanding changes in this connection to the store"""
        connection = self.get_connection(message.get_sender(), connection_key)        
        store_connection = connection.get_store_connection()
        store_connection.commit()

    def rollback(self, message, connection_key):
        """Rollback any outstanding changes in this connection to the store"""
        connection = self.get_connection(message.get_sender(), connection_key)        
        store_connection = connection.get_store_connection()
        store_connection.rollback()
        
    # Local Methods

    def get_connection(self, service_id, connection_key):
        return self._connection_tracker.get_connection(service_id, connection_key)

    # Called when the ServiceDeleted(service_id) signal is emitted by the bus
    def service_deleted_handler(self, interface, signal_name, bus_service, path, message):
        deleted_service_id = message.get_args_list()[0]
        connections = self._connection_tracker.get_connections_from_service(deleted_service_id)

        print "Service '" + deleted_service_id + "' disconnected, closing " + str(len(connections)) + " connections."

        for connection in connections:
            self._connection_tracker.close_connection(connection)
