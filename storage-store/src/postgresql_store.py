import os
import pgdb
import pg_lo

from storage_store import globals

class PostgresqlStore:
    """Manages the data store
    """

    class PostgresqlConnection:
        def __init__(self, database, host, port):
            self._database = database
            self._host = host
            self._port = port
            self._connection = pgdb.connect(database=database, host=":%d" % (port))

        def get_pgdb_connection(self):
            return self.connection
            
        def disconnect(self):
            self._connection.close()

        def getBinaryHandle(self, id):
            loconnection = pg_lo.Connection(self._host, self._port)
            return loconnection.open(id, 0x00020000) # FIXME: open read... write is 0x00040000

        def query(self, query_string):
            cursor = self._connection.cursor()
            cursor.execute(query_string)
            return cursor.fetchall()

        def commit(self):
            # FIXME: er, really need to throw errors here if commit fails!
            self._connection.commit()

        def rollback(self):
            self._connection.rollback()

        def __del__(self):
            self.disconnect()
            
    database = None
    database_path = None
    database_port = None
    database_host = None
    
    def __init__(self):
        # FIXME!
        self.database = "storage"
        self.database_path = globals.database_dir
        self.database_port = 11592
        self.database_host = ""

        print ("Database path is %s" % (self.database_path))
        
        if not self.store_exists():
            self.create_store()

        if not self._postgresql_is_running():
            self._start_postgresql()
            
        assert(self._postgresql_is_running())

    def __del__(self):
        print ("Deleting store")
        self._stop_postgresql()

    def open_connection(self):
        return self.PostgresqlConnection(self.database, self.database_host, self.database_port)
        
    def get_db_port_number(self):
        return self.database_port

    def get_db_host(self):
        return self.database_host
    
    def store_exists(self):
        """Returns true if the Postgres database exists, else false"""

        if os.path.isfile(self.database_path + "/PG_VERSION"):
            return True
        else:
            return False

    def create_store(self):
        """Creates a new store"""

        assert(not self.store_exists())

        self._generate_database_files()
        self._start_postgresql()
        self._build_database()

    def _generate_database_files(self):

        initdb_result = os.spawnl(os.P_WAIT, globals.initdb_path, 'initdb', self.database_path)
        if initdb_result != 0:
            raise Exception("initdb failed")

        config_file = open(self.database_path + "/postgresql.conf", "w")
        config_file.write(
"""max_connections = 100
port = %s
shared_buffers = 1000		
                          """ % (self.database_port))
        config_file.close()

    def _start_postgresql(self):
        os.spawnl(os.P_WAIT, globals.pg_ctl_path, 'pg_ctl', '-w', '-D', self.database_path, "start")

    def _stop_postgresql(self):
        os.spawnl(os.P_WAIT, globals.pg_ctl_path, 'pg_ctl', '-w', '-D', self.database_path, "stop")

    def _postgresql_is_running(self):
        result = os.spawnl(os.P_WAIT, globals.pg_ctl_path, 'pg_ctl', '-D', self.database_path, "status")
        if result == 0:
            return True
        else:
            return False
        
    def _build_database(self):

        createdb_result = os.spawnl(os.P_WAIT, globals.createdb_path, 'createdb',
				    '--port=%d' % (self.database_port), '--template', 'template0',  self.database)

        connection = self.open_connection()
        pgdb_connection = connection.get_pgdb_connection()
        cursor = pgdb_connection.cursor()

        cursor.execute("CREATE TABLE AttrSoup (recordID oid, attrName text, attrValue text)")
        cursor.execute("CREATE TABLE NodeChildren (parent Oid, child Oid, linknum int)")
                                                                                
        cursor.execute("CREATE TABLE AvailableRecordIDs (recordID oid)")
        cursor.execute("CREATE TABLE PrevHighestRecordID (recordID integer)")
        cursor.execute("INSERT INTO PrevHighestRecordID VALUES (1)")
                                                                                
        cursor.execute("create index ATTRNAME_INDEX_SOUP on attrsoup (attrname)")
        cursor.execute("create index RECORDID_INDEX_SOUP on attrsoup (recordid)")
                                                                                
        cursor.execute("CREATE TABLE TextRecords (recordID oid, content text)")
        cursor.execute("create index RECORDID_INDEX_TEXT on textrecords (recordid)")
                                                                                
        cursor.execute("CREATE TABLE DateSoup (recordID oid, attrName text, attrValue time)")
        cursor.execute("create index ATTRNAME_INDEX_DATESOUP on datesoup (attrname)")
        cursor.execute("create index RECORDID_INDEX_DATESOUP on datesoup (recordid)")
                                                                                
        cursor.execute("create TABLE NumberSoup (recordID oid, attrName text, attrValue integer)")
        cursor.execute("create index ATTRNAME_INDEX_NUMBERSOUP on numbersoup (attrname)")
        cursor.execute("create index RECORDID_INDEX_NUMBERSOUP on numbersoup (recordid)")
                                                                                
        cursor.execute("ANALYZE")

        cursor.close()

        pgdb_connection.commit()

        connection.disconnect()
