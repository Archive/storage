num_soups = 3
DATE, NUMBER, ATTR = range(num_soups)
soups = {DATE: "DateSoup", NUMBER: "NumberSoup", ATTR: "AttrSoup"}

def getAttributeValuePairs(item_id, store_connection, soup):
    query_string = "SELECT attrName, attrValue FROM " + soup + " WHERE recordID = " + item_id
    return store_connection.query(query_string)

def getAttribute(item_id, store_connection, soup, attrname):
    query_string = "SELECT attrValue FROM " + soup + " WHERE recordID = " + item_id + " AND attrName = '" + attrname + "'"
    return store_connection.query(query_string)

def setAttribute(item_id, store_connection, soup, attribute_name, attribute_value):
    # FIXME: ER, need to update first 
    query_string = "INSERT INTO " + soup + " VALUES (" + item_id + ",'" + attribute_name + "','" + attribute_value + "')"
    store_connection.query(query_string)

def getChildren(item_id, store_connection):
    query_string = "SELECT child FROM NodeChildren WHERE parent = " + item_id + " ORDER BY linknum ASC"
    return store_connection.query(query_string)

def getParents(item_id, store_connection):
    query_string = "SELECT parent FROM NodeChildren WHERE child = " + item_id
    return store_connection.query(query_string)

def getToplevelNodes(store_connection):
    query_string = "SELECT recordID FROM AttrSoup WHERE attrName='storage:top_level' AND attrValue='T'"
    return store_connection.query(query_string)

def getIDsMatchingQuery(store_connection, query_string):
    return store_connection.query(query_string)    


