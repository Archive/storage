import gtk
import gobject
import storage

class ItemInfo(gtk.Frame):
    def __init__(self, store):
        gtk.Frame.__init__(self)

        self._user_list = UserList(store)

        self._thumbnail = gtk.Image()
        self._text = gtk.Label()
        self._text.set_alignment(0.0, 0.5)
        
        hbox = gtk.HBox()
        hbox.pack_start(self._thumbnail, expand=False)
        hbox.pack_start(self._text)

        using_item_label = gtk.Label("Apps using this item:")
        using_item_label.set_alignment(0.0, 0.5)

        vbox = gtk.VBox()
        vbox.pack_start(hbox, expand=False)
        vbox.pack_start(using_item_label, expand=False)
        vbox.pack_start(self._user_list)
        
        self.add(vbox)
        
    def set_item(self, item):
        # Render things for purtiness
        renderer = item.get_renderer()
        self._thumbnail.set_from_pixbuf(renderer.render_thumbnail())
        self._text.set_label(renderer.render_text())
        
        self.set_label(item.get_item_path())
        self._user_list.set_item(item)
        
class UserList(gtk.ScrolledWindow):
    def __init__(self, storage_store):
        gtk.ScrolledWindow.__init__(self)
        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_shadow_type(gtk.SHADOW_IN)

        self._storage_store = storage_store

        self._model = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING)
        self._tree_view = gtk.TreeView(self._model)
        
        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn("DBus ID", renderer, text=0)
        self._tree_view.append_column(column)
        column = gtk.TreeViewColumn("Description", renderer, text=1)
        self._tree_view.append_column(column)        

        self.add(self._tree_view)

    def set_item(self, item):
        self._model.clear()        

        for connection_id in item.getConnections():
            self._add_user(connection_id, self._storage_store.getConnectionDescription(connection_id))

    def _add_user(self, connection_id, description):
        self._model.prepend([connection_id, description])
        
class ItemTree(gtk.ScrolledWindow):
    def __init__(self, storage_store, attribute_list, item_info):
        gtk.ScrolledWindow.__init__(self)
        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_shadow_type(gtk.SHADOW_IN)
        
        self._attribute_list = attribute_list
        self._item_info = item_info
        
        self._model = gtk.TreeStore(gobject.TYPE_PYOBJECT, gobject.TYPE_STRING)
        self._tree_view = gtk.TreeView(self._model)

        self._tree_view.get_selection().connect("changed", self._handle_selection_changed)
        
        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn("Items", renderer, text=1)

        for item in storage_store.getToplevelItems():
            self._add_item(item, parent=None)

        self._tree_view.append_column(column)

        self.add(self._tree_view)

    def _add_item(self, item, parent):
        attributes = item.getAttributes()
        try:
            title = attributes['storage:title']
        except KeyError:
            title = item.get_item_path()
        iter = self._model.prepend(parent, [item, title])

        children = item.getChildren()

        if children:
            for child_item in children:
                self._add_item(child_item, iter)
        

    def _handle_selection_changed(self, selection):
        (model, selected_rows) = selection.get_selected_rows()
        assert len(selected_rows) <= 1
        if len(selected_rows) == 1:
            iter = self._model.get_iter(selected_rows[0])
            item = self._model.get(iter, 0)[0]
            self._attribute_list.set_item(item)
            self._item_info.set_item(item)

class AttributeList(gtk.ScrolledWindow):
    def __init__(self):
        gtk.ScrolledWindow.__init__(self)
        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_shadow_type(gtk.SHADOW_IN)
        
        self._model = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING)
        self._tree_view = gtk.TreeView(self._model)
        
        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn("Attribute", renderer, text=0)
        self._tree_view.append_column(column)
        column = gtk.TreeViewColumn("Value", renderer, text=1)
        self._tree_view.append_column(column)        

        self.add(self._tree_view)

    def set_item(self, item):
        self._model.clear()
        attributes = item.getAttributes()
        for attribute in attributes.keys():
            self._add_attribute(attribute, attributes[attribute])

    def _add_attribute(self, attribute, value):
        self._model.prepend([attribute, value])

class BrowserWindow(gtk.Window):
    def __init__(self):
        gtk.Window.__init__(self)        
        self.set_title("Store Browser")

        self._store = storage.Store("Store Browser")

        attribute_list = AttributeList()
        item_info = ItemInfo(self._store)
        item_tree = ItemTree(self._store, attribute_list, item_info)

        vpane = gtk.VPaned()
        vpane.add1(item_info)        
        vpane.add2(attribute_list)

        hpane = gtk.HPaned()
        hpane.add1(item_tree)
        hpane.add2(vpane)

        self.add(hpane)
        
window = BrowserWindow()
window.show_all()
gtk.main()
