/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* dbfs-method.c: 

Copyright (C) 2003,2004 Seth Nickell, Brian Quistorff

The Gnome Library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

The Gnome Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with the Gnome Library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Authors: Seth Nickell      (snickell@stanfordalumni.org) 
         Brian Quistorff   (bquistorff@cs.stanford.edu) */


#include "connection.h"

#include <glib.h>
#include <string.h>

#include <libstorage/storage-store.h>

GMutex * connLock;
GHashTable *hostToFreeConnections;
GHashTable *connectionToFreeConnections;
GCond *availabilityCond;

#define NUM_CONNECTIONS 5

#define LOCAL_HOST_ID "local"

static PGconn *new_connection(const LoginInfo *info);

static void 
RemoveConnectionsFromQueue (gpointer key, gpointer value, gpointer user_data) 
{
	GQueue *connections = (GQueue *)value;
	while (!g_queue_is_empty (connections)) {
		PGconn *connection = g_queue_pop_head(connections);
		PQfinish(connection);
		/* FIXME: need to free the connection ? */
	}
}

void initConnections() {
	availabilityCond = g_cond_new();
	connLock = g_mutex_new();

	hostToFreeConnections       = g_hash_table_new (g_str_hash, g_str_equal);
	connectionToFreeConnections = g_hash_table_new(g_direct_hash, g_direct_equal);
}

void shutdownConnections() {
	g_mutex_free(connLock);
	g_cond_free(availabilityCond);
	g_hash_table_foreach (hostToFreeConnections, &RemoveConnectionsFromQueue, NULL);
}


static PGconn *
new_connection(const LoginInfo *info) 
{
	GString *dsn;
	PGconn *connection;
	char * error;
	ConnStatusType stat;

	dsn = g_string_new("");

	g_string_append_printf (dsn, "dbname = %s ", STORAGE_DB_NAME);

	if (info->hostname) {
		g_string_append_printf (dsn, "host = %s ", info->hostname);
	}

	if (info->port) {
		g_string_append_printf (dsn, "port = %d ", info->port);
	} else {
		g_string_append_printf (dsn, "port = %s ", STORAGE_DB_PORT);
	}

	if (info->password) {
		g_string_append_printf (dsn, "password = %s ", info->password);
	}

	if (info->username) {
		g_string_append_printf (dsn, "username = %s ", info->username);
	}

	connection = PQconnectdb(dsn->str); 

	g_string_free(dsn, TRUE);

	stat = PQstatus(connection);
	error = PQerrorMessage(connection);
	if (PQstatus(connection) == CONNECTION_BAD)
		{
		  /* printf("Connection to database '%s' failed.\n", dbName); */
			printf("%s", PQerrorMessage(connection));
		}
	if(strlen(error) > 0){
		fprintf(stderr, "DB connection error to " STORAGE_DB_NAME ": %s\n", error);
		return NULL;
	}

	return connection;
}

static GQueue *create_connections(const LoginInfo *info) {
	GQueue *connections = g_queue_new();
	
	int i;

	for (i = 0; i < NUM_CONNECTIONS; i++) {
		PGconn *connection = new_connection (info);
		g_queue_push_head(connections, connection);
		g_hash_table_insert(connectionToFreeConnections, connection, connections);
	}

	/* FIXME: also key by port number */
	if (info->hostname != NULL) {
		g_hash_table_insert (hostToFreeConnections, g_strdup(info->hostname),
				     connections);
	} else {
		g_hash_table_insert (hostToFreeConnections, g_strdup(LOCAL_HOST_ID),
                                     connections);
	}

	return connections;
}

PGconn * 
getConnection(const LoginInfo *info) 
{
	PGconn *connection;
	GQueue *connections;

	g_mutex_lock(connLock);

	/* FIXME: also key by port number */
	if (info->hostname) {	
		connections = g_hash_table_lookup (hostToFreeConnections, info->hostname);
	} else {
		connections = g_hash_table_lookup (hostToFreeConnections, LOCAL_HOST_ID);
	}

	if (connections == NULL) {
		connections = create_connections (info);
	}

	while(g_queue_is_empty(connections)){
		g_cond_wait(availabilityCond, connLock);
	}

	connection = g_queue_pop_head (connections);

	g_mutex_unlock(connLock);

	return connection;
}

void 
releaseConnection(PGconn *connection)
{
	GQueue *connections;

	if (connection == NULL) return;

	/* first free it in the table*/
	g_mutex_lock(connLock);
	connections = g_hash_table_lookup(connectionToFreeConnections, connection);
	if (connections == NULL) {
		printf ("WARNING: we have a connection, and we don't know where it goes in releaseConnection\n");
	}
	g_queue_push_tail(connections, connection);
	g_mutex_unlock(connLock);

	g_cond_broadcast(availabilityCond);
}
