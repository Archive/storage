/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* dbfs-method.c: 

Copyright (C) 2003,2004 Seth Nickell, Brian Quistorff

The Gnome Library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

The Gnome Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with the Gnome Library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Authors: Seth Nickell      (snickell@stanfordalumni.org) 
         Brian Quistorff   (bquistorff@cs.stanford.edu) */

#ifndef VFS_CONNECTION_H
#define VFS_CONNECTION_H

#include <libpq-fe.h>

typedef struct {
	const char *hostname;
	int         port;
	const char *username;
	const char *password;
} LoginInfo;

PGconn * getConnection(const LoginInfo *info);
void releaseConnection(PGconn *connection);

void initConnections();
void shutdownConnections();

#endif
