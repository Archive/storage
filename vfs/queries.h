#ifndef DBFS_QUERIES_H_
#define DBFS_QUERIES_H_

#define DBFS_GETBLOBID_PREFIX "SELECT attrValue FROM NumberSoup WHERE attrName = '" STORAGE_FILE_BLOB "' AND recordID = "

#define DBFS_TEXT_DELETE_PREFIX "DELETE FROM NodeRecords WHERE recordID = "
#define DBFS_ATTRS_DELETE_PREFIX "DELETE FROM AttrSoup WHERE recordID = "

#define DBFS_UPDATE_ACCESS_PREFIX "UPDATE DateSoup SET attrValue = CURRENT_TIMESTAMP WHERE attrName = '" STORAGE_FILE_ACCESS "' AND  recordID = "
#define DBFS_UPDATE_MODIFICATION_PREFIX "UPDATE DateSoup SET attrValue = CURRENT_TIMESTAMP WHERE attrName = '" STORAGE_FILE_MODIFICATION "' AND recordID = "

#define DBFS_GET_ALL_ATTRS_PREFIX  "SELECT attrName, attrValue FROM AttrSoup WHERE recordID = "
#define DBFS_GET_ALL_DATE_ATTRS_PREFIX  "SELECT attrName, attrValue FROM DateSoup WHERE recordID = "
#define DBFS_GET_ALL_NUMBER_ATTRS_PREFIX  "SELECT attrName, attrValue FROM NumberSoup WHERE recordID = "

#define DBFS_TITLE_PREFIX  "SELECT attrValue FROM AttrSoup WHERE attrName = '" STORAGE_FILE_NAME "'AND recordID = "
#define DBFS_RECORD_PREFIX "SELECT recordid FROM AttrSoup WHERE attrName = '" STORAGE_FILE_NAME "' AND attrValue = "

#define DBFS_ALL_RECORDS "SELECT DISTINCT(recordid) FROM AttrSoup"

#endif
