/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* dbfs-method.c: 

Copyright (C) 2003 Seth Nickell, Brian Quistorff

The Gnome Library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

The Gnome Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with the Gnome Library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Authors: Seth Nickell      (snickell@stanfordalumni.org) 
         Brian Quistorff   (bquistorff@cs.stanford.edu) */

// storage:///                              <- lists .desktop files for all items
// storage:///#.desktop                     <- .desktop file for item id #
// storage:///query/QUERY_CONTENTS          <- lists .desktop files for QUERY_CONTENTS
// storage:///query/*/#.desktop             <- .desktop file for item id #
// storage:///items/                        <- lists the blobs
// storage:///items/#/*                     <- blob (or translator result) for item id


#include <config.h>

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <libpq-fe.h>
#include <libpq/libpq-fs.h>

#include <libbonobo.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-module.h>
#include <libgnomevfs/gnome-vfs-mime.h>

#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#include <libstorage/storage-item.h>

#include "GNOME_StorageTranslator.h"

#include "queries.h"
#include "connection.h"

#define DBFS_DEBUG 1


#define GNOME_VFS_MIME_TYPE_UNKNOWN "application/octet-stream"

typedef enum {UpdateAccess, UpdateModification, 
	      GetAllAttrs, GetAllDateAttrs, GetAllNumberAttrs,
	      GetBLOBID, NumDBFSQueries} dbfs_query;

typedef enum { RECORD, DESKTOP_FILE, SQL_QUERY } HandleType;


typedef union {
	/* Valid for DESKTOP_FILE */
	struct {
		char *contents;
		char *name;
		int read;
		int size;
		Oid recordid;
	} desktop_file;
		

	/* Valid for SQL_QUERY */
	struct {
		char *contents;
		GList *matches;
		gboolean use_desktop_files;
	} query;

	/* Valid for RECORD */
	struct {
		/* For Reading */
		Oid id;
		int blobID;
		int fd;

		/* For Writing */
		GNOME_StorageTranslator translator;
		CORBA_Environment ev;
		const char *mime_type;
		char *file_name;
		gboolean try_magic_sniffing;
		gboolean translator_exists;
		gboolean create_translator;
	} record;
} HandleContents;


//The main handle structure
typedef struct {
	HandleType type;

	PGconn *connection;
	LoginInfo *login_info;

	HandleContents contents;
} DBFileHandle;

static DBFileHandle *dbfs_handle_new(const GnomeVFSURI *uri);
static void dbfs_handle_free(DBFileHandle *handle);

/* Module entry points. */
GnomeVFSMethod *vfs_module_init     (const char     *method_name,
				     const char     *args);
void            vfs_module_shutdown (GnomeVFSMethod *method);

//handles can be null if you don't have one yet (used to get a db connection if possible)
static PGresult * dbfs_query_maker(dbfs_query type, DBFileHandle *handle);

static char *get_desktop_file_contents (Oid recordid, const char *name, LoginInfo *info);
static char *get_title_from_recordid (Oid recordid, DBFileHandle *handle);
static Oid get_recordid_from_title (char *title, DBFileHandle *handle);
static LoginInfo *get_login_info_from_uri (const GnomeVFSURI *uri);
static GList *extract_uri_path_elements (const GnomeVFSURI *uri, GList *path_list);
static GnomeVFSResult set_file_info_for_record (DBFileHandle *handle, GnomeVFSFileInfo *file_info, GnomeVFSFileInfoOptions options);

static GList *
extract_uri_path_elements (const GnomeVFSURI *uri, GList *path_list) 
{
	GnomeVFSURI *parent_uri;
	char *path_name;

	path_name = gnome_vfs_uri_extract_short_path_name (uri);
	path_list = g_list_prepend (path_list, path_name);

	if (gnome_vfs_uri_has_parent (uri)) {
		parent_uri = gnome_vfs_uri_get_parent (uri);
		path_list = extract_uri_path_elements (parent_uri, path_list);
	}

	printf (" %s ", path_name);

	return path_list;
}

static char *
desktop_file_element_get_name (const char *element) {
	int element_size;
	int suffix_size;
	int name_length;

	element_size = strlen(element);
	suffix_size  = strlen(".desktop");
	name_length  = element_size - suffix_size;

	if (element_size >= suffix_size) {
		if (strcmp(".desktop", element + name_length) == 0) {
			char *name = malloc (name_length * sizeof(char *) + 1);
			strncpy (name, element, name_length);
			name[name_length] = '\0';
			return name;
		}
	}

	return NULL;
}

static void
init_desktop_file_handle (DBFileHandle *handle, const char *item_id_string) {
	Oid item_id;

	char *end_ptr;
	item_id = strtol (item_id_string, &end_ptr, 10);
	g_assert (*end_ptr == '\0');

	handle->type = DESKTOP_FILE;

	handle->contents.desktop_file.name = get_title_from_recordid (item_id, handle);
	handle->contents.desktop_file.contents = get_desktop_file_contents (item_id, handle->contents.desktop_file.name, 
								    handle->login_info);
	handle->contents.desktop_file.size = strlen (handle->contents.desktop_file.contents);
	handle->contents.desktop_file.read = 0;
	handle->contents.desktop_file.recordid = item_id;
}

static void
init_sql_query_handle (DBFileHandle *handle, const char *query, gboolean use_desktop_files) {
	handle->type = SQL_QUERY;
	handle->contents.query.contents = g_strdup (query);
	handle->contents.query.matches = NULL;
	handle->contents.query.use_desktop_files = use_desktop_files;
}

static void
init_record_handle (DBFileHandle *handle, const char *item_id_string) {
	Oid item_id;

	char *end_ptr;
	item_id = strtol (item_id_string, &end_ptr, 10);
	g_assert (*end_ptr == '\0');

	handle->type = RECORD;
	handle->contents.record.id = item_id;
	handle->contents.record.mime_type = NULL;
	handle->contents.record.file_name = NULL;
	handle->contents.record.try_magic_sniffing = FALSE;
	handle->contents.record.translator_exists = FALSE;
	handle->contents.record.create_translator = TRUE;
}


//allocates new space, so delete when you're done
//returns NULL, if it couldn't find it.
//currently, we only accept "/<id>"
//only fills in p_recordID if not NULL
DBFileHandle *
dbfs_handle_new (const GnomeVFSURI *uri)
{
	DBFileHandle *handle;
	char *name;

	GList *path_elements;
	const char *root_element;
	const char *first_element;
	const char *second_element;
	const char *third_element;



#if  DBFS_DEBUG
	char *string_uri = gnome_vfs_uri_to_string(uri, GNOME_VFS_URI_HIDE_USER_NAME);
	printf("dbfs_handle_new(%s)\n", string_uri);
	g_free(string_uri);
#endif

	handle = g_new0 (DBFileHandle, 1);
        handle->login_info = get_login_info_from_uri (uri);

        handle->connection = getConnection(handle->login_info);

	printf("Path: ");
	path_elements = extract_uri_path_elements (uri, NULL);
	printf("\n");
	root_element   = (const char *)g_list_nth_data(path_elements, 0);
	first_element  = (const char *)g_list_nth_data(path_elements, 1);
	second_element = (const char *)g_list_nth_data(path_elements, 2);
	third_element  = (const char *)g_list_nth_data(path_elements, 3);

	if (first_element == NULL) {
		/* storage:/// */
		init_sql_query_handle (handle, DBFS_ALL_RECORDS, TRUE);
	} else if (strcmp("query", first_element) == 0) {
		/* storage:///query */

		if ((name = desktop_file_element_get_name(third_element)) != NULL) {
			/* storage:///query/QUERY_CONTENTS/#.desktop */

			init_desktop_file_handle(handle, name);
		} else if (second_element != NULL && third_element == NULL) {
			/* storage:///query/QUERY_CONTENTS */
			init_sql_query_handle (handle, second_element, TRUE);
		} else {
			/* ERROR */
		}
	} else if (strcmp("items", first_element) == 0) {
		if (second_element != NULL) {
			/* storage:///items/#/SOMENAME */
			init_record_handle(handle, second_element);
		} else {
			/* storage:///items */
			init_sql_query_handle (handle, DBFS_ALL_RECORDS, FALSE);
		}
	} else if ((name = desktop_file_element_get_name(first_element)) != NULL) {
		/* storage:///#.desktop */
		init_desktop_file_handle(handle, name);
	} else {
		/* ERROR */
		g_error("ERROR: bad URI\n");
	}

	return handle;
}

static void
dbfs_handle_free(DBFileHandle *handle)
{
	releaseConnection(handle->connection);
	handle->connection = NULL;
	/* FIXME: release all the intervening stuff */
	g_free (handle);
	handle = NULL;
}

static char *get_base_uri_from_login_info (const LoginInfo *login_info) {
	char *password = login_info->password ? g_strdup_printf(":%s", login_info->password) : g_strdup("");
	char *username = login_info->username ? g_strdup_printf("%s", login_info->username) : g_strdup("");
	char *login;
	if ((strcmp(username, "") !=0) || strcmp(password, "") != 0) {
		login = g_strdup_printf("%s%s@", username, password);
	} else {
		login = g_strdup("");
	}

	char *hostname = login_info->hostname ? g_strdup_printf("%s", login_info->hostname) : g_strdup("");
	char *port     = login_info->port > 0 ? g_strdup_printf(":%d", login_info->port) : g_strdup("");

	char *base_uri = g_strdup_printf("storage://%s%s%s/", login, hostname, port);

	g_free(password);
	g_free(username);
	g_free(login);
	g_free(hostname);
	g_free(port);

	return base_uri;
}

static char *
get_item_uri_from_login_info (const LoginInfo *login_info, Oid item_id) 
{
	char *base_uri = get_base_uri_from_login_info (login_info);
	char *item_uri = g_strdup_printf("%sitems/%d", base_uri, item_id);
	g_free(base_uri);
	return item_uri;
}

static DBFileHandle *
get_item_handle (DBFileHandle *parent_handle, Oid item_id) {
	char *string_uri = get_item_uri_from_login_info(parent_handle->login_info, item_id);
	GnomeVFSURI *sub_file = gnome_vfs_uri_new (string_uri);
	DBFileHandle *item_handle = dbfs_handle_new(sub_file);
	
	gnome_vfs_uri_unref(sub_file);
	g_free(string_uri);

	return item_handle;
}

static LoginInfo *
get_login_info_from_uri (const GnomeVFSURI *uri) 
{
	LoginInfo *info;

	info = g_new (LoginInfo, 1);
	info->hostname = g_strdup(gnome_vfs_uri_get_host_name(uri));
	info->username = g_strdup(gnome_vfs_uri_get_user_name(uri));
	info->password = g_strdup(gnome_vfs_uri_get_password (uri));
	info->port     = gnome_vfs_uri_get_host_port(uri);

	return info;
}

static void
set_file_info_for_desktop_file (GnomeVFSFileInfo *file_info, char *name) {
	file_info->name = name;
	file_info->type = GNOME_VFS_FILE_TYPE_REGULAR;
	file_info->flags = GNOME_VFS_FILE_FLAGS_NONE;
	file_info->mime_type = g_strdup ("application/x-gnome-app-info");
	file_info->valid_fields = file_info->valid_fields | 
		GNOME_VFS_FILE_INFO_FIELDS_FLAGS |
		GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE |
		GNOME_VFS_FILE_INFO_FIELDS_TYPE;
}

static char *
get_title_from_recordid (Oid recordid, DBFileHandle *handle) 
{
	PGresult *results;
	char *error_msg;
	char *title;
	char *search_string = g_strdup_printf ("%s%d", DBFS_TITLE_PREFIX, recordid);

	printf ("Searching for %s\n", search_string);

	results = PQexec(handle->connection, search_string);
	error_msg = PQresultErrorMessage(results);
	if(strlen(error_msg) > 0){
		fprintf(stderr, "error[%s] from query[%s]\n", error_msg, search_string);
		title = g_strdup_printf("Item %d", recordid);
	} else {
		if (PQntuples(results) > 0) {
			title = g_strdup(PQgetvalue(results, 0, 0));
		} else {
			title = g_strdup_printf("Item %d", recordid);
		}
	}

	PQclear(results);

	g_free(search_string);

	return title;
}

static Oid
get_recordid_from_title (char *title, DBFileHandle *handle) 
{
	PGresult *results;
	char *error_msg;
	char *search_string = g_strdup_printf ("%s\'%s\'", DBFS_RECORD_PREFIX, title);
	char *recordid_str;
	Oid recordid = 0;

	printf ("Searching for %s\n", search_string);

	results = PQexec(handle->connection, search_string);
	error_msg = PQresultErrorMessage(results);
	if(strlen(error_msg) > 0){
		fprintf(stderr, "error[%s] from query[%s]\n", error_msg, search_string);
		title = NULL;
	} else {
		if (PQntuples(results) > 0) {
			recordid_str = g_strdup(PQgetvalue(results, 0, 0));
			recordid = strtol(recordid_str, NULL, 10);
		} else {
			title = NULL;
		}
	}

	PQclear(results);

	g_free(search_string);

	return recordid;
}

static char *
get_desktop_file_contents (Oid recordid, const char *name, LoginInfo *info)
{
	char *escaped_name = gnome_vfs_escape_string (name);
	char *item_uri = get_item_uri_from_login_info(info, recordid);
	char *contents = g_strdup_printf ("[Desktop Entry]\n"
					  "Encoding=UTF-8\n"
					  "Name=%s\n"
					  "Type=Link\n"
					  "URL=%s/%s\n"
					  "Icon=gnome-fs-server\n",
					  name, item_uri, escaped_name);
	g_free(escaped_name);
	g_free(item_uri);
	return contents;
}

static GnomeVFSResult
open_record_for_write (DBFileHandle *handle, GnomeVFSURI *uri) 
{
	handle->contents.record.mime_type = gnome_vfs_get_mime_type_from_uri (uri);
	if (strcmp(handle->contents.record.mime_type, GNOME_VFS_MIME_TYPE_UNKNOWN) == 0) {
		handle->contents.record.try_magic_sniffing = TRUE;
	}
	
	handle->contents.record.file_name = gnome_vfs_uri_extract_short_path_name (uri);

	return GNOME_VFS_OK;
}

static GnomeVFSResult 
open_record_for_read (DBFileHandle *handle) 
{
	PGresult * results;
	int num;

	/* do a search query in the big table to see if it exists */
	results = dbfs_query_maker(GetBLOBID, handle);
	num = PQntuples(results);
	if(num == 1){
		const char *value = PQgetvalue(results, 0,0);
		
		if (value == NULL) return GNOME_VFS_ERROR_NOT_FOUND;
		
		handle->contents.record.blobID =  strtol(value, NULL, 10);
		
		PQclear(results);
		
		PQclear(PQexec(handle->connection, "BEGIN"));
		handle->contents.record.fd = lo_open(handle->connection, 
						     handle->contents.record.blobID, 
						     INV_READ);
		
	}
	else if(num > 1){
		PQclear(results);
		fprintf(stderr, "Error: multiple blobs with this file\n");
		dbfs_handle_free(handle);
		return GNOME_VFS_ERROR_INVALID_URI;
	}
	else{
		PQclear(results);
		printf("fell through on open=%d\n",num);
		dbfs_handle_free(handle);
		return GNOME_VFS_ERROR_INVALID_URI;
	}

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_open (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle **method_handle,
	 GnomeVFSURI *uri,
	 GnomeVFSOpenMode mode,
	 GnomeVFSContext *context)
{
	DBFileHandle *handle;
	GnomeVFSResult result;

#if  DBFS_DEBUG
	printf("do_open\n");
#endif

	handle = dbfs_handle_new(uri);

	switch (handle->type) {
	case RECORD:
		/* Can't handle opening read/write atm, because we use translators for
		   writing and directly use lo_* for reading */
		g_assert(!((mode & GNOME_VFS_OPEN_READ) && (mode & GNOME_VFS_OPEN_WRITE)));
			 
		if (mode & GNOME_VFS_OPEN_READ) {
			return open_record_for_read(handle);
		} else {
			return open_record_for_write(handle, uri);
		}
		break;
	case DESKTOP_FILE:
		result = GNOME_VFS_OK;
		break;
	case SQL_QUERY:
		result = GNOME_VFS_ERROR_IS_DIRECTORY;
		break;
	default:
		result = GNOME_VFS_ERROR_INTERNAL;
	}
		
	if (result == GNOME_VFS_OK) {
		*method_handle = (GnomeVFSMethodHandle *) handle;
	} else {
		dbfs_handle_free(handle);
	}

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_create (GnomeVFSMethod *method,
	   GnomeVFSMethodHandle **method_handle,
	   GnomeVFSURI *uri,
	   GnomeVFSOpenMode mode,
	   gboolean exclusive,
	   guint perm,
	   GnomeVFSContext *context)
{
#if DBFS_DEBUG
	printf("do_create\n");
#endif
	/* FIXME: this is probably the wrong thing to do, but... */
	return do_open (method, method_handle, uri, mode, context);
}


static GnomeVFSResult
do_close (GnomeVFSMethod *method,
	  GnomeVFSMethodHandle *method_handle,
	  GnomeVFSContext *context)
{
	DBFileHandle *handle;
	GnomeVFSResult result = GNOME_VFS_OK;
	int db_result;

#if 	 DBFS_DEBUG
	printf("do_close\n");
#endif
	handle = (DBFileHandle *) method_handle;

	switch (handle->type) {
	case RECORD:
		db_result = lo_close(handle->connection, handle->contents.record.fd);
		PQclear(PQexec(handle->connection, "END"));
		break;
	default:
		break;
	}
		
	dbfs_handle_free(handle);

	return result;
}

static GnomeVFSResult
do_read (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle *method_handle,
	 gpointer buffer,
	 GnomeVFSFileSize num_bytes,
	 GnomeVFSFileSize *bytes_read,
	 GnomeVFSContext *context)
{
	DBFileHandle *handle;
	GnomeVFSResult result = GNOME_VFS_OK;
	int db_result;

	handle = (DBFileHandle *) method_handle;

#if  DBFS_DEBUG
	printf("do_read on %d bytes from\n", 
	       (int)num_bytes);
#endif

	switch (handle->type) {
	case RECORD:
		db_result = lo_read(handle->connection, handle->contents.record.fd, buffer, num_bytes);
		if(db_result > 0){
			*bytes_read = db_result;
		
		}
		else if(db_result == 0){
#if DBFS_DEBUG
			printf("EOFed.\n");
#endif
			*bytes_read = 0;
			result = GNOME_VFS_ERROR_EOF;
		}
		else{
			fprintf(stderr, "DBFS BLOB read error on recordid=%d, blobid=%d, "
				"fd=%d, error code=%d\n", handle->contents.record.id, handle->contents.record.blobID, 
				handle->contents.record.fd, db_result);
			result = GNOME_VFS_ERROR_GENERIC;
		}
	
		if(result == GNOME_VFS_OK)
			PQclear(dbfs_query_maker(UpdateAccess, handle));
		break;
	case DESKTOP_FILE:
		{
			int left_to_read = handle->contents.desktop_file.size - handle->contents.desktop_file.read;
			
			if (left_to_read == 0) {
				*bytes_read = 0;
				return GNOME_VFS_ERROR_EOF;
			} else {
				if (num_bytes > left_to_read) num_bytes = left_to_read;
				strncpy(buffer, handle->contents.desktop_file.contents + handle->contents.desktop_file.read, num_bytes);
				*bytes_read = num_bytes;
				handle->contents.desktop_file.read += num_bytes;
				result = GNOME_VFS_OK;
			}
		}
		break;
	case SQL_QUERY:
		result = GNOME_VFS_ERROR_IS_DIRECTORY;
	}

	return result;
}

static GnomeVFSResult
create_translator(GNOME_StorageTranslator *translator, const char *mime_type) {

	CORBA_Object translator_object;
	CORBA_Environment  ev;
	char *query;

	query = g_strdup_printf ("repo_ids.has ('IDL:GNOME/StorageTranslator:1.0') AND bonobo:supported_mime_types.has ('%s')", 
				 mime_type);
	g_print (query);

	CORBA_exception_init (&ev);
	translator_object = bonobo_activation_activate (query, NULL, Bonobo_ACTIVATION_RESULT_OBJECT, NULL, &ev);

	g_free (query);

	/* Check for exceptions */
	if (BONOBO_EX (&ev)) {
		char *err = bonobo_exception_get_text (&ev);
		g_warning (_("An exception occured '%s'"), err);
		g_free (err);
		return GNOME_VFS_ERROR_INTERNAL;
	}
	CORBA_exception_free (&ev);

	if (translator_object == CORBA_OBJECT_NIL) {
		g_warning (_("Could not create an instance of the translator"));
		return GNOME_VFS_ERROR_INTERNAL;
	}

	/* Send a message */
	CORBA_exception_init (&ev);

	*translator = Bonobo_Unknown_queryInterface (translator_object, "IDL:GNOME/StorageTranslator:1.0", &ev);
	
	/* Check for exceptions */
	if (BONOBO_EX (&ev)) {
		char *err = bonobo_exception_get_text (&ev);
		g_warning (_("An exception occured '%s'"), err);
		g_free (err);
		return GNOME_VFS_ERROR_INTERNAL;
	}
	CORBA_exception_free (&ev);

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_write (GnomeVFSMethod *method,
	  GnomeVFSMethodHandle *method_handle,
	  gconstpointer buffer,
	  GnomeVFSFileSize num_bytes,
	  GnomeVFSFileSize *bytes_written,
	  GnomeVFSContext *context)
{
	DBFileHandle *handle;
	GnomeVFSResult result = GNOME_VFS_OK;

	handle = (DBFileHandle *) method_handle;

#if  DBFS_DEBUG
	printf("do_write on %d bytes from\n", 
	       (int)num_bytes);
#endif

	if (handle->contents.record.try_magic_sniffing) {
		handle->contents.record.mime_type = gnome_vfs_get_mime_type_for_data (buffer, num_bytes);
	}

	if (handle->contents.record.create_translator) {
		handle->contents.record.create_translator = FALSE;

		printf ("Creating a translator for %s\n", handle->contents.record.mime_type);

		result = create_translator(&handle->contents.record.translator, handle->contents.record.mime_type);
		if (result != GNOME_VFS_OK) {
			printf ("Failed, falling back to a translator for application/octet-stream\n");
			result = create_translator(&handle->contents.record.translator, "application/octet-stream");
		}
		if (result == GNOME_VFS_OK) {
			printf ("We have translation\n");
			handle->contents.record.translator_exists = TRUE;
		}
	}

	if (handle->contents.record.translator_exists) {

	} else {
		result = GNOME_VFS_ERROR_INTERNAL;
	}

	return result;
}
//FIXME: if the db io ops fail, what should I leave my pos as? in an error state?
//FIXME: do I need to seek inside here? what if they go off the edge, and pass
//  through the calls to the db then I will report the same error, though is this
//  important?
static GnomeVFSResult
do_seek (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle *method_handle,
	 GnomeVFSSeekPosition whence,
	 GnomeVFSFileOffset offset,
	 GnomeVFSContext *context)
{
	DBFileHandle *handle;
	int db_result, db_whence;
	GnomeVFSResult result = GNOME_VFS_OK;

#if  DBFS_DEBUG
	printf("do_seek\n");
#endif

	handle = (DBFileHandle *) method_handle;

	switch (handle->type) {
	case RECORD:		
		switch(whence){
		default:
		case GNOME_VFS_SEEK_START:	
			db_whence = SEEK_SET;
			break;
		case GNOME_VFS_SEEK_CURRENT:
			db_whence = SEEK_CUR;
			break;
		case GNOME_VFS_SEEK_END:
			db_whence =  SEEK_END;
			break;
		}

		db_result = lo_lseek(handle->connection, handle->contents.record.fd, offset, db_whence);
		if(db_result < 0){
			fprintf(stderr, "DBFS BLOB seek error on recordid=%d, fd=%d error code=%d\n", 
				handle->contents.record.id, handle->contents.record.fd, db_result);
			return GNOME_VFS_ERROR_GENERIC;
		}
		break;
	default:
		result = GNOME_VFS_ERROR_NOT_SUPPORTED;
	}
	
	return result;
}

static GnomeVFSResult
do_tell (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle *method_handle,
	 GnomeVFSFileOffset *offset_return)
{
	DBFileHandle *handle;
	int db_result;
	GnomeVFSResult res = GNOME_VFS_OK;

#if  DBFS_DEBUG
	printf("do_tell\n");
#endif
	handle = (DBFileHandle *) method_handle;
	switch (handle->type) {
	case RECORD:
		db_result = lo_tell(handle->connection, handle->contents.record.fd);
		if(db_result < 0){  
			fprintf(stderr,  "DBFS BLOB tell error on recordid=%d, fd=%d, error code=%d\n", 
				handle->contents.record.id, handle->contents.record.fd, db_result);
			res = GNOME_VFS_ERROR_GENERIC; 
		}
		else{
			*offset_return = db_result;
		}
		break;
	default:
		res = GNOME_VFS_ERROR_NOT_SUPPORTED;
		*offset_return = 0;
	}

	return res;
}



static GnomeVFSResult
do_open_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle **method_handle,
		   GnomeVFSURI *uri,
		   GnomeVFSFileInfoOptions options,
		   GnomeVFSContext *context)
{
	DBFileHandle *handle;
	PGresult *results;
	char *error_msg, *query;
	GnomeVFSResult result = GNOME_VFS_OK;
#if DBFS_DEBUG
	printf("do_open_directory\n");
#endif

	printf ("Opening URI %s\n", gnome_vfs_uri_to_string(uri, GNOME_VFS_URI_HIDE_NONE));

	handle = dbfs_handle_new (uri);

	switch (handle->type) {
	case SQL_QUERY:
		query = gnome_vfs_unescape_string (handle->contents.query.contents, "");		
		results = PQexec(handle->connection, query);
		g_free(query);
			
		error_msg = PQresultErrorMessage(results);
		if(strlen(error_msg) > 0){
			fprintf(stderr, "While do_open_directory: error[%s]\n", error_msg);
		} else {
			int column = PQfnumber (results, "recordid");
			int numrows = PQntuples (results);
			int row;
			Oid recordid;
			char *recordid_str, *end_num;

			handle->contents.query.matches = NULL;

			for (row=0; row < numrows; row++) {
				recordid_str = PQgetvalue(results, row, column);
				recordid = strtol(recordid_str, &end_num, 10);
				if (end_num == recordid_str + strlen(recordid_str)) {
					if (!g_list_find(handle->contents.query.matches, (void *)recordid)) {
						handle->contents.query.matches = g_list_append (handle->contents.query.matches, (void *)recordid);
					}
				}
			}
			PQclear (results);
		}
		break;
	default:
		dbfs_handle_free(handle);
		result = GNOME_VFS_ERROR_NOT_A_DIRECTORY;
	}

	*method_handle = (GnomeVFSMethodHandle *) handle;
	return  result;;
}


static GnomeVFSResult
do_close_directory (GnomeVFSMethod *method,
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSContext *context)
{
	DBFileHandle *handle;

#if  DBFS_DEBUG
	printf("do_close_directory\n");
#endif
	handle = (DBFileHandle *) method_handle;
	dbfs_handle_free(handle);

	return GNOME_VFS_OK;
}
/*
  SearchDirectoryHandle *wrapped_handle;

  wrapped_handle = (SearchDirectoryHandle *) method_handle;

  search_directory_handle_destroy (wrapped_handle);

  return GNOME_VFS_OK;
  }*/

static GnomeVFSResult
do_read_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle *method_handle,
		   GnomeVFSFileInfo *file_info,
		   GnomeVFSContext *context)
{
	DBFileHandle *handle;

	printf("do_read_directory \n");
	
	handle = (DBFileHandle *) method_handle;

	switch (handle->type) {
	case SQL_QUERY:
		if (handle->contents.query.matches == NULL) {
			return GNOME_VFS_ERROR_EOF;
		} else {
			Oid recordid = (Oid)handle->contents.query.matches->data;
			DBFileHandle *subfile_handle = get_item_handle(handle, recordid);

			handle->contents.query.matches = g_list_remove (handle->contents.query.matches, (void *)recordid);
			if (handle->contents.query.use_desktop_files) {
				set_file_info_for_desktop_file (file_info, g_strdup_printf("%d.desktop",recordid));
			} else {
				set_file_info_for_record (subfile_handle, file_info, GNOME_VFS_FILE_INFO_DEFAULT);
			}

			dbfs_handle_free(subfile_handle);
		}
		break;
	default:
		return GNOME_VFS_ERROR_NOT_A_DIRECTORY;
	}

	printf ("Returning %s\n", file_info->name);

	return GNOME_VFS_OK;
}

#if DBFS_DEBUG
static const gchar *
type_to_string (GnomeVFSFileType type)
{
	switch (type) {
	case GNOME_VFS_FILE_TYPE_UNKNOWN:
		return "Unknown";
	case GNOME_VFS_FILE_TYPE_REGULAR:
		return "Regular";
	case GNOME_VFS_FILE_TYPE_DIRECTORY:
		return "Directory";
	case GNOME_VFS_FILE_TYPE_SYMBOLIC_LINK:
		return "Symbolic Link";
	case GNOME_VFS_FILE_TYPE_FIFO:
		return "FIFO";
	case GNOME_VFS_FILE_TYPE_SOCKET:
		return "Socket";
	case GNOME_VFS_FILE_TYPE_CHARACTER_DEVICE:
		return "Character device";
	case GNOME_VFS_FILE_TYPE_BLOCK_DEVICE:
		return "Block device";
	default:
		return "???";
	}
}
#endif 

static void
print_file_info (const GnomeVFSFileInfo *info)
{
#if DBFS_DEBUG
#define FLAG_STRING(info, which)				\
	(GNOME_VFS_FILE_INFO_##which (info) ? "YES" : "NO")
	printf ("Name              : %s\n", info->name);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_TYPE)
		printf ("Type              : %s\n", type_to_string (info->type));

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_SYMLINK_NAME && info->symlink_name != NULL)
		printf ("Symlink to        : %s\n", info->symlink_name);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE)
		printf ("MIME type         : %s\n", info->mime_type);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_SIZE)
		printf ("Size              : %" GNOME_VFS_SIZE_FORMAT_STR "\n",
			info->size);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_BLOCK_COUNT)
		printf ("Blocks            : %" GNOME_VFS_SIZE_FORMAT_STR "\n",
			info->block_count);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_IO_BLOCK_SIZE)
		printf ("I/O block size    : %d\n", info->io_block_size);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_FLAGS) {
		printf ("Local             : %s\n", FLAG_STRING (info, LOCAL));
		printf ("SUID              : %s\n", FLAG_STRING (info, SUID));
		printf ("SGID              : %s\n", FLAG_STRING (info, SGID));
		printf ("Sticky            : %s\n", FLAG_STRING (info, STICKY));
	}

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_PERMISSIONS)
		printf ("Permissions       : %04o\n", info->permissions);

	
	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_LINK_COUNT)
		printf ("Link count        : %d\n", info->link_count);
	
	printf ("UID               : %d\n", info->uid);
	printf ("GID               : %d\n", info->gid);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_ATIME)
		printf ("Access time       : %s", ctime (&info->atime));

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_MTIME)
		printf ("Modification time : %s", ctime (&info->mtime));

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_CTIME)
		printf ("Change time       : %s", ctime (&info->ctime));

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_DEVICE)
		printf ("Device #          : %ld\n", (gulong) info->device);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_INODE)
		printf ("Inode #           : %ld\n", (gulong) info->inode);

#undef FLAG_STRING
#endif
}

static void
dbfs_file_in_time(char *db_time, time_t *t_struct){
	*t_struct = (time_t)711111155;
}

static GnomeVFSResult
set_file_info_for_record (DBFileHandle *handle,
			   GnomeVFSFileInfo *file_info,
			   GnomeVFSFileInfoOptions options)
{
	PGresult *results;
	int num;

	results = dbfs_query_maker(GetAllAttrs, handle);
	num = PQntuples(results);
	if(num < 1){
		PQclear(results);
#if DBFS_DEBUG
		printf("result returned %d queries.\n", num);
#endif

		return GNOME_VFS_ERROR_INVALID_URI;
	}

	file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_TYPE |
		GNOME_VFS_FILE_INFO_FIELDS_FLAGS |
		GNOME_VFS_FILE_INFO_FIELDS_LINK_COUNT |
		0;

	//FIXME: support network later
	file_info->flags = GNOME_VFS_FILE_FLAGS_LOCAL;
	file_info->type = GNOME_VFS_FILE_TYPE_REGULAR;
	file_info->link_count = 1;

	file_info->name = g_strdup_printf("%d", handle->contents.record.id);

	for(num = PQntuples(results) - 1; num >= 0; num--){

		char *name, *value;
		name = PQgetvalue(results, num, 0);
		value = PQgetvalue(results, num, 1);
		if(strcmp(name, STORAGE_FILE_MIME) == 0){

			file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;
			//if(options & GNOME_VFS_FILE_INFO_GET_MIME_TYPE){
			file_info->mime_type =   g_strdup(value);
			//}
		}

	}
	PQclear(results);

	//FIXME: get rid of these defaults
	file_info->permissions = GNOME_VFS_PERM_USER_ALL |
		GNOME_VFS_PERM_GROUP_ALL |
		GNOME_VFS_PERM_OTHER_ALL;
	file_info->gid = 500;
	file_info->uid = 500;

	results = dbfs_query_maker(GetAllNumberAttrs, handle);
	for(num = PQntuples(results) - 1; num >= 0; num--){
		char *name, *value;

		name = PQgetvalue(results, num, 0);
		value = PQgetvalue(results, num, 1);

		if(strcmp(name, STORAGE_FILE_SIZE) == 0){
			file_info->valid_fields |= GNOME_VFS_FILE_INFO_FIELDS_SIZE;
			file_info->size = atoi(value);
			
			/* Size measured in units of 512-byte blocks.  */
			file_info->block_count = file_info->size/512;
		}
		else if(strcmp(name, STORAGE_FILE_PERM) == 0){
			file_info->valid_fields |=  GNOME_VFS_FILE_INFO_FIELDS_PERMISSIONS;
			//FIXME: do real permissions, this if fine for the demo though
			file_info->permissions = atoi(value);
		}
		else if(strcmp(name, STORAGE_FILE_UID) == 0){
			file_info->uid = atoi(value);
		}
		else if(strcmp(name, STORAGE_FILE_GID) == 0){
			file_info->gid = atoi(value);
		}
	}
	PQclear(results);

	//FIXME: get rid of these defaults
	file_info->valid_fields |=  GNOME_VFS_FILE_INFO_FIELDS_MTIME | 
		 GNOME_VFS_FILE_INFO_FIELDS_ATIME |
		GNOME_VFS_FILE_INFO_FIELDS_CTIME;
	dbfs_file_in_time(NULL, &(file_info->mtime));
	dbfs_file_in_time(NULL, &(file_info->atime));
	dbfs_file_in_time(NULL, &(file_info->ctime));

	results = dbfs_query_maker(GetAllDateAttrs, handle);
	for(num = PQntuples(results) - 1; num >= 0; num--){
		char *name, *value;
		//struct tm times;

		name = PQgetvalue(results, num, 0);
		value = PQgetvalue(results, num, 1);

		if(strcmp(name, STORAGE_FILE_ACCESS) == 0){
			file_info->valid_fields |=  GNOME_VFS_FILE_INFO_FIELDS_ATIME;
			//FIXME: can't get strptime to compile
			//strptime(value, "%Y-%m-%d %H:%M:%S", &times);
			//file_info->atime = mktime(&times);
		}
		else if(strcmp(name, STORAGE_FILE_MODIFICATION) == 0){
			file_info->valid_fields |=  GNOME_VFS_FILE_INFO_FIELDS_MTIME;
			//strptime(value, "%Y-%m-%d %H:%M:%S", &times);
			//file_info->mtime = mktime(&times);
		}
		else if(strcmp(name, STORAGE_FILE_CREATION) == 0){
			file_info->valid_fields |=  GNOME_VFS_FILE_INFO_FIELDS_CTIME;
			//strptime(value, "%Y-%m-%d %H:%M:%S", &times);
			//file_info->ctime = mktime(&times);
		}

	}
	PQclear(results);


	//FIXME: what is this?
	//guint refcount;

	//FIXME: error correcting?

	print_file_info (file_info);

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_get_file_info_from_handle (GnomeVFSMethod *method,
			      GnomeVFSMethodHandle *method_handle,
			      GnomeVFSFileInfo *file_info,
			      GnomeVFSFileInfoOptions options,
			      GnomeVFSContext *context)
{
	DBFileHandle *handle = (DBFileHandle *) method_handle;
	GnomeVFSResult result = GNOME_VFS_OK;


#if  DBFS_DEBUG
	printf("do_get_file_info_handle:%d\n", handle->contents.record.id);
#endif
	switch (handle->type) {
	case RECORD:
		printf ("Its a record\n");
		result = set_file_info_for_record(handle, file_info, options);
		break;
	case DESKTOP_FILE:
		printf ("Its a desktop file\n");
		set_file_info_for_desktop_file (file_info, g_strdup_printf("%d.desktop",handle->contents.desktop_file.recordid));
		break;
	case SQL_QUERY:
		printf ("Its an SQL query\n");
		file_info->name = g_strdup (handle->contents.query.contents);
		file_info->mime_type = g_strdup ("x-directory/normal");
		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->flags = GNOME_VFS_FILE_FLAGS_NONE;
		file_info->permissions =
			GNOME_VFS_PERM_USER_ALL |
			GNOME_VFS_PERM_GROUP_ALL |
			GNOME_VFS_PERM_OTHER_ALL;
		file_info->valid_fields =
			GNOME_VFS_FILE_INFO_FIELDS_TYPE |
			GNOME_VFS_FILE_INFO_FIELDS_PERMISSIONS |
			GNOME_VFS_FILE_INFO_FIELDS_FLAGS |
			GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;
		break;
	}
	
	return result;
}


static GnomeVFSResult
do_get_file_info (GnomeVFSMethod *method,
		  GnomeVFSURI *uri,
		  GnomeVFSFileInfo *file_info,
		  GnomeVFSFileInfoOptions options,
		  GnomeVFSContext *context)
{
	GnomeVFSResult res;

#if  DBFS_DEBUG
	printf("do_get_file_info w/ uri=%s\n", uri->text);
#endif

	DBFileHandle *handle = dbfs_handle_new (uri);	
	res = do_get_file_info_from_handle(method, (GnomeVFSMethodHandle *)handle, file_info, options, context);
	dbfs_handle_free(handle);

	return res;
}

static gboolean
do_is_local (GnomeVFSMethod *method,
	     const GnomeVFSURI *uri)
{
	gboolean result = FALSE;
#if  DBFS_DEBUG
	printf("do_is_local\n");
#endif
	return result;
}


static GnomeVFSResult
do_remove_directory (GnomeVFSMethod *method,
		     GnomeVFSURI *uri,
		     GnomeVFSContext *context)
{
#if  DBFS_DEBUG
	printf("do_remove_directory\n");
#endif
	return  GNOME_VFS_ERROR_GENERIC;
}


static GnomeVFSResult
do_move (GnomeVFSMethod *method,
	 GnomeVFSURI *old_uri,
	 GnomeVFSURI *new_uri,
	 gboolean force_replace,
	 GnomeVFSContext *context)
{
#if  DBFS_DEBUG
	printf("do_move\n");
#endif
	return  GNOME_VFS_ERROR_GENERIC;
}




PGresult * 
dbfs_query_maker(dbfs_query type, DBFileHandle *handle)
{
	PGresult * results;
	char *recordid_str;
	char *query_prefix, *full_query, *error_msg;

	g_assert (handle != NULL);
	g_assert (handle->connection != NULL);

	switch(type){
	case UpdateAccess:
		query_prefix = DBFS_UPDATE_ACCESS_PREFIX;
		break;
	case UpdateModification:
		query_prefix = DBFS_UPDATE_MODIFICATION_PREFIX;
		break;
	case GetAllAttrs:
		query_prefix = DBFS_GET_ALL_ATTRS_PREFIX;
		break;
	case GetAllDateAttrs:
		query_prefix = DBFS_GET_ALL_DATE_ATTRS_PREFIX;
		break;
	case GetAllNumberAttrs:
		query_prefix = DBFS_GET_ALL_NUMBER_ATTRS_PREFIX;
		break;
	case GetBLOBID:
		query_prefix = DBFS_GETBLOBID_PREFIX;
		break;
	default:
		fprintf(stderr, "Invalid query type=%d\n", (int)type);
		return NULL;
	}
	
	recordid_str = g_strdup_printf("%d", handle->contents.record.id);
	full_query = g_new(char, strlen(query_prefix) + strlen(recordid_str) + 1 + 1);//1 for the ';' and 1 for the '\0'
	full_query[0] = '\0';
	
	strcat(full_query, query_prefix);
	strcat(full_query, recordid_str);
	strcat(full_query, ";");
	
	g_free(recordid_str);

#if  DBFS_DEBUG
	printf("About to query for [%s]\n", full_query);
#endif

	results = PQexec(handle->connection, full_query); 

#if  DBFS_DEBUG
	printf("...done\n");
#endif

	error_msg = PQresultErrorMessage(results);
	if(strlen(error_msg) > 0){
		fprintf(stderr, "error[%s] from query[%s]\n", error_msg, full_query);
	}

	g_free(full_query);

	return results;
}




static GnomeVFSResult
do_set_file_info (GnomeVFSMethod *method,
		  GnomeVFSURI *uri,
		  const GnomeVFSFileInfo *info,
		  GnomeVFSSetFileInfoMask mask,
		  GnomeVFSContext *context)
{
	printf("do_set_file_info\n");
	return  GNOME_VFS_ERROR_GENERIC;
}


static GnomeVFSResult
  do_truncate (GnomeVFSMethod *method,
  GnomeVFSURI *uri,
  GnomeVFSFileSize where,
  GnomeVFSContext *context)
  {
	  printf("do_truncate\n");
	return  GNOME_VFS_ERROR_GENERIC;
}

static GnomeVFSResult
do_truncate_handle (GnomeVFSMethod *method, 
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSFileSize where,
		    GnomeVFSContext *context)
{
	printf("do_truncate_handle \n");
	return  GNOME_VFS_ERROR_GENERIC;
}


static GnomeVFSResult
do_find_directory (GnomeVFSMethod *method,
		   GnomeVFSURI *near_uri,
		   GnomeVFSFindDirectoryKind kind,
		   GnomeVFSURI **result_uri,
		   gboolean create_if_needed,
		   gboolean find_if_needed,
		   guint permissions,
		   GnomeVFSContext *context)
  {

	  printf("do_find_directory\n");
	return  GNOME_VFS_ERROR_GENERIC;
  }


static GnomeVFSMethod method = {
	sizeof (GnomeVFSMethod),
	do_open,
	do_create,
	do_close,
	do_read,
	do_write,
	do_seek,
	do_tell,
	do_truncate_handle,
	do_open_directory,
	do_close_directory,
	do_read_directory,
	do_get_file_info,
	do_get_file_info_from_handle,
	do_is_local,
	NULL,//do_make_directory,
	do_remove_directory,
	NULL /*do_move*/,
	NULL /*do_unlink*/,
	NULL /*do_check_same_fs*/,
	do_set_file_info,
	do_truncate,
	do_find_directory,
	NULL,// do_create_symbolic_link,
	NULL, //do_monitor_add,
	NULL //do_monitor_cancel
};

GnomeVFSMethod *
vfs_module_init (const char *method_name, const char *args)
{
#if  DBFS_DEBUG
	printf ("DBFS: initializing threads\n");
#endif

	if (!g_thread_supported ()) g_thread_init (NULL);

#if  DBFS_DEBUG
	printf ("DBFS: threads initialized\n");
	printf("do_init\n");
#endif
	initConnections();

	return &method;
}



void
vfs_module_shutdown (GnomeVFSMethod *method)
{
#if  DBFS_DEBUG
	printf("do_shutdown\n");
#endif
	shutdownConnections();

	/* FIXME: Need to free the rest of memory */
}


/*
  You need "do_open", "do_close", "do_read", "do_open_directory", "do_close_directory", and probably the do_get_file_info* ones
 
  etags -l c *.c *.h

  what is do_truncate(handle)
  what is gpointer? can it be a char * for write/reads
  maybe need to do another query to get BLOBid?
  g versions of strchr, strcat?
  unescape path?
  all binary reads/writes need to be in the same transaction.

  are the GnomeVFSURI's set up before they come to the open call (do I ahve to set the parent?)
  how many bits does integer have?
  should block_count round down or up?
  should I fill in refcount/link_count?
  some of the file info could be held
  complete dbfs_file_in_time

  //CREATE TABLE GenMetaData (recordID oid primary key, title text, 
  mime_type varchar(50), access timestamp, 
  modification timestamp, creation timestamp, 
  permissions integer, uid integer, gid integer, 
  size integer, top_level boolean);

create table numbersoup (recordid Oid, attrname text, attrvalue int);
create table datesoup (recordid Oid, attrname text, attrvalue timestamp);
create index numbersoup_rec on numbersoup(recordid);
create index numbersoup_attrname on numbersoup(attrname);
create index datesoup_rec on datesoup(recordid);
create index datesoup_attrname on datesoup(attrname);

  //primary key makes an index 'genmetadata_pkey'
  //CREATE INDEX GenMetaData_recordID_index ON GenMetaData (recordID);

  //CREATE TABLE BinaryRecords (blobID oid) INHERITS (GenMetaData);
  CREATE TABLE TextRecords (content text) INHERITS (GenMetaData);
  CREATE TABLE nodechildren (parent Oid, child Oid, linknum int);

  //shouldn't contain recordID-s for TextRecords
  CREATE TABLE AttrSoup (recordID oid references GenMetaData (recordID) ON DELETE CASCADE, attrName char(30), attrValue varchar(50));

  CREATE TABLE PrevHighestRecordID (recordID integer);
  CREATE TABLE AvailableRecordIDs (recordID oid);

utimebuf

  SELECT name, altitude
  FROM ONLY cities
  WHERE altitude > 500;
*/
