#include <stdio.h>
#include <libpq-fe.h> //in /usr/include
#include <libpq/libpq-fs.h>
#include <string.h>


int main(int argc, char **argv) {
  char * error;
  int i;
  PGconn **dbConns;

  dbConns = malloc (sizeof (PGconn *) * 100);

  for (i = 0; i < 18; i++) {
    dbConns[i] = PQconnectdb("host = bequw.stanford.edu dbname = gargamel user = senior password = senior");

    error = PQerrorMessage(dbConns[i]);
  
    if(strlen(error) > 0){
      fprintf(stderr, "DB connection error to gargamel on %d: %s\n", i, error);
    }
  }

  printf ("done allocating\n");

  while (-1);

  return 0;
}
