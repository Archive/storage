#include <libgnomevfs/gnome-vfs.h>
#include <stdio.h>
#include <time.h>

static const gchar *
type_to_string (GnomeVFSFileType type)
{
	switch (type) {
	case GNOME_VFS_FILE_TYPE_UNKNOWN:
		return "Unknown";
	case GNOME_VFS_FILE_TYPE_REGULAR:
		return "Regular";
	case GNOME_VFS_FILE_TYPE_DIRECTORY:
		return "Directory";
	case GNOME_VFS_FILE_TYPE_SYMBOLIC_LINK:
		return "Symbolic Link";
	case GNOME_VFS_FILE_TYPE_FIFO:
		return "FIFO";
	case GNOME_VFS_FILE_TYPE_SOCKET:
		return "Socket";
	case GNOME_VFS_FILE_TYPE_CHARACTER_DEVICE:
		return "Character device";
	case GNOME_VFS_FILE_TYPE_BLOCK_DEVICE:
		return "Block device";
	default:
		return "???";
	}
}


static void
print_file_info (const GnomeVFSFileInfo *info)
{
#define FLAG_STRING(info, which)				\
	(GNOME_VFS_FILE_INFO_##which (info) ? "YES" : "NO")

	printf ("Name              : %s\n", info->name);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_TYPE)
		printf ("Type              : %s\n", type_to_string (info->type));

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_SYMLINK_NAME && info->symlink_name != NULL)
		printf ("Symlink to        : %s\n", info->symlink_name);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE)
		printf ("MIME type         : %s\n", info->mime_type);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_SIZE)
		printf ("Size              : %" GNOME_VFS_SIZE_FORMAT_STR "\n",
			info->size);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_BLOCK_COUNT)
		printf ("Blocks            : %" GNOME_VFS_SIZE_FORMAT_STR "\n",
			info->block_count);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_IO_BLOCK_SIZE)
		printf ("I/O block size    : %d\n", info->io_block_size);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_FLAGS) {
		printf ("Local             : %s\n", FLAG_STRING (info, LOCAL));
		printf ("SUID              : %s\n", FLAG_STRING (info, SUID));
		printf ("SGID              : %s\n", FLAG_STRING (info, SGID));
		printf ("Sticky            : %s\n", FLAG_STRING (info, STICKY));
	}

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_PERMISSIONS)
		printf ("Permissions       : %04o\n", info->permissions);

	
	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_LINK_COUNT)
		printf ("Link count        : %d\n", info->link_count);
	
	printf ("UID               : %d\n", info->uid);
	printf ("GID               : %d\n", info->gid);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_ATIME)
		printf ("Access time       : %s", ctime (&info->atime));

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_MTIME)
		printf ("Modification time : %s", ctime (&info->mtime));

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_CTIME)
		printf ("Change time       : %s", ctime (&info->ctime));

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_DEVICE)
		printf ("Device #          : %ld\n", (gulong) info->device);

	if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_INODE)
		printf ("Inode #           : %ld\n", (gulong) info->inode);

     if(info->valid_fields&GNOME_VFS_FILE_INFO_FIELDS_ACCESS) {
             printf ("Readable          : %s\n", 
                     (info->permissions&GNOME_VFS_PERM_ACCESS_READABLE?"YES":"NO"));
             printf ("Writable          : %s\n", 
                     (info->permissions&GNOME_VFS_PERM_ACCESS_WRITABLE?"YES":"NO"));
             printf ("Executable        : %s\n", 
                     (info->permissions&GNOME_VFS_PERM_ACCESS_EXECUTABLE?"YES":"NO"));
     }
     

#undef FLAG_STRING
}

int check_result (GnomeVFSResult result, char *uri) 
{
		if (result != GNOME_VFS_OK) {
		  g_print ("(Failed)\n");
			fprintf (stderr, "TEST: reading %s, %s\n",
				 uri, gnome_vfs_result_to_string (result));
			return 0;
		} else {
		  g_print ("(OK)\n");
		}
		return -1;
}

int
main (int argc,
      char **argv)
{
	GnomeVFSURI *vfs_uri;
	GnomeVFSResult result;
	GnomeVFSHandle *handle;
	GnomeVFSFileSize size;
	GnomeVFSFileSize bytesread;
	GnomeVFSFileInfo *info;

	char *data;

	gchar *uri;
	int i=1;
	
	if (argc < 2) {
		fprintf (stderr, "Usage: %s <uri> [<uri>...]\n", argv[0]);
		return 1;
	}

	if (!gnome_vfs_init ()) {
		fprintf (stderr, "%s: Cannot initialize the GNOME Virtual File System.\n",
			 argv[0]);
		return 1;
	}

	while (i < argc) {
		const char *path;

		uri = argv[i];

		g_print("TEST: Getting info for \"%s\"...", uri);
		
		info = gnome_vfs_file_info_new ();
		result = gnome_vfs_get_file_info (uri, 
						  info,
						  (GNOME_VFS_FILE_INFO_GET_MIME_TYPE
						   | GNOME_VFS_FILE_INFO_GET_ACCESS_RIGHTS
						   | GNOME_VFS_FILE_INFO_FOLLOW_LINKS));
		
		
		if (!check_result(result, uri)) return -1;
		
		print_file_info (info);
		
		size = info->size;
		
		gnome_vfs_file_info_unref (info);
		
		vfs_uri = gnome_vfs_uri_new (uri);
		path = gnome_vfs_uri_get_path (vfs_uri);
		printf ("TEST: Path: %s\n", path ? path : "<null>");
		printf (gnome_vfs_uri_is_local (vfs_uri)
			? "File is local\n" : "File is not local\n");
		gnome_vfs_uri_unref (vfs_uri);
		
		i++;
		
		g_print("TEST: Doing open for \"%s\"...", uri);
		
		result = gnome_vfs_open (&handle, uri, GNOME_VFS_OPEN_READ);
		if (!check_result(result, uri)) return -1;
		
		g_print ("TEST: Doing read for \"%s\"...", uri);
		
		data = (char *)malloc (sizeof(char) * size);
		
		result = gnome_vfs_read(handle, data, size, &bytesread);
		if (!check_result(result, uri)) return -1;
		
		g_print("TEST: Result is {\n%s\n}\n", data);
	}

	return 0;
}
